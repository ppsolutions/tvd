﻿using System.Collections.Generic;
using tvd.model.Helpers;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.Models;

namespace tvd.ViewModel
{
    public class MemberViewModel : BasePageViewModel
    {
        public IEnumerable<Title> PersonTitles { get; set; }
		public IEnumerable<Title> CorporateTitles { get; set; }
		public IEnumerable<City> Cities { get; set; }

        public IEnumerable<RedeemPointsType> RedeemPointsTypes { get; set; }

        public MemberViewModel(Member member)
        {
            MappingPersonData(member.Person);
            MappingAddressData(member.AddressGroup);
            MemberPointDetail = member.MemberPointDetail;
            MemberMappings = member.MemberMappings;
        }

        public List<MemberMapping> MemberMappings { get; set; }
        public MemberPointDetail MemberPointDetail { get; set; }
        public bool IsLegalPerson { get; set; }
        public RecStatus Recstatus { get; set; }
        public int MemberId { get; set; }
		public string ReferenceId { get; set; }
		public string MemberCode { get; set; }
		public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
		public bool IsSpecificYear { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string CorporateName { get; set; }
        public string CorporatePhone { get; set; }
        public string CorporateEmail { get; set; }

		public int CurrentAddressId { get; set; }
        public string CurrentAddress { get; set; }
		public int CurrentAddressProvinceId { get; set; }
        public string CurrentAddressProvince { get; set; }
        public string CurrentAddressPostalCode { get; set; }

		public bool ShipmentUseCurrentAddress { get; set; }
        public bool ShipmentUseNewAddress { get; set; }
		public int ShipmentAddressId { get; set; }
		public string ShipmentAddress { get; set; }
		public int ShipmentProvinceId { get; set; }
		public string ShipmentProvince { get; set; }
        public string ShipmentPostalCode { get; set; }

        public bool InvoiceUseCurrentAddress { get; set; }
        public bool InvoiceUseShipmentAddress { get; set; }
        public bool InvoiceUseNewAddress { get; set; }
		public int InvoiceAddressId { get; set; }
		public string InvoiceAddress { get; set; }
		public int InvoiceProvinceId { get; set; }
		public string InvoiceProvince { get; set; }
        public string InvoicePostalCode { get; set; }

        public string Password { get; set; }

        public int FirstQuestion { get; set; }
        public string FirstAnswer { get; set; }
        public int SecondQuestion { get; set; }
        public string SecondAnswer { get; set; }

        public string Token { get; set; }

        public int UpdatedBy { get; set; }

		public Source Source { get; set; }

		public PageTypeId PageTypeId { get; set; }

        private void MappingPersonData(Person person)
        {
            MemberId = person.PersonId;
			ReferenceId = person.ReferenceId;
			MemberCode = person.Code;
            IsLegalPerson = person.IsLegalPerson.HasValue && person.IsLegalPerson.Value;
			TitleId = person.TitleId;
            Recstatus = person.RecStatus;
            if (IsLegalPerson)
            {				
                FirstName = person.FirstName;
                LastName = person.LastName;
                DOB = person.BirthDate.Value.ToString("dd/MM/yyyy");
				IsSpecificYear = person.IsSpecificYear.HasValue ? person.IsSpecificYear.Value : false;
                Phone = person.ContactNumber;
                Email = person.EmailAddress;
            }
            else
            {
                CorporateName = person.FirstName;
                CorporatePhone = person.ContactNumber;
                CorporateEmail = person.EmailAddress;
            }

			Source = person.Source;
			Password = person.Password;
        }

        private void MappingAddressData(PersonAddressGroup addressGroup)
        {
            var currentAddress = addressGroup?.CurrentAddress;
            if (currentAddress != null)
            {
				CurrentAddressId = currentAddress.PersonAddressId;
                CurrentAddress = currentAddress.Address1;
				CurrentAddressProvinceId = currentAddress.City.CityId;
				CurrentAddressProvince = currentAddress.City.CityName;
                CurrentAddressPostalCode = currentAddress.PostalCode;
            }

            var shippingAddress = addressGroup?.ShipmentAddress;
            if (shippingAddress != null)
            {
                if (currentAddress != null && currentAddress.PersonAddressId == shippingAddress.PersonAddressId)
                {
                    ShipmentUseCurrentAddress = true;
                }
                else
                {
                    ShipmentUseNewAddress = true;
                }

				ShipmentAddressId = shippingAddress.PersonAddressId;
                ShipmentAddress = shippingAddress.Address1;
				ShipmentProvinceId = shippingAddress.City.CityId; 
                ShipmentProvince = shippingAddress.City.CityName;
                ShipmentPostalCode = shippingAddress.PostalCode;

            }

            var invoiceAddress = addressGroup?.InvoiceAddress;
            if (invoiceAddress != null)
            {
                if (currentAddress != null && currentAddress.PersonAddressId == invoiceAddress.PersonAddressId)
                {
                    InvoiceUseCurrentAddress = true;
                }
                else if (shippingAddress != null && shippingAddress.PersonAddressId == invoiceAddress.PersonAddressId)
                {
                    InvoiceUseShipmentAddress = true;
                }
                else
                {
                    InvoiceUseNewAddress = true;
                }

				InvoiceAddressId = invoiceAddress.PersonAddressId;
                InvoiceAddress = invoiceAddress.Address1;
				InvoiceProvinceId = invoiceAddress.City.CityId;
				InvoiceProvince = invoiceAddress.City.CityName;
                InvoicePostalCode = invoiceAddress.PostalCode;
            }
        }

    }

}