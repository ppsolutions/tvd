﻿using System.Collections.Generic;
using tvd.Models;

namespace tvd.ViewModel
{
    public abstract class BasePageViewModel
    {
        public BasePageViewModel() { }

        public string MemberPageUrl { get; set; }
        public string RewardPageUrl { get; set; }
        public string ApprovalPageUrl { get; set; }
        public string NewsPageUrl { get; set; }
        public string ReportPageUrl { get; set; }
        public string SettingsEmployeePageUrl { get; set; }
        public string SettingsPermissionPageUrl { get; set; }
        public IList<BreadcrumbItem> SubBreadcrumb { get; set; }
        public IList<SidebarItem> Sidebar { get; set; }
        public string SubPageTitle { get; set; }
        public NotificationGroup NotificationGroup { get; set; }

    }

    public class NotificationGroup
    {
        public int AllPendings { get; set; }
        public List<NotificationItem> Items { get; set; }

    }

    public class NotificationItem
    {
        public int Count { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
}