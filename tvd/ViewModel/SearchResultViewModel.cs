﻿using tvd.model.Models.Request;

namespace tvd.ViewModel
{
    public class SearchResultViewModel : BasePageViewModel
    {
        public SearchCriteria SearchCriteria;
    }
}