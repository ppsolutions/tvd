﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.CommonDTO.Rule;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;

namespace tvd.ViewModel
{
    public class PointViewModel : BasePageViewModel
    {
        public IEnumerable<PointLevel> PointLevels { get; set; }
        public DateTime? PointLevelExpireDate { get; set; }
		public int? ExpireYear { get; set; }
        public decimal? MinPurchase { get; set; }
        public IEnumerable<RedeemPointsType> RedeemPointsTypes { get; set; }
        public IEnumerable<Rule> Rules { get; set; }
        public IEnumerable<ApplyType> ApplyTypes { get; set; }

        public IEnumerable<Bank> Banks { get; set; }
        public IEnumerable<CardType> CardTypes { get; set; }
        public IEnumerable<PaymentType> PaymentTypes { get; set; }
        
        public string RewardCreateUrl { get; set; }
        public string RuleCreateUrl { get; set; }
        public string GamificationCreateUrl { get; set; }
        public GamificationPoint GamificationInfo { get; set; }
        public RewardInfo RewardInfo { get; set; }
        public PointRuleInfo PointRuleInfo { get; set; }
        public PromotionInfo PromotionInfo { get; set; }

        public List<PromotionInfo> PromotionUsedInRule { get; set; }
        public List<PromotionInfo> CampaignUsedInRule { get; set; }
    }
}