﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;

namespace tvd.ViewModel
{
    public class NewsViewModel : BasePageViewModel
    {
        public NewsViewModel() { }
        public IList<NewsStatus> NewsStatus { get; set; }
        public string CreateNewsUrl { get; set; }
        
        public int NewsId { get; set; }
        public News NewsInfo { get; set; }
    }
}