﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.CommonDTO.Employee;

namespace tvd.ViewModel
{
    public class PermissionViewModel : BasePageViewModel
    {
        public RolePermission RolePermission { get; set; }
    }

    //public class Permission
    //{
    //    public int PermissionId { get; set; }
    //    public string PermissionDescription { get; set; }
    //    public bool IsSelected { get; set; }
    //}
}