﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Employee;
using tvd.Models;

namespace tvd.ViewModel
{
    public class UserSettingViewModel : BasePageViewModel
    {
        public IEnumerable<Role> RoleList { get; set; }
        public string EmployeeUrl { get; set; }
        public string EmployeeCreateUrl { get; set; }
        public string EmployeeEditUrl { get; set; }
        public Employee Employee { get; set; }
        public int RoleId { get; set; }
        public string PermissionUrl { get; set; }
        public string PermissionCreateUrl { get; set; }
        public string PermissionEditUrl { get; set; }
        public RolePermission RolePermission { get; set; }
        public IList<AccessControl> AccessControlList { get; set; }
    }
}