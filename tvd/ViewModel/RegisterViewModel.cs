﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.Enum;

namespace tvd.ViewModel
{
	public class RegisterViewModel : BasePageViewModel
	{
		public RegisterViewModel()
		{

		}

		public IEnumerable<City> Cities { get; set; }
		public IEnumerable<Title> PersonTitles { get; set; }
		public IEnumerable<Title> CorporateTitles { get; set; }
		public IEnumerable<Question> Questions { get; set; }
		public IEnumerable<Member> Members { get; set; }

		public int Id { get; set; }
		public Source Source { get; set; }
		public int TitleId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime? BirthDate { get; set; }
		public string ContactNumber { get; set; }
		public string EmailAddress { get; set; }
		public DateTime? RecCreatedWhen { get; set; }

		public string SelectedMergeMembers { get; set; }
	}
}