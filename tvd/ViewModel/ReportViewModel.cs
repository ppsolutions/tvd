﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Point;

namespace tvd.ViewModel
{
    public class ReportViewModel : BasePageViewModel
    {
        public IEnumerable<Role> RoleList { get; set; }
        public IEnumerable<PointLevel> Levels { get; set; }
    }
}