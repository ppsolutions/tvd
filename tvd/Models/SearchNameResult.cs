﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tvd.Models
{
    public class SearchNameResult
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public int Count { get; set; }
    }
}