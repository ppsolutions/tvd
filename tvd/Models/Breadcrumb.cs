﻿using System.Collections.Generic;

namespace tvd.Models
{
    public class Breadcrumb
    {
        public IList<BreadcrumbItem> Breadcrumbs { get; set; }
    }

    public class BreadcrumbItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public bool HasUrl { get; set; }
    }
}