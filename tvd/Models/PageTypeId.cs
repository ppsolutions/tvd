﻿using System.ComponentModel;

namespace tvd.Models
{
	public enum PageTypeId
	{
		Unknow = 0,

		LoginPage = 1,

		[Description("ทะเบียนสมาชิก หน้าค้นหาสมาชิกหน้าแรก")]
		PreSearchPage = 2,

		[Description("ทะเบียนสมาชิก หน้าค้นหาสมาชิก")]
		SearchPage = 3,

		[Description("ตั้งค่าผู้ใช้งาน หน้าพนักงาน")]
		SettingEmployeePage = 4,

		[Description("ตั้งค่าผู้ใช้งาน หน้าเพิ่มพนักงาน")]
		SettingEmployeeCreatePage = 5,

		[Description("ตั้งค่าผู้ใช้งาน หน้าแก้ไขพนักงาน")]
		SettingEmployeeEditPage = 6,

		[Description("ข่าวสาร หน้าข่าวสารสมาชิก")]
		NewsMemberPage = 7,

		[Description("ข่าวสาร หน้าข่าวสารโปรโมชั่น")]
		NewsPromotionPage = 8,

		[Description("ข่าวสาร หน้ากฏระเบียบสมาชิก")]
		NewsMemberRulePage = 9,

		[Description("ตั้งค่าผู้ใช้งาน หน้าหน้าที่การใช้งาน")]
		SettingPermissionPage = 10,

		[Description("ตั้งค่าผู้ใช้งาน หน้าเพิ่มหน้าที่การใช้งาน")]
		SettingPermissionCreatePage = 11,

		[Description("ตั้งค่าผู้ใช้งาน หน้าแก้ไขหน้าที่การใช้งาน")]
		SettingPermissionEditPage = 12,

		[Description("ข่าวสาร หน้าสิทธิประโยชน์สมาชิก")]
		NewsMemberBenefitPage = 13,

		[Description("ข่าวสาร หน้าเพิ่มข่าวสาร")]
		NewsCreatePage = 14,

		[Description("ข่าวสาร หน้าแก้ไขข่าวสาร")]
		NewsEditPage = 15,

		[Description("กำหนดคะแนน หน้าคะแนนแลกของกำนัล")]
		PointRewardPage = 16,

		[Description("กำหนดคะแนน หน้าเพิ่มคะแนนแลกของกำนัล")]
		PointRewardCreatePage = 17,

		[Description("กำหนดคะแนน หน้าคะแนนเลื่อนขั้น")]
		PointLevelPage = 18,

		[Description("กำหนดคะแนน หน้าตั้งค่าคะแนนแบบพื้นฐาน")]
		PointBasicLevelPage = 19,

		[Description("กำหนดคะแนน หน้าตั้งกฏการคำนวณ")]
		PointRulePage = 20,

		[Description("กำหนดคะแนน หน้าเพิ่มตั้งกฏการคำนวณ")]
		PointRuleCreatePage = 21,

		[Description("กำหนดคะแนน หน้าคะแนนพื้นฐาน")]
		PointBasicPage = 22,

		[Description("กำหนดคะแนน หน้าสำหรับPromotion")]
		PointPromotionPage = 23,

		[Description("กำหนดคะแนน หน้าตั้งกฏสำหรับCampaign")]
		PointCampaignPage = 24,

		[Description("กำหนดคะแนน หน้าคะแนนGamification")]
		PointGamificationPage = 25,

		[Description("กำหนดคะแนน หน้าเพิ่มคะแนนGamification")]
		PointGamificationCreatePage = 26,

		[Description("การอนุมัติ หน้าตั้งค่าการอนุมัติ")]
		ApproveAutoSetting = 27,

		[Description("กำหนดคะแนน หน้าแก้ไขคะแนนแลกของกำนัล")]
		PointRewardEditPage = 28,

		[Description("กำหนดคะแนน หน้าแก้ไขคะแนนGamification")]
		PointGamificationEditPage = 29,

		[Description("กำหนดคะแนน หน้าแก้ไขกฎการคำนวณ")]
		PointRuleEditPage = 30,

		[Description("ตั้งค่าผู้ใช้งาน หน้าดูรายละเอียดพนักงาน")]
		SettingEmployeeDetailPage = 31,

		[Description("กำหนดคะแนน หน้าดูรายละเอียดตั้งกฎการคำนวณ")]
		PointRuleDetailPage = 32,

		[Description("การอนุมัติ หน้าอนุมัติสมาชิกใหม่")]
		ApproveNewMemberPage = 33,

		[Description("การอนุมัติ หน้าอนุมัติการเปลี่ยนแปลงข้อมูลสมาชิก")]
		ApproveUpdateMemberPage = 34,

		[Description("การอนุมัติ หน้าคำขออนุมัติบล็อคสมาชิก")]
		ApproveBlockMemberPage = 35,

		[Description("การอนุมัติ หน้าคำขออนุมัติยกเลิกสมาชิก")]
		ApproveCancelMemberPage = 36,

		[Description("การอนุมัติ หน้าคำขออนุมัติเปลี่ยนคะแนน")]
		ApproveUpdatePointPage = 37,

		[Description("การอนุมัติ หน้าคำขออนุมัติแลกสินค้า")]
		ApproveRedemptionPage = 38,

		[Description("การอนุมัติ หน้าคำขออนุมัติเลื่อนระดับ")]
		ApproveUpgradeLevelPage = 39,

		[Description("กำหนดคะแนน หน้าดูรายละเอียดตั้งกฏสำหรับPromotion")]
		PointPromotionDetailPage = 40,

		[Description("กำหนดคะแนน หน้าดูรายละเอียดตั้งกฏสำหรับCampaign")]
		PointCampaignDetailPage = 41,

		[Description("กำหนดคะแนน หน้าเพิ่มตั้งกฏสำหรับPromotion")]
		PointPromotionCreatePage = 42,

		[Description("กำหนดคะแนน หน้าเพิ่มตั้งกฏสำหรับCampaign")]
		PointCampaignCreatePage = 43,

		[Description("ทะเบียนสมาชิก หน้ารวมสมาชิกซ้ำซ้อน")]
		MemberMergePage = 44,

		[Description("การลงทะเบียนสมาชิก")]
		ReportNewMember = 45,

		[Description("รายงาน หน้ารายงานการลงทะเบียนสมาชิก")]
		ReportUpgradeMember = 46,

		[Description("รายงาน หน้ารายงานการยกเลิกสมาชิก")]
		ReportCancelMember = 47,

		[Description("รายงาน หน้ารายงานการบล็อคสมาชิก")]
		ReportBlockMember = 48,

		[Description("รายงาน หน้าการแลกคะแนนสะสม")]
		ReportRedemption = 49,

		[Description("รายงาน หน้าคะแนนสะสมทั้งหมด")]
		ReportMemberPoint = 50,

		[Description("รายงาน หน้าคะแนนสะสมใกล้ครบอายุ")]
		ReportNearlyExpiredPoint = 51,

		[Description("รายงาน หน้าการปรับปรุงคะแนน")]
		ReportUpdateMemberPoint = 52,

		[Description("รายงาน หน้าการยกเลิกคะแนน")]
		ReportCancelMemberPoint = 53,

		[Description("รายงาน หน้าการเปลี่ยนรหัสผ่านสมาชิก")]
		ReportChangePassword = 54,

		[Description("รายงาน หน้าประวัติการเรียกดูทะเบียนสมาชิก")]
		ReportHistoryMember = 55,

		[Description("รายงาน หน้าประวัติการเรียกดูรายงาน")]
		ReportHistory = 56,

		[Description("ทะเบียนสมาชิก หน้าข้อมูลสมาชิก")]
		MemberPage = 57,

		[Description("ทะเบียนสมาชิก หน้าปรับปรุงคะแนน")]
		MemberPointAdjustmentPage = 58,

		[Description("ทะเบียนสมาชิก หน้าความปลอดภัย")]
		MemberChangePasswordPage = 59,

		[Description("ทะเบียนสมาชิก หน้าประวัติข้อมูลสมาชิก")]
		MemberHistoryInformationPage = 60,

		[Description("ทะเบียนสมาชิก หน้าประวัติการเปลี่ยนคะแนน")]
		MemberHistoryPointAdjustmentPage = 61,

		[Description("ทะเบียนสมาชิก หน้ารายการรออนุมัติ")]
		MemberApproveReportPage = 62,

		[Description("ทะเบียนสมาชิก เรียกดูข้อมูลสมาชิก")]
		MemberInformationDetail = 63,

		[Description("ข่าวสาร หน้าข่าวสารสมาชิกทั้งหมด")]
		NewsMainPage = 64,

		[Description("ข่าวสาร หน้าดูรายละเอียดข่าวสารสมาชิกทั้งหมด")]
		NewsViewPage = 65,

		[Description("กำหนดคะแนน หน้าปรับปรุงคะแนนสมาชิก")]
		UploadMemberPoint = 66,

		[Description("ทะเบียนสมาชิก หน้าลงทะเบียนสมาชิก")]
		NewMemberPage = 67,

		ReportPage = 1001,
		ApprovalPage = 1002
	}
}