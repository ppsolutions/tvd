﻿using System.Collections.Generic;

namespace tvd.Models
{
    public class Sidebar
    {
        public IList<SidebarItem> Sidebars { get; set; }
    }

    public class SidebarItem
    {
        public string IconImageUrl { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
        public bool IsHeader { get; set; }
        public int MenuId { get; set; }
        public int ReferMenuId { get; set; }
    }
}