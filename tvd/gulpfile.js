const gulp = require('gulp');
const runSequence = require('run-sequence');
const path = require('path');
const debug = require('gulp-debug');
const del = require('del');
const uglify = require('gulp-uglify');
const webpackStream = require('webpack-stream');
const webpackDevConfig = require('./webpack.config.dev.js');
const webpackProdConfig = require('./webpack.config.prod.js');
const named = require('vinyl-named');
const eslint = require('gulp-eslint');
const webpack = require('webpack');
const gutil = require('gulp-util');
const config = require('./gulp-tasks/config');
const os = require('os');
const machineName = os.hostname().toUpperCase();
const msbuild = require('gulp-msbuild');
const zip = require('gulp-zip');
const add = require('gulp-add');
const fs = require('fs-extra');

gulp.task('resolve-js', function () {
    webpackDevConfig.watch = true;
    return gulp.src([
        path.resolve(__dirname, 'src/index.js'),
        path.resolve(__dirname, 'src/**/index.js')
        ////path.resolve(__dirname, 'node_modules/froala-editor/js/plugins/**/*.min.js'),
        ])
        // .pipe(eslint())
        // .pipe(eslint.format())
        // .pipe(eslint.failOnError())
        .pipe(debug())
        ////.pipe(named(function (file) {
        ////    file.named = file.relative.replace(/\.js$/, '');
        ////    this.queue(file);
        ////}))
        .pipe(webpackStream(webpackDevConfig))
        .on('error', function () {
            this.emit('end');
        })
        .pipe(gulp.dest('dist'));
});

gulp.task('publish', ['build-package'], function () {
    config.set('release');
    const publishDir = config.destWebsitePath();

    console.log('Start publish: ' + publishDir);
    console.log('Machine name: ' + machineName);
    console.log(path.join(publishDir, '/**/*'));

    return gulp.src([path.join(publishDir, '/**/*')])
        .pipe(add('./version.txt', new Date().toString()))
        .pipe(zip('package.zip'))
        .pipe(gulp.dest(publishDir));
});

gulp.task('build-package', function (done) {
    runSequence('clean-publishDir', 'msbuild-release', ['publish-assets', 'publish-views'], done);
});

gulp.task('publish-views', function () {
    config.set('release');
    const publishDir = config.destWebsitePath();
    return gulp.src(['**/*.*', '!**/*.cs'])
        .pipe(gulp.dest(publishDir));
});

gulp.task('publish-assets', function () {
    config.set('release');
    const publishDir = config.destWebsitePath();
    webpackProdConfig.watch = false;
    return gulp.src([
        path.resolve(__dirname, 'src/index.js'),
        path.resolve(__dirname, 'src/**/index.js')
    ])
    .pipe(webpackStream(webpackProdConfig))
    .on('error', function () {
        this.emit('end');
    })
    .pipe(gulp.dest(publishDir + '/dist'));
});

gulp.task('clean', function () {
    return del([
        'dist/**/*'
    ]);
});

gulp.task('clean-publishDir', function () {
    config.set('release');
    const publishDir = config.destWebsitePath();
    fs.emptyDir(publishDir, function (err) {
        if (!err) 
            console.log(publishDir + 'was cleaned successfully!');
    });
});

gulp.task('default', function (cb) {
    runSequence(['clean', 'resolve-js'], cb);
});

gulp.task('build', function (callback) {
    webpack(webpackProdConfig, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString(webpackProdConfig.stats));
        callback();
    });
});

gulp.task('msbuild-release', function () {
    return gulp.src('tvd.csproj')
        .pipe(msbuild({
            logCmmand: true,
            stdout: true,
            errorOnFail: true,
            maxBuffer: 10000 * 1024,
            toolsVersion: 14.0,
            properties: {
                Configuration: 'Release',
                Platform: 'AnyCPU',
                DeployOnBuild: true,
                verbosity: 'minimal'
            }
        }));
});