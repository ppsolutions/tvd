const webpack = require("webpack");
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production')
};

module.exports = { // Note: If use ES6 please use export default 
  entry: './src/index',
  output: {
      path: path.resolve(__dirname, "dist"),
      publicPath: '/dist/',
      filename: 'bundle.js'
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
    new webpack.DefinePlugin(GLOBALS),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    }),
    new webpack.optimize.DedupePlugin()
  ],
  module: {
      loaders: [
          { test: /\.(js|jsx)$/, loader: 'babel', exclude: [/node_modules/, /particles.js/] },
          { test: /\.hbs/, loader: "handlebars-template-loader" },
          { test: /(\.css)$/, loader: ExtractTextPlugin.extract("css?sourceMap") },
          { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/, loader: "file-loader?name=fonts/[name].[ext]" },
          { test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=images/[name].[ext]" },
          { test: /\.json$/, loader: "json-loader" }
      ]
  },
  resolve: {
      descriptionFiles: ["package.json", "bower.json"],
      alias: {
          'handlebars': path.resolve(__dirname, 'node_modules/handlebars')
      }
  }
};
