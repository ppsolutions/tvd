const webpack = require("webpack");
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
//var handlebarsHelperDir = path.resolve(__dirname, 'src/components/utils/handlebarsHelpers');

module.exports = { // Note: If use ES6 please use export default 
  debug: true,
  devtool: 'source-map',
  noInfo: false,
  //entry: './src/index',
  output: {
      path: path.resolve(__dirname, "dist"),
      publicPath: '/dist/',
      filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
    , new webpack.NoErrorsPlugin()
    , new ExtractTextPlugin('styles.css')
    , new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
  ],
  //externals: {
  //  'jquery': 'jQuery'
  //},
  module: {
    loaders: [
        { test: /\.(js|jsx)$/, loader: 'babel', exclude: [/node_modules/, /particles.js/] },
        { test: /\.hbs/, loader: "handlebars-template-loader" },
        //{ test: /\.hbs$/, loaders: ['handlebars?helperDirs[]=' + handlebarsHelperDir] },
        { test: /(\.css)$/, loader: ExtractTextPlugin.extract("css?sourceMap") },
        //{ test: /\.scss$/, loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader']) },
        //{ test: /\.scss$/, loader: ExtractTextPlugin.extract(['css', 'sass']) },
        { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/, loader: "file-loader?name=fonts/[name].[ext]" },
        { test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=images/[name].[ext]" },
        { test: /\.json$/, loader: "json-loader" }
    ]
  },
  resolve: {
      extensions: ['', '.js', '.jsx', '.scss'],
      alias: {
        'handlebars': path.resolve(__dirname, 'node_modules/handlebars')
      }
  }
};

