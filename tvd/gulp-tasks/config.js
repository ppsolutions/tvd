let env = 'development';
const config = module.exports = {};
const websitePath = './',
    releasePublishPath = '../Publish.Website/';

config.set = function (environment) {
    'use strict';
    env = environment;
};

config.get = function () {
    'use strict';
    return env;
};

config.destWebsitePath = function () {
    'use strict';
    if (env === 'release') {
        return releasePublishPath;
    }
    return websitePath;
};