﻿using System.Web;
using System.Web.Http;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace tvd.AppStart
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            var config = GlobalConfiguration.Configuration;

            GlobalConfiguration.Configure(x => x.MapHttpAttributeRoutes());

            RouteTable.Routes.MapHttpRoute(
                name: "Api",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { area = "WebApi" },
                constraints: new { }
            ).RouteHandler = new SessionStateRouteHandler();
        }

        public class SessionStateRouteHandler : IRouteHandler
        {
            IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
            {
                return new SessionableControllerHandler(requestContext.RouteData);
            }
        }
    }

    public class SessionableControllerHandler : HttpControllerHandler, IRequiresSessionState
    {
        public SessionableControllerHandler(RouteData routeData)
            : base(routeData)
        { }
    }
}