﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using tvd.Controllers.Api.Services;
using tvd.Factory;
using tvd.Factory.Interfaces;
using tvd.Http.Repository;
using tvd.model.Helpers;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.AppStart
{
    public static class AutofacInitializer
    {
        public static void Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly()); //Register MVC Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()); //Register WebApi Controllers
            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);

            RegisterType(builder);

            var container = builder.Build();

            //Set the MVC DependencyResolver
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            //Set the WebApi DependencyResolver
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterType(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterType<ApproveService>().As<IApproveService>().SingleInstance();
            builder.RegisterType<AuditLogService>().As<IAuditLogService>().SingleInstance();
            builder.RegisterType<LookupService>().As<ILookupService>().SingleInstance();
            builder.RegisterType<PointService>().As<IPointService>().SingleInstance();
            builder.RegisterType<MemberService>().As<IMemberService>().SingleInstance();
            builder.RegisterType<PersonAddressService>().As<IPersonAddressService>().SingleInstance();
            builder.RegisterType<PersonQuestionService>().As<IPersonQuestionService>().SingleInstance();
            builder.RegisterType<UrlGeneratorService>().As<IUrlGeneratorService>().SingleInstance();
            builder.RegisterType<EmployeeService>().As<IEmployeeService>().InstancePerLifetimeScope();
            builder.RegisterType<RoleService>().As<IRoleService>().SingleInstance();
            builder.RegisterType<NewsService>().As<INewsService>().SingleInstance();
            builder.RegisterType<RewardService>().As<IRewardService>().SingleInstance();
            builder.RegisterType<RuleService>().As<IRuleService>().SingleInstance();
            builder.RegisterType<GamificationService>().As<IGamificationService>().SingleInstance();
            builder.RegisterType<PromotionService>().As<IPromotionService>().SingleInstance();
            builder.RegisterType<ProductService>().As<IProductService>().SingleInstance();
            builder.RegisterType<ReportService>().As<IReportService>().SingleInstance();
            builder.RegisterType<MemberPointService>().As<IMemberPointService>().SingleInstance();

            builder.RegisterType<ApproveRepository>().As<IApproveRepository>().SingleInstance();
            builder.RegisterType<AuditLogRepository>().As<IAuditLogRepository>().SingleInstance();
            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().SingleInstance();
            builder.RegisterType<LookupRepository>().As<ILookupRepository>().SingleInstance();
            builder.RegisterType<PointRepository>().As<IPointRepository>().SingleInstance();
            builder.RegisterType<MemberRepository>().As<IMemberRepository>().SingleInstance();
            builder.RegisterType<PersonAddressRepository>().As<IPersonAddressRepository>().SingleInstance();
            builder.RegisterType<PersonQuestionRepository>().As<IPersonQuestionRepository>().SingleInstance();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().SingleInstance();
            builder.RegisterType<NewsRepository>().As<INewsRepository>().SingleInstance();
            builder.RegisterType<RuleRepository>().As<IRuleRepository>().SingleInstance();
            builder.RegisterType<GamificationPointRepository>().As<IGamificationPointRepository>().SingleInstance();

            builder.RegisterType<DBHelper>().As<IDBHelper>().SingleInstance();
            builder.RegisterType<RegisterFactory>().As<IRegisterFactory>().SingleInstance();
            builder.RegisterType<UpdateMemberFactory>().As<IUpdateMemberFactory>().SingleInstance();
            builder.RegisterType<RoleFactory>().As<IRoleFactory>().SingleInstance();
            builder.RegisterType<NewsFactory>().As<INewsFactory>().SingleInstance();
            builder.RegisterType<LogHelper>().As<ILogHelper>().SingleInstance();
            builder.RegisterType<EmployeeFactory>().As<IEmployeeFactory>().SingleInstance();
            builder.RegisterType<ApproveFactory>().As<IApproveFactory>().SingleInstance();
            builder.RegisterType<ApproveHelper>().As<IApproveHelper>().SingleInstance();

            builder.RegisterType<ContextProvider>().As<IContextProvider>().InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var contextProvider = c.Resolve<IContextProvider>();
                var userData = new DefaultUserData(contextProvider);
                return userData;
            }).As<IUserData>().InstancePerRequest();

            builder.RegisterType<HttpRepository>().As<IHttpRepository>().InstancePerRequest();
            builder.RegisterType<PointServiceSet>().As<IPointServiceSet>().SingleInstance();

            // Api
            builder.RegisterType<LookupServiceApi>().As<ILookupServiceApi>().SingleInstance();
            
            // Common properties service
            RegisterCommonPropertiesService(builder);
        }

        private static void RegisterCommonPropertiesService(ContainerBuilder builder)
        {
            builder.RegisterType<CommonPropertiesService>().As<ICommonPropertiesService>();
            builder.RegisterType<CommonPropertiesServiceSet>().As<ICommonPropertiesServiceSet>().SingleInstance();
        }
    }
}
