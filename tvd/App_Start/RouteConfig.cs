﻿using System.Web.Mvc;
using System.Web.Routing;

namespace tvd
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{pageTypeId}",
                defaults: new { action = "Index", pageTypeId = UrlParameter.Optional }
            );
        }
    }
}

