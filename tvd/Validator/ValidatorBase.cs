﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static System.String;


namespace tvd.Validator
{
    public abstract class ValidatorBase<T>
    {
        public List<string> brokenRules;

        protected ValidatorBase()
        {
            brokenRules = new List<string>();
        }

        public bool IsValid(T entity)
        {
            return !BrokenRules(entity).Any();
        }

        public abstract IEnumerable<string> BrokenRules(T entity);

        public string GetBrokenRulesInHtmlFormat()
        {
            var text = Join(Environment.NewLine, brokenRules);
            // HTML encode text
            text = HttpUtility.HtmlEncode(text);
            // Convert single newlines to <br>
            text = text.Replace(Environment.NewLine, "<br />\r\n");
            return text;
        }
    }
}