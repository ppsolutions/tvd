﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.CommonDTO.News;

namespace tvd.Validator
{
    public class NewsValidator  : ValidatorBase<News>
    {
        public override IEnumerable<string> BrokenRules(News entity)
        {
            if (entity.StartDate >= entity.EndDate)
            {
                brokenRules.Add("วันใช้งานไม่ถูกต้อง");
            }
            if (string.IsNullOrEmpty(entity.Title))
            {
                brokenRules.Add("กรุณากรอกหัวข้อข่าว");
            }
            if (string.IsNullOrEmpty(entity.Content))
            {
                brokenRules.Add("กรุณากรอกเนื้อหาข่าว");
            }
            return brokenRules;
        }
    }
}