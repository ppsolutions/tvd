﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;

namespace tvd.Validator
{
    public class GamificationValidator : ValidatorBase<GamificationPoint>
    {
        private readonly IGamificationService _service;
        public GamificationValidator(IGamificationService service)
        {
            _service = service;
        }
        public override IEnumerable<string> BrokenRules(GamificationPoint entity)
        {
            if (entity.ValidFrom >= entity.ValidTo)
            {
                brokenRules.Add("วันใช้งานไม่ถูกต้อง");
            }
            if (string.IsNullOrEmpty(entity.GamificationCode))
            {
                brokenRules.Add("กรุณากรอกรหัส Gamification");
            }
            else
            {
                var isExist = _service.GetGamification(entity.GamificationCode) != null;
                if (isExist)
                {
                    brokenRules.Add("รหัสนี้มีอยู่ในระบบแล้ว");
                }
            }
            return brokenRules;
        }
    }
}