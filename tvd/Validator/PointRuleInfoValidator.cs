﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.Point;

namespace tvd.Validator
{
    public class PointRuleInfoValidator : ValidatorBase<PointRuleInfoDto>
    {
        public override IEnumerable<string> BrokenRules(PointRuleInfoDto entity)
        {
            if (entity.ValidFrom >= entity.ValidTo)
            {
                brokenRules.Add("วันใช้งานไม่ถูกต้อง");
            }
            return brokenRules;
        }
    }
}