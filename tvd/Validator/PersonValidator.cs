﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.Validator
{
    public class PersonValidator : ValidatorBase<Person>
    {

        public override IEnumerable<string> BrokenRules(Person entity)
        {
            if (entity.UserName.Length < 3)
            {
                brokenRules.Add("ชื่อผู้ใช้ควรมากกว่า 3 ตัวอักษร");
            }
            if (entity.Password.Length < 8)
            {
                brokenRules.Add("รหัสผ่านควรมากกว่า 8 ตัวอักษร");
            }
            if (entity.IsLegalPerson.HasValue && entity.IsLegalPerson.Value)
            {
                if (!entity.BirthDate.HasValue)
                {
                    brokenRules.Add("กรุณากรอกวันเดือนปีเกิด");
                } 
            }
            return brokenRules;
        }
    }
}