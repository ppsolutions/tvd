﻿using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class SearchController : DirectLandingController
    {
        public SearchController(
            IContextProvider contextProvider,
            IUserData userData,
            ICommonPropertiesService commonService) : base(contextProvider, userData, commonService)
        {

        }

        public ActionResult Index()
        {
            var vm = new SearchViewModel();
            return View(vm);
        }
    }
}