﻿using System;
using System.Linq;
using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class SettingController : DirectLandingController
    {
        private readonly ILookupService _lookupService;
        private readonly IEmployeeService _employeeService;
        private readonly IRoleService _roleService;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IContextProvider _contextProvider;

        public SettingController(
            IContextProvider contextProvider,
            IUserData userData,
            ICommonPropertiesService commonService,
            ILookupService lookupService, 
            IEmployeeService employeeService,
            IUrlGeneratorService urlGeneratorService,
            IRoleService roleService) : base(contextProvider, userData, commonService)
        {
            _lookupService = lookupService;
            _employeeService = employeeService;
            _roleService = roleService;
            _urlGeneratorService = urlGeneratorService;
            _contextProvider = contextProvider;
        }

        public ActionResult Employee()
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();

            return View(vm);
        }

        public ActionResult EmployeeCreate()
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();

            return View(vm);
        }

        public ActionResult EmployeeDetail(string id)
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            vm.Employee = _employeeService.GetEmployee(id);
            vm.EmployeeEditUrl = this._urlGeneratorService.GetSettingsEmployeeEditPageUrl(id);

            return View(vm);
        }

        public ActionResult EmployeeEdit(string id)
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            vm.Employee = _employeeService.GetEmployee(id);
            vm.EmployeeEditUrl = this._urlGeneratorService.GetSettingsEmployeeEditPageUrl(id);

            //var accessControlUsers = _employeeService.GetAccessControlUser(id);
            //if(accessControlUsers != null &&
            //   accessControlUsers.Any())
            //{
            //    foreach(var a in vm.AccessControlList)
            //    {
            //        var c = accessControlUsers.Where(b => b.PageTypeId == a.PageTypeId).FirstOrDefault();
            //        if(c != null)
            //        {
            //            a.IsChecked = true;
            //        }
            //        else
            //        {
            //            a.IsChecked = false;
            //        }
            //    }
            //}

            return View(vm);
        }

        public ActionResult Permission()
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult PermissionCreate()
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult PermissionEdit(int id)
        {
            var vm = _employeeService.CreateSettingViewModel(_contextProvider);
            vm.RoleId = id;

            // get exist permission info
            vm.RolePermission = _roleService.GetRolePermission(id);
            vm.PermissionUrl = this._urlGeneratorService.GetSettingsPermissionPageUrl();
            vm.PermissionEditUrl = this._urlGeneratorService.GetSettingsPermissionEditPageUrl(id);

            var accessControlRoles = _employeeService.GetAccessControlRole(id);
            if (accessControlRoles != null &&
               accessControlRoles.Any())
            {
                foreach (var a in vm.AccessControlList)
                {
                    var c = accessControlRoles.Where(b => b.PageTypeId == a.PageTypeId).FirstOrDefault();
                    if (c != null)
                    {
                        a.IsChecked = true;
                    }
                    else
                    {
                        a.IsChecked = false;
                    }
                }
            }

            return View(vm);
        }
    }
}