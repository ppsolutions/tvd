﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.Enum;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class RegisterController : DirectLandingController
    {
        private readonly ILookupService _lookupService;
		private readonly IMemberService _memberService;
		private readonly IContextProvider _contextProvider;

		public RegisterController(IContextProvider contextProvider,
								  IUserData userData,
								  ICommonPropertiesService commonService,
								  ILookupService lookupService,
								  IMemberService memberService) : base(contextProvider, userData, commonService)
        {
            _lookupService = lookupService;
			_memberService = memberService;
			_contextProvider = contextProvider;
        }

        public ActionResult Index()
        {
			return View(this.GenerateViewModels());
        }

		public ActionResult Merge()
		{
			return View(this.GenerateViewModels());
		}

		private RegisterViewModel GenerateViewModels()
		{
			var vm = _memberService.CreateRegisterViewModel(_contextProvider);

			var allTitles = _lookupService.GetTitles().ToList();
			vm.PersonTitles = allTitles.Where(o => o.IsLegalPerson);
			vm.CorporateTitles = allTitles.Where(o => o.IsLegalPerson == false);
			vm.Cities = _lookupService.GetCities();
			vm.Questions = _lookupService.GetQuestions();

			string query = Request.QueryString["m"];
			if (string.IsNullOrEmpty(query))
			{
				return vm;
			}
			else
			{
				string[] datas = query.Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
				var members = this.GetMembers(datas);
				var selectedMember = this.GetDefaultValue(members);

				if (selectedMember != null)
				{
					vm.Id = selectedMember.Person.PersonId;
					vm.Source = (Source)selectedMember.Person.Source;
					vm.TitleId = selectedMember.Person.TitleId;
					vm.FirstName = selectedMember.Person.FirstName;
					vm.LastName = selectedMember.Person.LastName;
					vm.BirthDate = selectedMember.Person.BirthDate;
					vm.ContactNumber = selectedMember.Person.ContactNumber;
					vm.EmailAddress = selectedMember.Person.EmailAddress;
					vm.RecCreatedWhen = selectedMember.Person.RecCreatedWhen;
					vm.SelectedMergeMembers = query;
					vm.Members = members;
				}

			}

			return vm;
		}

		private Member[] GetMembers(string[] datas)
		{
			List<Member> members = new List<Member>();
			foreach (string data in datas)
			{
				string[] items = data.Split(new char[] { '_' }, System.StringSplitOptions.RemoveEmptyEntries);
				members.Add(_memberService.GetMemberDetail(Convert.ToInt32(items[0]), (Source)Convert.ToInt32(items[1])));
			}

			return members.ToArray();
		}

		private Member GetDefaultValue(Member[] comparers)
		{
			Member main = null;

			if (comparers != null && comparers.Any())
			{
				main = comparers.OrderByDescending(m => m.Person.RecCreatedWhen).First();
				Member[] filter = comparers.Where(c => c.Person.PersonId != main.Person.PersonId || c.Person.ReferenceId != main.Person.ReferenceId).ToArray();

				if (filter.Count(f => f.Person.TitleId == main.Person.TitleId) != filter.Count())
				{
					main.Person.TitleId = 0;
				}

				if (filter.Count(f => f.Person.FirstName == main.Person.FirstName) != filter.Count())
				{
					main.Person.FirstName = string.Empty;
				}

				if (filter.Count(f => f.Person.LastName == main.Person.LastName) != filter.Count())
				{
					main.Person.LastName = string.Empty;
				}

				if (filter.Count(f => f.Person.BirthDate == main.Person.BirthDate) != filter.Count())
				{
					main.Person.BirthDate = null;
				}

				if (filter.Count(f => f.Person.ContactNumber == main.Person.ContactNumber) != filter.Count())
				{
					main.Person.ContactNumber = string.Empty;
				}

				if (filter.Count(f => f.Person.EmailAddress == main.Person.EmailAddress) != filter.Count())
				{
					main.Person.EmailAddress = string.Empty;
				}
			}

			return main;
		}
	}
}