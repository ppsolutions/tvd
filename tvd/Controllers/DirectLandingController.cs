﻿using System.Web.Mvc;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    public abstract class DirectLandingController : BaseController
    {
        private readonly IContextProvider contextProvider;
        private readonly IUserData userData;
        private readonly ICommonPropertiesService commonPropertiesService;

        public DirectLandingController(IContextProvider contextProvider, 
            IUserData userData, 
            ICommonPropertiesService commonPropertiesService) 
            :  base(contextProvider, userData)
        {
            this.contextProvider = contextProvider;
            this.userData = userData;
            this.commonPropertiesService = commonPropertiesService;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (userData == null ||
                userData.UserInfo == null)
            {
                filterContext.HttpContext.Response.Redirect("~/login/index");
                return;
            }

            var employeeInRole = userData.UserInfo.EmployeeInRole;
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpper();

            if (!IsPermit(controllerName, employeeInRole))
            {
                filterContext.HttpContext.Response.Redirect("~/permission/index");
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewResultBase = filterContext.Result as ViewResult;
            if (viewResultBase != null)
            {
                object viewModel = viewResultBase.Model;
                commonPropertiesService.SetCommonProperties(viewModel, base.ContextBase, base.UserData);
            }
        }

        private bool IsPermit(string name, RolePermission employeeInRole)
        {
            if (name.Equals("REGISTER") || name.Equals("MEMBERDETAIL"))
            {
                return employeeInRole.IsPermitInMemberRegistrationSystem;
            }
            if (name.Equals("APPROVAL"))
            {
                return employeeInRole.IsPermitInApproveSystem;
            }
            if (name.Equals("NEWS"))
            {
                return employeeInRole.IsPermitInNewsSystem;
            }
            if (name.Equals("POINT"))
            {
                return employeeInRole.IsPermitInPointSettingSystem;
            }
            if (name.Equals("REPORT"))
            {
                return employeeInRole.IsPermitInReportSystem;
            }
            if (name.Equals("SEARCH") || name.Equals("SEARCHRESULT") || name.Equals("LOGIN"))
            {
                return true;
            }
            if (name.Equals("SETTING"))
            {
                return employeeInRole.IsPermitInEmployeeManagementSystem;
            }

            return false;
        }
    }
}