﻿using System.Web.Mvc;
using tvd.model.Provider;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    public class LoginController : Controller
    {
        private readonly IContextProvider _contextProvider;
        private readonly IEmployeeService _employeeService;
        private readonly IUrlGeneratorService _urlGeneratorService;

        public LoginController(IContextProvider contextProvider,
            IEmployeeService employeeService,
            IUrlGeneratorService urlGeneratorService)
        {
            _employeeService = employeeService;
            _contextProvider = contextProvider;
            _urlGeneratorService = urlGeneratorService;
        }

        // GET: Index
        public ActionResult Index()
        {
            return View();
        }

        public void Logout()
        {
            _employeeService.Logout(_contextProvider);

            _contextProvider.GetContext().Response.Redirect(_urlGeneratorService.GetLoginUrl());
        }
    }
}