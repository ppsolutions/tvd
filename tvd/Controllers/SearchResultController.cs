﻿using System.Web.Mvc;
using tvd.model.Models.Request;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.ModelBinder;
using tvd.Security;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class SearchResultController : DirectLandingController
    {
        private IContextProvider _contextProvider { get; set; }

        public SearchResultController(IContextProvider contextProvider, 
            IUserData userData, ICommonPropertiesService commonPropertiesService) : base(contextProvider, userData, commonPropertiesService)
        {
        }

        // GET: SearchResult
        public ActionResult Index([ModelBinder(typeof(SearchCriteriaModelBinder))]SearchCriteria searchCriteria)
        {
            var vm = new SearchResultViewModel();
            vm.SearchCriteria = searchCriteria;

            return View(vm);
        }
    }
}