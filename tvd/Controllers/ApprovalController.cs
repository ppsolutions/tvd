﻿using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class ApprovalController : DirectLandingController
    {
        private readonly IApproveService _approvalService;
        private readonly IContextProvider _contextProvider;

        public ApprovalController(IContextProvider contextProvider, IUserData userData, ICommonPropertiesService commonPropertiesService,
            IApproveService approvalService) : base(contextProvider, userData, commonPropertiesService)
        {
            _approvalService = approvalService;
            _contextProvider = contextProvider;
        }

        public ActionResult NewMember()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult UpdateMember()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult BlockMember()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult CancelMember()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult UpdatePoint()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult Redemption()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult UpgradeLevel()
        {
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult AutoSetting()
        {
            var setting = _approvalService.GetApproveSetting();
            var vm = _approvalService.CreateApprovalViewModel(_contextProvider);
            vm.ApproveSetting = setting;
            return View(vm);
        }
    }
}