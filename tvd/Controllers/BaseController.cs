﻿using System;
using System.Web;
using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;

namespace tvd.Controllers
{
    public class BaseController : Controller
    {
        protected HttpContextBase ContextBase;
        protected IUserData UserData;

        public BaseController(IContextProvider contextProvider, IUserData userData)
        {
            ContextBase = contextProvider.GetContext();
            UserData = userData;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is System.Threading.Tasks.TaskCanceledException)
            {
                base.OnException(filterContext);
                return;
            }

            if (filterContext.HttpContext != null && 
                filterContext.HttpContext.Request != null && 
                filterContext.HttpContext.Request.RawUrl != null)
            {
                filterContext.Exception = CustomExceptionLog(filterContext);
            }
        }

        protected virtual Exception CustomExceptionLog(ExceptionContext exceptionContext)
        {
            return exceptionContext.Exception;
        }
    }
}