﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using tvd.model.Models.Enum;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Models;
using tvd.Security;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class MemberDetailController : DirectLandingController
	{
		private readonly IMemberService _memberService;
		private readonly ILookupService _lookupService;
        private readonly IAuditLogService _logService;
        private readonly IUserData _userData;
		public MemberDetailController(IContextProvider contextProvider, IUserData userData,
			ICommonPropertiesService commonPropertiesService,
			IMemberService memberService, ILookupService lookupService, IAuditLogService logService) : base(contextProvider, userData, commonPropertiesService)
		{
			_memberService = memberService;
			_lookupService = lookupService;
            _logService = logService;
            _userData = userData;
		}

		// GET: MemberDetail
		public ActionResult Index(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberPage, null, empCode);

			return View(vm);
		}

		public ActionResult PointAdjustment(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberPointAdjustmentPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberPointAdjustmentPage, null, empCode);

			return View(vm);
		}

		public ActionResult ChangePassword(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberChangePasswordPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberChangePasswordPage, null, empCode);

			return View(vm);
		}

		public ActionResult MemberHistoryInformation(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberHistoryInformationPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberHistoryInformationPage, null, empCode);

			return View(vm);
		}

		public ActionResult PointAdjustmentHistoryInformation(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberHistoryPointAdjustmentPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberHistoryPointAdjustmentPage, null, empCode);

			return View(vm);
		}

		public ActionResult ItemWaitingForApprove(int personId, int source)
		{
			var vm = this.GetMemberInfomation(personId, source, PageTypeId.MemberApproveReportPage);

			//Log audit
			var empCode = _userData.UserInfo.EmployeeCode;
			_logService.InsertViewHistory(personId, PageTypeId.MemberApproveReportPage, null, empCode);

			return View(vm);
		}

		private MemberViewModel GetMemberInfomation(int personId, int source, PageTypeId pageTypeId)
		{
			//get general informaton
			var member = _memberService.GetMemberDetail(personId, (Source)source);

			if (member == null)
			{
				throw new HttpException(404, "ไม่ค้นพบบุคคลที่ท่านค้นหา");
			}

			var titles = _lookupService.GetTitles().ToList();
			var vm = new MemberViewModel(member)
			{
				Cities = _lookupService.GetCities(),
				RedeemPointsTypes = _lookupService.GetRedeemPointsTypes(),
				PersonTitles = titles.Where(t => t.IsLegalPerson),
				CorporateTitles = titles.Where(t => !t.IsLegalPerson),
				PageTypeId = pageTypeId
			};

			var title = titles.Where(t => t.TitleId == vm.TitleId).FirstOrDefault();
			if (title != null)
			{
				vm.TitleName = title.TitleName;
			}

			return vm;
		}
	}
}