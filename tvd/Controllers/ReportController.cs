﻿using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Models;
using tvd.Security;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class ReportController : DirectLandingController
    {
        private readonly IContextProvider _contextProvider;
        private readonly IReportService _reportService;
        private readonly ILookupService _lookupService;
        private readonly IAuditLogService _logService;
        private readonly IUserData _userData;

        public ReportController(IContextProvider contextProvider,
            IReportService reportService,
            IUserData userData,
            ILookupService lookupService,
            ICommonPropertiesService commonService,
            IAuditLogService logService) : base(contextProvider, userData, commonService)
        {
            _contextProvider = contextProvider;
            _reportService = reportService;
            _lookupService = lookupService;
            _logService = logService;
            _userData = userData;
        }
        // GET: Report
        public ActionResult NewMember()
        {
            var vm =_reportService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportNewMember, _userData.UserInfo.EmployeeCode);
            return View(vm);
        }

        public ActionResult BlockMember()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportBlockMember, _userData.UserInfo.EmployeeCode);

            return View(vm);
        }

        public ActionResult CancelMember()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportCancelMember, _userData.UserInfo.EmployeeCode);

            return View(vm);
        }

        public ActionResult UpgradeMember()
        {

            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            vm.Levels = _lookupService.GetPointLevels();
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportUpgradeMember, _userData.UserInfo.EmployeeCode);

            return View(vm);
        }

        public ActionResult ChangePassword()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportChangePassword, _userData.UserInfo.EmployeeCode);
            return View(vm);
        }

        public ActionResult Redemption()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            vm.RoleList = _lookupService.GetRoles();
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportRedemption, _userData.UserInfo.EmployeeCode);

            return View(vm);
        }

        public ActionResult MemberPoint()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportMemberPoint, _userData.UserInfo.EmployeeCode);

            return View(vm);
        }

        public ActionResult MemberPointExpire()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            _logService.InsertViewHistory(null, PageTypeId.ReportPage, PageTypeId.ReportNearlyExpiredPoint, _userData.UserInfo.EmployeeCode);
            return View(vm);
        }

        public ActionResult UpdateMemberPoint()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            return View(vm); 
        }

        public ActionResult CancelMemberPoint()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult MemberHistory()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult History()
        {
            var vm = _reportService.CreateSettingViewModel(_contextProvider);
            return View(vm);
        }
    }
}