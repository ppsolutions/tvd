﻿using System;
using System.Web.Mvc;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class NewsController : DirectLandingController
    {
        private readonly INewsService _newsService;
        private readonly ILookupService _lookupService;
        private readonly IContextProvider _contextProvider;

        public NewsController(IContextProvider contextProvider, 
            IUserData userData, 
            ICommonPropertiesService commonPropertiesService, 
            INewsService newsService, 
            ILookupService lookupService) 
            : base(contextProvider, userData, commonPropertiesService)
        {
            _newsService = newsService;
            _lookupService = lookupService;
            _contextProvider = contextProvider;
        }

        public ActionResult Index()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult Member()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult Promotion()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult MemberRules()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult MemberBenefits()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult Create()
        {
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider);
            return View(newsViewModel);
        }

        public ActionResult Edit(int id)
        {
            if(id <= 0)
            {
                throw new ArgumentNullException();
            }

            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider, id);
            return View(newsViewModel);
        }

        public ActionResult View(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentNullException();
            }
            var newsViewModel = _newsService.CreateNewsViewModel(_contextProvider, id);
            return View(newsViewModel);
        }

    }
}