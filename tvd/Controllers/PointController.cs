﻿using System.Linq;
using System.Web.Mvc;
using tvd.model.Models.Enum;
using tvd.model.Models.UserData;
using tvd.model.Provider;
using tvd.Security;
using tvd.Services.Interfaces;

namespace tvd.Controllers
{
    [SecurityAuthorize]
    public class PointController : DirectLandingController
    {
        private readonly ILookupService _lookupService;
        private readonly IPointService _pointService;
        private readonly IGamificationService _gamificationService;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IContextProvider _contextProvider;
        public PointController(IContextProvider contextProvider,
            IUserData userData, 
            ICommonPropertiesService commonPropertiesService,
            IPointService pointService,
            IGamificationService gamificationService,
            ILookupService lookupService,
            IUrlGeneratorService urlGeneratorService) : base(contextProvider, userData, commonPropertiesService)
        {
            _pointService = pointService;
            _lookupService = lookupService;
            _gamificationService = gamificationService;
            _urlGeneratorService = urlGeneratorService;
            _contextProvider = contextProvider;
        }

		public ActionResult UploadMemberPoint()
		{
			var vm = _pointService.CreatePointViewModel(_contextProvider);
			return View(vm);
		}

		public ActionResult RewardList()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult RewardCreate()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult RewardEdit(int id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.RewardInfo = _pointService.FetchRewardInfo(id);

            return View(vm);
        }

        public ActionResult PointLevel()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.PointLevels = _lookupService.GetPointLevels().Where(o => o.RequiredPoint > 0);

            return View(vm);
        }

        public ActionResult BasicPointLevel()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            var config = _lookupService.GetPointConfig();

			vm.ExpireYear = config.ExpireYear;
            vm.PointLevelExpireDate = config.ExpireDate;
            vm.MinPurchase = config.MinPurchase;
            vm.PointLevels = _lookupService.GetPointLevels();

            return View(vm);
        }

        public ActionResult BasicPoint()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult Rule()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);           
            return View(vm);
        }

        public ActionResult RuleCreate()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.PointLevels = _lookupService.GetPointLevels();
            vm.Banks = _lookupService.GetBanks();
            vm.CardTypes = _lookupService.GetCardTypes();
            vm.PaymentTypes = _lookupService.GetPaymentType();
            return View(vm);
        }

        public ActionResult RuleEdit(int id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider, id);
            vm.PointRuleInfo = _pointService.FetchPointRuleInfo(id);
            vm.PointLevels = _lookupService.GetPointLevels();
            vm.Banks = _lookupService.GetBanks();
            vm.CardTypes = _lookupService.GetCardTypes();
            vm.PaymentTypes = _lookupService.GetPaymentType();
            return View(vm);
        }

        public ActionResult RuleDetail(int id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider, id);
            vm.PointLevels = _lookupService.GetPointLevels();
            vm.Banks = _lookupService.GetBanks();
            vm.CardTypes = _lookupService.GetCardTypes();
            vm.PaymentTypes = _lookupService.GetPaymentType();
            vm.PointRuleInfo = _pointService.FetchPointRuleInfo(id);
            var ruleUsed = _pointService.FetchRuleUsed(id);
            if(ruleUsed != null && ruleUsed.Any())
            {
                vm.PromotionUsedInRule = ruleUsed.Where(a => a.PromotionType == PromotionType.Promotion)?.ToList();
                vm.CampaignUsedInRule = ruleUsed.Where(a => a.PromotionType == PromotionType.Campaign)?.ToList();
            }

            return View(vm);
        }

        public ActionResult Promotion()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.Rules = _pointService.FetchRuleUsed();
            return View(vm);
        }

        public ActionResult PromotionCreate()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);

            return View(vm);
        }

        public ActionResult PromotionDetail(string id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.PromotionInfo = _pointService.FetchPromotionInfo(id);
            return View(vm);
        }

        public ActionResult Campaign()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.Rules = _pointService.FetchRuleUsed();
            return View(vm);
        }

        public ActionResult CampaignCreate()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            return View(vm);
        }

        public ActionResult CampaignDetail(string id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.PromotionInfo = _pointService.FetchCampaignInfo(id);
            return View(vm);
        }

        public ActionResult Gamification()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.ApplyTypes = _lookupService.GetApplyTypes();
            return View(vm);
        }

        public ActionResult GamificationCreate()
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.ApplyTypes = _lookupService.GetApplyTypes().Where(o => o != model.Models.Enum.ApplyType.ALL);
            return View(vm);
        }

        public ActionResult GamificationEdit(int id)
        {
            var vm = _pointService.CreatePointViewModel(_contextProvider);
            vm.ApplyTypes = _lookupService.GetApplyTypes().Where(o => o != model.Models.Enum.ApplyType.ALL);
            vm.GamificationInfo = _gamificationService.GetGamification(id);
            return View(vm);
        }
    }
}