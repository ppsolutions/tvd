﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace tvd.Controllers.Api.Attributes
{
    public class AntiForgeryValidate: ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            IEnumerable<string> tokenHeaders;
            if (actionContext.Request.Headers.TryGetValues("token", out tokenHeaders))
            {
                var tokens = tokenHeaders.First().Split(':');
                if (tokens.Length == 2)
                {
                    var cookiesToken = tokens[0];
                    var formToken = tokens[1];
                    AntiForgery.Validate(cookiesToken, formToken);
                }
            }

            base.OnActionExecuting(actionContext);
        }
    }
}