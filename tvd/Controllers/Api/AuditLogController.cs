﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.model.Models;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.Request;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
	public class AuditLogController : BaseApiController
	{
		private readonly IAuditLogService _auditLogService;
		public AuditLogController(HttpContextBase httpContextBase, IAuditLogService auditLogService) : base(httpContextBase)
		{
			_auditLogService = auditLogService;
		}

		[HttpPost]
		[Route("api/auditlog/fetch")]
		public IHttpActionResult GetAuditLog(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetAuditLogs(request.Type, request.RecStatus, request.CreatedBy,
				request.FromDate, request.ToDate, request.RoleId, request.ObjectId, request.Value, request.Pager.PageSize, request.Pager.PageNumber);

			var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}


		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/auditlog/export")]
		public IHttpActionResult ExportAuditLog(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetAuditLogs(request.Type
													   , request.RecStatus
													   , request.CreatedBy
													   , request.FromDate
													   , request.ToDate
													   , request.RoleId
													   , request.ObjectId
													   , request.Value);


			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/report/memberpointexport")]
		public IHttpActionResult ExportMemberPointReport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetMemberPointReport(request.MinPoint
															  , request.MaxPoint
															  , request.DaysBeforeExpire);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/report/memberpointexpiryexport")]
		public IHttpActionResult ExportMemberPointExpiryReport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var startDate = new DateTime(request.ExpireYear, 1, 1, 0, 0, 0);
			var endDate = new DateTime(request.ExpireYear, 12, 31, 23, 59, 59);

			var result = _auditLogService.GetMemberPointExpiryReport(request.MinPoint
															  , request.MaxPoint
															  , startDate
															  , endDate);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		[Route("api/report/memberpoint")]
		public IHttpActionResult GetMemberPointReport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetMemberPointReport(request.MinPoint, request.MaxPoint, request.DaysBeforeExpire, request.Pager.PageSize, request.Pager.PageNumber);
			var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		[Route("api/report/memberpointexpiry")]
		public IHttpActionResult GetMemberPointExpiryReport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var startDate = new DateTime(request.ExpireYear, 1, 1, 0, 0, 0);
			var endDate = new DateTime(request.ExpireYear, 12, 31, 23, 59, 59);

			var result = _auditLogService.GetMemberPointExpiryReport(request.MinPoint, request.MaxPoint, startDate, endDate, request.Pager.PageSize, request.Pager.PageNumber);
			var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		[Route("api/history/fetch")]
		public IHttpActionResult GetHistory(GetHistoryRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetHistory(request.FromDate, request.ToDate, request.PageId, request.Pager.PageSize, request.Pager.PageNumber);
			var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		[Route("api/history/export")]
		public IHttpActionResult ExportHistory(GetHistoryRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _auditLogService.GetHistory(request.FromDate, request.ToDate, request.PageId);

			return SendSuccessResponse(result);
		}

		[HttpPost]
		[Route("api/report/memberhistory")]
		public IHttpActionResult GetMemberHistory(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId &&
											   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_INFO ||
												al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_ADDRESS ||
												al.AuditActionType == model.Models.Enum.AuditActionType.CANCEL_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.BLOCK_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.ACTIVE_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.CHANGE_PASSWORD ||
												al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));


			var pageDataResult = filterResult.Skip((request.Pager.PageNumber - 1) * request.Pager.PageSize).Take(request.Pager.PageSize);

			var returnResult = new ResultSet<AuditLog>(pageDataResult, filterResult.Count());

			var pager = new Pager(returnResult.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(returnResult, pager);
		}

		[HttpPost]
		[Route("api/report/memberhistory/export")]
		public IHttpActionResult GetMemberHistoryExport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId &&
											   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_INFO ||
												al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_ADDRESS ||
												al.AuditActionType == model.Models.Enum.AuditActionType.CANCEL_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.BLOCK_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.ACTIVE_MEMBERSHIP ||
												al.AuditActionType == model.Models.Enum.AuditActionType.CHANGE_PASSWORD ||
												al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));
			
			var returnResult = new ResultSet<AuditLog>(filterResult, filterResult.Count());
			return SendSuccessResponse(returnResult);
		}

		[HttpPost]
		[Route("api/report/pointadjustmenthistory")]
		public IHttpActionResult GetPointAdjustmentHistory(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId &&
														   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP_POINT_UPDATE ||
															al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));

			var pageDataResult = filterResult.Skip((request.Pager.PageNumber - 1) * request.Pager.PageSize).Take(request.Pager.PageSize);

			var returnResult = new ResultSet<AuditLog>(pageDataResult, filterResult.Count());

			var pager = new Pager(returnResult.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(returnResult, pager);
		}

		[HttpPost]
		[Route("api/report/pointadjustmenthistory/export")]
		public IHttpActionResult GetPointAdjustmentHistoryExport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId &&
														   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP_POINT_UPDATE ||
															al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));

			var returnResult = new ResultSet<AuditLog>(filterResult, filterResult.Count());

			return SendSuccessResponse(returnResult);
		}

		[HttpPost]
		[Route("api/report/itemwaitingforapprove")]
		public IHttpActionResult GetItemWaitingForApprove(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId && al.RecStatus == model.Models.Enum.RecStatus.PENDING &&
														   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_INFO ||
															al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_ADDRESS ||
															al.AuditActionType == model.Models.Enum.AuditActionType.CANCEL_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.BLOCK_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.ACTIVE_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.CHANGE_PASSWORD ||
															al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP_POINT_UPDATE ||
															al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));

			var pageDataResult = filterResult.Skip((request.Pager.PageNumber - 1) * request.Pager.PageSize).Take(request.Pager.PageSize);

			var returnResult = new ResultSet<AuditLog>(pageDataResult, filterResult.Count());

			var pager = new Pager(returnResult.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(returnResult, pager);
		}

		[HttpPost]
		[Route("api/report/itemwaitingforapprove/export")]
		public IHttpActionResult GetItemWaitingForApproveExport(GetAuditLogRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.FromDate > request.ToDate)
			{
				return SendFailedResponse("ค้นหาข้อมูลจากวันที่ ต้องน้อยกว่าข้อมูลถึงวันที่");
			}

			var result = _auditLogService.GetAuditLogs(request.FromDate, request.ToDate);

			var filterResult = result.ToList().Where(al => al.ObjectId == request.ObjectId && al.RecStatus == model.Models.Enum.RecStatus.PENDING &&
														   (al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_INFO ||
															al.AuditActionType == model.Models.Enum.AuditActionType.UPDATE_MEMBERSHIP_ADDRESS ||
															al.AuditActionType == model.Models.Enum.AuditActionType.CANCEL_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.BLOCK_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.ACTIVE_MEMBERSHIP ||
															al.AuditActionType == model.Models.Enum.AuditActionType.CHANGE_PASSWORD ||
															al.AuditActionType == model.Models.Enum.AuditActionType.MEMBERSHIP_POINT_UPDATE ||
															al.AuditActionType == model.Models.Enum.AuditActionType.REWARD_REDEMPTION));

			var returnResult = new ResultSet<AuditLog>(filterResult, filterResult.Count());
			
			return SendSuccessResponse(returnResult);
		}
	}
}
