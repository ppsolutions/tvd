﻿using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using FluentEmail;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.Request;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class ApproveController : BaseApiController
    {
        private readonly IApproveService _approveService;
        private readonly IApproveFactory _approveFactory;
        public ApproveController(IApproveService approveService, 
            IApproveFactory approveFactory,
            HttpContextBase contextBase) : base(contextBase)
        {
            _approveService = approveService;
            _approveFactory = approveFactory;
        }


        //POST
        [HttpPost]
        [Route("api/approve/fetch")]
        public IHttpActionResult GetApproveList(GetApproveRequest request)
        {
            var result = _approveService.GetApproveList
                (request.ApproveType, null, request.RecStatus, request.Pager.PageSize, request.Pager.PageNumber);
            var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);
            
            return SendSuccessResponse(result.ToList(), pager);
        }

        [HttpPut]
        [Route("api/approve")]
        public IHttpActionResult Approve(UpdateApproveRequest request)
        {
            foreach (var requestApproveId in request.ApproveIds)
            {
                var result = _approveService.ApprovePendingRequest(requestApproveId, request.UpdatedBy);
                if (!result.IsValid)
                {
                    return SendFailedResponse(result.ValidationMessage);
                }
            }
            return SendSuccessResponse();
        }

        [HttpDelete]
        [Route("api/approve")]
        public IHttpActionResult RejectApprove(UpdateApproveRequest request)
        {
            foreach (var requestApproveId in request.ApproveIds)
            {
                _approveService.RejectPendingRequest(requestApproveId, request.UpdatedBy, request.Type);
            }
            return SendSuccessResponse();
        }



        [HttpPut]
        [ActionName("setting")]
        public IHttpActionResult SetAutoApproveSetting(UpdateApproveSettingRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var token = request.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);
            
            var dto = _approveFactory.Create(request);
             _approveService.UpdateApproveSetting(dto);
            return SendSuccessResponse();
        }
    }
}
