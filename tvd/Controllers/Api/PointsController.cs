﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.Controllers.Api.Services;
using tvd.model.Helpers;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;
using tvd.model.Models.Request;
using tvd.model.V;
using tvd.Services.Interfaces;
using tvd.Validator;

namespace tvd.Controllers.Api
{
	public class PointsController : BaseApiController
	{
		private readonly IPointServiceSet _pointServiceApi;
		private readonly IPointService _pointService;
		private readonly ILookupServiceApi _lookupServiceApi;
		private readonly ILookupService _lookupService;
		private readonly IMemberPointService _memberPointService;
		private readonly IMemberService _memberService;

		public PointsController(HttpContextBase httpContextBase,
			IPointService pointService,
			IPointServiceSet pointServiceApi,
			ILookupServiceApi lookupServiceApi,
			IMemberPointService memberPointService,
			ILookupService lookupService,
			IMemberService memberService) : base(httpContextBase)
		{
			_pointService = pointService;
			_pointServiceApi = pointServiceApi;
			_lookupServiceApi = lookupServiceApi;
			_memberPointService = memberPointService;
			_lookupService = lookupService;
			_memberService = memberService;
		}

		[HttpGet]
		[Route("api/points")]
		public IHttpActionResult GetMemberPointDetail(int memberId)
		{
			var memberPoint = _memberPointService.GetMemberPointDetail(memberId);

			return SendSuccessResponse(memberPoint);
		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/points/level")]
		public IHttpActionResult UpdatePointLevels(UpdatePointLevelRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			if (request.UpdatedLevels == null || request.UpdatedLevels.Any() == false)
			{
				return SendSuccessResponse();
			}

            _pointService.UpdatePointLevelAndExpireDate(request.UpdatedLevels, request.ExpireYear, request.MinPurchase, request.UpdatedBy);
			return SendSuccessResponse();
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/reward/fetch/all")]
		public IHttpActionResult FetchAllReward(RewardDto rewardDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = this._pointServiceApi.RewardService.FetchAllReward();
			if (result != null && result.Count > 0)
			{
				var filterResult = result.Where(r => r.Description.StartsWith(rewardDto.Description) && r.RecStatus == RecStatus.ACTIVE);

				return SendSuccessResponse(filterResult);
			}

			return SendSuccessResponse(null);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/reward/fetch")]
		public IHttpActionResult FetchReward(RewardDto rewardDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = this._pointServiceApi.RewardService.FetchRewards(rewardDto);
			var pager = new Pager(result.TotalItems, rewardDto.Pager.PageNumber, rewardDto.Pager.PageSize);
			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/reward/upsert")]
		public IHttpActionResult UpSertReward(RewardInfoDto rewardInfoDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			//Validate exists reward
			var products = _pointService.GetRedeemPoint(rewardInfoDto.Code);
			if (products != null)
			{
				return SendFailedResponse("มีรหัสนี้ในระบบอยู่แล้ว");
			}


			//Validate code is in DB
			var prod = _lookupService.GetProductInfoFromView(rewardInfoDto.Code);
			if (prod == null)
			{
				return SendFailedResponse("ไม่ค้นพ้นรหัสนี้ในระบบ");
			}

			var result = _pointServiceApi.RewardService.UpsertReward(rewardInfoDto, rewardInfoDto.UserId);
			return SendSuccessResponse(result);
		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/point/reward/upsert")]
		public IHttpActionResult UpSertRewardPut(RewardInfoDto rewardInfoDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.RewardService.UpsertReward(rewardInfoDto, rewardInfoDto.UserId);
			return SendSuccessResponse(result);
		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/point/reward/updatestatus")]
		public IHttpActionResult UpdateRewardStatusPUT(RewardUpdateStatusDto rewardUpdateStatusDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.RewardService.UpdateRewardStatus(rewardUpdateStatusDto, rewardUpdateStatusDto.UserId);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/reward/export")]
		public IHttpActionResult ExportReward(RewardDto rewardDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = this._pointServiceApi.RewardService.FetchRewards(rewardDto);
			var json = result
				.Select(x => new
				{
					Code = x.Code.ToString(),
					Description = x.Description,
					Money = x.Money,
					Point = x.Point,
					PointOnly = x.PointOnly,
					RewardId = x.RewardId,
					RecStatus = x.RecStatus == model.Models.Enum.RecStatus.ACTIVE ? "Active" : "Inactive"
				})
				.ToList();

			return SendSuccessResponse(json);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/gamification/export")]
		public IHttpActionResult ExportGamification(GetGamificationRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}
			var result = _pointServiceApi.GamificationService.GetGamificationPoints(request.Code, request.ApplyTo,
				request.RecStatus, null, null);
			var json = result
				.Select(x => new
				{
					ApplyTo = EnumHelper.GetEnumDescription(x.ApplyTo),
					Description = x.Description,
					GamificationCode = x.GamificationCode,
					GamificationPointId = x.GamificationPointId,
					IncreasePoint = x.IncreasePoint,
					ValidTo = x.ValidTo,
					ValidFrom = x.ValidFrom,
					RecStatus = x.RecStatus == model.Models.Enum.RecStatus.ACTIVE ? "Active" : "Inactive"
				})
				.ToList();
			return SendSuccessResponse(json);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/gamification")]
		public IHttpActionResult CreateGamification(UpsertGamificationRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}


			var newGame = new GamificationPoint
			{
				GamificationCode = HttpUtility.HtmlEncode(request.GameCode),
				Description = HttpUtility.HtmlEncode(request.Description),
				ApplyTo = request.ApplyTo,
				IncreasePoint = request.Point,
				RecStatus = request.RecStatus,
				RecCreatedBy = request.UpdatedBy,
				ValidFrom = request.ValidFrom,
				ValidTo = request.ValidTo
			};

			// validate game
			var validator = new GamificationValidator(_pointServiceApi.GamificationService);
			if (!validator.IsValid(newGame))
			{
				return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
			}

			_pointServiceApi.GamificationService.CreateGamification(newGame);
			return SendSuccessResponse();



		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/point/gamification")]
		public IHttpActionResult UpdateGamification(UpsertGamificationRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			return UpdateGamificationInfo(request);

		}

		[HttpDelete]
		//[AntiForgeryValidate]
		[Route("api/point/gamification")]
		public IHttpActionResult DeleteGamification(UpsertGamificationRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			return UpdateGamificationStatus(request, RecStatus.DELETE);

		}

		private IHttpActionResult UpdateGamificationInfo(UpsertGamificationRequest request)
		{

			var game = new GamificationPoint
			{
				GamificationPointId = request.GameId.Value,
				GamificationCode = request.GameCode,
				Description = request.Description,
				RecStatus = request.RecStatus,
				ApplyTo = request.ApplyTo,
				IncreasePoint = request.Point,
				RecModifiedBy = request.UpdatedBy,
				ValidFrom = request.ValidFrom,
				ValidTo = request.ValidTo
			};

			_pointServiceApi.GamificationService.UpdateGamificationPoint(game);
			return SendSuccessResponse();
		}

		private IHttpActionResult UpdateGamificationStatus(UpsertGamificationRequest request, RecStatus status)
		{
			if (request.GameId.HasValue == false)
				return SendFailedResponse("กรุณาระบุรหัสเกมที่ต้องการลบ");

			var game = new GamificationPoint
			{
				GamificationPointId = request.GameId.Value,
				RecStatus = status,
				RecModifiedBy = request.UpdatedBy,
			};
			_pointServiceApi.GamificationService.UpdateGamificationStatus(game);
			return SendSuccessResponse();
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/gamification/fetch")]
		public IHttpActionResult FetchGamification(GetGamificationRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.GamificationService.GetGamificationPoints(request.Code, request.ApplyTo,
				request.RecStatus, request.Pager.PageSize, request.Pager.PageNumber);
			var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/rule/fetch")]
		public IHttpActionResult FetchPointRule(PointRuleDto pointRuleDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = this._pointServiceApi.RuleService.FetchPointRule(pointRuleDto);
			var pager = new Pager(result.TotalItems, pointRuleDto.Pager.PageNumber, pointRuleDto.Pager.PageSize);

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/rule/upsert")]
		public IHttpActionResult UpSertPointRule(PointRuleInfoDto pointRuleInfoDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			//var validator = new PointRuleInfoValidator();
			//if (!validator.IsValid(pointRuleInfoDto))
			//{
			//    return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
			//}
			var result = _pointServiceApi.RuleService.UpsertPointRule(pointRuleInfoDto, pointRuleInfoDto.By);
			return SendSuccessResponse(result);
		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/point/rule/upsert")]
		public IHttpActionResult UpSertPointRulePut(PointRuleInfoDto pointRuleInfoDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			//var validator = new PointRuleInfoValidator();
			//if (!validator.IsValid(pointRuleInfoDto))
			//{
			//    return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
			//}

			var result = _pointServiceApi.RuleService.UpsertPointRule(pointRuleInfoDto, pointRuleInfoDto.By);
			return SendSuccessResponse(result);
		}

		[HttpPut]
		//[AntiForgeryValidate]
		[Route("api/point/rule/updatestatus")]
		public IHttpActionResult UpdatePointRuleStatusPUT(PointRuleUpdateStatusDto pointRuleUpdateStatusDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.RuleService.UpdatePointRuleStatus(pointRuleUpdateStatusDto, pointRuleUpdateStatusDto.UserId);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/rule/export")]
		public IHttpActionResult ExportPointRule(PointRuleDto pointRuleDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = this._pointServiceApi.RuleService.FetchPointRule(pointRuleDto);

			var bankTypes = _lookupService.GetBanks();
			var paymentTypes = _lookupService.GetPaymentType();
			var pointLevel = _lookupService.GetPointLevels();
			var cardType = _lookupService.GetCardTypes();

			var json = result
				.Select(x => new
				{
					RuleId = x.RuleId,
					Code = x.Code,
					Description = x.Description,
					PointLevel = pointLevel.FirstOrDefault(o => o.PointLevelId == x.PointLevelId)?.LevelName ?? "ทุกระดับ",
					PaymentType = paymentTypes.FirstOrDefault(o => o.Id == x.PaymentTypeId)?.Name ?? "ทุกทาง",
					CardType = cardType.FirstOrDefault(o => o.Id == x.CardTypeId)?.Name ?? "ทุกบัตร",
					BankType = bankTypes.FirstOrDefault(o => o.Id == x.BankId)?.Name ?? "ทุุกธนาคาร",
					CalculationType = GetCalculationType(x.CalculationType),
					ReceivedPoint = x.ReceivedPoint,
					Per = x.IsPerItem == true ? "Item" : "Order",
					ValidTo = x.ValidTo,
					ValidFrom = x.ValidFrom,
					RecStatus = x.RecStatus == model.Models.Enum.RecStatus.ACTIVE ? "Active" : "Inactive"
				})
				.ToList();
			return SendSuccessResponse(json);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/fetch")]
		public IHttpActionResult FetchPromotion(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<PromotionInfo> result = null;

			var enumerable = this._pointServiceApi.PromotionService.FetchPromotion(promotionDto);
			Pager pager = null;
			if (enumerable != null)
			{
				pager = new Pager(enumerable.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = enumerable.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/fetchrule")]
		public IHttpActionResult FetchRule(PromotionRuleDto promotionRuleDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var enumerable = this._pointServiceApi.PromotionService.FetchRuleInPromotion(promotionRuleDto);
			return SendSuccessResponse(enumerable);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/productInfo/fetch")]
		public IHttpActionResult FetchProductInfo(GetProductInfoRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var products = _pointServiceApi.ProductService.GetProductInfosWithBasicPoints(request.Code, request.Pager.PageSize, request.Pager.PageNumber);

			var pager = new Pager(products.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);

			return SendSuccessResponse(products, pager);
		}


		[HttpGet]
		[Route("api/lookup/productInfo")]
		public IHttpActionResult GetProductInfo(string query)
		{
			if (!string.IsNullOrEmpty(query))
			{
				var products = _lookupService.GetProductInfoFromName(query);

				return SendSuccessResponse(products);
			}

			return SendSuccessResponse();
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/lookup/fetchproductinfo")]
		public IHttpActionResult FetchProductInfo(SearchProductDto dto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}
			if (!string.IsNullOrEmpty(dto.Code))
			{
				var products = _lookupService.GetProductInfoFromView(dto.Code);

				return SendSuccessResponse(products);
			}
			return SendSuccessResponse();
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/productInfo/export")]
		public IHttpActionResult ExportProductInfo(GetProductInfoRequest dto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.ProductService.GetProductInfosWithBasicPoints(dto.Code, null, null);
			var json = result
				.Select(x => new
				{
					ProductInfo = x.Id,
					Detail = x.Name,
					PricePerUnit = x.UnitPrice,
					RegularPoint = x.RegularPoint,
					SilverPoint = x.SilverPoint,
					GoldPoint = x.GoldPoint,
					PlatinumPoint = x.PlatinumPoint
				})
				.ToList();
			return SendSuccessResponse(json);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/searchpromotion")]
		public IHttpActionResult SearchPromotion(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<PromotionInfo> result = null;

			var enumerable = this._pointServiceApi.PromotionService.FetchViewPromotion(promotionDto);
			Pager pager = null;
			if (enumerable != null)
			{
				pager = new Pager(enumerable.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = enumerable.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/searchcampaign")]
		public IHttpActionResult SearchCampaign(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<PromotionInfo> result = null;

			var enumerable = this._pointServiceApi.PromotionService.FetchViewCampaign(promotionDto);
			Pager pager = null;
			if (enumerable != null)
			{
				pager = new Pager(enumerable.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = enumerable.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/searchproductinpromotion")]
		public IHttpActionResult SearchProductInPromotion(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<ProductInfo> result = null;
			var productInfo = _pointServiceApi.PromotionService.FetchAllProductInPromotion(promotionDto);
			var ruleWithPromotionInfo = _pointServiceApi.PromotionService.FetchAllRuleInPromotion(promotionDto);

			var basicPointConfig = _lookupServiceApi.GetPointLevels()?.ToList();
            		var basicPointExpired = _lookupService.GetPointConfig()?.ExpireDate;
			var basicPointHasExpired = basicPointExpired.Value >= DateTime.Now;

			if (ruleWithPromotionInfo == null)
			{
				if (basicPointConfig != null &&
					basicPointConfig.Any() &&
					basicPointHasExpired)
				{
					var regularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					var silverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					var goldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					var platinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					productInfo.Select(p =>
					{
						p.RegularPoint = Convert.ToInt32(p.UnitPrice) / regularPoint;
						p.SilverPoint = Convert.ToInt32(p.UnitPrice) / silverPoint;
						p.GoldPoint = Convert.ToInt32(p.UnitPrice) / goldPoint;
						p.PlatinumPoint = Convert.ToInt32(p.UnitPrice) / platinumPoint;
						return p;
					}).ToList();
				}
			}

			if (ruleWithPromotionInfo != null &&
				ruleWithPromotionInfo.Any())
			{
				foreach (var r in ruleWithPromotionInfo)
				{
					foreach (var p in productInfo)
					{
						ApplyPoint(r, p);
					}
				}
			}

			Pager pager = null;
			if (productInfo != null)
			{
				pager = new Pager(productInfo.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = productInfo.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		private void ApplyPoint(PromotionRuleInfo r, ProductInfo p)
		{
			if (r.IsPerItem)
			{
				if (ValidateDate(r.RuleStartDate, r.RuleEndDate))
				{
					if ((PointLevelType)r.PointLevelId == PointLevelType.Regular)
					{
						p.RegularPoint = SetPoint(r, p.RegularPoint, Convert.ToInt32(p.UnitPrice));
					}
					if ((PointLevelType)r.PointLevelId == PointLevelType.Silver)
					{
						p.SilverPoint = SetPoint(r, p.SilverPoint, Convert.ToInt32(p.UnitPrice));
					}
					if ((PointLevelType)r.PointLevelId == PointLevelType.Gold)
					{
						p.GoldPoint = SetPoint(r, p.GoldPoint, Convert.ToInt32(p.UnitPrice));
					}
					if ((PointLevelType)r.PointLevelId == PointLevelType.Platinum)
					{
						p.PlatinumPoint = SetPoint(r, p.PlatinumPoint, Convert.ToInt32(p.UnitPrice));
					}
					if ((PointLevelType)r.PointLevelId == PointLevelType.All)
					{
						p.RegularPoint = SetPoint(r, p.RegularPoint, Convert.ToInt32(p.UnitPrice));
						p.SilverPoint = SetPoint(r, p.SilverPoint, Convert.ToInt32(p.UnitPrice));
						p.GoldPoint = SetPoint(r, p.GoldPoint, Convert.ToInt32(p.UnitPrice));
						p.PlatinumPoint = SetPoint(r, p.PlatinumPoint, Convert.ToInt32(p.UnitPrice));
					}
				}
			}
		}

		private bool ValidateDate(DateTime validFrom, DateTime validTo)
		{
			var valid = true;
			var currentDate = DateTime.Now;
			if (currentDate < validFrom || currentDate > validTo)
			{
				valid = false;
			}
			return valid;
		}

		private int SetPoint(PromotionRuleInfo r, int basePoint, int baseUnitPrice)
		{
			if (baseUnitPrice == 0)
			{
				return 0;
			}

			var point = 0;
			switch ((CalculationType)r.CalculationType)
			{
				case CalculationType.INCREASE_POINT:
					{
						point = IncreasePoint(basePoint, r.ReceivedPoint);
					}
					break;
				case CalculationType.MULTIPLE_POINT:
					{
						point = MultiplePoint(basePoint, r.ReceivedPoint);
					}
					break;
				case CalculationType.CHANGE_BASE_POINT:
					{
						point = ChangeBasePoint(Convert.ToInt32(baseUnitPrice), r.ReceivedPoint);
					}
					break;
			}
			return point;
		}

		private int IncreasePoint(int currentPoint, int incPoint)
		{
			return currentPoint + incPoint;
		}

		private int MultiplePoint(int currentPoint, int incPoint)
		{
			//if (currentPoint == 0)
			//    currentPoint = 1;

			return currentPoint * incPoint;
		}

		private int ChangeBasePoint(int productPrice, int unitPrice)
		{
			return (productPrice / unitPrice);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/searchpromotionincampaign")]
		public IHttpActionResult SearchPromotionInCampaign(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<PromotionInfo> result = null;
			var basicPointConfig = _lookupServiceApi.GetPointLevels()?.ToList();
			var productInfo = _pointServiceApi.PromotionService.FetchAllPromotionInCampaign(promotionDto);

			//if (basicPointConfig != null &&
			//    basicPointConfig.Any())
			//{
			//    var regularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
			//    var silverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
			//    var goldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
			//    var platinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
			//    productInfo.Select(p =>
			//    {
			//        p.RegularPoint = int.Parse(p.UnitPrice) / regularPoint;
			//        p.SilverPoint = int.Parse(p.UnitPrice) / silverPoint;
			//        p.GoldPoint = int.Parse(p.UnitPrice) / goldPoint;
			//        p.PlatinumPoint = int.Parse(p.UnitPrice) / platinumPoint;
			//        return p;
			//    }).ToList();
			//}

			Pager pager = null;
			if (productInfo != null)
			{
				pager = new Pager(productInfo.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = productInfo.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/addruleinpromotion")]
		public IHttpActionResult AddRuleInPromotion(PromotionRuleDto promotionRuleDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.PromotionService.AddRuleInPromotion(promotionRuleDto);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/searchruleinpromotion")]
		public IHttpActionResult SearchRuleInPromotion(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			IList<PromotionRuleInfo> result = null;
			var productInfo = _pointServiceApi.PromotionService.FetchAllRuleInPromotion(promotionDto);

			Pager pager = null;
			if (productInfo != null)
			{
				pager = new Pager(productInfo.Count(), promotionDto.Pager.PageNumber, promotionDto.Pager.PageSize);
				result = productInfo.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
			}

			return SendSuccessResponse(result, pager);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/updateruleinpromotion")]
		public IHttpActionResult UpdateRuleInPromotion(PromotionDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.PromotionService.RemoveRuleInPromotion(promotionDto);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/savecampaigndate")]
		public IHttpActionResult SaveCampaignDate(CampaignDto campaignDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.PromotionService.UpdateCampaignDate(campaignDto);
			return SendSuccessResponse(result);
		}

		private string GetCalculationType(int calculationType)
		{
			if (calculationType == 1)
			{
				return "เพิ่มคะแนน(แต้ม)";
			}
			else if (calculationType == 2)
			{
				return "เพิ่มเท่า(เท่า)";
			}
			else
			{
				return "เปลี่ยนฐานคะแนน(บาทต่อ 1 แต้ม)";
			}
		}

		[HttpPost]
		//[AntiForgeryValidate]
		[Route("api/point/promotion/updateruleorder")]
		public IHttpActionResult UpdateRuleOrder(PromotionRuleDto promotionDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var result = _pointServiceApi.PromotionService.UpdateRuleOrder(promotionDto);
			return SendSuccessResponse(result);
		}

		[HttpPost]
		[Route("api/point/uploadmemberpoint")]
		public IHttpActionResult UploadMemberPoint()
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			var httpRequest = HttpContext.Current.Request;
			if (httpRequest.Files.Count == 0)
			{
				return BadRequest();
			}

			var postedFile = httpRequest.Files[0];
			var filename = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["uploaded_file_path"] + postedFile.FileName);

			postedFile.SaveAs(filename);

			List<UploadMemberPoint> uploadMemberPoints = new List<UploadMemberPoint>();
			var fileInfo = new FileInfo(filename);
			using (var pkg = new ExcelPackage(fileInfo))
			{
				var workbook = pkg.Workbook;

				var worksheet = workbook.Worksheets.First();
				int totalRows = worksheet.Dimension.End.Row;


				for (int i = 2; i <= totalRows; i++)
				{
					string note = string.Empty;
					string memberCode = worksheet.Cells[i, 1].Text.ToString();
					int memberId = _memberService.GetMemberId(memberCode);

					var person = _memberService.GetMemberInfo(memberId);

					if (memberId == 0)
					{
						note += "ไม่พบรหัสสมาชิกที่ใส่มา" + Environment.NewLine;
					}

					int point = 0;
					if (!int.TryParse(worksheet.Cells[i, 2].Text.ToString(), out point))
					{
						note += "จำนวนคะแนนไม่ถูกต้อง" + Environment.NewLine;
					}

					int actionType = 0;
					if (!int.TryParse(worksheet.Cells[i, 3].Text.ToString(), out actionType))
					{
						note += "รูปแบบคะแนนไม่ถูกต้อง" + Environment.NewLine;
					}

					string actionTypeValue = string.Empty;
					switch (actionType)
					{
						case -1: actionTypeValue = "ลดคะแนน"; break;
						case 1: actionTypeValue = "เพิ่มคะแนน"; break;
						default: actionTypeValue = "ไม่ได้ระบุ"; break;
					}


					string remark = worksheet.Cells[i, 4].Text.ToString();

					uploadMemberPoints.Add(new UploadMemberPoint()
					{
						MemberCode = memberCode,
						MemberId = memberId,
						MemberName = person.Person.FullName,
						Point = point,
						ActionType = (ActionType)actionType,
						ActionTypeValue = actionTypeValue,
						Remark = remark,
						Note = note
					});
				}
			}
			fileInfo.Delete();

			return SendSuccessResponse(uploadMemberPoints);
		}

		[HttpPut]
		[Route("api/point/massmemberpointupdate")]
		public IHttpActionResult UpdateMemberPoint(UpdatePointRequest[] requests)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			string message = string.Empty;
			int countValid = 0;
			foreach(UpdatePointRequest request in requests)
			{
				var result = _memberPointService.InsertMemberPoints(request.PersonId, request.Points, request.UpdatedBy, request.ActionType, request.Remark);
				if (!result.IsValid)
				{
					message += result.ValidationMessage;
				}
				else
				{
					countValid++;
				}
			}
			
			if(countValid == requests.Length)
			{
				return SendSuccessResponse(null);
			}
			else
			{
				return SendFailedResponse(message);
			}
		}
	}
}
