﻿using System.Web;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.model.Models;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class LookupController : BaseApiController
    {
        private readonly ILookupService _lookupService;

        public LookupController(ILookupService lookupService, HttpContextBase contextBase) : base(contextBase)
        {
            _lookupService = lookupService;
        }

        //GET api/lookup/cities
        [ActionName("cities")]
        public IHttpActionResult GetCities()
        {
            return Ok(_lookupService.GetCities());
        }

        //GET api/lookup/titiles
        [ActionName("titles")]
        public IHttpActionResult GetTitles()
        {
            return Ok(_lookupService.GetTitles());
        }

        //GET api/lookup/questions
        [ActionName("questions")]
        public IHttpActionResult GetQuestions()
        {
            return Ok(_lookupService.GetQuestions());
        }

        //GET api/lookup/persontypes
        [ActionName("personTypes")]
        public IHttpActionResult GetPersonTypes()
        {
            return Ok(_lookupService.GetPersonTypes());
        }

        [ActionName("addressTypes")]
        public IHttpActionResult GetAddressTypes()
        {
            return Ok(_lookupService.GetAddressTypes());
        }

        //GET api/lookup/pointLevels
        [ActionName("pointLevels")]
        public IHttpActionResult GetPointLevels()
        {
            return Ok(_lookupService.GetPointLevels());
        }

        
    }
}
