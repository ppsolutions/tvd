﻿using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.Request;
using tvd.Services.Interfaces;
using tvd.Validator;

namespace tvd.Controllers.Api
{
    public class NewsController : BaseApiController
    {
        private readonly INewsService _newsService;
        private readonly INewsFactory _newsFactory;
        public NewsController(HttpContextBase httpContextBase, INewsService newsService
            , INewsFactory newsFactory) : base(httpContextBase)
        {
            _newsService = newsService;
            _newsFactory = newsFactory;
        }

        //POST
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/news/fetch")]
        public IHttpActionResult GetNews(GetNewsRequest request)
        {
            //var enumerable = _newsService.GetNews(request.NewsType, request.FilterType);
            var result = _newsService.GetNews(request.NewsType, request.FilterType, request.Pager.PageSize,
                request.Pager.PageNumber);
            var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);
            //var result = enumerable.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize);

            return SendSuccessResponse(result, pager);
        }

        //POST
        [HttpPost]
        [Route("api/news")]
        //[AntiForgeryValidate]
        public IHttpActionResult CreateNews(UpsertNewsRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var news = _newsFactory.CreateNews(request);

            // validate news
            var validator = new NewsValidator();
            if (!validator.IsValid(news))
            {
                return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
            }
            _newsService.InsertNews(news);
            return SendSuccessResponse();

        }

        //PUT
        [HttpPut]
        [Route("api/news")]
        //[AntiForgeryValidate]
        public IHttpActionResult UpdateNews(UpsertNewsRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var news = _newsFactory.CreateNews(request);
            // validate news
            var validator = new NewsValidator();
            if (!validator.IsValid(news))
            {
                return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
            }
            _newsService.UpdateNews(news);
            return SendSuccessResponse();

        }

        //DELETE
        [HttpDelete]
        [Route("api/news")]
        //[AntiForgeryValidate]
        public IHttpActionResult DeleteNews(DeleteNewsRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _newsService.DeleteNews(request.NewsIds, request.UpdatedBy);
            return SendSuccessResponse();
        }
    }
}

