﻿using System.Web;
using System.Web.Http;
using tvd.model.Models;

namespace tvd.Controllers.Api
{
    public abstract class BaseApiController : ApiController
    {
        public readonly HttpContextBase ContextBase;

        public BaseApiController(HttpContextBase httpContextBase)
        {
            this.ContextBase = httpContextBase;
        }

        public IHttpActionResult SendResponse(bool success, string url, object data, string message, Pager pager = null)
        {
            var response = new ResponseResult
            {
                Success = success,
                Message = message,
                ResponseData = data,
                ReturnUrl = url,
                Pager = pager
            };
            return Ok(response);
        }

        public IHttpActionResult SendSuccessResponse(object data, Pager pager)
        {
            return SendResponse(true, "", data, "", pager);
        }

        public IHttpActionResult SendSuccessResponse(string url, object data, Pager pager)
        {
            return SendResponse(true, url, data, "", pager);
        }

        public IHttpActionResult SendSuccessResponse(object data)
        {
            return SendResponse(true, "", data, "");
        }

        public IHttpActionResult SendSuccessResponse(string url = "", object data = null, string message = "")
        {
            return SendResponse(true, url, data, message);
        }

        public IHttpActionResult SendFailedResponse(string url = "", object data = null, string message = "")
        {
            return SendResponse(false, url, data, message);
        }

        public IHttpActionResult SendFailedResponse(string message)
        {
            return SendResponse(false, null, null, message);
        }

    }
}