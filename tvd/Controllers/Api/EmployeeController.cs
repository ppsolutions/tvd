﻿using System.Web;
using tvd.Services.Interfaces;
using System.Web.Http;
using tvd.model.Models.Employee;
using tvd.model.Models;
using System.Linq;
using System.Web.Helpers;
using tvd.Controllers.Api.Attributes;
using tvd.Factory.Interfaces;
using tvd.model.Models.Enum;

namespace tvd.Controllers.Api
{
    public class EmployeeController : BaseApiController
    {
        private readonly IEmployeeService _employeeService;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IRoleService _roleService;
        public EmployeeController(IRoleService roleService, IEmployeeService employeeService, IUrlGeneratorService urlGeneratorService, HttpContextBase contextBase) : base(contextBase)
        {
            _employeeService = employeeService;
            _urlGeneratorService = urlGeneratorService;
            _roleService = roleService;
        }

        [HttpGet]
        [Route("api/employee")]
        public IHttpActionResult GetEmployeeWithoutRole(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var employee = _employeeService.GetEmployee(id);
            if (employee == null)
            {
                return SendFailedResponse("ไม่ค้นพบพนักงานในระบบ");
            }
            return employee.EmployeeInRole?.Role != null ?
                SendFailedResponse("มีพนักงานคนนี้อยู่ในระบบแล้ว") :
                SendSuccessResponse(employee);
        }


        [HttpPost]
        //[AntiForgeryValidate]
        [Route("api/employee/role")]
        public IHttpActionResult EditEmployeeRole(EmployeeUpSertDto request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var emp = _employeeService.GetEmployee(request.EmployeeCode);
            if (emp == null)
                return SendFailedResponse("ไม่ค้นพบพนักงานในระบบ");

            _roleService.AssignRoleToUser(request.EmployeeCode, request.RoleId, request.UpdatedBy);

            return SendSuccessResponse(null);
        }

        [HttpDelete]
        //[AntiForgeryValidate]
        [Route("api/employee/role")]
        public IHttpActionResult DeleteEmployeeRole(EmployeeUpSertDto request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _roleService.RemoveRoleFromUser(request.EmployeeCode, request.UpdatedBy);
            return SendSuccessResponse(null);
        }

        [HttpPost]
        [Route("api/employee/fetch")]
        public IHttpActionResult GetEmployees(EmployeeDto request)
        {
            var result = _employeeService.GetEmployees(request.Keyword, request.RoleId, request.Pager.PageSize, request.Pager.PageNumber);
            var pager = new Pager(result.TotalItems, request.Pager.PageNumber, request.Pager.PageSize);
            var url = _urlGeneratorService.GetSettingsEmployeeDetailPageUrl("");
            return SendSuccessResponse(url, result, pager);
        }
    }
}
