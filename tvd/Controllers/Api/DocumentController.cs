﻿using System.Web.Http;
using tvd.model.Helpers;

namespace tvd.Controllers.Api
{
    public class DocumentController : ApiController
    {
        [HttpGet]
        [Route("api/document/encryptPassword")]
        public IHttpActionResult EncryptPassword(string password)
        {
            return Ok(PasswordHasher.EncryptString(password));
        }

        [HttpGet]
        [Route("api/document/decryptPassword")]
        public IHttpActionResult DecryptPassword(string password)
        {
            return Ok(PasswordHasher.DecryptString(password));
        }
    }
}
