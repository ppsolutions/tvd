﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.Enum;
using tvd.Services;
using tvd.Services.Interfaces;
using tvd.Validator;

namespace tvd.Controllers.Api
{
    public class RegisterController : BaseApiController
    {
        private readonly IMemberService _memberService;
        private readonly IRegisterFactory _registerFactory;
        private readonly IUrlGeneratorService _urlGeneratorService;

        public RegisterController(HttpContextBase contextBase,
            IMemberService memberService,
            IRegisterFactory registerFactory,
            IUrlGeneratorService generatorService) : base(contextBase)
        {
            _memberService = memberService;
            _registerFactory = registerFactory;
            _urlGeneratorService = generatorService;
        }

        //POST api/register/create
        [HttpPost]
        public IHttpActionResult Create(RegisterRequest dto)
        {
            //TODO: pass creatd by in request
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = dto.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);

            var person = _registerFactory.CreatePerson(dto);
            var addressGroup = _registerFactory.CreateAddressGroup(dto);
            var questions = _registerFactory.CreatePersonQuestions(dto);

            //check user is exist
            if (IsContactNumberAlreadyRegis(person.ContactNumber))
            {
                return SendFailedResponse("อีเมลล์นี้มีอยู่ในระบบแล้ว");
            }

            // validate person
            var validator = new PersonValidator();
            if (!validator.IsValid(person))
            {
                return SendFailedResponse(validator.GetBrokenRulesInHtmlFormat());
            }

			string[] datas = null;
			if (!string.IsNullOrEmpty(dto.SelectedMergeMembers))
			{
				datas = dto.SelectedMergeMembers.Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
			}

			var personId = _memberService.CreateMember(person, addressGroup, questions, datas, dto.UpdatedBy);

			return SendSuccessResponse(_urlGeneratorService.GetMemberDetail(personId), null,
                "ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ");
        }

        private bool IsContactNumberAlreadyRegis(string email)
        {
            //Check email address in Db
            var personFromDb = _memberService.GetPersonFromContactNumber(email);
            return personFromDb != null;
        }

		private MemberMapping[] GetMemberMapping(string[] datas)
		{
			List<MemberMapping> memberMappings = new List<MemberMapping>();
			

			return memberMappings.ToArray();
		}
	}
}