﻿using System.Web;
using System.Web.Http;
using tvd.Factory.Interfaces;
using tvd.model.Models.Enum;
using tvd.model.Models.Request;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class MemberController : BaseApiController
    {
        private readonly IMemberService _memberService;
        private readonly IPersonQuestionService _questionService;
        private readonly IUpdateMemberFactory _memberFactory;
		private readonly IMemberPointService _memberPointService;

		public MemberController(IMemberService memberService
								, IPersonQuestionService questionService
								, IUpdateMemberFactory memberFactory
								, IMemberPointService memberPointService
								, HttpContextBase contextBase) : base(contextBase)
        {
            _memberService = memberService;
            _questionService = questionService;
            _memberFactory = memberFactory;
			_memberPointService = memberPointService;
        }

		//POST api/member/merge
		[HttpPost]
		[Route("api/member/merge")]
		public IHttpActionResult MergeCustomer(MergeCustomerRequest request)
		{
			_memberService.MergeCustomer(request.CustomerBase, request.MergeCustomerIds);
			return SendSuccessResponse(null);
		}

		//GET api/member/detail
		[HttpGet]
		[Route("api/member/detail")]
		public IHttpActionResult GetMemberDetail(int personId, Source source)
		{
			//Get person detail
			var member = _memberService.GetMemberDetail(personId, source);
			return SendSuccessResponse(member);
		}

		//PUT api/member/status
		[HttpPut]
        [Route("api/member/status")]
        public IHttpActionResult statusPerson(UpdatePersonStatusRequest request)
        {
			if(request.RecStatus == -1)
			{
				_memberService.DeleteUser(request.PersonId, request.UpdatedBy);
			}
			else if(request.RecStatus == 0)
			{
				_memberService.BlockUser(request.PersonId, request.UpdatedBy);
			}
			else if(request.RecStatus == 1)
			{
				_memberService.ActiveUser(request.PersonId, request.UpdatedBy);
			}
             
            return SendSuccessResponse(null);
        }
	
		//DELETE api/member/delete
		[HttpDelete]
        [Route("api/member/delete")]
        public IHttpActionResult DeactivatePerson(UpdatePersonStatusRequest request)
        {
            _memberService.DeleteUser(request.PersonId, request.UpdatedBy);
            return SendSuccessResponse(null);
        }

        [HttpPut]
		[Route("api/member/basicInfo")]
		public IHttpActionResult UpdateBasicInformtion(UpdateMemberRequest request)
        {
            var basicInfo = _memberFactory.CreatePerson(request);
            var result = _memberService.UpdateBasicInfo(basicInfo);
            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }

        //PUT api/member/questions
        [HttpPut]
		[Route("api/member/questions")]
		public IHttpActionResult UpdateQuestions(UpdateQuestionRequest request)
        {
            var question = _memberFactory.CreatePersonQuestions(request);
            var result = _memberService.UpdateQuestions(question);
            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }

        [HttpPut]
		[Route("api/member/password")]
		public IHttpActionResult UpdatePassword(UpdatePasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var result =_memberService.UpdatePassword(request.PersonId, request.OldPassword, request.NewPassword,
                request.UpdatedBy);

            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }

        [HttpPut]
		[Route("api/member/address")]
		public IHttpActionResult UpdateAddress(UpdateAddressRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }


            var addressGroup = _memberFactory.CreateAddressGroup(request);

            var result = _memberService.UpdateAddress(addressGroup);
            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }

		[HttpPut]
		[Route("api/member/point")]
		public IHttpActionResult UpdatePoint(UpdatePointRequest request)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}
			request.ActionType = (ActionType)request.ActionTypeValue;
			var result = _memberPointService.InsertMemberPoints(request.PersonId, request.Points, request.UpdatedBy, request.ActionType, request.Remark);
			return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
		}
    }
}
