﻿using System;
using System.Collections.Generic;
using System.Linq;
using tvd.model.Models.Point;
using tvd.model.Repositories.Interfaces;
using tvd.model.V;

namespace tvd.Controllers.Api.Services
{
    public class PromotionService : IPromotionService
    {
        private readonly IPointRepository pointRepository;

        public PromotionService(IPointRepository pointRepository)
        {
            this.pointRepository = pointRepository;
        }

        public int AddRuleInPromotion(PromotionRuleDto promotionRuleDto)
        {
            return pointRepository.AddRuleInPromotion(promotionRuleDto);
        }

        public IList<PromotionRuleInfo> FetchAllRuleInPromotion(PromotionDto promotionDto)
        {
            var ruleInPromo = pointRepository.FetchAllRuleInPromotion(promotionDto);
            if(ruleInPromo != null && ruleInPromo.Any())
            {

            }
            return ruleInPromo;
        }

        public IList<ProductInfo> FetchAllProductInPromotion(PromotionDto promotionDto)
        {
            return pointRepository.FetchAllProductInPromotion(promotionDto);
        }

        public IList<PromotionInfo> FetchViewPromotion(PromotionDto promotionDto)
        {
            return pointRepository.FetchViewPromotion(promotionDto);
        }

        public IList<PromotionInfo> FetchViewCampaign(PromotionDto promotionDto)
        {
            return pointRepository.FetchViewCampaign(promotionDto);
        }

        public int RemoveRuleInPromotion(PromotionDto promotionDto)
        {
            return pointRepository.RemoveRuleInPromotion(promotionDto);
        }

        public IList<PromotionInfo> FetchPromotion(PromotionDto promotionDto)
        {
            return pointRepository.FetchPromotion(promotionDto);
        }

        public IList<PromotionRuleInfo> FetchRuleInPromotion(PromotionRuleDto promotionRuleDto)
        {
            return pointRepository.FetchRuleInPromotion(promotionRuleDto);
        }

        public IList<PromotionInfo> FetchAllPromotionInCampaign(PromotionDto promotionDto)
        {
            return pointRepository.FetchAllPromotionInCampaign(promotionDto);
        }

        public int UpdateCampaignDate(CampaignDto campaignDto)
        {
            return pointRepository.UpdateCampaignDate(campaignDto);
        }

        public int UpdateRuleOrder(PromotionRuleDto promotionRuleDto)
        {
            return pointRepository.UpdateRuleOrder(promotionRuleDto);
        }
    }
}