﻿using tvd.Services.Interfaces;

namespace tvd.Controllers.Api.Services
{
    public interface IPointServiceSet
    {
        IRewardService RewardService { get; }
        IRuleService RuleService { get; }
        IPromotionService PromotionService { get; }
        IGamificationService GamificationService { get; }
        IProductService ProductService { get; }
    }
}