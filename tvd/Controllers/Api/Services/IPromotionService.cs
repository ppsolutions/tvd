﻿using System.Collections.Generic;
using tvd.model.Models.Point;
using tvd.model.V;

namespace tvd.Controllers.Api.Services
{
    public interface IPromotionService
    {
        IList<PromotionInfo> FetchPromotion(PromotionDto promotionDto);
        IList<PromotionInfo> FetchViewPromotion(PromotionDto promotionDto);
        IList<ProductInfo> FetchAllProductInPromotion(PromotionDto promotionDto);
        IList<PromotionRuleInfo> FetchAllRuleInPromotion(PromotionDto promotionDto);
        int AddRuleInPromotion(PromotionRuleDto promotionRuleDto);
        int RemoveRuleInPromotion(PromotionDto promotionDto);
        IList<PromotionRuleInfo> FetchRuleInPromotion(PromotionRuleDto promotionRuleDto);
        IList<PromotionInfo> FetchViewCampaign(PromotionDto promotionDto);
        IList<PromotionInfo> FetchAllPromotionInCampaign(PromotionDto promotionDto);
        int UpdateCampaignDate(CampaignDto campaignDto);
        int UpdateRuleOrder(PromotionRuleDto promotionRuleDto);
    }
}