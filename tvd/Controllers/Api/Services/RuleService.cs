﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Point;
using tvd.model.Repositories.Interfaces;

namespace tvd.Controllers.Api.Services
{
    public class RuleService : IRuleService
    {
        private readonly IPointRepository pointRepository;

        public RuleService(IPointRepository pointRepository)
        {
            this.pointRepository = pointRepository;
        }

        public ResultSet<PointRuleInfo> FetchPointRule(PointRuleDto pointRuleDto)
        {
            return this.pointRepository.FetchPointRule(pointRuleDto);
        }

        public int UpdatePointRuleStatus(PointRuleUpdateStatusDto pointRuleUpdateStatusDto, string by)
        {
            return this.pointRepository.UpdatePointRuleStatus(pointRuleUpdateStatusDto, by);
        }

        public PointRuleInfo UpsertPointRule(PointRuleInfoDto pointRuleInfoDto, string by)
        {
            return this.pointRepository.UpsertPointRule(pointRuleInfoDto, by);
        }
    }
}