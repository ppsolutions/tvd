﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Point;

namespace tvd.Controllers.Api.Services
{
    public interface IRewardService
    {
        ResultSet<RewardInfo> FetchRewards(RewardDto rewardDto);
        RewardInfo UpsertReward(RewardInfoDto rewardInfoDto, string by);
        int UpdateRewardStatus(RewardUpdateStatusDto rewardUpdateStatusDto, string by);
        IList<RewardInfo> FetchAllReward();
    }
}