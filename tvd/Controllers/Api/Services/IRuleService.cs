﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Point;

namespace tvd.Controllers.Api.Services
{
    public interface IRuleService
    {
        ResultSet<PointRuleInfo> FetchPointRule(PointRuleDto pointRuleDto);
        PointRuleInfo UpsertPointRule(PointRuleInfoDto pointRuleInfoDto, string by);
        int UpdatePointRuleStatus(PointRuleUpdateStatusDto pointRuleUpdateStatusDto, string by);
    }
}