﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;

namespace tvd.Controllers.Api.Services
{
    public interface ILookupServiceApi
    {
        IEnumerable<PointLevel> GetPointLevels();
    }
}
