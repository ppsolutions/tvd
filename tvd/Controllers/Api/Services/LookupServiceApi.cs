﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Repositories;

namespace tvd.Controllers.Api.Services
{
    public class LookupServiceApi : ILookupServiceApi
    {
        private readonly ILookupRepository _lookupRepository;

        public LookupServiceApi(ILookupRepository lookupRepository)
        {
            _lookupRepository = lookupRepository;
        }

        public IEnumerable<PointLevel> GetPointLevels()
        {
            return _lookupRepository.GetPointLevels();
        }
    }
}