﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Point;
using tvd.model.Repositories.Interfaces;

namespace tvd.Controllers.Api.Services
{
    public class RewardService : IRewardService
    {
        private readonly IPointRepository pointRepository;

        public RewardService(IPointRepository pointRepository)
        {
            this.pointRepository = pointRepository;
        }

        public IList<RewardInfo> FetchAllReward()
        {
            return this.pointRepository.FetchAllReward();
        }

        public ResultSet<RewardInfo> FetchRewards(RewardDto rewardDto)
        {
            return this.pointRepository.FetchRewards(rewardDto);
        }

        public int UpdateRewardStatus(RewardUpdateStatusDto rewardUpdateStatusDto, string by)
        {
            return this.pointRepository.UpdateRewardStatus(rewardUpdateStatusDto, by);
        }

        public RewardInfo UpsertReward(RewardInfoDto rewardInfoDto, string by)
        {
            return this.pointRepository.UpsertReward(rewardInfoDto, by);
        }
    }
}