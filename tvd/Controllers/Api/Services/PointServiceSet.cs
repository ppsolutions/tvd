﻿using System;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api.Services
{
    public class PointServiceSet : IPointServiceSet
    {
        public IRewardService RewardService { get; private set; }
        public IRuleService RuleService { get; private set; }
        public IPromotionService PromotionService { get; private set; }

        public IGamificationService GamificationService { get; private set; }

        public IProductService ProductService { get; private set; }

        public PointServiceSet(IRewardService rewardService, IRuleService ruleService, IPromotionService promotionService, IGamificationService gamificaitionService, IProductService productService)
        {
            this.RewardService = rewardService;
            this.RuleService = ruleService;
            this.PromotionService = promotionService;
            this.GamificationService = gamificaitionService;
            this.ProductService = productService;
        }
    }
}