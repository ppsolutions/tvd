﻿using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using tvd.model.Models.Request;
using tvd.model.Provider;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class AuthenController : BaseApiController
    {
        private readonly IEmployeeService _employeeService;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IContextProvider _contextProvider;

        public AuthenController(HttpContextBase contextBase,
            IEmployeeService employeeService,
            IUrlGeneratorService urlGeneratorService,
            IContextProvider contextProvider) : base(contextBase)
        {
            _employeeService = employeeService;
            _urlGeneratorService = urlGeneratorService;
            _contextProvider = contextProvider;
        }
        
        [HttpPost]
        [Route("api/employee/login")]
        public IHttpActionResult Login(LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = request.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);

            var employee = _employeeService.Authenticate(request.UserName, request.Password);
            if (employee != null)
            {
				_employeeService.SetCookie(_contextProvider, employee, request.IsRemember);
            }
                
            return SendSuccessResponse(_urlGeneratorService.GetSearchUrl());
        }

        [HttpGet]
        public IHttpActionResult Logout()
        {
            _employeeService.Logout(_contextProvider);

            return SendSuccessResponse(_urlGeneratorService.GetLoginUrl());
        }
    }
}