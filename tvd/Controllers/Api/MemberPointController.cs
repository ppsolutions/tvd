﻿using System.Web;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Request;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class MemberPointController : BaseApiController
    {
        private readonly IMemberPointService _pointService;
        public MemberPointController(HttpContextBase httpContextBase, IMemberPointService pointService) : base(httpContextBase)
        {
            _pointService = pointService;
        }

        //POST api/member/points
        [HttpPost]
        //[AntiForgeryValidate]
        [Route("api/member/points")]
        public IHttpActionResult UpdateMemberPoints(UpdatePointRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = _pointService.InsertMemberPoints(request.PersonId, request.Points, request.UpdatedBy, request.ActionType, request.Remark);
            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }


        [HttpPost]
        //[AntiForgeryValidate]
        [Route("api/member/pointLevel")]
        public IHttpActionResult UpdateMemberPointLevel(UpdatePointRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = _pointService.UpdateMemberPointLevel(request.PersonId, request.PointLevelId, request.Points, request.UpdatedBy);
            return result.IsValid ? SendSuccessResponse(null) : SendFailedResponse(result.ValidationMessage);
        }

        [HttpPost]
        //[AntiForgeryValidate]
        [Route("api/member/redeempoint")]
        public IHttpActionResult RedeemPoint(RedeemPointRequest[] requests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
			int successCount = 0;
			string validateMessage = string.Empty;
			foreach(RedeemPointRequest request in requests)
			{
				UpdateResult result = _pointService.InsertRedeemPointTransaction(request.RedeemPointId, request.PersonId, request.Type, request.UpdatedBy);
				if(result.IsValid)
				{
					successCount++;
				}
				else
				{
					validateMessage += $"{request.Description} - {result.ValidationMessage} <br />";
				}

			}

            return successCount == requests.Length ? SendSuccessResponse(null) : SendFailedResponse(validateMessage);
        }


    }
}
