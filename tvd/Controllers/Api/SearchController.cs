﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using tvd.Helper;
using tvd.model.Models;
using tvd.model.Models.Request;
using tvd.model.Models.UserData;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class SearchController : BaseApiController
    {
        private readonly IMemberService _memberService;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IUserData _userData;
        public SearchController(HttpContextBase httpContextBase, IMemberService memberService, IUrlGeneratorService urlGeneratorService, IUserData userData) : base(httpContextBase)
        {
           _memberService = memberService;
            _urlGeneratorService = urlGeneratorService;
            _userData = userData;
        }

        //POST  api/search/memberName
        [HttpPost]
        [ActionName("memberName")]
        public IHttpActionResult FetchAutoCompleteByName(SearchRequest dto)
        {
           var nameList = _memberService.GetPersonName(dto.SearchName);
            var queryString =
                QueryStringBuilder.BuildSearchResultQuery(dto.SearchName, dto.SearchIdCard, dto.SearchPhone, dto.SearchMember);
            return SendSuccessResponse(_urlGeneratorService.GetSearchResultUrl(queryString), nameList);
        }

        //POST  api/search/memberUrl
        [HttpPost]
        [ActionName("memberUrl")]
        public IHttpActionResult SearchMemberUrl(SearchRequest dto)
        {
            var user = _memberService.GetMemberInfoList(dto.SearchName, dto.SearchIdCard, dto.SearchPhone, dto.SearchMember).ToList();
            var urlString = "";
            // If api found only one user, redirect to member detail
            if (user.Count == 1)
            {
				var selectedUser = user.First().Person;
                urlString = _urlGeneratorService.GetMemberDetail(selectedUser.PersonId == 0 ? Convert.ToInt32(selectedUser.ReferenceId) : selectedUser.PersonId, selectedUser.Source);
            }
            else
            {
                var queryString =
                    QueryStringBuilder.BuildSearchResultQuery(dto.SearchName, dto.SearchIdCard, dto.SearchPhone, dto.SearchMember);
                urlString = _urlGeneratorService.GetSearchResultUrl(queryString);
            }
            return SendSuccessResponse(urlString);
        }

        //POST api/search/members
        [HttpPost]
        [ActionName("members")]
        public IHttpActionResult SearchMembers(SearchRequest dto)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(dto.SearchMember) &&
                    string.IsNullOrEmpty(dto.SearchName) &&
					string.IsNullOrEmpty(dto.SearchIdCard) &&
					string.IsNullOrEmpty(dto.SearchPhone))
                {
                    return SendSuccessResponse("", new Pager(0, 1, dto.Pager.PageSize));
                }

                var merberInfoList = _memberService.GetMemberInfoList(dto.SearchName, dto.SearchIdCard, dto.SearchPhone, dto.SearchMember, dto.Pager.PageSize, dto.Pager.PageNumber);
                if (merberInfoList.Any())
                {
                    var pager = new Pager(merberInfoList.TotalItems, dto.Pager.PageNumber, dto.Pager.PageSize);
                    return SendSuccessResponse(merberInfoList, pager);
                }
                else
                {
                    return SendFailedResponse("", null, "ไม่ค้นพบสมาชิกตามเงื่อนไข");
                }
            }
            return BadRequest();
        }

    }
}
