﻿using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using tvd.Controllers.Api.Attributes;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.Request;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.Controllers.Api
{
    public class RoleController : BaseApiController
    {
        private readonly IRoleService _roleService;
        private readonly IRoleFactory _roleFactory;
        private readonly ILookupService _lookupService;
        public RoleController(HttpContextBase httpContextBase, IRoleService roleService, ILookupService lookupService,
            IRoleFactory roleFactory) : base(httpContextBase)
        {
            _roleService = roleService;
            _lookupService = lookupService;
            _roleFactory = roleFactory;
        }

        //POST api/roles
        [HttpPost]
        [Route("api/roles")]
        public IHttpActionResult GetRoles(GetRolesRequest request)
        {
            var enumerable = _lookupService.GetRoles();
            var pager = new Pager(enumerable.Count(), request.Pager.PageNumber, request.Pager.PageSize);
            var result = enumerable.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize);

            return SendSuccessResponse(result, pager);
        }

        //POST api/role
        [HttpPost]
        [Route("api/role")]
        public IHttpActionResult CreateRole(UpsertRolePermissionRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = request.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);

            var rolePermission = _roleFactory.CreateRolePermission(request);
            _roleService.InsertRoleAndPermission(rolePermission);

            if (rolePermission != null &&
                rolePermission.Role != null &&
                request.AccessControl != null &&
                request.AccessControl.Any())
            {
                _roleService.AssignAccessControlToRole(rolePermission.Role.RoleId, request.AccessControl, request.UpdatedBy);
            }

            return SendSuccessResponse();

        }

        //DELETE api/role
        [HttpDelete]
        [Route("api/role")]
        public IHttpActionResult DeleteRole(UpdateRoleRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = request.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);

            var role = _roleFactory.CreateRole(request);
            _roleService.DeleteRole(role);
            return SendSuccessResponse();
        }



        //POST api/role/permission
        [HttpPost]
        [Route("api/role/permission")]
        public IHttpActionResult EditRolePermission(UpsertRolePermissionRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var token = request.Token.Split(':');
            var cookiesToken = token[0];
            var formToken = token[1];
            AntiForgery.Validate(cookiesToken, formToken);

            var rolePermission = _roleFactory.CreateRolePermission(request);
            _roleService.UpdateRolePermisson(rolePermission);

            if (rolePermission != null &&
                rolePermission.Role != null &&
                request.AccessControl != null &&
                request.AccessControl.Any())
            {
                _roleService.AssignAccessControlToRole(rolePermission.Role.RoleId, request.AccessControl, request.UpdatedBy);
            }

            return SendSuccessResponse();
        }

    }
}
