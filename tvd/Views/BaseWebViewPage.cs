﻿using System;
using System.Web.Helpers;
using System.Web.Mvc;
using tvd.Http;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.UserData;
using tvd.model.Provider;

namespace tvd.Views
{
    public class BaseWebViewPage<T> : WebViewPage<T>
    {
        private IContextProvider ContextData;
        private IUserData UserData;

        public override void InitHelpers()
        {
            base.InitHelpers();
            ContextData = DependencyResolver.Current.GetService<IContextProvider>();
            UserData = DependencyResolver.Current.GetService<IUserData>();
        }

        public override void Execute()
        {

        }

        public Employee UserInfo
        {
            get
            {
                return UserData.UserInfo;
            }
        }

        public string TokenHeaderValue
        {
            get
            {
                string cookieToken, formToken;
                AntiForgery.GetTokens(null, out cookieToken, out formToken);
                return cookieToken + ":" + formToken;
            }
        }

        public string MachineName
        {
            get
            {
                return Environment.MachineName;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return ContextData.IsAuthenticated();
            }
        }

        public bool IsLoginPage
        {
            get
            {
                return ContextData.GetContext().Request.RequestContext.RouteData.Values["controller"].ToString().Equals("login", StringComparison.OrdinalIgnoreCase);
            }
        }

        public IRequestInfo RequestInfo
        {
            get
            {
                return ContextData.GetContext()?.Items["IRequestInfo"] as IRequestInfo ?? null;
            }
        }
    }

    public class BaseWebViewPage : BaseWebViewPage<Object>
    {

    }
}