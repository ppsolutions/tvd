﻿/*eslint-disable import/default*/
import 'babel-polyfill';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/toastr/build/toastr.min.css';
import '../src/styles/font-awesome.css';
import '../src/styles/index.css';
import '../src/styles/sidebar.css';
import '../src/styles/style.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import './dependency/index.js';
import '../src/components/plugin/handlebar-helpers';

