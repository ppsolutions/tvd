
$(document).ready(function () {

    $(".side-left-menu-block-wrapper").click(function () {
        $(this).find("li").removeClass("active");
    });

    function openNav() {
        document.getElementById("mySidenav").style.width = "80%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    $(".head-side-menu").click(function () {
        $(this).toggleClass("active");
        $(this).closest("div").next("").find("ul").slideToggle();
    });

    $(".btn-side-left-menu").click(function () {
        $(this).find(".head-side-menu-mobile").toggleClass("display-hide");
    });

    $('.btn-side-left-menu').click(function () {
        $(this).find(".head-side-menu-mobile").toggleClass("display-hide");
    });

    $(".head-side-menu-none").click(function () {
        $(this).closest("div").toggleClass("active");
    });

    $(".infomobile-toggle").click(function (e) {
        $(".side-info-mobile").slideToggle();
        $(".side-notification-mobile").hide();
    });

    $(".notification-toggle").click(function () {
        $(".side-notification-mobile").slideToggle();
        $(".side-info-mobile").hide();
    });

    $('#tab1 a').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr('href') + '"]').tab('show');
    });

    $('.sub-side-menu li a[data-toggle="tab"]').on('click', function (e) {
        e.preventDefault();
        window.location = $(e.target).attr('href');
        return false;
    });
});