import '../../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker';
import '../../../node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css';
import Bacon from 'baconjs';
import Register from './register';
import RegisterService from './register.Service';
import toastr from 'toastr';
import './register.css';

$(document).ready(function(){
    $('#datetimepicker1 input').datepicker({
		multidate: false,
		format: 'dd/mm/yyyy',
		todayHighlight: true,
		autoclose:true
	});

    const options = {
        registerSubmit: '#register',
        radioPerson: '#radioPerson',
        radioCorporate: '#radioCorporate',
        tabPerson: '#tabPerson',
        tabCorporate: '#tabCorporate',

        personTitle: '#personTitle',
        firstName: '#firstName',
        lastName: '#lastName',
		dob: '#dob',
		checkboxNotSpecificYear: '#chk-not-specific-year',
        phone: '#phone',
        email: '#email',
        corporateTitle: '#corporateTitle',
        corporateName: '#corporateName',
        corporatePhone: '#corporatePhone',
        corporateEmail: '#corporateEmail',

        currentAddress: '#currentAddress',
        currentAddressProvince: '#currentAddressProvince',
        currentAddressPostalCode: '#currentAddressPostalCode',

        shipmentUseCurrentAddress: '#shipmentUseCurrentAddress',
        shipmentUseNewAddress: '#shipmentUseNewAddress',
        shipmentAddress: '#shipmentAddress',
        shipmentProvince: '#shipmentProvince',
        shipmentPostalCode: '#shipmentPostalCode',

        invoiceUseCurrentAddress: '#invoiceUseCurrentAddress',
        invoiceUseShipmentAddress: '#invoiceUseShipmentAddress',
		invoiceUseNewsAddress: '#invoiceUseNewAddress',
        invoiceAddress: '#invoiceAddress',
        invoiceProvince: '#invoiceProvince',
        invoicePostalCode: '#invoicePostalCode',

        password: '#password',
        repeatPassword: '#repeatPassword',

        firstQuestion: '#firstQuestion',
        firstAnswer: '#firstAnswer',
        secondQuestion: '#secondQuestion',
        secondAnswer: '#secondAnswer',

        updatedBy: '#hidden-routeinfo'
    };

    const registerForm = new Register(options);
    const service = new RegisterService();

    registerForm.on("submit", function () {
        if(this.valid()) {
            const callback = function (data) {
                if (data.Success) {
                    registerForm.reset();
					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
					setTimeout(function () {
						window.location.href = '/SearchResult/Index';
					}, 500);
                }
                else {
                    let errorMsgFromServer = data.Message;
                    if (errorMsgFromServer.length == 0) {
                        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
                    } else {
                        toastr.error(errorMsgFromServer, 'Oop!');                       
                    }
                }
            };

            const failure = function (xhr, textstatus) {
                let errorMsgFromServer = xhr.responseText;
                if (errorMsgFromServer.length == 0) {
                    toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
                } else {
                    toastr.error(errorMsgFromServer, 'Oop!');
                }
            };

            let dto = {
                selectedPerson: this.radioPerson.prop("checked"),
                selectedCorporate: this.radioCorporate.prop("checked"),

                personTitle: this.personTitle.val(),
                firstName: this.firstName.val().trim(),
                lastName: this.lastName.val().trim(),
				dob: this.dob.val(),
				isSpecificYear: this.checkboxNotSpecificYear.prop('checked'),
                phone: this.phone.val().trim(),
                email: this.email.val().trim(),

                corporateTitle: this.corporateTitle.val(),
                corporateName: this.corporateName.val().trim(),
                corporatePhone: this.corporatePhone.val().trim(),
                corporateEmail: this.corporateEmail.val().trim(),

                currentAddress: this.currentAddress.val().trim(),
                currentAddressProvince: this.currentAddressProvince.val(),
                currentAddressPostalCode: this.currentAddressPostalCode.val().trim(),

                shipmentUseCurrentAddress: this.shipmentUseCurrentAddress.prop("checked"),
                shipmentUseNewAddress: this.shipmentUseNewAddress.prop("checked"),
                shipmentAddress: this.shipmentAddress.val().trim(),
                shipmentProvince: this.shipmentProvince.val(),
                shipmentPostalCode: this.shipmentPostalCode.val().trim(),

                invoiceUseCurrentAddress: this.invoiceUseCurrentAddress.prop("checked"),
                invoiceUseShipmentAddress: this.invoiceUseShipmentAddress.prop("checked"),
                invoiceUseNewAddress: this.invoiceUseNewAddress.prop("checked"),
                invoiceAddress: this.invoiceAddress.val().trim(),
                invoiceProvince: this.invoiceProvince.val(),
                invoicePostalCode: this.invoicePostalCode.val().trim(),

                password: this.password.val().trim(),

                firstQuestion: this.firstQuestion.val(),
                firstAnswer: this.firstAnswer.val().trim(),
				secondQuestion: this.secondQuestion.val() === '' ? 0 : this.secondQuestion.val(),
                secondAnswer: this.secondAnswer.val().trim(),

                updatedBy: this.updatedBy.data().empId,
				selectedMergeMembers: $("input[name='selectedMergeMembers']").size() > 0 ? $("input[name='selectedMergeMembers']").val() : '',					
                token: $("input[name='requestVerificationToken']").val()
            };
            
            service.create(dto, callback, failure);
        }
        else {
            toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
        }
    });

});