﻿var $ = require('jquery');
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../shared/validator';

class Register {
    constructor(options) {
        EventEmitter.call(this);

        this.validator = new Validator();
        this.submit = $(options.registerSubmit);
        this.radioPerson = $(options.radioPerson);
        this.radioCorporate = $(options.radioCorporate);
        this.tabPerson = $(options.tabPerson);
        this.tabCorporate = $(options.tabCorporate);

        this.personTitle = $(options.personTitle);
        this.firstName = $(options.firstName);
        this.lastName = $(options.lastName);
		this.dob = $(options.dob);
		this.checkboxNotSpecificYear = $(options.checkboxNotSpecificYear);
        this.phone = $(options.phone);
        this.email = $(options.email);

        this.corporateTitle = $(options.corporateTitle);
        this.corporateName = $(options.corporateName);
        this.corporatePhone = $(options.corporatePhone);
        this.corporateEmail = $(options.corporateEmail);

        this.currentAddress = $(options.currentAddress);
        this.currentAddressProvince = $(options.currentAddressProvince);
        this.currentAddressPostalCode = $(options.currentAddressPostalCode);

        this.shipmentUseCurrentAddress = $(options.shipmentUseCurrentAddress);
        this.shipmentUseNewAddress = $(options.shipmentUseNewAddress);
        this.shipmentAddress = $(options.shipmentAddress);
        this.shipmentProvince = $(options.shipmentProvince);
        this.shipmentPostalCode = $(options.shipmentPostalCode);

        this.invoiceUseCurrentAddress = $(options.invoiceUseCurrentAddress);
		this.invoiceUseShipmentAddress = $(options.invoiceUseShipmentAddress);
		this.invoiceUseNewAddress = $(options.invoiceUseNewAddress);
        this.invoiceAddress = $(options.invoiceAddress);
        this.invoiceProvince = $(options.invoiceProvince);
        this.invoicePostalCode = $(options.invoicePostalCode);

        this.password = $(options.password);
        this.repeatPassword = $(options.repeatPassword);
        this.firstQuestion = $(options.firstQuestion);
        this.firstAnswer = $(options.firstAnswer);
        this.secondQuestion = $(options.secondQuestion);
        this.secondAnswer = $(options.secondAnswer);
        this.updatedBy = $(options.updatedBy);

        eventBinding(this);
    }
}

inherits(Register, EventEmitter);

function eventBinding(register) {
    const seft = register;

    seft.submit.on("click", function () {
        seft.emit('submit');
    });

    seft.radioPerson.on("click", function(){
        seft.tabPerson.show();
        seft.tabCorporate.hide();
    });

    seft.radioCorporate.on("click", function(){
        seft.tabPerson.hide();
        seft.tabCorporate.show();
    });

    seft.firstName.on("keyup", function (e) {
        if(seft.validator.name(seft.firstName.val()).valid){
            seft.firstName.attr("style", "border-color:#ccc");
        }
        else {
            seft.firstName.attr("style", "border-color:red");
        }
    });

    seft.lastName.on("keyup", function (e) {
        if(seft.validator.name(seft.lastName.val()).valid){
            seft.lastName.attr("style", "border-color:#ccc");
        }
        else {
            seft.lastName.attr("style", "border-color:red");
        }
    });

    seft.dob.on("keydown", function (e) {
        return false;
    });

    seft.phone.on("keyup", function (e) {
        if(seft.validator.phone(seft.phone.val()).valid){
            seft.phone.attr("style", "border-color:#ccc");
        }
        else {
            seft.phone.attr("style", "border-color:red");
        }
    });

    seft.email.on("keyup", function (e) {
        if(seft.validator.email(seft.email.val()).valid){
            seft.email.attr("style", "border-color:#ccc");
        }
        else {
            seft.email.attr("style", "border-color:red");
        }
    });

    seft.corporateName.on("keyup", function (e) {
        if(seft.validator.empty(seft.corporateName.val()).valid){
            seft.corporateName.attr("style", "border-color:#ccc");
        }
        else {
            seft.corporateName.attr("style", "border-color:red");
        }
    });

    seft.corporatePhone.on("keyup", function (e) {
        if(seft.validator.phone(seft.corporatePhone.val()).valid){
            seft.corporatePhone.attr("style", "border-color:#ccc");
        }
        else {
            seft.corporatePhone.attr("style", "border-color:red");
        }
    });

    seft.corporateEmail.on("keyup", function (e) {
        if(seft.validator.email(seft.corporateEmail.val()).valid){
            seft.corporateEmail.attr("style", "border-color:#ccc");
        }
        else {
            seft.corporateEmail.attr("style", "border-color:red");
        }
    });

    seft.currentAddress.on("keyup", function (e) {
        if(seft.validator.empty(seft.currentAddress.val()).valid){
			seft.currentAddress.attr("style", "border-color:#ccc");
        }
        else {
            seft.currentAddress.attr("style", "border-color:red");
		}

		if (seft.shipmentUseCurrentAddress.prop('checked')) {
			seft.shipmentAddress.val(seft.currentAddress.val());
		}

		if (seft.invoiceUseCurrentAddress.prop('checked')) {
			seft.invoiceAddress.val(seft.currentAddress.val());
		}
	});

	seft.currentAddressProvince.on("change", function () {
		if (seft.currentAddressProvince.val() === "") {
			seft.currentAddressProvince.attr("style", "border-color:red");
		}
		else {
			seft.currentAddressProvince.attr("style", "border-color:#ccc");
		}

		if (seft.shipmentUseCurrentAddress.prop('checked')) {
			seft.shipmentProvince.val(seft.currentAddressProvince.val());
		}

		if (seft.invoiceUseCurrentAddress.prop('checked')) {
			seft.invoiceProvince.val(seft.currentAddressProvince.val());
		}
	});

    seft.currentAddressPostalCode.on("keyup", function (e) {
        if(seft.validator.empty(seft.currentAddressPostalCode.val()).valid){
            seft.currentAddressPostalCode.attr("style", "border-color:#ccc");
        }
        else {
            seft.currentAddressPostalCode.attr("style", "border-color:red");
		}

		if (seft.shipmentUseCurrentAddress.prop('checked')) {
			seft.shipmentPostalCode.val(seft.currentAddressPostalCode.val());
		}

		if (seft.invoiceUseCurrentAddress.prop('checked')) {
			seft.invoicePostalCode.val(seft.currentAddressPostalCode.val());
		}
    });

	seft.shipmentUseCurrentAddress.on('click', function () {
		seft.shipmentAddress.attr('disabled', 'disabled');
		seft.shipmentProvince.attr('disabled', 'disabled');
		seft.shipmentPostalCode.attr('disabled', 'disabled');

		seft.shipmentAddress.val(seft.currentAddress.val());
		seft.shipmentProvince.val(seft.currentAddressProvince.val());
		seft.shipmentPostalCode.val(seft.currentAddressPostalCode.val());
	});

	seft.shipmentUseNewAddress.on('click', function () {
		seft.shipmentAddress.removeAttr('disabled');
		seft.shipmentProvince.removeAttr('disabled');
		seft.shipmentPostalCode.removeAttr('disabled');
	});	

    seft.shipmentAddress.on("keyup", function (e) {
        if(seft.validator.empty(seft.shipmentAddress.val()).valid){
            seft.shipmentAddress.attr("style", "border-color:#ccc");
        }
        else {
            seft.shipmentAddress.attr("style", "border-color:red");
		}

		if (seft.invoiceUseShipmentAddress.prop('checked')) {
			seft.invoiceAddress.val(seft.shipmentAddress.val());
		}
	});

	seft.shipmentProvince.on("change", function () {
		if (seft.shipmentProvince.val() === "") {
			seft.shipmentProvince.attr("style", "border-color:red");
		}
		else {
			seft.shipmentProvince.attr("style", "border-color:#ccc");
		}

		if (seft.invoiceUseShipmentAddress.prop('checked')) {
			seft.invoiceProvince.val(seft.shipmentProvince.val());
		}
	});

    seft.shipmentPostalCode.on("keyup", function (e) {
        if(seft.validator.empty(seft.shipmentPostalCode.val()).valid){
            seft.shipmentPostalCode.attr("style", "border-color:#ccc");
        }
        else {
            seft.shipmentPostalCode.attr("style", "border-color:red");
		}

		if (seft.invoiceUseShipmentAddress.prop('checked')) {
			seft.invoicePostalCode.val(seft.shipmentPostalCode.val());
		}
    });

	seft.invoiceUseCurrentAddress.on('click', function () {
		seft.invoiceAddress.attr('disabled', 'disabled');
		seft.invoiceProvince.attr('disabled', 'disabled');
		seft.invoicePostalCode.attr('disabled', 'disabled');

		seft.invoiceAddress.val(seft.currentAddress.val());
		seft.invoiceProvince.val(seft.currentAddressProvince.val());
		seft.invoicePostalCode.val(seft.currentAddressPostalCode.val());
	});

	seft.invoiceUseShipmentAddress.on('click', function () {
		seft.invoiceAddress.attr('disabled', 'disabled');
		seft.invoiceProvince.attr('disabled', 'disabled');
		seft.invoicePostalCode.attr('disabled', 'disabled');

		seft.invoiceAddress.val(seft.shipmentAddress.val());
		seft.invoiceProvince.val(seft.shipmentProvince.val());
		seft.invoicePostalCode.val(seft.shipmentPostalCode.val());
	});

	$('#invoiceUseNewAddress').on('click', function () {
		seft.invoiceAddress.removeAttr('disabled');
		seft.invoiceProvince.removeAttr('disabled');
		seft.invoicePostalCode.removeAttr('disabled');
	});

    seft.invoiceAddress.on("keyup", function (e) {
        if(seft.validator.empty(seft.invoiceAddress.val()).valid){
            seft.invoiceAddress.attr("style", "border-color:#ccc");
        }
        else {
            seft.invoiceAddress.attr("style", "border-color:red");
        }
    });

	seft.invoiceProvince.on("change", function () {
		if (seft.invoiceProvince.val() === "") {
			seft.invoiceProvince.attr("style", "border-color:red");
		}
		else {
			seft.invoiceProvince.attr("style", "border-color:#ccc");
		}
	});

    seft.invoicePostalCode.on("keyup", function (e) {
        if(seft.validator.empty(seft.invoicePostalCode.val()).valid){
            seft.invoicePostalCode.attr("style", "border-color:#ccc");
        }
        else {
            seft.invoicePostalCode.attr("style", "border-color:red");
        }
    });

    seft.password.on("keyup", function (e) {
        if(seft.validator.empty(seft.password.val()).valid){
            seft.password.attr("style", "border-color:#ccc");
        }
        else {
            seft.password.attr("style", "border-color:red");
        }
    });

    seft.repeatPassword.on("keyup", function (e) {
        if(seft.validator.empty(seft.repeatPassword.val()).valid){
            seft.repeatPassword.attr("style", "border-color:#ccc");
        }
        else {
            seft.repeatPassword.attr("style", "border-color:red");
        }
    });

    seft.firstAnswer.on("keyup", function (e) {
        if(seft.validator.empty(seft.firstAnswer.val()).valid){
            seft.firstAnswer.attr("style", "border-color:#ccc");
        }
        else {
            seft.firstAnswer.attr("style", "border-color:red");
        }
    });

    seft.secondAnswer.on("keyup", function (e) {
        if(seft.validator.empty(seft.secondAnswer.val()).valid){
            seft.secondAnswer.attr("style", "border-color:#ccc");
        }
        else {
            seft.secondAnswer.attr("style", "border-color:red");
        }
    });

}

Register.prototype.valid = function () {
    this.resetError();

    if(this.radioPerson.prop("checked")) {
        this.validateName(this.firstName.val(), this.firstName);
        this.validateName(this.lastName.val(), this.lastName);
        this.validateDOB(this.dob.val(), this.dob);
        this.validatePhone(this.phone.val(), this.phone);
        this.validateEmail(this.email.val(), this.email);
    }
    else {
        this.validateName(this.corporateName.val(), this.corporateName);
        this.validatePhone(this.corporatePhone.val(), this.corporatePhone);
        this.validateEmail(this.corporateEmail.val(), this.corporateEmail);
    }

    this.validateEmpty(this.currentAddress.val(), this.currentAddress);
    this.validateEmpty(this.currentAddressPostalCode.val(), this.currentAddressPostalCode);

    if(this.shipmentUseNewAddress.prop("checked")) {
        this.validateEmpty(this.shipmentAddress.val(), this.shipmentAddress);
        this.validateEmpty(this.shipmentPostalCode.val(), this.shipmentPostalCode);
    }
    else {
        this.shipmentAddress.val(this.currentAddress.val());
        this.shipmentPostalCode.val(this.currentAddressPostalCode.val());
    }

    if(this.invoiceUseNewAddress.prop("checked")) {
        this.validateEmpty(this.invoiceAddress.val(), this.invoiceAddress);
        this.validateEmpty(this.invoicePostalCode.val(), this.invoicePostalCode);
    }
    else {
        if(this.invoiceUseCurrentAddress.prop("checked")) {
            this.invoiceAddress.val(this.currentAddress.val());
            this.invoicePostalCode.val(this.currentAddressPostalCode.val());
        }
        if(this.invoiceUseShipmentAddress.prop("checked")) {
            this.invoiceAddress.val(this.shipmentAddress.val());
            this.invoicePostalCode.val(this.shipmentPostalCode.val());
        }
    }

    this.validatePassword(this.password.val(), this.password);
    this.validatePassword(this.repeatPassword.val(), this.repeatPassword);
    this.validateBothPassword(this.password.val(), this.repeatPassword.val(), this.password);

	
	this.validateEmpty(this.firstQuestion.val(), this.firstQuestion);
	this.validateEmpty(this.firstAnswer.val(), this.firstAnswer);

	if (this.secondQuestion.val() !== '') {
		this.validateEmpty(this.secondQuestion.val(), this.secondQuestion);
		this.validateEmpty(this.secondAnswer.val(), this.secondAnswer);
		this.validateShoudnotSame(this.firstQuestion.val(), this.secondQuestion.val(), this.secondQuestion);
	}
	
    var errorCount = this.validator.basket.length;
    if (errorCount > 0) 
    {
        //Hilight elements
        for (var i = 0; i < errorCount; i++) {
            var element = this.validator.basket[i];
            element.attr("style", "border-color:red");
        }
    }
    return (errorCount === 0);
}

Register.prototype.resetError = function() {
    for (var i = 0; i < this.validator.basket.length; i++) {
        var element = this.validator.basket[i];
        element.removeAttr("style", "border-color:red");
    }
    this.validator.basket = [];
}

Register.prototype.reset = function() {
    this.resetError();
    this.radioPerson.prop("checked", true);
    this.tabPerson.show();
    this.tabCorporate.hide();
    this.personTitle.val(0);
    this.firstName.val("");
    this.lastName.val("");
    this.dob.val("");
    this.phone.val("");
    this.email.val("");
    this.corporateTitle.val(0);
    this.corporateName.val("");
    this.corporatePhone.val("");
    this.corporateEmail.val("");
    this.currentAddress.val("");
    this.currentAddressProvince.val(0);
    this.currentAddressPostalCode.val("");
    this.shipmentUseCurrentAddress.prop("checked", false);
    this.shipmentUseNewAddress.prop("checked", false);
    this.shipmentAddress.val("");
    this.shipmentProvince.val(0);
    this.shipmentPostalCode.val("");
    this.invoiceUseCurrentAddress.prop("checked", false);
    this.invoiceUseShipmentAddress.prop("checked", false);
    this.invoiceUseNewAddress.prop("checked", false);
    this.invoiceAddress.val("");
    this.invoiceProvince.val(0);
    this.invoicePostalCode.val("");
    this.password.val("");
    this.repeatPassword.val("");
    this.firstAnswer.val("");
    this.secondAnswer.val("");
    this.firstQuestion.val(0);
    this.secondQuestion.val();
}

Register.prototype.validateName = function (str, element) {
    return this.validator.name(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validateDOB = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validatePhone = function (str, element) {
    return this.validator.phone(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validateEmail = function (str, element) {
    return this.validator.email(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validateEmpty = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validatePassword = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

Register.prototype.validateBothPassword = function (password, cpassword, element) {
    if (password !== cpassword) {
        this.validator.basket.push(element);
        return false;
    }
    return true;
}

Register.prototype.validateShoudnotSame = function(srcvalue, tarvalue, element) {
    if (srcvalue == tarvalue) {
        this.validator.basket.push(element);
        return false;
    }
}

export default Register;