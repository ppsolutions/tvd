﻿import $ from 'jquery';

export default class RegisterService {
    constructor() {
        const location = 'http://' + window.location.host;
		this.registerUrl = location + '/api/register/create';
    }

    create(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.registerUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}