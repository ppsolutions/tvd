import toastr from 'toastr';
import SearchResult from './searchResult';
import '../shared/tvd.custom.css';


$(document).ready(function(){
    const options = {
        element: "#search-data",
        searchButton: "#search-button-2",
		searchName: "#search-name",
		searchIdCard: '#search-id-card',
        searchPhone: '#search-phone',
		searchMember: '#search-member',
		mergeButton: '#merge-button',
		newButton: '#new-button',
		pager: {
			defaultPageSize: 15,
			defaultPageNumber: 1
		},
    };
    
	const searchResult = new SearchResult(options);
	if(searchResult && searchResult.element){

		const dto = {
			SearchName: searchResult.textSearch.val(),
			SearchIdCard: searchResult.textIdCard.val(),
			SearchPhone: searchResult.textPhone.val(),
			SearchMember: searchResult.textMember.val(),
			pager: {
				pageSize: options.pager.defaultPageSize,
				pageNumber: options.pager.defaultPageNumber
			}
		};

		searchResult.fetch(dto);

		searchResult.on("searchClick", function () {
			const dto = {
				SearchName: searchResult.textSearch.val(),
				SearchIdCard: searchResult.textIdCard.val(),
				SearchPhone: searchResult.textPhone.val(),
				SearchMember: searchResult.textMember.val(),
				pager: {
					pageSize: options.pager.defaultPageSize,
					pageNumber: options.pager.defaultPageNumber
				}
			};
			this.fetch(dto);
		});
	}
});
