﻿import SearchResultTemplate from './searchResult.hbs';
import PagerTemplate from '../shared/pager.hbs';
import toastr from 'toastr';

var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import DomSelectorValidator from '../shared/domSelectorValidator';
class SearchResult {
	constructor(options) {
		if (!validateElement($(options.element)).valid) return;
		EventEmitter.call(this);
		this.template = SearchResultTemplate;
		this.pagerTemplate = PagerTemplate;
		this.element = $(options.element);
		this.searchButton = $(options.searchButton);
		this.mergeButton = $(options.mergeButton);
		this.newButtion = $(options.newButton);
		this.textSearch = $(options.searchName);
		this.textPhone = $(options.searchPhone);
		this.textMember = $(options.searchMember);
		this.textIdCard = $(options.searchIdCard);
		this.options = $.extend(true, options, {
			pager: {
				id: "#search-result-pager"
			}
		});
		this.pager = $(this.options.pager.id);
		eventBinding(this);
	}
}

function validateElement(opt) {
	var selectorValidator = new DomSelectorValidator(opt);
	var valid = selectorValidator.validate();

	return valid;
}

inherits(SearchResult, EventEmitter);

function eventBinding(searchResult) {
	searchResult.searchButton.on("click", function () {
		searchResult.emit("searchClick");
	});

	$(searchResult.options.pager.id).on("click", "ul#paging > li > a", function () {
		const dto = {
			SearchName: searchResult.textSearch.val(),
			SearchPhone: searchResult.textPhone.val(),
			SearchMember: searchResult.textMember.val(),
			pager: {
				pageSize: searchResult.options.pager.defaultPageSize,
				pageNumber: $(this).data().pn
			}
		};
		searchResult.fetch(dto);
	});

	searchResult.mergeButton.on("click", function () {
		searchResult.mergeMemberFromList();
	});

	searchResult.newButtion.on("click", function () {
		searchResult.newMemberFromList();
	});
}

function getDataFromTable(selector) {
	let items = [];
	$(selector).each(function () {
		var obj = {
			Id: 0,
			Source: parseInt($(this).attr('data-source')),
			CurrentPoint: parseInt($(this).attr('data-current-point')),
		};

		if ($(this).attr('data-person-id') === undefined ||
			$(this).attr('data-person-id') === '' ||
			$(this).attr('data-person-id') === '0') {
			obj.Id = $(this).attr('data-reference-id');
		}
		else {
			obj.Id = $(this).attr('data-person-id');
		}
		items.push(obj);
	});

	return items;
}

function generateQuery(items) {
	let query = '';
	if (items.length > 0) {
		query = '?m=';
		$.each(items, function (i, data) {
			query += data.Id + '_' + data.Source + '_' + data.CurrentPoint;
			if (i != items.length - 1) {
				query += ',';
			}
		});
	}

	return query;
}

SearchResult.prototype.fetch = function (dto) {

	const location = 'http://' + window.location.host;
	const searchUrl = location + '/api/search/members';
	const searchResult = this;
	$.ajax({
		type: "POST",
		url: searchUrl,
		data: JSON.stringify(dto),
		xhrFields: { withCredentials: true },
		contentType: "application/json",
		dataType: "json",
		success: function (data) {
			if (!data.Success) {
				toastr.error(data.Message, 'Opp!');
				//clear data
				$("#search-data").html('');

			} else {
				searchResult.renderTemplateWithData(data, data.Pager);

				$('.tooltips').popover({
					title: 'ดูข้อมูล'
					, trigger: 'hover'
					, delay: { show: 0, hide: 2000 }
					, content: function () {
						var template = '<p>' + $(this).attr('data-fullname') + '</p>';
						template += '<p>' + $(this).attr('data-birthdate') + '</p>';
						template += '<p>โทรศัทพ์:' + $(this).attr('data-cotactnumber') + '</p>';
						template += '<p>สมัครโดย:' + $(this).attr('data-sourcestring') + '</p>';
						template += '<p>';
						template += '<button class="btn" onclick="window.location.href=' + $(this).attr('data-link') + '">ดูข้อมูล</button >';
						template += '</p>';
						return template;
					}
					, placement: 'bottom'
					, html: true
				});
			}
		},
		error: function (xhr, status) {
			toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
		}
	});
}

SearchResult.prototype.renderTemplateWithData = function (data, pager) {

	const result = {
		items: data.ResponseData,
	};

	const compiledHtml = this.template(result);
	this.element.html(compiledHtml);

	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
}

SearchResult.prototype.newMemberFromList = function () {
	const location = 'http://' + window.location.host;
	const newUrl = location + '/register/index';

	if ($('#search-data tr td input[type="checkbox"]:checked').size() > 1) {
		toastr.error('ไม่สามารถเลือกสมาชิกเกิน 1คน สำหรับสร้างสมาชิกใหม่', 'ไม่สามารถสร้างสมาชิกใหม่ได้');
		return;
	}
	let items = getDataFromTable('#search-data tr td input[type="checkbox"]:checked');
	let query = generateQuery(items);

	window.location.href = newUrl + query;
}

SearchResult.prototype.mergeMemberFromList = function () {
	const location = 'http://' + window.location.host;
	const mergeUrl = location + '/register/merge';
	if ($('#search-data tr td input[type="checkbox"]:checked').size() == 0) {
		toastr.error('กรุณาเลือกรายการสมาชิกก่อนกดปุ่ม รวมสมาชิกซ้ำซ้อน', 'ไม่สามารถรวมสมาชิกซ้ำซ้อนได้');
		return;
	}

	let items = getDataFromTable('#search-data tr td input[type="checkbox"]:checked');

	let canMerge = true;
	let currentSource = null;
	$.each(items, function (i, data) {
		if (currentSource === null) {
			currentSource = data.Source;
		}
		else {
			if (currentSource !== data.Source) {
				canMerge = false;
				return;
			}
			currentSource = data.Source;
		}
	});

	if (canMerge === true) {
		let query = generateQuery(items);
		window.location.href = mergeUrl + query;
	}
	else {
		toastr.error('ไม่สามารถรวมสมาชิกที่สมัครโดย Member Website กับสมัครโดยช่องทางอื่นๆ', 'ไม่สามารถรวมสมาชิกซ้ำซ้อนได้');
	}
}

export default SearchResult;
