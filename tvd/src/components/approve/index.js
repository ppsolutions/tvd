﻿import Approve from './approve';
import toastr from 'toastr';

$(document).ready(function (){

    if($("#hidden-routeinfo").data() == undefined)
        return;
    
    const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
    const by = $("#hidden-routeinfo").data().empId;
    const token = $("input[name='requestVerificationToken']").val();
   

   


    //Approve new member / update member
    if (pageTypeId === 33 || pageTypeId === 34 || pageTypeId === 35 || pageTypeId === 36 || pageTypeId === 37 || pageTypeId === 38|| pageTypeId === 39) {

        let approveType = $("#appove-type-id").val();
        let pager = {
            pageSize: 15,
            pageNumber: 1
        };

        const options = {
            element: "#pending-result",
            selectedAll: "#selected-all",
            approveAll: "#approve-all",
            clearAll: "#clear-all",
            rejectAll: "#reject-all",
            recStatus: '#filter-dd',
            approveType: approveType,
            updatedBy : by,
            pager: {
                id: "#member-pager",
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            token : token,
            pageTypeId : pageTypeId
        };

        const approve = new Approve(options);
        approve.fetch(pager);

    } else if (pageTypeId === 27) {
        //Approve auto setting
        const options = {
            pager: {
                id: "#member-pager",
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            element: "#pending-result",
            isNewMember: '#newmember-checkbox',
            isUpdateMember: '#updatemember-checkbox',
            isCancelMember: '#cancelmember-checkbox',
            isChangePoint: '#changepoint-checkbox',
            isRedeemPoint: '#redeempoint-checkbox',
            isPointLevel: '#pointlevel-checkbox',
            memberRetension: '#delete-in-period-member',
            pointRetension: '#delete-in-period-point',
            approveSettingId: '#approve-setting-id',
            updateBtn: '#update-button',
            updatedBy: by,
            token: token,
            pageTypeId: pageTypeId
        };

        const approve = new Approve(options);

    }

});