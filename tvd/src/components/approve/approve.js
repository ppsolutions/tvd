const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import ApproveService from './approve.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import PendingMemberTemplate from './pendingMember.hbs';
import UpdateMemberTemplate from './updateMember.hbs';
import UpdatePointTemplate from './updatePoint.hbs';
import UpgradePointLevelTemplate from './upgradePointLevel.hbs';
import RedeemPointTemplate from './redeemPoint.hbs';

class Approve {
    constructor(options) {
        EventEmitter.call(this);
       
        this.service = new ApproveService();
        this.pageTypeId = options.pageTypeId;
        this.pendingMemberTemplate = PendingMemberTemplate;
        this.updateMemberTemplate = UpdateMemberTemplate;
        this.updatePointTemplate = UpdatePointTemplate;
        this.upgradePointLevelTemplate = UpgradePointLevelTemplate;
        this.redeemPointTemplate = RedeemPointTemplate;
        this.pagerTemplate = PagerTemplate;
        this.element = $(options.element);
        this.selectedAll = $(options.selectedAll);
        this.isNewMember = $(options.isNewMember);
        this.isUpdateMember = $(options.isUpdateMember);
        this.isCancelMember = $(options.isCancelMember);
        this.updateBtn = $(options.updateBtn);
        this.isChangePoint =  $(options.isChangePoint);
        this.isRedeemPoint = $(options.isRedeemPoint);
        this.isPointLevel = $(options.isPointLevel);
        this.memberRetension = $(options.memberRetension);
        this.pointRetension = $(options.pointRetension);
        this.approveSettingId = $(options.approveSettingId);
        this.approveType = options.approveType;
        this.recStatus = $(options.recStatus);
        this.approveAll = $(options.approveAll);
        this.rejectAll = $(options.rejectAll);
        this.clearAll = $(options.clearAll);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.token = options.token;
        this.by = options.updatedBy;

        eventBinding(this);
    }
}

inherits(Approve, EventEmitter);

function eventBinding(from) {
    const seft  = from;
    $(seft.updateBtn).on("click",
        function() {
            let dto = {
                ApproveSettingId: seft.approveSettingId.val(),
                IsAutoApproveNewMembership: seft.isNewMember.prop( "checked" ),
                IsAutoApproveChangeMembershipInfo: seft.isUpdateMember.prop( "checked" ),
                IsAutoApproveCancelMembership: seft.isCancelMember.prop( "checked" ),
                IsAutoApproveChangeMembershipPoint: seft.isChangePoint.prop( "checked" ),
                IsAutoApproveRewardRedeemMembership: seft.isRedeemPoint.prop( "checked" ),
                IsAutoApproveLevelUpMembership: seft.isPointLevel.prop( "checked" ),
                PointRetensionPeriod: parseInt(seft.pointRetension.val()),
                MembershipRetensionPeriod: parseInt(seft.memberRetension.val()),
                updatedBy: seft.by,
                token: seft.token
            };

            seft.editAutoSetting(dto);
        });

    $(seft.selectedAll).on("click", function () {
        var val = $(this).is(':checked');
        if (val) {
            $(seft.element).find("input[type=checkbox]").prop("checked", val);
        }
        else {
            $(seft.element).find("input[type=checkbox]").prop("checked", val);
        }
    });

    $(seft.approveAll).on("click", function() {

            var val = [];
            $(".checkbox:checked").each(function(i){
                val[i] = $(this).data().id;
            });
            const dto = {
                approveIds: val,
                token:seft.token,
                updatedBy: seft.by
            };

            seft.approve(dto);

        });

    $(seft.rejectAll).on("click", function() {
            if (confirm('ยืนยันการทำรายการ')) {
                var val = [];
                $(".checkbox:checked").each(function(i) {
                    val[i] = $(this).data().id;
                });

                const dto = {
                    approveIds: val,
                    type: approveType,
                    token:seft.token,
                    updatedBy: seft.by
                };

                seft.rejectApprove(dto);

            }

    });

    $(seft.clearAll).on("click", function() {
        $(seft.recStatus).val(0).trigger('change');
        seft.reloadSelectedCount();
    });

    $(seft.recStatus).change(function(){
        var pager = {
            pageSize: 15,
            pageNumber: 1
        };
        seft.fetch(pager);          
    });

    $("input[type='checkbox']").on("click", function() {
        seft.reloadSelectedCount();
    });

    $(".card-block").on("click", "#pending-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        if(id) {
            const dto = {
                approveIds: [id],
                token:seft.token,
                updatedBy: seft.by
            };
            seft.approve(dto);
        }
    });

    $(".card-block").on("click", "#pending-result > tr > td > a[name=delete]", function(){

        if(confirm('ยืนยันการทำรายการ')) {
           
            const id = $(this).data().id;
            if(id) {              
                const dto = {
                    approveIds: [id],
                    token:seft.token,
                    type: seft.approveType,
                    updatedBy: seft.by
                };

                seft.rejectApprove(dto);
            }
        }
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        var pager = {
            pageSize: seft.defaultPageSize,
            pageNumber: $(this).data().pn
        };
        seft.fetch(pager);  
    });

}


Approve.prototype.reloadSelectedCount = function() {
    var numberOfChecked = "(" + $(".checkbox:checked").size() + ")";
    $(".approve-num").text(numberOfChecked);
}

Approve.prototype.editAutoSetting = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.edit(dto, callback, failure);
}

Approve.prototype.approve = function(dto){
    var seft = this;

    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
            var pager = {
                pageSize: 15,
                pageNumber: 1
            };
            seft.fetch(pager);
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr, 'Oop!');
    };

    seft.service.approve(dto, callback, failure);

}

Approve.prototype.rejectApprove = function(dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');          
            var pager = {
                pageSize: 15,
                pageNumber: 1
            };
            seft.fetch(pager);
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr, 'Oop!');
    };

    seft.service.rejectApprove(dto, callback, failure);
}

Approve.prototype.fetch = function(pager) {
    
    const seft = this;

    let dto = {
        approveType: seft.approveType,
        recStatus: seft.recStatus.val() === "" ? null : seft.recStatus.val(),
        pager: pager
    }
    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
            seft.reloadSelectedCount();
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

Approve.prototype.renderTemplateWithData = function (data, pager) {
    
    const location = 'http://' + window.location.host;
    const result = {
        items: data,
        baseUrl: location
    };

    Object.keys(result.items).forEach(function(key,index) {
        result.items[index].ObjectValue = JSON.parse(result.items[index].ObjectValue);
    });

    if(this.pageTypeId === 34){
        this.element.html(this.updateMemberTemplate(result));
    }
    else if (this.pageTypeId === 37) {
        this.element.html(this.updatePointTemplate(result));
    }
    else if (this.pageTypeId === 38) {
        this.element.html(this.redeemPointTemplate(result));
    } else if (this.pageTypeId === 39) {
        this.element.html(this.upgradePointLevelTemplate(result));
    } else {
        this.element.html(this.pendingMemberTemplate(result));
    }
    //event binding
    $("input[type='checkbox']").on("click", function() {
        var numberOfChecked = "(" + $(".checkbox:checked").size() + ")";
        $(".approve-num").text(numberOfChecked);

    });

    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

export default Approve;