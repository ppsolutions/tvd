﻿export default class ApproveService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.approveSettingUrl = location + '/api/approve/setting';
        this.getApproveListUrl = location + '/api/approve/fetch';
        this.approveUrl = location + '/api/approve';
    }

    
    edit(dto, callback, failure) {
        $.ajax({
            type: "PUT",
            url: this.approveSettingUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.getApproveListUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    approve(dto, callback, failure) {
        $.ajax({
            type: "PUT",
            url: this.approveUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    rejectApprove(dto, callback, failure) {
        $.ajax({
            type: "DELETE",
            url: this.approveUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

}