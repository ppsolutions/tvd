﻿export default class ChangePasswordService {
	constructor() {
		const location = 'http://' + window.location.host;
		this.updatePasswordUrl = location + '/api/member/password';
	}

	updateMemberPassword(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.updatePasswordUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}

}