﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../../shared/validator';
import PointAdjustmentService from './changepassword.service';
import toastr from 'toastr';

class ChangePassword {
	constructor(options) {
		EventEmitter.call(this);

		this.service = new PointAdjustmentService();
		this.validator = new Validator();

		this.labelMemberId = $(options.labelMemberId);

		this.changePasswordReadOnly = $(options.changePasswordReadOnly);
		this.changePasswordEdit = $(options.changePasswordEdit);

		this.buttonEditPassword = $(options.buttonEditPassword);
		this.buttonSavePassword = $(options.buttonSavePassword);
		this.buttonClosePassword = $(options.buttonClosePassword);

		this.inputPassword = $(options.inputPassword);
		this.buttonShowHidePassword = $(options.buttonShowHidePassword);

		this.inputCurrentPassword = $(options.inputCurrentPassword);
		this.buttonShowHideCurrentPassword = $(options.buttonShowHideCurrentPassword);

		this.inputNewPassword = $(options.inputNewPassword);
		this.buttonShowHideNewPassword = $(options.buttonShowHideNewPassword);

		this.inputConfirmNewPassword = $(options.inputConfirmNewPassword);
		this.buttonShowHideConfirmNewPassword = $(options.buttonShowHideConfirmNewPassword);

		this.buttonRewardRedemption = $(options.buttonRewardRedemption);

		this.updatedBy = $(options.updatedBy);

		eventBinding(this);
	}
}

inherits(ChangePassword, EventEmitter);

function eventBinding(changePassword) {
	const seft = changePassword;

	seft.inputCurrentPassword.on('keyup', function () {
		if (seft.inputCurrentPassword.val() === "") {
			seft.inputCurrentPassword.attr("style", "border-color:red");
		}
		else {
			seft.inputCurrentPassword.attr("style", "border-color:#ccc");
		}
	});

	seft.inputNewPassword.on('keyup', function () {
		if (seft.inputNewPassword.val() === "") {
			seft.inputNewPassword.attr("style", "border-color:red");
		}
		else {
			seft.inputNewPassword.attr("style", "border-color:#ccc");
		}
	});

	seft.inputConfirmNewPassword.on('keyup', function () {
		if (seft.inputConfirmNewPassword.val() === "") {
			seft.inputConfirmNewPassword.attr("style", "border-color:red");
		}
		else {
			seft.inputConfirmNewPassword.attr("style", "border-color:#ccc");
		}
	});

	seft.buttonShowHidePassword.on('click', function () {
		seft.showHidePassword(seft.inputPassword, seft.buttonShowHidePassword);
	});

	seft.buttonShowHideCurrentPassword.on('click', function () {
		seft.showHidePassword(seft.inputCurrentPassword, seft.buttonShowHideCurrentPassword);
	});

	seft.buttonShowHideNewPassword.on('click', function () {
		seft.showHidePassword(seft.inputNewPassword, seft.buttonShowHideNewPassword);
	});

	seft.buttonShowHideConfirmNewPassword.on('click', function () {
		seft.showHidePassword(seft.inputConfirmNewPassword, seft.buttonShowHideConfirmNewPassword);
	});

	seft.buttonEditPassword.on('click', function () {
		seft.changePasswordReadOnly.hide();
		seft.buttonEditPassword.addClass('hide');
		seft.changePasswordEdit.show();
		seft.buttonSavePassword.removeClass('hide');
		seft.buttonClosePassword.removeClass('hide');
	});

	seft.buttonSavePassword.on('click', function () {
		if (seft.valid()) {
			const obj = {
				PersonId: seft.labelMemberId.val(),
				OldPassword: seft.inputCurrentPassword.val(),
				NewPassword: seft.inputNewPassword.val(),
				UpdatedBy: seft.updatedBy.data().empId
			};

			const success = function (data) {
				if (data.Success) {

					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
					seft.inputCurrentPassword.val('');
					seft.inputNewPassword.val('');
					seft.inputConfirmNewPassword.val('');

					seft.changePasswordReadOnly.show();
					seft.changePasswordEdit.hide();
					seft.buttonEditPassword.removeClass('hide');
					seft.buttonSavePassword.addClass('hide');
					seft.buttonClosePassword.addClass('hide');
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};
			const failure = function (xhr, textstatus) {
				let errorMsgFromServer = xhr.responseText;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			};

			seft.service.updateMemberPassword(obj, success, failure);
		}
	});

	seft.buttonClosePassword.on('click', function () {
		seft.inputCurrentPassword.val('');
		seft.inputNewPassword.val('');
		seft.inputConfirmNewPassword.val('');

		seft.changePasswordReadOnly.show();
		seft.changePasswordEdit.hide();
		seft.buttonEditPassword.removeClass('hide');
		seft.buttonSavePassword.addClass('hide');
		seft.buttonClosePassword.addClass('hide');
	});

	seft.buttonRewardRedemption.on('click', function () {
		///MemberDetail/PointAdjustment?personId=1039&source=1
		window.location.href = '/MemberDetail/PointAdjustment?personId=' + seft.labelMemberId.val() + '&source=' + seft.labelMemberId.data('source');
	});

}

ChangePassword.prototype.showHidePassword = function (inputElement, buttonElement) {
	if (!buttonElement.data('showpassword')) {
		buttonElement.data('showpassword', true);
		buttonElement.find('span').removeClass('glyphicon-eye-open');
		buttonElement.find('span').addClass('glyphicon-eye-close');
		inputElement.attr('type', 'text');
	}
	else {
		buttonElement.data('showpassword', false);
		buttonElement.find('span').removeClass('glyphicon-eye-close');
		buttonElement.find('span').addClass('glyphicon-eye-open');
		inputElement.attr('type', 'password');
	}
}

ChangePassword.prototype.valid = function () {
	this.resetError();
	let msg = '';
	if (!this.validCurrentPassword(this.inputPassword.val(), this.inputCurrentPassword.val(), this.inputCurrentPassword)) {
		msg += 'รหัสผ่านปัจจุบันไม่ถูกต้อง' + '<br />';
	}
	if (!this.validConfirmWithNewPassword(this.inputNewPassword.val(), this.inputConfirmNewPassword.val(), this.inputConfirmNewPassword)) {
		msg += 'ยืนยันรหัสผ่านใหม่ไม่ถูกต้อง';
	}

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}

		toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง <br />' + msg, 'Oop!');
	}
	return (errorCount === 0);
}

ChangePassword.prototype.resetError = function () {
	for (var i = 0; i < this.validator.basket.length; i++) {
		var element = this.validator.basket[i];
		element.removeAttr("style", "border-color:red");
	}
	this.validator.basket = [];
}

ChangePassword.prototype.validCurrentPassword = function (password, currentPassword, element) {
	if (password !== currentPassword) {
		this.validator.basket.push(element);
		return false;
	}

	return true;
}

ChangePassword.prototype.validConfirmWithNewPassword = function (newPassword, confirmNewPassword, element) {
	if (newPassword !== confirmNewPassword) {
		this.validator.basket.push(element);
		return false;
	}

	return true;
}


export default ChangePassword;