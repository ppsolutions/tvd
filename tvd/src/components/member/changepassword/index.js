﻿import Bacon from 'baconjs';
import ChangePassword from './changepassword';
import ChangePasswordService from './changepassword.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";
	
	if ($("#hidden-routeinfo").data() == undefined) return;

	if (window.location.href.indexOf('#reward-redemption') > 0) {
		$('#a-reward-redemption').click();
	}

	$('.change-password-edit').hide();

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 59) {
		const options = {
			labelMemberId: '#member-id',

			changePasswordReadOnly: '.change-password-readonly',
			changePasswordEdit: '.change-password-edit',

			buttonEditPassword: '#edit-change-password-mode',
			buttonSavePassword: '#save-change-password-mode',
			buttonClosePassword: '#close-change-password-mode',

			inputPassword: '#password-input',
			buttonShowHidePassword: '#show-hide-password-button',

			inputCurrentPassword: '#current-password-input',
			buttonShowHideCurrentPassword: '#current-show-hide-password-button',

			inputNewPassword: '#new-password-input',
			buttonShowHideNewPassword: '#new-show-hide-password-button',

			inputConfirmNewPassword: '#confirm-password-input',
			buttonShowHideConfirmNewPassword: '#confirm-show-hide-password-button',

			buttonRewardRedemption: '#btn-reward-redemption',
			updatedBy: '#hidden-routeinfo'
		};

		const changePasswordForm = new ChangePassword(options);
	}
});