﻿export default class MemberHistroyService {
	constructor() {
		const location = 'http://' + window.location.host;
		this.searchUrl = location + '/api/report/memberhistory';
		this.exportUrl = location + '/api/report/memberhistory/export';
	}

	searchMemberHistroy(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.searchUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	export(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.exportUrl,
			headers: {
				"token": dto.token
			},
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}
}