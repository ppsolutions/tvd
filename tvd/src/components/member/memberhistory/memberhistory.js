﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
var moment = require('moment');

import Validator from '../../shared/validator';
import MemberHistoryService from './memberhistory.service';
import MemberHistoryTemplate from './memberhistory.hbs';
import PagerTemplate from '../../shared/pager.hbs';
import ExportService from '../../shared/export';
import toastr from 'toastr';
import { diff, addedDiff, deletedDiff, updatedDiff, detailedDiff } from 'deep-object-diff';

class MemberHistory {
	constructor(options) {
		EventEmitter.call(this);

		this.exportService = new ExportService();
		this.service = new MemberHistoryService();
		this.validator = new Validator();

		this.datePickerFrom = $(options.mh_datePickerFrom);
		this.datePickerTo = $(options.mh_datePickerTo);
		this.inputFromDate = $(options.mh_inputFromDate);
		this.inputToDate = $(options.mh_inputToDate);
		this.buttonSearch = $(options.mh_buttonSearch);
		this.buttonExport = $(options.mh_buttonExport);
		this.element = $(options.mh_element);


		this.template = MemberHistoryTemplate;
		this.pagerTemplate = PagerTemplate;
		this.updatedBy = $(options.updatedBy);

		this.pager = $(options.mh_pager.id);
		this.defaultPageSize = options.mh_pager.defaultPageSize;

		this.objectId = $(options.objectId);
		this.token = options.mh_token;

		eventBinding(this);
	}
}

inherits(MemberHistory, EventEmitter);

function eventBinding(memberHistory) {
	const seft = memberHistory;

	seft.datePickerFrom.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.datePickerTo.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.buttonSearch.on('click', function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: 1
		};
		seft.fetchMemberHistroy(pager);
	});

	seft.buttonExport.on('click', function () {
		seft.export();
	});

	$(seft.pager).on("click", "ul#paging > li > a", function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: $(this).data().pn
		};
		seft.fetchMemberHistroy(pager);
	});
}

MemberHistory.prototype.export = function () {
	const seft = this;
	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : moment(seft.inputFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.inputToDate.val() === "" ? null : moment(seft.inputToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		objectId: seft.objectId.val(),
		pager: {
			pageSize: 100000,
			pageNumber: 1
		},
		token: seft.token,
		fields: ['DateTime', 'AuditActionType', 'By'],
		filename: 'member_history.csv'
	};

	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				for (let i = 0; i < data.ResponseData.length; i++) {
					const obj = data.ResponseData[i];
					let item = {
						DateTime: obj.RecCreatedWhen,
						AuditActionType: obj.AuditActionTypeDescription,
						By: obj.DescriptionObject.Source.FullName
					};
					exportData.push(item);
				}

				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.export(dto, callback, failure);
}

MemberHistory.prototype.fetchMemberHistroy = function (pager) {

	const seft = this;

	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : seft.inputFromDate.val(),
		toDate: seft.inputToDate.val() === "" ? null : seft.inputToDate.val(),
		pager: pager,
		objectId: seft.objectId.val()
	}

	const callback = function (data) {
		if (data.Success) {

			let items = [];

			for (let i = 0; i < data.ResponseData.length; i++) {
				let item = data.ResponseData[i];

				let obj = {
					RecCreatedWhen: item.RecCreatedWhen,
					AuditActionTypeDescription: item.AuditActionTypeDescription,
					FullName: item.DescriptionObject.Source.FullName
				};

				let oldData = null;
				let oldDatas = [];
				try {
					oldData = JSON.parse(item.OldValueObject);

					$.each(oldData, function (key, value) {
						oldDatas.push(key + ':' + value);
					});
				}
				catch (e) {

				}

				let newData = null;
				let newDatas = [];
				try {
					newData = JSON.parse(item.NewValueObject);

					$.each(newData, function (key, value) {
						newDatas.push(key + ':' + value);
					});
				}
				catch (e) {

				}

				let diff = detailedDiff(oldData, newData);
				$.each(diff.updated, function (k, v) {
					for (let i = 0; i < oldDatas.length; i++) {
						if (oldDatas[i].includes(k)) {
							oldDatas[i] = '<span style="background-color:#FFFF00">' + oldDatas[i] + '</span>'
							break;
						}						
					}
					for (let i = 0; i < newDatas.length; i++) {
						if (newDatas[i].includes(k)) {
							newDatas[i] = '<span style="background-color:#FFFF00">' + newDatas[i] + '</span>'
							break;
						}
					}
				});

				obj.OldData = oldDatas.join(' ');
				obj.NewData = newDatas.join(' ');

				items.push(obj);
			}


			seft.renderTemplateWithData(items, data.Pager);
		}
		else {
			toastr.error(data.Message, 'Oop!');
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	seft.service.searchMemberHistroy(dto, callback, failure);
}

MemberHistory.prototype.renderTemplateWithData = function (data, pager) {
	//const result = {
	//	items: data
	//};

	let datas = '';
	for (let i = 0; i < data.length; i++) {
		datas += '<tr>';
		datas += '<td class="text-center" width="15%">' + data[i].RecCreatedWhen + '</td>';
		datas += '<td class="text-center" width="20%">' + data[i].AuditActionTypeDescription + '</td>';
		datas += '<td width="20%"><p style="width:250px;">' + data[i].OldData + '</p></td>';
		datas += '<td width="20%"><p style="width:250px;">' + data[i].NewData + '</p></td>';
		datas += '<td class="text-center" width="20%">' + data[i].FullName + '</td>';
		datas += '</tr>';
	}

	
	//this.element.html(this.template(result));
	this.element.html(datas);
	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
};

export default MemberHistory;