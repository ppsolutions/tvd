﻿import Bacon from 'baconjs';
import MemberHistory from './memberhistory';
import MemberHistoryService from './memberhistory.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 60) {

		const options = {
			mh_datePickerFrom: '#dtp-from input',
			mh_datePickerTo: '#dtp-to input',

			mh_inputFromDate: '#from-date',
			mh_inputToDate: '#to-date',

			mh_buttonSearch: '#btn-search',
			mh_buttonExport: '#btn-export',
			mh_element: "#table-result",

			mh_pager: {
				id: "#pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},

			objectId: '#member-id',

			updatedBy: '#hidden-routeinfo',
			mh_token: $("input[name='requestVerificationToken']").val()
		};

		const memberHistoryForm = new MemberHistory(options);

	}
});