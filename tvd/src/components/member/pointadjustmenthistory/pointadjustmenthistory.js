﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
var moment = require('moment');

import Validator from '../../shared/validator';
import PointAdjustmentHistoryService from './pointadjustmenthistory.service';
import PointAdjustmentHistoryTemplate from './pointadjustmenthistory.hbs';
import PagerTemplate from '../../shared/pager.hbs';
import ExportService from '../../shared/export';
import toastr from 'toastr';

class PointAdjustmentHistory {
	constructor(options) {
		EventEmitter.call(this);

		this.exportService = new ExportService();
		this.service = new PointAdjustmentHistoryService();
		this.validator = new Validator();

		this.datePickerFrom = $(options.pah_datePickerFrom);
		this.datePickerTo = $(options.pah_datePickerTo);
		this.inputFromDate = $(options.pah_inputFromDate);
		this.inputToDate = $(options.pah_inputToDate);
		this.buttonSearch = $(options.pah_buttonSearch);
		this.buttonExport = $(options.pah_buttonExport);
		this.element = $(options.pah_element);


		this.template = PointAdjustmentHistoryTemplate;
		this.pagerTemplate = PagerTemplate;
		this.updatedBy = $(options.updatedBy);

		this.pager = $(options.pah_pager.id);
		this.defaultPageSize = options.pah_pager.defaultPageSize;

		this.objectId = $(options.objectId);

		this.token = options.pah_token;

		eventBinding(this);
	}
}

inherits(PointAdjustmentHistory, EventEmitter);

function eventBinding(pointAdjustmentHistory) {
	const seft = pointAdjustmentHistory;

	seft.datePickerFrom.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.datePickerTo.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.buttonSearch.on('click', function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: 1
		};
		seft.fetchPointAdjustmentHistory(pager);
	});

	seft.buttonExport.on('click', function () {
		seft.export();
	});

	$(seft.pager).on("click", "ul#paging > li > a", function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: $(this).data().pn
		};
		seft.fetchPointAdjustmentHistory(pager);
	});
}

PointAdjustmentHistory.prototype.export = function () {
	const seft = this;
	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : moment(seft.inputFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.inputToDate.val() === "" ? null : moment(seft.inputToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		objectId: seft.objectId.val(),
		pager: {
			pageSize: 100000,
			pageNumber: 1
		},
		token: seft.token,
		fields: ['DateTime', 'AuditActionType', 'Detail', 'UpdatePoint', 'RemainPoint', 'By', 'Status'],
		filename: 'point_adjustment_history.csv'
	};

	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				for (let i = 0; i < data.ResponseData.length; i++) {
					const obj = data.ResponseData[i];
					let item = {
						DateTime: obj.RecCreatedWhen,
						AuditActionType: obj.AuditActionTypeDescription,
						Detail: obj.DescriptionObject.Detail,
						UpdatePoint: obj.DescriptionObject.UpdatePoint,
						RemainPoint: obj.DescriptionObject.RamainPoint,
						By: obj.DescriptionObject.Source.FullName,
						Status: obj.RecStatusDescription
					};
					exportData.push(item);
				}

				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.export(dto, callback, failure);
}

PointAdjustmentHistory.prototype.fetchPointAdjustmentHistory = function (pager) {

	const seft = this;

	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : moment(seft.inputFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.inputToDate.val() === "" ? null : moment(seft.inputToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		pager: pager,
		objectId: seft.objectId.val()
	}

	const callback = function (data) {
		if (data.Success) {
			seft.renderTemplateWithData(data.ResponseData, data.Pager);
		}
		else {
			toastr.error(data.Message, 'Oop!');
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	seft.service.searchPointAdjustmentHistory(dto, callback, failure);
}

PointAdjustmentHistory.prototype.renderTemplateWithData = function (data, pager) {
	const result = {
		items: data
	};

	this.element.html(this.template(result));
	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
};

export default PointAdjustmentHistory;