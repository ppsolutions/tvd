﻿import Bacon from 'baconjs';
import PointAdjustmentHistory from './pointadjustmenthistory';
import PointAdjustmentHistoryService from './pointadjustmenthistory.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 61) {

		const options = {
			pah_datePickerFrom: '#dtp-from input',
			pah_datePickerTo: '#dtp-to input',

			pah_inputFromDate: '#from-date',
			pah_inputToDate: '#to-date',

			pah_buttonSearch: '#btn-search',
			pah_buttonExport: '#btn-export',
			pah_element: "#table-result",

			pah_pager: {
				id: "#pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},

			objectId: '#member-id',

			updatedBy: '#hidden-routeinfo',
			pah_token: $("input[name='requestVerificationToken']").val()
		};

		const pointAdjustmentHistoryForm = new PointAdjustmentHistory(options);
	}
});