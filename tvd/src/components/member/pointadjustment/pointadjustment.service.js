﻿export default class PointAdjustmentService {
	constructor() {
		const location = 'http://' + window.location.host;
		this.updateMemberPointUrl = location + '/api/member/point';
		this.fetchProductUrl = location + '/api/point/reward/fetch/all';
		this.saveMemberRewardRedemptionUrl = location + '/api/member/redeempoint';
	}

	updateMemberPoint(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.updateMemberPointUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}

	fetchProduct(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.fetchProductUrl,
			headers: {
				"token": dto.Token
			},
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	insertRedeemPoint(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.saveMemberRewardRedemptionUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}
}