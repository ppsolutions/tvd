﻿import Bacon from 'baconjs';
import PointAdjustment from './pointadjustment';
import PointAdjustmentService from './pointadjustment.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 58) {

		if (window.location.href.indexOf('#reward-redemption') > 0) {
			$('#a-reward-redemption').click();
		}

		const options = {
			labelMemberId: '#member-id',
			inputIncreasePoint: '#increase-point',
			buttonIncreasePoint: '#increase-button',

			inputDecreasePoint: '#decrease-point',
			buttonDecreasePoint: '#decrease-button',

			inputProductSearch: '#product-search',
			buttonProductSearch: '#product-search-button',

			labelRedemptionInformation: '#redemption-information',
			buttonRedemption: '#redemption-button',

			selectRedemptionTypeClass: '.redemption-type',

			checkboxSelectAllProduct: '#product-selected-all',
			checkboxProductClass: '.product-selected',

			element: "#product-result",

			updatedBy: '#hidden-routeinfo'
		};

		const pointadjustmentForm = new PointAdjustment(options);
	}
});