﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../../shared/validator';
import PointAdjustmentService from './pointadjustment.service';
import ProductTemplate from './product.hbs';
import toastr from 'toastr';

class PointAdjustment {
	constructor(options) {
		EventEmitter.call(this);

		this.service = new PointAdjustmentService();
		this.validator = new Validator();

		this.labelMemberId = $(options.labelMemberId);
		this.inputIncreasePoint = $(options.inputIncreasePoint);
		this.buttonIncreasePoint = $(options.buttonIncreasePoint);
		this.inputDecreasePoint = $(options.inputDecreasePoint);
		this.buttonDecreasePoint = $(options.buttonDecreasePoint);
		this.inputProductSearch = $(options.inputProductSearch);
		this.buttonProductSearch = $(options.buttonProductSearch);

		this.labelRedemptionInformation = $(options.labelRedemptionInformation);
		this.buttonRedemption = $(options.buttonRedemption);

		this.checkboxSelectAllProduct = $(options.checkboxSelectAllProduct);
		this.checkboxProductClass = options.checkboxProductClass;
		this.selectRedemptionTypeClass = options.selectRedemptionTypeClass;

		this.updatedBy = $(options.updatedBy);
		this.template = ProductTemplate;
		this.element = $(options.element);
		eventBinding(this);
	}
}

inherits(PointAdjustment, EventEmitter);

function eventBinding(pointAdjustment) {
	const seft = pointAdjustment;

	seft.inputIncreasePoint.on('keyup', function () {
		if (seft.inputIncreasePoint.val() === "") {
			seft.inputIncreasePoint.attr("style", "border-color:red");
		}
		else {
			seft.inputIncreasePoint.attr("style", "border-color:#ccc");
		}
	});

	seft.inputDecreasePoint.on('keyup', function () {
		if (seft.inputDecreasePoint.val() === "") {
			seft.inputDecreasePoint.attr("style", "border-color:red");
		}
		else {
			seft.inputDecreasePoint.attr("style", "border-color:#ccc");
		}
	});

	seft.buttonIncreasePoint.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		seft.resetError();

		if (seft.validIncreasePoint()) {
			const obj = {
				UpdatedBy: seft.updatedBy.data().empId,
				PersonId: seft.labelMemberId.val(),
				Points: seft.inputIncreasePoint.val(),
				ActionTypeValue: 1,
				Remark: '',
				Token: token
			};

			const success = function (data) {
				if (data.Success) {

					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
					seft.inputIncreasePoint.val('');
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};
			const failure = function (xhr, textstatus) {
				let errorMsgFromServer = xhr.responseText;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			};

			seft.service.updateMemberPoint(obj, success, failure);
		}
		else {
			toastr.error('เพิ่มคะแนนม่ถูกต้อง กรุณาลองใหม่อีกครั้ง คะแนนต้องอยู่ระหว่าง 1 ถึง 99999', 'Oop!');
		}
	});

	seft.buttonDecreasePoint.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		seft.resetError();

		if (seft.validDecreasePoint()) {
			const obj = {
				UpdatedBy: seft.updatedBy.data().empId,
				PersonId: seft.labelMemberId.val(),
				Points: seft.inputDecreasePoint.val(),
				ActionTypeValue: -1,
				Token: token
			};

			const success = function (data) {
				if (data.Success) {

					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
					seft.inputDecreasePoint.val('');
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};
			const failure = function (xhr, textstatus) {
				let errorMsgFromServer = xhr.responseText;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			};

			seft.service.updateMemberPoint(obj, success, failure);
		}
		else {
			toastr.error('ลดคะแนนไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง คะแนนต้องอยู่ระหว่าง 1 ถึง 99999', 'Oop!');
		}
	});

	seft.buttonProductSearch.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		const dto = {
			Description: seft.inputProductSearch.val(),
			Token: token
		};

		const callback = function (data) {
			if (data.Success) {
				seft.renderTemplateWithData(data.ResponseData);
				$(seft.selectRedemptionTypeClass).on('change', function () {
					if ($(this).val() === "-1") {
						$(this).attr("style", "border-color:red");
					}
					else {
						$(this).attr("style", "border-color:#ccc");
					}
				});
			}
		};

		const failure = function (xhr, textstatus) {
			toastr.error(xhr.statusText, 'Oop!');
		};

		seft.service.fetchProduct(dto, callback, failure);
	});

	seft.checkboxSelectAllProduct.on('click', function () {
		$(seft.checkboxProductClass).prop('checked', seft.checkboxSelectAllProduct.prop('checked'));
	});

	seft.buttonRedemption.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		let totalPoint = 0;
		let totalMoney = 0;
		let totalError = 0;

		let dto = [];

		seft.element.find('tr').each(function () {
			if ($(this).find(seft.checkboxProductClass).prop('checked')) {

				$(this).find(seft.selectRedemptionTypeClass).attr("style", "border-color:#CCC");
				let redemptionType = parseInt($(this).find(seft.selectRedemptionTypeClass + ' option:selected').val());

				if (redemptionType === -1) {
					totalError++;
					$(this).find(seft.selectRedemptionTypeClass).attr("style", "border-color:red");
				}
				else if (redemptionType === 0) {
					totalPoint += $(this).find('td.point-only').data('pointonly');
				}
				else if (redemptionType === 1) {
					totalPoint += $(this).find('td.point').data('point');
					totalMoney += $(this).find('td.money').data('money');
				}

				let obj = {
					UpdatedBy: seft.updatedBy.data().empId,
					PersonId: seft.labelMemberId.val(),
					RedeemPointId: $(this).find(seft.checkboxProductClass).data('rewardid'),
					Description: $(this).find('td.description').text(),
					Type: parseInt($(this).find(seft.selectRedemptionTypeClass + ' option:selected').val()),
					Token: token
				}
				dto.push(obj);
			}
		});
		const currentMemberPoint = seft.element.data('currentpoint');
		if (totalPoint > currentMemberPoint) {
			toastr.error('จำนวนคะแนนที่ใช้แลกเกิน คะแนนปัจจุบัน', 'Oop!');
		}
		else {
			const callback = function (data) {
				if (data.Success) {
					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
					window.location.href = '/MemberDetail/PointAdjustment?personId=' + seft.labelMemberId.val() + '&source=' + seft.labelMemberId.data('source') + '#reward-redemption';
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};

			const failure = function (xhr, textstatus) {
				toastr.error(xhr.statusText, 'Oop!');
			};

			seft.service.insertRedeemPoint(dto, callback, failure);
		}
	});
}

PointAdjustment.prototype.renderTemplateWithData = function (data) {
	const result = {
		items: data
	};

	this.element.html(this.template(result));
};

PointAdjustment.prototype.validIncreasePoint = function () {
	this.resetError();
	this.validatePointRange(this.inputIncreasePoint.val(), this.inputIncreasePoint);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

PointAdjustment.prototype.validDecreasePoint = function () {
	this.resetError();
	this.validatePointRange(this.inputDecreasePoint.val(), this.inputDecreasePoint);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

PointAdjustment.prototype.validatePointRange = function (str, element) {
	const value = parseInt(str);
	if (value < 0 || value > 99999) {
		this.validator.basket.push(element);
		return false;
	}

	return true;
}


PointAdjustment.prototype.resetError = function () {
	for (var i = 0; i < this.validator.basket.length; i++) {
		var element = this.validator.basket[i];
		element.removeAttr("style", "border-color:red");
	}
	this.validator.basket = [];
}

export default PointAdjustment;