﻿import Bacon from 'baconjs';
import ItemWaitingForApprove from './itemwaitingforapprove';
import ItemWaitingForApproveService from './itemwaitingforapprove.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 62) {
		const options = {
			iwfa_datePickerFrom: '#dtp-from input',
			iwfa_datePickerTo: '#dtp-to input',

			iwfa_inputFromDate: '#from-date',
			iwfa_inputToDate: '#to-date',

			iwfa_buttonSearch: '#btn-search',
			iwfa_buttonExport: '#btn-export',
			iwfa_element: "#table-result",

			iwfa_pager: {
				id: "#m1-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},

			objectId: '#member-id',

			updatedBy: '#hidden-routeinfo',

			iwfa_token: $("input[name='requestVerificationToken']").val()
		};

		const itemWaitingForApproveForm = new ItemWaitingForApprove(options);
	}
});