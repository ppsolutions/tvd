﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
var moment = require('moment');

import Validator from '../../shared/validator';
import ItemWaitingForApproveService from './itemwaitingforapprove.service';
import ItemWaitingForApproveTemplate from './itemwaitingforapprove.hbs';
import PagerTemplate from '../../shared/pager.hbs';
import ExportService from '../../shared/export';
import toastr from 'toastr';

class ItemWaitingForApprove {
	constructor(options) {
		EventEmitter.call(this);

		this.exportService = new ExportService();
		this.service = new ItemWaitingForApproveService();
		this.validator = new Validator();

		this.datePickerFrom = $(options.iwfa_datePickerFrom);
		this.datePickerTo = $(options.iwfa_datePickerTo);
		this.inputFromDate = $(options.iwfa_inputFromDate);
		this.inputToDate = $(options.iwfa_inputToDate);
		this.buttonSearch = $(options.iwfa_buttonSearch);
		this.buttonExport = $(options.iwfa_buttonExport);
		this.element = $(options.iwfa_element);


		this.template = ItemWaitingForApproveTemplate;
		this.pagerTemplate = PagerTemplate;
		this.updatedBy = $(options.updatedBy);

		this.pager = $(options.iwfa_pager.id);
		this.objectId = $(options.objectId);
		this.defaultPageSize = options.iwfa_pager.defaultPageSize;
		this.token = options.iwfa_token;

		eventBinding(this);
	}
}

inherits(ItemWaitingForApprove, EventEmitter);

function eventBinding(itemWaitingForApprove) {
	const seft = itemWaitingForApprove;

	seft.datePickerFrom.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.datePickerTo.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
		todayHighlight: true
	});

	seft.buttonSearch.on('click', function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: 1
		};
		seft.fetchItemWaitingForApprove(pager);
	});

	seft.buttonExport.on('click', function () {
		seft.export();
	});

	$(seft.pager).on("click", "ul#paging > li > a", function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: $(this).data().pn
		};
		seft.fetchItemWaitingForApprove(pager);
	});
}

ItemWaitingForApprove.prototype.export = function () {
	const seft = this;

	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : moment(seft.inputFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.inputToDate.val() === "" ? null : moment(seft.inputToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		objectId: seft.objectId.val(),
		pager: {
			pageSize: 100000,
			pageNumber: 1
		},
		token: seft.token,
		fields: ['DateTime', 'AuditActionType', 'Description', 'By'],
		filename: 'item_waiting_for_approve.csv'
	};

	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				for (let i = 0; i < data.ResponseData.length; i++) {
					const obj = data.ResponseData[i];
					let description;
					switch (obj.AuditActionType) {
						case 2: description = 'เปลี่ยนแปลงข้อมูลสมาชิก'; break;
						case 3: description = 'ปรับปรุงคะแนนสมาชิก ' + obj.DescriptionObject.Name + ' จำนวน ' + obj.DescriptionObject.UpdatePoint; break;
						case 4: description = 'แลกของกำนัล ' + obj.DescriptionObject.Product; break;
						case 29: description = 'เปลี่ยนรหัสผ่านสมาชิก ' + obj.DescriptionObject.Name; break;
						case 30: description = 'เปลี่ยนแปลงข้อมูลสมาชิก ' + obj.DescriptionObject.Name; break;
						case 31: description = 'เปลี่ยนแปลงข้อมูลที่อยู่สมาชิก ' + obj.DescriptionObject.Name; break;
						case 32: description = 'Block สมาชิก ' + obj.DescriptionObject.Name; break;
						case 33: description = 'Active สมาชิก ' + obj.DescriptionObject.Name; break;
						case 34: description = 'ยกเลิกสมาชิก ' + obj.DescriptionObject.Name; break;
					}


					let item = {
						DateTime: obj.RecCreatedWhen,
						AuditActionType: obj.AuditActionTypeDescription,
						Description: description,
						By: obj.Source.FullName,
					};
					exportData.push(item);
				}

				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.export(dto, callback, failure);
}

ItemWaitingForApprove.prototype.fetchItemWaitingForApprove = function (pager) {

	const seft = this;

	let dto = {
		fromDate: seft.inputFromDate.val() === "" ? null : moment(seft.inputFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.inputToDate.val() === "" ? null : moment(seft.inputToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		pager: pager,
		objectId: seft.objectId.val()
	}

	const callback = function (data) {
		if (data.Success) {

			for (var i = 0; i < data.ResponseData.length; i++) {
				switch (data.ResponseData[i].AuditActionType) {
					case 2: data.ResponseData[i].Description = 'เปลี่ยนแปลงข้อมูลสมาชิก'; break;
					case 3: data.ResponseData[i].Description = 'ปรับปรุงคะแนนสมาชิก ' + data.ResponseData[i].DescriptionObject.Name + ' จำนวน ' + data.ResponseData[i].DescriptionObject.UpdatePoint; break;
					case 4: data.ResponseData[i].Description = 'แลกของกำนัล ' + data.ResponseData[i].DescriptionObject.Product; break;
					case 29: data.ResponseData[i].Description = 'เปลี่ยนรหัสผ่านสมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
					case 30: data.ResponseData[i].Description = 'เปลี่ยนแปลงข้อมูลสมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
					case 31: data.ResponseData[i].Description = 'เปลี่ยนแปลงข้อมูลที่อยู่สมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
					case 32: data.ResponseData[i].Description = 'Block สมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
					case 33: data.ResponseData[i].Description = 'Active สมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
					case 34: data.ResponseData[i].Description = 'ยกเลิกสมาชิก ' + data.ResponseData[i].DescriptionObject.Name; break;
				}
			}


			seft.renderTemplateWithData(data.ResponseData, data.Pager);
		}
		else {
			toastr.error(data.Message, 'Oop!');
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	seft.service.searchItemWaitingForApprove(dto, callback, failure);
}

ItemWaitingForApprove.prototype.renderTemplateWithData = function (data, pager) {
	const result = {
		items: data
	};

	this.element.html(this.template(result));
	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
};

export default ItemWaitingForApprove;