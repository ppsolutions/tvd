﻿export default class MemberDetailService {
	constructor() {
		const location = 'http://' + window.location.host;
		this.updateBasicInfoUrl = location + '/api/member/basicInfo';
		this.updateAddressUrl = location + '/api/member/address';
		this.updateStatusUrl = location + '/api/member/status';
	}

	updatePerson(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.updateBasicInfoUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}

	updateAddress(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.updateAddressUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}

	updateStatus(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.updateStatusUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}
}