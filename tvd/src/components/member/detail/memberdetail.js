﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
var moment = require('moment');

import Validator from '../../shared/validator';
import MemberDetailService from './memberdetail.service';
import toastr from 'toastr';

class MemberDetail {
	constructor(options) {
		EventEmitter.call(this);

		this.service = new MemberDetailService();
		this.validator = new Validator();
		this.editPersonOrCorperateMode = $(options.editPersonOrCorperateMode);
		this.savePersonOrCorperateMode = $(options.savePersonOrCorperateMode);
		this.editAddressMode = $(options.editAddressMode);
		this.saveAddressMode = $(options.saveAddressMode);

		this.labelMemberId = $(options.labelMemberId);
		this.labelPersonTitleName = $(options.labelPersonTitleName);
		this.labelPersonFirstName = $(options.labelPersonFirstName);
		this.labelPersonLastName = $(options.labelPersonLastName);
		this.labelPersonDob = $(options.labelPersonDob);
		this.labelPersonIsSpecificYear = $(options.labelPersonIsSpecificYear);
		this.labelPersonPhone = $(options.labelPersonPhone);
		this.labelPersonEmail = $(options.labelPersonEmail);
		this.inputPersonTitle = $(options.inputPersonTitle);
		this.inputPersonFirstName = $(options.inputPersonFirstName);
		this.inputPersonLastName = $(options.inputPersonLastName);
		this.inputPersonDob = $(options.inputPersonDob);
		this.inputPersonIsSpecificYear = $(options.inputPersonIsSpecificYear);
		this.inputPersonPhone = $(options.inputPersonPhone);
		this.inputPersonEmail = $(options.inputPersonEmail);

		this.labelCorporateTitleName = $(options.labelCorporateTitleName);
		this.labelCorporateName = $(options.labelCorporateName);
		this.labelCorporatePhone = $(options.labelCorporatePhone);
		this.labelCorporateEmail = $(options.labelCorporateEmail);
		this.inputCorporateTitle = $(options.inputCorporateTitle);
		this.inputCorporateName = $(options.inputCorporateName);
		this.inputCorporatePhone = $(options.inputCorporatePhone);
		this.inputCorporateEmail = $(options.inputCorporateEmail);

		this.labelCurrentAddress = $(options.labelCurrentAddress);
		this.labelCurrentAddressProvince = $(options.labelCurrentAddressProvince);
		this.labelCurrentAddressPostalCode = $(options.labelCurrentAddressPostalCode);
		this.inputCurrentAddress = $(options.inputCurrentAddress);
		this.inputCurrentAddressProvince = $(options.inputCurrentAddressProvince);
		this.inputCurrentAddressPostalCode = $(options.inputCurrentAddressPostalCode);
		this.radioShipmentUseCurrentAddress = $(options.radioShipmentUseCurrentAddress);
		this.radioShipmentUseNewAddress = $(options.radioShipmentUseNewAddress);
		this.labelShipmentAddress = $(options.labelShipmentAddress);
		this.labelShipmentProvince = $(options.labelShipmentProvince);
		this.labelShipmentPostalCode = $(options.labelShipmentPostalCode);
		this.inputShipmentAddress = $(options.inputShipmentAddress);
		this.inputShipmentProvince = $(options.inputShipmentProvince);
		this.inputShipmentPostalCode = $(options.inputShipmentPostalCode);
		this.radioInvoiceUseCurrentAddress = $(options.radioInvoiceUseCurrentAddress);
		this.radioInvoiceUseShipmentAddress = $(options.radioInvoiceUseShipmentAddress);
		this.radioInvoiceUseNewAddress = $(options.radioInvoiceUseNewAddress);
		this.labelInvoiceAddress = $(options.labelInvoiceAddress);
		this.labelInvoiceProvince = $(options.labelInvoiceProvince);
		this.labelInvoicePostalCode = $(options.labelInvoicePostalCode);
		this.inputInvoiceAddress = $(options.inputInvoiceAddress);
		this.inputInvoiceProvince = $(options.inputInvoiceProvince);
		this.inputInvoicePostalCode = $(options.inputInvoicePostalCode);


		this.buttonRewardRedemption = $(options.buttonRewardRedemption);
		this.buttonRecStatus = $(options.buttonRecStatus);
		this.buttonRecStatusAction = $(options.buttonRecStatusAction);
		this.buttonRemoveRelateAccount = $(options.buttonRemoveRelateAccount);

		this.updatedBy = $(options.updatedBy);
		eventBinding(this);
	}
}

inherits(MemberDetail, EventEmitter);

function eventBinding(memberDetail) {
	const seft = memberDetail;

	seft.inputPersonTitle.on('change', function () {
		if (seft.inputPersonTitle.val() === "") {
			seft.inputPersonTitle.attr("style", "border-color:red");
		}
		else {
			seft.inputPersonTitle.attr("style", "border-color:#ccc");
		}
	});

	seft.inputPersonFirstName.on('keyup', function () {
		if (seft.validator.name(seft.inputPersonFirstName.val()).valid) {
			seft.inputPersonFirstName.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputPersonFirstName.attr("style", "border-color:red");
		}
	});

	seft.inputPersonLastName.on('keyup', function () {
		if (seft.validator.name(seft.inputPersonLastName.val()).valid) {
			seft.inputPersonLastName.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputPersonLastName.attr("style", "border-color:red");
		}
	});

	seft.inputPersonDob.on('keyup', function () {
		if (seft.validator.empty(seft.inputPersonDob.val()).valid) {
			seft.inputPersonDob.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputPersonDob.attr("style", "border-color:red");
		}
	});

	seft.inputPersonPhone.on("keyup", function (e) {
		if (seft.validator.phone(seft.inputPersonPhone.val()).valid) {
			seft.inputPersonPhone.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputPersonPhone.attr("style", "border-color:red");
		}
	});

	seft.inputPersonEmail.on('keyup', function () {
		if (seft.validator.email(seft.inputPersonEmail.val()).valid) {
			seft.inputPersonEmail.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputPersonEmail.attr("style", "border-color:red");
		}
	});

	seft.inputCorporateTitle.on('change', function () {
		if (seft.inputCorporateTitle.val() === "") {
			seft.inputCorporateTitle.attr("style", "border-color:red");
		}
		else {
			seft.inputCorporateTitle.attr("style", "border-color:#ccc");
		}
	});

	seft.inputCorporateName.on('keyup', function () {
		if (seft.validator.name(seft.inputCorporateName.val()).valid) {
			seft.inputCorporateName.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputCorporateName.attr("style", "border-color:red");
		}
	});

	seft.inputCorporatePhone.on('keyup', function () {
		if (seft.validator.name(seft.inputCorporatePhone.val()).valid) {
			seft.inputCorporatePhone.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputCorporatePhone.attr("style", "border-color:red");
		}
	});

	seft.inputCorporateEmail.on('keyup', function () {
		if (seft.validator.email(seft.inputCorporateEmail.val()).valid) {
			seft.inputCorporateEmail.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputCorporateEmail.attr("style", "border-color:red");
		}
	});

	seft.inputCurrentAddress.on('keyup', function () {
		if (seft.validator.empty(seft.inputCurrentAddress.val()).valid) {
			seft.inputCurrentAddress.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputCurrentAddress.attr("style", "border-color:red");
		}

		if (seft.radioShipmentUseCurrentAddress.prop('checked')) {
			seft.inputShipmentAddress.val(seft.inputCurrentAddress.val());
		}

		if (seft.radioInvoiceUseCurrentAddress.prop('checked')) {
			seft.inputInvoiceAddress.val(seft.inputCurrentAddress.val());
		}
	});

	seft.inputCurrentAddressProvince.on("change", function () {
		if (seft.inputCurrentAddressProvince.val() === "") {
			seft.inputCurrentAddressProvince.attr("style", "border-color:red");
		}
		else {
			seft.inputCurrentAddressProvince.attr("style", "border-color:#ccc");
		}

		if (seft.radioShipmentUseCurrentAddress.prop('checked')) {
			seft.inputShipmentProvince.val(seft.inputCurrentAddressProvince.val());
		}

		if (seft.radioInvoiceUseCurrentAddress.prop('checked')) {
			seft.inputInvoiceProvince.val(seft.inputCurrentAddressProvince.val());
		}
	});

	seft.inputCurrentAddressPostalCode.on("keyup", function (e) {
		if (seft.validator.empty(seft.inputCurrentAddressPostalCode.val()).valid) {
			seft.inputCurrentAddressPostalCode.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputCurrentAddressPostalCode.attr("style", "border-color:red");
		}

		if (seft.radioShipmentUseCurrentAddress.prop('checked')) {
			seft.inputShipmentPostalCode.val(seft.inputCurrentAddressPostalCode.val());
		}

		if (seft.radioInvoiceUseCurrentAddress.prop('checked')) {
			seft.inputInvoicePostalCode.val(seft.inputCurrentAddressPostalCode.val());
		}
	});

	seft.radioShipmentUseCurrentAddress.on('click', function () {
		seft.inputShipmentAddress.attr('disabled', 'disabled');
		seft.inputShipmentProvince.attr('disabled', 'disabled');
		seft.inputShipmentPostalCode.attr('disabled', 'disabled');

		seft.inputShipmentAddress.val(seft.inputCurrentAddress.val());
		seft.inputShipmentProvince.val(seft.inputCurrentAddressProvince.val());
		seft.inputShipmentPostalCode.val(seft.inputCurrentAddressPostalCode.val());
	});

	seft.radioShipmentUseNewAddress.on('click', function () {
		seft.inputShipmentAddress.removeAttr('disabled');
		seft.inputShipmentProvince.removeAttr('disabled');
		seft.inputShipmentPostalCode.removeAttr('disabled');
	});

	seft.inputShipmentAddress.on("keyup", function (e) {
		if (seft.validator.empty(seft.inputShipmentAddress.val()).valid) {
			seft.inputShipmentAddress.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputShipmentAddress.attr("style", "border-color:red");
		}

		if (seft.radioInvoiceUseShipmentAddress.prop('checked')) {
			seft.inputInvoiceAddress.val(seft.inputShipmentAddress.val());
		}
	});

	seft.inputShipmentProvince.on("change", function () {
		if (seft.inputShipmentProvince.val() === "") {
			seft.inputShipmentProvince.attr("style", "border-color:red");
		}
		else {
			seft.inputShipmentProvince.attr("style", "border-color:#ccc");
		}

		if (seft.radioInvoiceUseShipmentAddress.prop('checked')) {
			seft.inputInvoiceProvince.val(seft.inputShipmentProvince.val());
		}
	});

	seft.inputShipmentPostalCode.on("keyup", function (e) {
		if (seft.validator.empty(seft.inputShipmentPostalCode.val()).valid) {
			seft.inputShipmentPostalCode.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputShipmentPostalCode.attr("style", "border-color:red");
		}

		if (seft.radioInvoiceUseShipmentAddress.prop('checked')) {
			seft.inputInvoicePostalCode.val(seft.inputShipmentPostalCode.val());
		}
	});

	seft.radioInvoiceUseCurrentAddress.on('click', function () {
		seft.inputInvoiceAddress.attr('disabled', 'disabled');
		seft.inputInvoiceProvince.attr('disabled', 'disabled');
		seft.inputInvoicePostalCode.attr('disabled', 'disabled');

		seft.inputInvoiceAddress.val(seft.inputCurrentAddress.val());
		seft.inputInvoiceProvince.val(seft.inputCurrentAddressProvince.val());
		seft.inputInvoicePostalCode.val(seft.inputCurrentAddressPostalCode.val());
	});

	seft.radioInvoiceUseShipmentAddress.on('click', function () {
		seft.inputInvoiceAddress.attr('disabled', 'disabled');
		seft.inputInvoiceProvince.attr('disabled', 'disabled');
		seft.inputInvoicePostalCode.attr('disabled', 'disabled');

		seft.inputInvoiceAddress.val(seft.inputShipmentAddress.val());
		seft.inputInvoiceProvince.val(seft.inputShipmentProvince.val());
		seft.inputInvoicePostalCode.val(seft.inputShipmentPostalCode.val());
	});

	seft.radioInvoiceUseNewAddress.on('click', function () {
		seft.inputInvoiceAddress.removeAttr('disabled');
		seft.inputInvoiceProvince.removeAttr('disabled');
		seft.inputInvoicePostalCode.removeAttr('disabled');
	});

	seft.inputInvoiceAddress.on("keyup", function (e) {
		if (seft.validator.empty(seft.inputInvoiceAddress.val()).valid) {
			seft.inputInvoiceAddress.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputInvoiceAddress.attr("style", "border-color:red");
		}
	});

	seft.inputInvoiceProvince.on("change", function () {
		if (seft.inputInvoiceProvince.val() === "") {
			seft.inputInvoiceProvince.attr("style", "border-color:red");
		}
		else {
			seft.inputInvoiceProvince.attr("style", "border-color:#ccc");
		}
	});

	seft.inputInvoicePostalCode.on("keyup", function (e) {
		if (seft.validator.empty(seft.inputInvoicePostalCode.val()).valid) {
			seft.inputInvoicePostalCode.attr("style", "border-color:#ccc");
		}
		else {
			seft.inputInvoicePostalCode.attr("style", "border-color:red");
		}
	});


	seft.editPersonOrCorperateMode.on('click', function () {
		const isLegalPerson = seft.editPersonOrCorperateMode.data('islegalperson');
		seft.editPersonOrCorperateMode.addClass('hide');
		seft.savePersonOrCorperateMode.removeClass('hide');
		if (isLegalPerson) {
			$('.person-readonly').hide();
			$('.person-edit').show();
		}
		else {
			$('.corporate-readonly').hide();
			$('.corporate-edit').show();
		}
	});

	seft.savePersonOrCorperateMode.on('click', function () {
		const isLegalPerson = seft.editPersonOrCorperateMode.data('islegalperson');
		const token = $("input[name='requestVerificationToken']").val();
		let isValid = false;

		if (isLegalPerson) {
			isValid = seft.validPerson();
		}
		else {
			isValid = seft.validCorperate();
		}

		if (isValid) {
			const obj = {
				RegisterType: 0,
				PersonId: seft.labelMemberId.val(),
				PersonTitle: 0,
				CorporateTitle: 0,
				Firstname: '',
				CorporateName: '',
				Lastname: '',
				DateOfBirth: '',
				IsSpecificYear: '',
				ContactNumber: '',
				CorporatePhone: '',
				Email: '',
				CorporateEmail: '',
				UpdatedBy: seft.updatedBy.data().empId,
				Token: token,
				RecStatus: seft.buttonRecStatus.data('status')
			};
			if (isLegalPerson) {
				obj.RegisterType = 1;
				obj.PersonTitle = parseInt(seft.inputPersonTitle.find('option:selected').val());
				obj.Firstname = seft.inputPersonFirstName.val();
				obj.Lastname = seft.inputPersonLastName.val();
				obj.DateOfBirth = moment(seft.inputPersonDob.val(), "DD/MM/YYYY").format("YYYY-MM-DD");
				obj.IsSpecificYear = seft.inputPersonIsSpecificYear.prop('checked');
				obj.ContactNumber = seft.inputPersonPhone.val();
				obj.Email = seft.inputPersonEmail.val();

				seft.labelPersonTitleName.text(seft.inputPersonTitle.find('option:selected').text());
				seft.labelPersonFirstName.text(seft.inputPersonFirstName.val());
				seft.labelPersonLastName.text(seft.inputPersonLastName.val());
				seft.labelPersonDob.text(seft.inputPersonDob.val());
				seft.labelPersonIsSpecificYear.prop('checked', seft.inputPersonIsSpecificYear.prop('checked'));
				seft.labelPersonPhone.text(seft.inputPersonPhone.val());
				seft.labelPersonEmail.text(seft.inputPersonEmail.val());

				$('.person-readonly').show();
				$('.person-edit').hide();
			}
			else {
				obj.RegisterType = 2;
				obj.CorporateTitle = parseInt(seft.inputCorporateTitle.find('option:selected').val());
				obj.CorporateName = seft.inputCorporateName.val();
				obj.CorporatePhone = seft.inputCorporatePhone.val();
				obj.CorporateEmail = seft.inputCorporateEmail.val();

				seft.labelCorporateTitleName.text(seft.inputCorporateTitle.find('option:selected').text());
				seft.labelCorporateName.text(seft.inputCorporateName.val());
				seft.labelCorporatePhone.text(seft.inputCorporatePhone.val());
				seft.labelCorporateEmail.text(seft.inputCorporateEmail.val());

				$('.corporate-readonly').show();
				$('.corporate-edit').hide();
			}

			const success = function (data) {
				if (data.Success) {
					seft.savePersonOrCorperateMode.addClass('hide');
					seft.editPersonOrCorperateMode.removeClass('hide');

					toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};
			const failure = function (xhr, textstatus) {
				let errorMsgFromServer = xhr.responseText;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			};

			seft.service.updatePerson(obj, success, failure);
		}
		else {
			toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
		}
	});

	seft.editAddressMode.on('click', function () {
		seft.editAddressMode.addClass('hide');
		seft.saveAddressMode.removeClass('hide');
		$('.address-readonly').hide();
		$('.address-edit').show();
		seft.radioInvoiceUseCurrentAddress.removeAttr('disabled');
		seft.radioInvoiceUseShipmentAddress.removeAttr('disabled');
		seft.radioInvoiceUseNewAddress.removeAttr('disabled');
		seft.radioShipmentUseCurrentAddress.removeAttr('disabled');
		seft.radioShipmentUseNewAddress.removeAttr('disabled');

		seft.inputCurrentAddress.text($.trim(seft.labelCurrentAddress.text()));
		seft.inputShipmentAddress.text($.trim(seft.labelShipmentAddress.text()));
		seft.inputInvoiceAddress.text($.trim(seft.labelInvoiceAddress.text()));

		if (seft.radioShipmentUseCurrentAddress.prop('checked')) {
			seft.radioShipmentUseCurrentAddress.click();
		}
		else {
			seft.radioShipmentUseNewAddress.click();
		}

		if (seft.radioInvoiceUseCurrentAddress.prop('checked')) {
			seft.radioInvoiceUseCurrentAddress.click();
		}
		else if (seft.radioInvoiceUseShipmentAddress.prop('checked')) {
			seft.radioInvoiceUseShipmentAddress.click();
		}
		else {
			seft.radioInvoiceUseNewAddress.click();
		}
	});

	seft.saveAddressMode.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		if (seft.validAddress()) {
			const obj = {
				PersonId: seft.labelMemberId.val(),
				CurrentAddressId: seft.inputCurrentAddress.data('currentaddressid'),
				CurrentAddress: seft.inputCurrentAddress.val(),
				CurrentAddressProvince: seft.inputCurrentAddressProvince.val(),
				CurrentAddressPostalCode: seft.inputCurrentAddressPostalCode.val(),
				ShipmentUseCurrentAddress: seft.radioShipmentUseCurrentAddress.prop('checked'),
				ShipmentUseNewAddress: seft.radioShipmentUseNewAddress.prop('checked'),
				ShipmentAddressId: seft.inputShipmentAddress.data('shipmentaddressid'),
				ShipmentAddress: seft.inputShipmentAddress.val(),
				ShipmentProvince: seft.inputShipmentProvince.find('option:selected').val(),
				ShipmentPostalCode: seft.inputShipmentPostalCode.val(),
				InvoiceUseCurrentAddress: seft.radioInvoiceUseCurrentAddress.prop('checked'),
				InvoiceUseShipmentAddress: seft.radioInvoiceUseShipmentAddress.prop('checked'),
				InvoiceUseNewAddress: seft.radioInvoiceUseNewAddress.prop('checked'),
				InputInvoiceAddressId: seft.inputInvoiceAddress.data('invoiceaddressid'),
				InvoiceAddress: seft.inputInvoiceAddress.val(),
				InvoiceProvince: seft.inputInvoiceProvince.find('option:selected').val(),
				InvoicePostalCode: seft.inputInvoicePostalCode.val(),
				Token: token,
				UpdatedBy: seft.updatedBy.data().empId
			};



			const success = function (data) {
				if (data.Success) {

					seft.labelCurrentAddress.text(seft.inputCurrentAddress.val());
					seft.labelCurrentAddressProvince.text(seft.inputCurrentAddressProvince.find('option:selected').text());
					seft.labelCurrentAddressPostalCode.text(seft.inputCurrentAddressPostalCode.val());

					seft.labelShipmentAddress.text(seft.inputCurrentAddress.val());
					seft.labelShipmentProvince.text(seft.inputShipmentProvince.find('option:selected').text());
					seft.labelShipmentPostalCode.text(seft.inputShipmentPostalCode.val());

					seft.labelInvoiceAddress.text(seft.inputInvoiceAddress.val());
					seft.labelInvoiceProvince.text(seft.inputInvoiceProvince.find('option:selected').text());
					seft.labelShipmentPostalCode.text(seft.inputShipmentPostalCode.val());

					seft.saveAddressMode.addClass('hide');
					seft.editAddressMode.removeClass('hide');
					$('.address-readonly').show();
					$('.address-edit').hide();

					seft.radioInvoiceUseCurrentAddress.attr('disabled', 'disabled');
					seft.radioInvoiceUseShipmentAddress.attr('disabled', 'disabled');
					seft.radioInvoiceUseNewAddress.attr('disabled', 'disabled');
					seft.radioShipmentUseCurrentAddress.attr('disabled', 'disabled');
					seft.radioShipmentUseNewAddress.attr('disabled', 'disabled');
				}
				else {
					let errorMsgFromServer = data.Message;
					if (errorMsgFromServer.length == 0) {
						toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
					} else {
						toastr.error(errorMsgFromServer, 'Oop!');
					}
				}
			};
			const failure = function (xhr, textstatus) {
				let errorMsgFromServer = xhr.responseText;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			};

			seft.service.updateAddress(obj, success, failure);
		}
		else {
			toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
		}

	});

	seft.buttonRewardRedemption.on('click', function () {
		///MemberDetail/PointAdjustment?personId=1039&source=1
		window.location.href = '/MemberDetail/PointAdjustment?personId=' + seft.labelMemberId.val() + '&source=' + seft.labelMemberId.data('source') + '#reward-redemption';
	});

	seft.buttonRecStatusAction.on('click', function () {
		const token = $("input[name='requestVerificationToken']").val();

		const obj = {
			PersonId: seft.labelMemberId.val(),
			UpdatedBy: seft.updatedBy.data().empId,
			Token: token,
			RecStatus: seft.buttonRecStatusAction.find('span').data('status')
		};

		const success = function (data) {
			if (data.Success) {
				if (obj.RecStatus === 0) {
					seft.buttonRecStatusAction.find('span').data('status', 1);
					seft.buttonRecStatusAction.find('span').text('Unblock');

					seft.buttonRecStatus.data('status', 1);
					seft.buttonRecStatus.removeClass('btn-success').addClass('btn-danger');
					seft.buttonRecStatus.text('BLOCK');
				}
				else if (obj.RecStatus === 1) {
					seft.buttonRecStatusAction.find('span').data('status', 0);
					seft.buttonRecStatusAction.find('span').text('Block');

					seft.buttonRecStatus.data('status', 0);
					seft.buttonRecStatus.removeClass('btn-danger').addClass('btn-success');
					seft.buttonRecStatus.text('ACTIVE');
				}
			}
			else {
				let errorMsgFromServer = data.Message;
				if (errorMsgFromServer.length == 0) {
					toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
				} else {
					toastr.error(errorMsgFromServer, 'Oop!');
				}
			}
		};

		const failure = function (xhr, textstatus) {
			let errorMsgFromServer = xhr.responseText;
			if (errorMsgFromServer.length == 0) {
				toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
			} else {
				toastr.error(errorMsgFromServer, 'Oop!');
			}
		};

		seft.service.updateStatus(obj, success, failure);
	});
}

MemberDetail.prototype.validPerson = function () {
	this.resetError();
	this.validateSelect(this.inputPersonTitle.find('option:selected').val(), this.inputPersonTitle);
	this.validateEmpty(this.inputPersonFirstName.val(), this.inputPersonFirstName);
	this.validateEmpty(this.inputPersonLastName.val(), this.inputPersonLastName);
	this.validateEmpty(this.inputPersonDob.val(), this.inputPersonDob);
	this.validateEmpty(this.inputPersonPhone.val(), this.inputPersonPhone);
	this.validateEmpty(this.inputPersonEmail.val(), this.inputPersonEmail);
	this.validateName(this.inputPersonFirstName.val(), this.inputPersonFirstName);
	this.validateName(this.inputPersonLastName.val(), this.inputPersonLastName);
	this.validateDOB(this.inputPersonDob.val(), this.inputPersonDob);
	this.validatePhone(this.inputPersonPhone.val(), this.inputPersonPhone);
	this.validateEmail(this.inputPersonEmail.val(), this.inputPersonEmail);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

MemberDetail.prototype.validCorperate = function () {
	this.resetError();
	this.validateSelect(this.inputPersonTitle.find('option:selected').val(), this.inputPersonTitle);
	this.validateEmpty(this.inputPersonFirstName.val(), this.inputPersonFirstName);
	this.validateEmpty(this.inputPersonPhone.val(), this.inputPersonPhone);
	this.validateEmpty(this.inputPersonEmail.val(), this.inputPersonEmail);
	this.validateName(this.inputPersonFirstName.val(), this.inputPersonFirstName);
	this.validatePhone(this.inputPersonPhone.val(), this.inputPersonPhone);
	this.validateEmail(this.inputPersonEmail.val(), this.inputPersonEmail);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

MemberDetail.prototype.validAddress = function () {
	this.resetError();

	this.validateEmpty(this.inputCurrentAddress.val(), this.inputCurrentAddress);
	this.validateSelect(this.inputCurrentAddressProvince.find('option:selected').val(), this.inputCurrentAddressProvince);
	this.validateEmpty(this.inputCurrentAddressPostalCode.val(), this.inputCurrentAddressPostalCode);

	this.validateEmpty(this.inputShipmentAddress.val(), this.inputShipmentAddress);
	this.validateSelect(this.inputShipmentProvince.find('option:selected').val(), this.inputShipmentProvince);
	this.validateEmpty(this.inputShipmentPostalCode.val(), this.inputShipmentPostalCode);

	this.validateEmpty(this.inputInvoiceAddress.val(), this.inputInvoiceAddress);
	this.validateSelect(this.inputInvoiceProvince.find('option:selected').val(), this.inputInvoiceProvince);
	this.validateEmpty(this.inputInvoicePostalCode.val(), this.inputInvoicePostalCode);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

MemberDetail.prototype.resetError = function () {
	for (var i = 0; i < this.validator.basket.length; i++) {
		var element = this.validator.basket[i];
		element.removeAttr("style", "border-color:red");
	}
	this.validator.basket = [];
}

MemberDetail.prototype.validateSelect = function (str, element) {
	return this.validator.selected(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validateName = function (str, element) {
	return this.validator.name(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validateDOB = function (str, element) {
	return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validatePhone = function (str, element) {
	return this.validator.phone(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validateEmail = function (str, element) {
	return this.validator.email(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validateEmpty = function (str, element) {
	return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validatePassword = function (str, element) {
	return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

MemberDetail.prototype.validateBothPassword = function (password, cpassword, element) {
	if (password !== cpassword) {
		this.validator.basket.push(element);
		return false;
	}
	return true;
}

MemberDetail.prototype.validateShoudnotSame = function (srcvalue, tarvalue, element) {
	if (srcvalue == tarvalue) {
		this.validator.basket.push(element);
		return false;
	}
}
export default MemberDetail;