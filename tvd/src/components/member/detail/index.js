﻿import Bacon from 'baconjs';
import MemberDetail from './memberdetail';
import MemberDetailService from './memberdetail.service';
import toastr from 'toastr';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	if (pageTypeId === 57) {
		$('.person-edit').hide();
		$('.corporate-edit').hide();
		$('.address-edit').hide();
		$('#person-dtp input').datepicker({
			format: 'dd/mm/yyyy',
			multidate: false,
			todayHighlight: true
		});

		const options = {
			labelMemberId: '#member-id',
			labelPersonTitleName: '#person-titlename-label',
			labelPersonFirstName: '#person-firstname-label',
			labelPersonLastName: '#person-lastname-label',
			labelPersonDob: '#person-dob-label',
			labelPersonIsSpecificYear: '#chk-not-specific-year-label',
			labelPersonPhone: '#person-phone-label',
			labelPersonEmail: '#person-email-label',
			inputPersonTitle: '#person-title',
			inputPersonFirstName: '#person-firstName',
			inputPersonLastName: '#person-lastName',
			inputPersonDob: '#dob',
			inputPersonIsSpecificYear: '#chk-not-specific-year',
			inputPersonPhone: '#person-phone',
			inputPersonEmail: '#person-email',

			labelCorporateTitleName: '#corporate-titlename-label',
			labelCorporateName: '#corporate-name-label',
			labelCorporatePhone: '#corporate-phone-label',
			labelCorporateEmail: '#corporate-email-label',
			inputCorporateTitle: '#corporate-title',
			inputCorporateName: '#corporate-name',
			inputCorporatePhone: '#corporate-phone',
			inputCorporateEmail: '#corporate-email',

			labelCurrentAddress: '#current-address-label',
			labelCurrentAddressProvince: '#current-address-province-label',
			labelCurrentAddressPostalCode: '#current-address-postalcode-label',
			inputCurrentAddress: '#current-address',
			inputCurrentAddressProvince: '#current-province',
			inputCurrentAddressPostalCode: '#current-address-postalcode',

			radioShipmentUseCurrentAddress: '#shipment-use-current-address',
			radioShipmentUseNewAddress: '#shipment-use-new-address',
			labelShipmentAddress: '#shipment-address-label',
			labelShipmentProvince: '#shipment-province-label',
			labelShipmentPostalCode: '#shipment-postal-label',
			inputShipmentAddress: '#shipment-address',
			inputShipmentProvince: '#shipment-province',
			inputShipmentPostalCode: '#shipment-postalcode',

			radioInvoiceUseCurrentAddress: '#invoice-use-current-address',
			radioInvoiceUseShipmentAddress: '#invoice-use-shipment-address',
			radioInvoiceUseNewAddress: '#invoice-use-new-address',
			labelInvoiceAddress: '#invoice-address-label',
			labelInvoiceProvince: '#invoice-province-label',
			labelInvoicePostalCode: '#invoice-postal-label',
			inputInvoiceAddress: '#invoice-address',
			inputInvoiceProvince: '#invoice-province',
			inputInvoicePostalCode: '#invoice-postalcode',

			editAddressMode: '#edit-address-mode',
			saveAddressMode: '#save-address-mode',
			editPersonOrCorperateMode: '#edit-person-mode',
			savePersonOrCorperateMode: '#save-person-mode',

			buttonRewardRedemption: '#btn-reward-redemption',
			buttonRecStatus: '#btn-rec-status',
			buttonRecStatusAction: '#btn-rec-status-action',
			buttonRemoveRelateAccount: '#btn-remove-relate-account',

			updatedBy: '#hidden-routeinfo'
		};

		const memberDetailForm = new MemberDetail(options);
	}
});