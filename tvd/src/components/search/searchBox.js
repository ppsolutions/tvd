import Bacon from 'baconjs';
import TextEditor from './textEditor';
import Autocomplete from './autocomplete';
import AutocompleteTemplate from './autoCompletePanel.hbs';
import toastr from 'toastr';

export default class SearchBox {

    constructor(options) {

        this.textEditor = new TextEditor({ element: options.searchName, limit: 3 });
        this.textPhone = $(options.searchPhone);
		this.textMember = $(options.searchMember);
		this.textIdCard = $(options.searchIdCard);
        this.searchResultUrl = "";
        this.searchButton = $(options.searchButton);

        this.autocomplete = new Autocomplete({
            element: $(options.autocompleteSelector),
            template: AutocompleteTemplate,
            textElement: $(options.searchName),
            onSelected: function (item) {
                var data = {
                    text: item.getAttribute("data-text"),
                    url: item.getAttribute("data-url")
                }
                this.setData(data);
            }
        });

        eventBinding(this);
    }

}

function eventBinding(searchbox) {
    const textEditor = searchbox.textEditor;
    const autocomplete = searchbox.autocomplete;
    const searchButton = searchbox.searchButton;

    Bacon.fromEvent(textEditor, 'focus')
    .debounce(200)
    .map(function (value) {
        return textEditor.getValue();
    })
    .onValue(function (value) {
        renderAutocomplete.bind(searchbox)(value);
    });

    Bacon.fromEvent(textEditor, "enterKey").onValue(function () {
        searchbox.search();
    });

    Bacon.fromEvent(textEditor, 'mouseup')
    .onValue(function () {
        textEditor.toggleSelectAllText();
    });

    Bacon.fromEvent(textEditor, 'textChanged')
    .debounce(200)
    .map(function (value) {
        return textEditor.getValue();
    })
    .onValue(function (value) {
        renderAutocomplete.bind(searchbox)(value);
    });

    Bacon.fromEvent(textEditor, 'blur')
    .onValue(
        autocomplete.hide.bind(autocomplete)
    );

    Bacon.fromEvent(autocomplete, 'select')
    .onValue(function (data) {
        if (data != undefined) {
            window.location = data.url;
            return;
        }
    });

    Bacon.fromEvent(autocomplete, 'updated')
    .onValue(function (searchResultUrl) {
        if (textEditor.isFocus()) {
            autocomplete.show();
            searchbox.searchResultUrl = searchResultUrl;
        }
    });

    Bacon.fromEvent(autocomplete, 'toggle')
    .map(function (isShowed) {
        return isShowed ? 'visible' : 'hidden';
    })
    .onValue(function (classValue) {

    });

    searchButton.on("click", function(){
        searchbox.search();
    });
}

function renderAutocomplete(value) {
    this.autocomplete.hide();

    if (this.textEditor.validate(value).valid) {

		const searchOptions = {
			searchIdCard: this.textIdCard.val(),
            searchPhone: this.textPhone.val(),
            searchMember: this.textMember.val()
        };

        this.autocomplete.searchAutocomplete(value, searchOptions);
    }
    else {
        this.autocomplete.clear();
    }
}

SearchBox.prototype.search = function () {

    if (this.textEditor.getValue() === '' && this.textIdCard.val() === '' && this.textPhone.val() === '' && this.textMember.val() === '') {
        return;
    }
    const dto = {
            SearchName: this.textEditor.getValue(),
			SearchIdCard: this.textIdCard.val(),
            SearchPhone: this.textPhone.val(),
            SearchMember: this.textMember.val()
        };

        const location = window.location.protocol + '//' + window.location.host;
        const searchMembersUrl = location + '/api/search/memberUrl';

        $.ajax({
            type: "POST",
            url: searchMembersUrl,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(dto),
            success: function(data) {
                if (data.Success) {
                    window.location.href = data.ReturnUrl;
                }
            },
            error: function(xhr, status) {
                toastr.error(status, 'Oop!');
            },
            timeout: 9999
        });

}