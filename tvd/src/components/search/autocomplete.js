const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import toastr from 'toastr';

export default class Autocomplete {
    constructor(options) {
        EventEmitter.call(this);
        
        this.element = options.element;
        this.template = options.template;
        this.textElement = options.textElement;

        eventBinding(this);
    }
}

inherits(Autocomplete, EventEmitter);

function eventBinding(autocomplete) {
    autocomplete.element.on('mousedown', 'li', function () {
        autocomplete.emit('select', $(this).data());
    });
}

Autocomplete.prototype.searchAutocomplete = function (text, opts) {

    const dto = {
		SearchName: text,
		SearchIdCard: opts.SearchIdCard,
        SearchPhone: opts.searchPhone,
        SearchMember: opts.searchMember
    };

    const autocomplete = this;
    const location = window.location.protocol + '//' + window.location.host;
    const searchAutocompleteUrl = location + '/api/search/memberName';

    $.ajax({
        type: "POST",
        url: searchAutocompleteUrl,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dto),
        success: function (data) {
            autocomplete.renderTemplateWithData(data);
        },
        error: function (xhr, status) {
            toastr.error(status, 'Oop!');
        },
        timeout: 9999
    });
};

Autocomplete.prototype.renderTemplateWithData = function (data) {

    const result = {
        items: data.ResponseData,
        search: this.textElement.val(),
        searchResultUrl: data.ReturnUrl
    };

    const compiledHtml = this.template(result);
    this.element.html(compiledHtml);
    this.emit('updated', result.searchResultUrl);
};

Autocomplete.prototype.show = function () {
    if (!isEmpty(this.element.text())) {
        this.element.show();
        this.emit('toggle', true);
    }
};

Autocomplete.prototype.hide = function () {
    this.element.hide();
    this.emit('toggle', false);
};

Autocomplete.prototype.clear = function () {
    this.renderTemplateWithData("");
};

function isEmpty(str) {
    return /^\s*$/.test(str);
}
