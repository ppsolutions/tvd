import toastr from 'toastr';
import Bacon from 'baconjs';
import SearchBox from './searchBox';
import '../plugin/jquery.rangyinputs';
import './search.css';

$(document).ready(function (){

    const options = {
		searchName: '#search-text',
		searchIdCard: '#search-id-card',
        searchPhone: '#search-phone',
        searchMember: '#search-member',
        searchButton: '#search-button',
        autocompleteSelector: '#auto-complete-panel'
    };

    const searchBox = new SearchBox(options);

});