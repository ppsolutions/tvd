const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');

export default class TextEditor {
    constructor(options) {
        EventEmitter.call(this);
        this.txtElement = $(options.element);
        this.limit = options.limit;
        eventBinding(this);
    }
}

inherits(TextEditor, EventEmitter);

function eventBinding(textEditor) {
    const seft = textEditor;

    seft.txtElement.on('keyup', function (e) {
        if (e.keyCode == 13) {
            seft.emit('enterKey');
        }
        else if ([37, 38, 39, 40].indexOf(e.keyCode) === -1) {
            // do not emit the textChanged event if the key pressed is left, up, right or down
            seft.emit('textChanged', seft.getValue());
        }
    });

    seft.txtElement.on('focus', function () {
        seft.emit('focus');
    });

    seft.txtElement.on('blur', function () {
        seft.emit('blur');
    });

    seft.txtElement.on('mouseup', function () {
        seft.emit('mouseup');
    });
}

TextEditor.prototype.getValue = function () {
    return this.txtElement.val();
};

TextEditor.prototype.setValue = function (value) {
    this.txtElement.val(value);
};

TextEditor.prototype.toggleSelectAllText = function () {
    var selectedTextRange = this.txtElement.getSelection();
    if (selectedTextRange.length === 0)
        this.txtElement.select();
};

TextEditor.prototype.isFocus = function () {
    return this.txtElement.is(':focus');
};

TextEditor.prototype.validate = function (value) {
    value = value || this.getValue();
    let result = {
        valid: true,
        message: null
    };
    if (!value || !(value.length >= this.limit)) {
        result.valid = false;
    }
    return result;
}