﻿export default class ReportService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.reportListUrl = location + '/api/auditlog/fetch';
		this.memberPointUrl = location + '/api/report/memberpoint';
		this.memberPointExportUrl = location + '/api/report/memberpointexport';
		this.exportUrl = location + '/api/auditlog/export';
		this.memberPointExpireUrl = location + '/api/report/memberpointexpiry';
		this.memberPointExpiryExportUrl = location + '/api/report/memberpointexport';
    }


    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.reportListUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetchMemberPoint(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.memberPointUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
	}

	fetchMemberPointExpire(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.memberPointExpireUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	export(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.exportUrl,
			headers: {
				"token": dto.token
			},
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	exportMemberPoint(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.memberPointExportUrl,
			headers: {
				"token": dto.token
			},
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	exportMemberPointExpire(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.memberPointExpiryExportUrl,
			headers: {
				"token": dto.token
			},
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}
}