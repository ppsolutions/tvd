const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
const moment = require('moment');
import ReportService from './report.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import MemberTemplate from './member.hbs';
import LevelTemplate from './level.hbs';
import ChangePasswordTemplate from './changepassword.hbs';
import RedemptionTemplate from './redemption.hbs'
import UpdateMemberPointTemplate from './updatePoint.hbs';
import ExportService from '../shared/export';

class Report {
	constructor(options) {
		EventEmitter.call(this);

		this.service = new ReportService();
		this.exportService = new ExportService();
		this.pageTypeId = options.pageTypeId;
		this.memberTemplate = MemberTemplate;
		this.levelTemplate = LevelTemplate;
		this.changePasswordTemplate = ChangePasswordTemplate;
		this.redemptionTemplate = RedemptionTemplate;
		this.updateMemberPointTemplate = UpdateMemberPointTemplate;
		this.pagerTemplate = PagerTemplate;
		this.element = $(options.element);
		this.createdBy = $(options.createdBy);
		this.level = $(options.level);
		this.recStatus = $(options.recStatus);
		this.fromDate = $(options.fromDate);
		this.toDate = $(options.toDate);
		this.objectId = $(options.objectId);
		this.clearButton = $(options.clearButton);
		this.searchButton = $(options.searchButton);
		this.exportButton = $(options.exportButton);
		this.pager = $(options.pager.id);
		this.auditType = options.auditType;
		this.defaultPageSize = options.pager.defaultPageSize;
		this.token = options.token;
		this.by = options.updatedBy;

		eventBinding(this);
	}
}

inherits(Report, EventEmitter);

function eventBinding(from) {
	const seft = from;

	$(seft.clearButton).click(
		function () {
			seft.fromDate.val('');
			seft.toDate.val('');
			seft.recStatus.val('');
			seft.createdBy.val('');
			seft.element.html('');
			seft.objectId.val('');
			seft.pager.html('');
		});
	$(seft.searchButton).click(
		function () {
			var pager = {
				pageSize: seft.defaultPageSize,
				pageNumber: 1
			};
			seft.fetch(pager);
		});

	$(seft.pager).on("click", "ul#paging > li > a", function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: $(this).data().pn
		};
		seft.fetch(pager);
	});

	seft.exportButton.on('click', function () {
		const dto = {
			roleId: seft.createdBy.val() === "" ? null : seft.createdBy.val(),
			fromDate: seft.fromDate.val() === "" ? null : moment(seft.fromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
			toDate: seft.toDate.val() === "" ? null : moment(seft.toDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
			type: seft.auditType,
			recStatus: seft.recStatus.val() === "" ? null : seft.recStatus.val(),
			pager: {
				pageSize: 100000,
				pageNumber: 1
			},
			token: seft.token
		}

		if (seft.pageTypeId === 45) {
			dto.fields = ['DateTime', 'MemberCode', 'MemerName', 'RegisterBy', 'Status'];
			dto.filename = 'report_new_member.csv';
		}
		else if (seft.pageTypeId === 46) {
			dto.value = seft.level.val() === "" ? null : seft.level.val();
			dto.fields = ['DateTime', 'MemberCode', 'MemerName', 'UpgradeTo', 'By', 'Status'];
			dto.filename = 'report_upgrade_member.csv';
		}
		else if (seft.pageTypeId === 47) {
			dto.fields = ['DateTime', 'MemberCode', 'CancelledMember', 'By', 'Status'];
			dto.filename = 'report_cancel_member.csv';
		}
		else if (seft.pageTypeId === 48) {
			dto.fields = ['Date Time', 'MemberCode', 'BlockedMember', 'By', 'Status'];
			dto.filename = 'report_block_member.csv';
		}
		else if (seft.pageTypeId === 49) {
			dto.fields = ['DateTime', 'MemberCode', 'Reward', 'UsedPoint', 'UsedMoney', 'By', 'Status'];
			dto.filename = 'report_reward_redemption.csv';
		}
		else if (seft.pageTypeId === 52) {
			//Update member point 
			dto.objectId = seft.objectId.val() === "" ? null : seft.objectId.val();
			dto.fields = ['DateTime', 'MemberCode', 'MemberName', 'UpdatedPoint', 'CurrentTotalPoint', 'By'];
			dto.filename = 'report_update_member_point.csv';
		}
		else if (seft.pageTypeId === 53) {
			//Reject member point
			dto.objectId = seft.objectId.val() === "" ? null : seft.objectId.val();
			dto.recStatus = -1;
			dto.fields = ['DateTime', 'MemberCode', 'MemberName', 'CancelledPoint', 'CurrentTotalPoint', 'By'];
			dto.filename = 'report_cancel_member_point.csv';
		}
		//else if (seft.pageTypeId === 54) {
		//	dto.fields = ['DateTime', 'EmployeeCode', 'ChangedBy', 'MemberCode', 'MemberName'];
		//	dto.filename = 'report_change_password.csv';
		//}
		else if (seft.pageTypeId === 55) {
			dto.fields = ['DateTime', 'EmployeeCode', 'ChangedBy', 'MemberCode', 'MemberName'];
			dto.filename = 'report_view_member_history.csv';
		}
		else if (seft.pageTypeId === 56) {
			dto.fields = ['DateTime', 'EmployeeCode', 'ViewedBy', 'ViewedReport'];
			dto.filename = 'report_view_report.csv';
		}

		seft.export(dto);
	});
}


Report.prototype.fetch = function (pager) {

	const seft = this;

	let dto = {
		roleId: seft.createdBy.val() === "" ? null : seft.createdBy.val(),
		fromDate: seft.fromDate.val() === "" ? null : moment(seft.fromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.toDate.val() === "" ? null : moment(seft.toDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		type: seft.auditType,
		recStatus: seft.recStatus.val() === "" ? null : seft.recStatus.val(),
		pager: pager
	}

	if (seft.pageTypeId === 46) {
		dto.value = seft.level.val() === "" ? null : seft.level.val();
	}
	else if (seft.pageTypeId === 52) {
		//Update member point 
		dto.objectId = seft.objectId.val() === "" ? null : seft.objectId.val();
	}
	else if (seft.pageTypeId === 53) {
		//Reject member point
		dto.objectId = seft.objectId.val() === "" ? null : seft.objectId.val();
		dto.recStatus = -1;
	}
	const callback = function (data) {
		if (data.Success) {
			seft.renderTemplateWithData(data.ResponseData, data.Pager);
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.fetch(dto, callback, failure);
}

Report.prototype.export = function (dto) {
	const seft = this;
	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				if (seft.pageTypeId === 45) {
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemerName: obj.NewValueObject.FullName,
							RegisterBy: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')',
							Status: obj.RecStatusDescription
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 46) {
					//dto.fields = ['DateTime', 'MemberCode', 'MemerName', 'UpgradeTo', 'By', 'Status'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemerName: obj.DescriptionObject.Name,
							UpgradeTo: obj.DescriptionObject.Level,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')',
							Status: obj.RecStatusDescription
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 47) {
					//dto.fields = ['DateTime', 'MemberCode' ,'CancelledMember', 'By', 'Status'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemerName: obj.DescriptionObject.Name,
							UpgradeTo: obj.DescriptionObject.Level,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')',
							Status: obj.RecStatusDescription
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 48) {
					//dto.fields = ['Date Time', 'MemberCode', 'Blocked Member', 'By', 'Status'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemerName: obj.DescriptionObject.Name,
							UpgradeTo: obj.DescriptionObject.Level,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')',
							Status: obj.RecStatusDescription
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 49) {
					//dto.fields = ['DateTime', 'MemberCode', 'Reward', 'UsedPoint', 'UsedMoney', 'By', 'Status'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							Reward: obj.DescriptionObject.Product,
							UsedPoint: obj.DescriptionObject.Point,
							UsedMoney: obj.DescriptionObject.Money,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')',
							Status: obj.RecStatusDescription
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 52) {
					//dto.fields = ['DateTime', 'MemberCode', 'MemberName', 'UpdatedPoint', 'CurrentTotalPoint', 'By'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemberName: obj.DescriptionObject.Name,
							UpdatedPoint: obj.DescriptionObject.UpdatePoint,
							CurrentTotalPoint: obj.DescriptionObject.RamainPoint,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')'
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 53) {
					//dto.fields = ['DateTime', 'MemberCode', 'MemberName', 'CancelledPoint', 'CurrentTotalPoint', 'By'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							MemberCode: obj.ObjectCode,
							MemberName: obj.DescriptionObject.Name,
							CancelledPoint: obj.DescriptionObject.UpdatePoint,
							CurrentTotalPoint: obj.DescriptionObject.RamainPoint,
							By: obj.DescriptionObject.Source.FullName + ' (' + obj.DescriptionObject.Source.RoleName + ')'
						};
						exportData.push(item);
					}
				}
				
				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.export(dto, callback, failure);
}



Report.prototype.renderTemplateWithData = function (data, pager) {

	const location = 'http://' + window.location.host;
	const result = {
		items: data,
		baseUrl: location
	};

	Object.keys(result.items).forEach(function (key, index) {
		result.items[index].Description = JSON.parse(result.items[index].Description);
	});

	if (this.pageTypeId === 45 || this.pageTypeId === 47 || this.pageTypeId === 48) {
		//new / block / cancel user
		this.element.html(this.memberTemplate(result));
	}
	else if (this.pageTypeId === 46) {
		//Upgrade member
		this.element.html(this.levelTemplate(result));
	}
	else if (this.pageTypeId === 49) {
		//Redemption
		this.element.html(this.redemptionTemplate(result));
	}
	else if (this.pageTypeId === 52 || this.pageTypeId === 53) {
		//Update member point or Reject member point
		this.element.html(this.updateMemberPointTemplate(result));
	}
	else if (this.pageTypeId === 54) {
		//change password
		this.element.html(this.changePasswordTemplate(result));
	}
	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
};

export default Report;