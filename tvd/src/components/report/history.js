const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
const moment = require('moment');

import HistoryService from './history.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import MemberTemplate from './historyMember.hbs';
import HistoryTemplate from './history.hbs';
import ExportService from '../shared/export';

class History {
    constructor(options) {
        EventEmitter.call(this);

		this.exportService = new ExportService();
        this.service = new HistoryService();
        this.pageTypeId = options.pageTypeId;
        this.pageId = $(options.pageId);
        this.memberTemplate = MemberTemplate;
        this.historyTemplate = HistoryTemplate;
        this.pagerTemplate = PagerTemplate;
        this.element = $(options.element);
        this.fromDate = $(options.fromDate);
        this.toDate = $(options.toDate);
        this.clearButton = $(options.clearButton);
		this.searchButton = $(options.searchButton);
		this.exportButton = $(options.exportButton);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.token = options.token;
        this.by = options.updatedBy;

        eventBinding(this);
    }
}

inherits(History, EventEmitter);

function eventBinding(from) {
    const seft  = from;

    $(seft.clearButton).click(
        function() {
            seft.fromDate.val('');
            seft.toDate.val('');
            seft.element.html('');
            seft.pager.html('');
        });

    $(seft.searchButton).click(
        function() {
            var pager = {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            };
            seft.fetch(pager);  
        });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        var pager = {
            pageSize: seft.defaultPageSize,
            pageNumber: $(this).data().pn
        };
        seft.fetch(pager);  
    });

	seft.exportButton.on('click', function () {
		let dto = {
			fromDate: seft.fromDate.val() === "" ? null : seft.fromDate.val(),
			toDate: seft.toDate.val() === "" ? null : seft.toDate.val(),
			pageId: seft.pageId.val(),
			pager: {
				pageSize: 100000,
				pageNumber: 1
			},
			token: seft.token

		}

		if (seft.pageTypeId === 55) {
			dto.fields = ['DateTime', 'EmployeeCode', 'ChangedBy', 'MemberCode', 'MemberName'];
			dto.filename = 'report_view_member_history.csv';
		}
		else if (seft.pageTypeId === 56) {
			dto.fields = ['DateTime', 'EmployeeCode', 'ViewedBy', 'ViewedReport'];
			dto.filename = 'report_view_report.csv';
		}

		seft.export(dto);
	});

}


History.prototype.fetch = function(pager) {
    
    const seft = this;

    let dto = {
		fromDate: seft.fromDate.val() === "" ? null : moment(seft.fromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
		toDate: seft.toDate.val() === "" ? null : moment(seft.toDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
        pageId: seft.pageId.val(),
        pager: pager
    }

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

History.prototype.renderTemplateWithData = function (data, pager) {
    
    const location = 'http://' + window.location.host;
    const result = {
        items: data
    };

    if(this.pageTypeId === 55){
    //member history
        this.element.html(this.memberTemplate(result));
    }else if(this.pageTypeId === 56){
        this.element.html(this.historyTemplate(result));
    }
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

History.prototype.export = function (dto) {
	const seft = this;
	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				if (seft.pageTypeId === 55) {
					//dto.fields = ['DateTime', 'EmployeeCode', 'ChangedBy', 'MemberCode', 'MemberName'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							EmployeeCode: obj.EmployeeCode,
							ChangedBy: obj.EmployeeName,
							MemberCode: obj.PersonCode,
							MemberName: obj.PersonName
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 56) {
					//dto.fields = ['DateTime', 'EmployeeCode', 'ViewedBy', 'ViewedReport'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							DateTime: obj.RecCreatedWhen,
							EmployeeCode: obj.EmployeeCode,
							ViewedBy: obj.EmployeeName,
							ViewedReport: obj.SubPageName
						};
						exportData.push(item);
					}
				}

				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	this.service.export(dto, callback, failure);
}
export default History;