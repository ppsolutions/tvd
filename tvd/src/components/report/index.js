﻿import Report from './report';
import ReportMemberPoint from './reportMemberPoint';
import History from './history';
import toastr from 'toastr';

$(document).ready(function (){

    if($("#hidden-routeinfo").data() == undefined)
        return;
    
    const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
    const by = $("#hidden-routeinfo").data().empId;
    const token = $("input[name='requestVerificationToken']").val();
    const auditType = $("#audit-type-id").val();
    const pager = {
        pageSize: 15,
        pageNumber: 1
    };
   


    //Approve new member / update member
    if (pageTypeId === 45 || pageTypeId === 46 || pageTypeId === 47 || pageTypeId === 48 || pageTypeId === 49 || pageTypeId === 52 || pageTypeId === 53 || pageTypeId === 54) {


		$('#datetimepicker1 input').datepicker({
			format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });

		$('#datetimepicker2 input').datepicker({
			format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });


        const options = {
            element: "#result",
            createdBy: '#emp-role',
            level: '#point-level',
            objectId: '#person-id',
            recStatus: '#filter-dd',
            fromDate: '#from-date',
			searchButton: '#report-search-button',
			exportButton: '#report-export-button',
            clearButton: '#report-clear',
            toDate: '#to-date',
            auditType: auditType,
            updatedBy : by,
            pager: {
                id: "#pager",
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            token : token,
            pageTypeId : pageTypeId
        };

        const report = new Report(options);
        //report.fetch(pager);

    }else if(pageTypeId === 50 || pageTypeId === 51){

        const options = {
            element: "#result",
            expireYear: '#filter-dd',
			searchButton: '#report-search-button',
			exportButton: '#report-export-button',
            clearButton: '#report-clear',
            minPoint: '#min-point',
            maxPoint: '#max-point',
            updatedBy : by,
            pager: {
                id: "#pager",
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            token : token,
            pageTypeId : pageTypeId
        };

        const report = new ReportMemberPoint(options);
        //report.fetch(pager);
    } 
    
    else if(pageTypeId === 55 || pageTypeId === 56)
    {
        //History Member
		$('#datetimepicker1 input').datepicker({
			format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });

		$('#datetimepicker2 input').datepicker({
			format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });


        const options = {
            element: "#result",
            fromDate: '#from-date',
            pageId: '#page-id',
            searchButton:'#report-search-button',
			exportButton: '#report-export-button',
            clearButton: '#report-clear',
            toDate: '#to-date',
            updatedBy : by,
            pager: {
                id: "#pager",
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            token : token,
            pageTypeId : pageTypeId
        };

        const history = new History(options);
        //history.fetch(pager);
    }

});