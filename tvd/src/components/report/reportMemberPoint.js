const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import ReportService from './report.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import MemberPointTemplate from './memberPoint.hbs';
import ExportService from '../shared/export';

class ReportMemberPoint {
	constructor(options) {
		EventEmitter.call(this);

		this.exportService = new ExportService();
		this.service = new ReportService();
		this.pageTypeId = options.pageTypeId;
		this.memberPointTemplate = MemberPointTemplate;
		this.pagerTemplate = PagerTemplate;
		this.element = $(options.element);
		this.expireYear = $(options.expireYear);
		this.minPoint = $(options.minPoint);
		this.maxPoint = $(options.maxPoint);
		this.clearButton = $(options.clearButton);
		this.searchButton = $(options.searchButton);
		this.exportButton = $(options.exportButton);
		this.pager = $(options.pager.id);
		this.defaultPageSize = options.pager.defaultPageSize;
		this.token = options.token;
		this.by = options.updatedBy;

		eventBinding(this);
	}
}

inherits(ReportMemberPoint, EventEmitter);

function eventBinding(from) {
	const seft = from;

	$(seft.clearButton).click(
		function () {
			seft.minPoint.val('');
			seft.maxPoint.val('');
			seft.element.html('');
			seft.pager.html('');
		});

	$(seft.searchButton).click(
		function () {
			var pager = {
				pageSize: seft.defaultPageSize,
				pageNumber: 1
			};
			seft.fetch(pager);
		});

	$(seft.pager).on("click", "ul#paging > li > a", function () {
		var pager = {
			pageSize: seft.defaultPageSize,
			pageNumber: $(this).data().pn
		};
		seft.fetch(pager);
	});

	seft.exportButton.on('click', function () {
		let dto = {
			minPoint: seft.minPoint.val() === "" ? null : seft.minPoint.val(),
			maxPoint: seft.maxPoint.val() === "" ? null : seft.maxPoint.val(),
			pager: {
				pageSize: 100000,
				pageNumber: 1
			},
			token: seft.token
		};

		if (seft.pageTypeId === 50) {
			dto.fields = ['MemberCode', 'MemberName', 'TotalPoint'];
			dto.filename = 'report_member_total_point.csv';
		}
		else if (seft.pageTypeId === 51) {
			dto.expireYear = seft.expireYear.find("option:selected").val();
			dto.fields = ['MemberCode', 'MemberName', 'PointNearlyExpire', 'ExpireDate'];
			dto.filename = 'report_expiry_point.csv';
		}

		seft.export(dto);
	});
}


ReportMemberPoint.prototype.fetch = function (pager) {

	const seft = this;

	let dto = {
		minPoint: seft.minPoint.val() === "" ? null : seft.minPoint.val(),
		maxPoint: seft.maxPoint.val() === "" ? null : seft.maxPoint.val(),
		pager: pager,
		token: seft.token
	}

	const callback = function (data) {
		if (data.Success) {
			seft.renderTemplateWithData(data.ResponseData, data.Pager);
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	if (seft.pageTypeId === 51) {
		dto.expireYear = seft.expireYear.find("option:selected").val();
		this.service.fetchMemberPointExpire(dto, callback, failure);
	}
	else {
		this.service.fetchMemberPoint(dto, callback, failure);
	}	
}

ReportMemberPoint.prototype.export = function (dto) {
	const seft = this;
	const callback = function (data) {
		if (data.Success) {
			try {

				let exportData = [];

				if (seft.pageTypeId === 50) {
					//dto.fields = ['MemberCode', 'MemberName', 'TotalPoint'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							MemberCode: obj.PersonCode,
							MemberName: obj.PersonName,
							TotalPoint: obj.Points 
						};
						exportData.push(item);
					}
				}
				else if (seft.pageTypeId === 51) {
					//dto.fields = ['MemberCode', 'MemberName', 'PointNearlyExpire', 'ExpireDate'];
					for (let i = 0; i < data.ResponseData.length; i++) {
						const obj = data.ResponseData[i];
						let item = {
							MemberCode: obj.PersonCode,
							MemberName: obj.PersonName,
							TotalPoint: obj.Points,
							ExpireDate: obj.ExpiredDate
						};
						exportData.push(item);
					}
				}

				seft.exportService.download({ data: exportData, fields: dto.fields }, dto.filename);
			}
			catch (err) {
				toastr.error(err, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		toastr.error(xhr.statusText, 'Oop!');
	};

	if (seft.pageTypeId === 50) {
		this.service.exportMemberPoint(dto, callback, failure);
	}
	else if (seft.pageTypeId === 51) {
		this.service.exportMemberPointExpire(dto, callback, failure);
	}
}

ReportMemberPoint.prototype.renderTemplateWithData = function (data, pager) {

	const location = 'http://' + window.location.host;
	const result = {
		items: data,
		baseUrl: location
	};

	if (this.pageTypeId === 50 || this.pageTypeId === 51) {
		//active point
		this.element.html(this.memberPointTemplate(result));
	}
	if (pager) {
		this.pager.html(this.pagerTemplate(pager));
	}
};

export default ReportMemberPoint;