import NonEmptyTextValidator from './nonEmptyTextValidator';

export class DomSelectorValidator {
    constructor(selector) {
        const textValidator = new NonEmptyTextValidator();
        if (!textValidator.validate(selector).valid)
            throw Error("");

        this.selector = selector;
    }
}

DomSelectorValidator.prototype.validate = function () {

    var result = { valid: true, message: null };

    var textValidator = new NonEmptyTextValidator();
    if (!textValidator.validate(this.selector).valid)
        result = {
            valid: false,
            message: "Invalid selector value, cannot be null, empty or undefined"
        }
    else if ($(this.selector).length == 0)
        result = {
            valid: false,
            message: "Invalid selector value, No DOM element '" + this.selector + "' found"
        }

    return result;
};

export default DomSelectorValidator;
