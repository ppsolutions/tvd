﻿import RegexValidator from '../shared/regexValidator';

class Validator {
    constructor() {
        this.result = { valid: true, message: null };
        this.basket = [];
    }

 //   userName(value) {
 //       this.result.valid = false;
 //       const options = {
 //           regEx: /^[a-zA-Z0-9]+$/i
 //       }
 //       var regex = new RegexValidator(options);
 //       if (regex.validate(value).valid) {
 //           this.result.valid = true;
 //       }
 //       return this.result;
	//}

	userName(value) {
		this.result.valid = false;
		if (value.length > 0) {
			if (value.indexOf(' ') === -1) {
				this.result.valid = true;
			}
		}
		return this.result;
	}

    password(value) {
        this.result.valid = false;
        if (value.length > 2) {
            this.result.valid = true;
        }
        return this.result;
    }
    
    name(value) {
        this.result.valid = false;
        const options = {
            regEx: /^[\u0E00-\u0E7Fa-zA-Z\s]+$/i
        }
        var regex = new RegexValidator(options);
        if (regex.validate(value).valid) {
            this.result.valid = true;
        }
        return this.result;
    }

    phone(value) {
        this.result.valid = false;
        const options = {
            regEx: /^[0-9-]+$/
        }
        var regex = new RegexValidator(options);
        if (regex.validate(value).valid) {
            this.result.valid = true;
        }
        return this.result;
    }

    email(value) {
        this.result.valid = false;
        const options = {
            regEx: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
        }
        var regex = new RegexValidator(options);
        if (regex.validate(value).valid) {
            this.result.valid = true;
        }
        return this.result;
    }

    empty(value) {
        this.result.valid = false;
        if (value.length > 0) {
            this.result.valid = true;
        }
        return this.result;
    }

    selected(value) {
        this.result.valid = false;
        if (parseInt(value) > 0) {
            this.result.valid = true;
        }
        return this.result;
    }

    positiveNumber(value) {
        this.result.valid = false;
        const options = {
            regEx: /^[+]?\d+([.]\d+)?$/
        }
        var regex = new RegexValidator(options);
        if (regex.validate(value).valid) {
            this.result.valid = true;
        }
        return this.result;
    }
}

export default Validator;