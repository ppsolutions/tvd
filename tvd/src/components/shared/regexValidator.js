﻿
export default class RegexValidator {

    constructor(options) {
        this.result = { valid: true, message: null };

        if (typeof (options.regEx) === 'undefined' || options.regEx === null) {
            throw "invalid input: regEx should not be null";
        }
        if (typeof (options.regEx) === "string") {
            this.regEx = new RegExp(options.regEx);
        } else if (options.regEx instanceof RegExp) {
            this.regEx = options.regEx;
        } else {
            throw "invalid input: regEx should be either a string or a RegExp";
        }
    }
}

RegexValidator.prototype.validate = function (value) {
    if (!this.regEx.test(value)) {
        this.result.valid = false;
    }

    return this.result;
};