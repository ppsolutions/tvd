import json2csv from 'json2csv';
import fs from 'browserify-fs';

export default class ExportService {
    constructor() {

    }
}

ExportService.prototype.download = function(data, fileName) {
    const seft = this;
    const csv = json2csv({ data: data.data, fields: data.fields });
    if(csv) {
        seft.save(csv, fileName, 'text/csv;encoding:utf-8;charset=utf-8;');
    }
}

ExportService.prototype.save = function(content, fileName, mimeType) {
    let a = document.createElement('a');
    mimeType = mimeType || 'application/octet-stream';
  
    if (navigator.msSaveBlob) { // IE10
      navigator.msSaveBlob(new Blob(["\ufeff", content], {
        type: mimeType
      }), fileName);
    } else if (URL && 'download' in a) { //html5 A[download]
      a.href = URL.createObjectURL(new Blob(["\ufeff", content], {
        type: mimeType
      }));
      a.setAttribute('download', fileName);
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    } else {
      location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
    }
}