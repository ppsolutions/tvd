
export default class NotNullValidator {
    constructor(options) {
        options = options || {};
        options.message = options.message || "";
        options.type = options.type || "object";
        this.options = options;
    }
};

NotNullValidator.prototype.validate = function (value) {
    var result = { valid: true, message: this.options.message };
    if (value === undefined || value === null) {
        result.valid = false;
        return result;
    }
    return result;
};

