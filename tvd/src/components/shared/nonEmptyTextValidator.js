import NotNullValidator from './notNullValidator';

export default class NonEmptyTextValidator {
    constructor(options) {
        options = options || {};
        options.message = options.message || "";
        this.options = options;
    }
};

NonEmptyTextValidator.prototype.validate = function (value) {
    let result = { valid: true, message: null };
    const notNullValidator = new NotNullValidator({ type: String, message: "Invalid input" });
    if (!notNullValidator.validate(value).valid || value === "") {
        result.valid = false;
        result.message = this.options.message;
    }
    return result;
};
