﻿import $ from 'jquery';

export default class LoginService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.loginUrl = location + '/api/employee/login';
    }

    logIn(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.loginUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}