﻿import Bacon from 'baconjs';
import Login from './login';
import LoginService from './login.Service';
import toastr from 'toastr';
import './login.css';

$(document).ready(function () {

    const options = {
        userName: '#signin-email',
        userPassword: '#signin-password',
        loginSubmit: '#signin-submit'
    };

    const location = 'http://' + window.location.host;
    const loginForm = new Login(options);
    const service = new LoginService();

    loginForm.on("submit", function (userName, password) {
        
        if(this.valid()) {

            const callback = function (data) {
                if (data.Success) {
                    window.location.href = location + data.ReturnUrl;
                }
                else {
                    toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
                }
            };

            const failure = function (xhr, textstatus) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            };

            let dto = {
                userName: userName.val(),
                password: password.val(),
                token: $("input[name='requestVerificationToken']").val(),
                isRemember: $("#signin-remember").prop( "checked" )
            };

            service.logIn(dto, callback, failure);
        }
    });


});