﻿var $ = require('jquery');
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../shared/validator';

class Login {
    constructor(options) {
        EventEmitter.call(this);
        this.userName = $(options.userName);
        this.password = $(options.userPassword);
        this.submit = $(options.loginSubmit);
        this.validator = new Validator();

        eventBinding(this);
    }
}

inherits(Login, EventEmitter);

function eventBinding(login) {
    const seft = login;

    seft.submit.on("click", function () {
        login.emit('submit', login.userName, login.password);
    });

    seft.userName.on("keyup", function (e) {
        if (e.which === 13) {
            seft.emit('submit', seft.userName, seft.password);
        }

        if(seft.validator.userName(seft.userName.val()).valid){
            seft.userName.attr("style", "border-color:#ccc");
        }
        else {
            seft.userName.attr("style", "border-color:red");
        }
    });

    seft.password.on("keyup", function (e) {
        if (e.which === 13) {
            seft.emit('submit', seft.userName, seft.password);
        }

        if(seft.validator.password(seft.password.val()).valid){
            seft.password.attr("style", "border-color:#ccc");
        }
        else {
            seft.password.attr("style", "border-color:red");
        }
    });
}

Login.prototype.valid = function () {
    this.reset();

    if(this.userName.val().length === 0 && this.password.val().length === 0) {
        this.userName.attr("style", "border-color:red");
        this.password.attr("style", "border-color:red");
        return;
    }

    if(!this.validator.userName(this.userName.val()).valid)
    {
        this.userName.attr("style", "border-color:red");
        return false;
    }
    else if(!this.validator.password(this.password.val()).valid)
    {
        this.password.attr("style", "border-color:red");
        return false;
    }

    return true;
}

Login.prototype.reset = function() {
    this.userName.attr("style", "border-color:#ccc");
    this.password.attr("style", "border-color:#ccc");
}

export default Login;