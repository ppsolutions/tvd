﻿var Handlebars = require('handlebars-template-loader/runtime');
var moment = require('moment');

Handlebars.registerHelper('replace_and_remove_html_tags', function (str, replaceStr, searchStr) {
    var returnVal = str;

    //replace
    returnVal = returnVal.replace(searchStr, replaceStr);

    //-- remove BR tags and replace them with line break
    returnVal = returnVal.replace(/<br>/gi, "\n");
    returnVal = returnVal.replace(/<br\s\/>/gi, "\n");
    returnVal = returnVal.replace(/<br\/>/gi, "\n");

    return returnVal;

});

Handlebars.registerHelper('decimal_fixed', function (number, fragment) {
    return number.toFixed(fragment);
});

Handlebars.registerHelper('replacetext', function (str, replaceStr, searchStr) {
    var result = str.replace(searchStr, replaceStr);
    return result;
});

Handlebars.registerHelper('times', function (n, block) {
    var accum = '';
    for (var i = 0; i < n; ++i)
        accum += block.fn(i);
    return accum;
});

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
Handlebars.registerHelper('replace', function (str, replaceStr, tagname) {
    return new Handlebars.SafeString(str.replace(new RegExp(escapeRegExp(tagname), 'g'), replaceStr));
});

Handlebars.registerHelper('for', function (from, to, incr, block) {
    var accum = '';
    for (var i = from; i < to; i += incr)
        accum += block.fn(i);
    return accum;
});

Handlebars.registerHelper('for_eq', function (from, to, incr, block) {
    var accum = '';
    for (var i = from; i <= to; i += incr)
        accum += block.fn(i);
    return accum;
});

Handlebars.registerHelper('commalist', function (items, options) {
    var out = '';
    out = items.join(', ');
    return out;
});

Handlebars.registerHelper('for_eq_or', function (from, to, or, incr, block) {
    var accum = '';
    for (var i = from; i <= to && i <= or; i += incr)
        accum += block.fn(i);
    return accum;
});

Handlebars.registerHelper('if_not_equals', function (v1, v2, options) {
    if (v1 !== v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_equals', function (v1, v2, options) {
    if (v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_any', function (v1, v2, options) {
    if (v1 || v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_equals_even', function (v1, options) {
    if (v1 % 2 == 0) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_null', function (v1, options) {
	if (v1 === null) {
		return options.fn(this);
	}
	return options.inverse(this);
});

Handlebars.registerHelper('if_not_null', function (v1, options) {
    if (v1 !== null) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_n1_or_n2_not_null', function (v1, v2, options) {
    if (v1 !== null || v2 != null) {
        return options.fn(this);
    }
    return options.inverse(this);
});


Handlebars.registerHelper('if_not_null_not_blank', function () {
    var args = $.makeArray(arguments);

    for (var i = 0; i < args.length - 1; i++) {
        if (args[i] !== null && args[i] != "" && args[i] != undefined) {
            return args[args.length - 1].fn(this);
        }
    }
    return args[args.length - 1].inverse(this);
});

Handlebars.registerHelper('if_gt', function (v1, v2, options) {
    if (v1 > v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_date_gt', function (v1, v2, options) {
    // works with ms implementation
    var d1 = new Date(parseInt(v1.substr(6)));
    var d2 = new Date(v2);
    if (d1 > d2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper("equals", function (a, b) {
    return (a == b);
});

Handlebars.registerHelper('if_gt_equals', function (v1, v2, options) {
    if (v1 >= v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_lt', function (v1, v2, options) {
    if (v1 < v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_lt_equals', function (v1, v2, options) {
    if (v1 <= v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_lt_first_item_and_gt_second_item', function (v1, v2, v3, options) {
    if ((v1 <= v2) && (v1 > v3)) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper("math", function (lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});

Handlebars.registerHelper('is_true', function (v1, options) {
    if (v1 === true) {
        return options.fn(this);
    }
    return options.inverse(this);
});


Handlebars.registerHelper('string_format', function () {
    var string = arguments[0];
    return string.format.apply(string, arguments);
});

Handlebars.registerHelper('each_facilities', function (facilities, options) {
    var str = '';
    if (facilities != null && facilities != "" && facilities != undefined) {
        var col1 = [];
        var col2 = [];
        var col3 = [];

        var col1Index = 0;
        var col2Index = 0;
        var col3Index = 0;

        for (var i = 0; i < facilities.length; i++) {
            if (i % 3 == 0) {
                col1[col1Index] = facilities[i];
                col1Index++;
            } else if (i % 3 == 1) {
                col2[col2Index] = facilities[i];
                col2Index++;
            } else if (i % 3 == 2) {
                col3[col3Index] = facilities[i];
                col3Index++;
            }
        }

        var str = '<ul>';
        for (var i = 0; i < col1.length; i++) {
            str += '<li class=\"green\"><span class=\"font\">' + col1[i] + '</span></li>';
        }
        str += '</ul>'

        str += '<ul>';
        for (var i = 0; i < col2.length; i++) {
            str += '<li class=\"green\"><span class=\"font\">' + col2[i] + '</span></li>';
        }
        str += '</ul>'

        str += '<ul>';
        for (var i = 0; i < col3.length; i++) {
            str += '<li class=\"green\"><span class=\"font\">' + col3[i] + '</span></li>';
        }
        str += '</ul>'

    }
    return str;
});

Handlebars.registerHelper('if_gt_list', function (v1, v2, options) {
    if (v1.length > v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('max_length', function (v1) {
    return v1.length;
});

Handlebars.registerHelper('each_images', function (obj, options) {
    var str = '';
    if (obj != null && obj != "" && obj != undefined) {
        var data = obj.join(',');
        if (data != null) {
            var images = data.split(',');

            for (var row = 0; row < images.length; row++) {
                if (images[row] != null && images[row] != "") {
                    if (row == 0) {
                        str += "<img width='312px' height='235px' id='" + row + "' src='" + images[row] + "' style='display:block;' />";
                    } else {
                        str += "<img width='312px' height='235px' id='" + row + "' src='" + images[row] + "' style='display:none;' />";
                    }
                }
            }
        }
    }
    return str;
});

Handlebars.registerHelper('each_extrabed_policies', function (obj, options) {
    var str = '';
    if (obj != null && obj != "" && obj != undefined) {
        for (var row = 0; row < obj.length; row++) {
            if (obj[row] != null && obj[row] != "") {
                str += "<p class='space'>";
                str += "<i class=\"gbullet room-info-sprite\"></i>";
                str += obj[row].Description + "";
                str += "</p>";
            }
        }
    }
    return str;
});

Handlebars.registerHelper('replace_dot', function (v1) {
    return v1.toString().split('.').join('');
});

Handlebars.registerHelper('options', function (value, options) {
    return value.html();
});

Handlebars.registerHelper("count", function (items, options) {
    return items.length;
});

Handlebars.registerHelper('inc', function (number, options) {
    if (typeof (number) === 'undefined' || number === null)
        return null;

    // Increment by inc parameter if it exists or just by one
    return number + (options.hash.inc || 1);
});

Handlebars.registerHelper('dec', function (number, options) {
    if (typeof (number) === 'undefined' || number === null)
        return null;

    // Decrement by inc parameter if it exists or just by one
    return number - (options.hash.inc || 1);
});

Handlebars.registerHelper('formatCurrency', function (value) {
    return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
});

Handlebars.registerHelper('formatDecimal', function (value) {
    return parseFloat(value.toString().trim()).toFixed(2);
});

Handlebars.registerHelper('formatDate', function (date, format) {
    if (date == null || date == undefined)
        return '';

    return moment(date).format(format);
});

Handlebars.registerHelper('each_rules_option', function (items, value, options) {
    var str = '';
    if (items) {
        for (var i = 0; i < items.length; i++) {
            const ruleOrderNumber = items[i].RuleOrderNumber;
            str += "<option value='"+ruleOrderNumber+"'>"+ruleOrderNumber+"</option>";
        }
        var strReplace = "<option value='"+value+"' selected=\"selected\">"+value+"</option>";
        var strFind = "<option value='"+value+"'>"+value+"</option>";
        str = str.replace(strFind, strReplace);
    }
    return str;
});