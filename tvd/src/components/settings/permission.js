﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import toastr from 'toastr';
import PermissionResultTemplate from './permissionResult.hbs';
import PagerTemplate from '../shared/pager.hbs';
import PermissionService from './permission.Service';
import DomSelectorValidator from '../shared/domSelectorValidator';
class Permission {
    constructor(options) {
        if (!validateElement($(options.element)).valid) return;

        EventEmitter.call(this);

        this.service = new PermissionService();
        this.template = PermissionResultTemplate;
        this.pagerTemplate = PagerTemplate;

        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.defaultPageNumber = options.pager.defaultPageNumber;

        eventBinding(this);
    }
}

function validateElement(opt) {
    var selectorValidator = new DomSelectorValidator(opt);
    var valid = selectorValidator.validate();

    return valid;
}

inherits(Permission, EventEmitter);

function eventBinding(from) {

    const seft = from;

    $(".card-block").on("click", "#role-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        window.location.href =  '/setting/permission/edit?id=' + id;
    });

    $(".card-block").on("click", "#role-result > tr > td > a[name=delete]", function(){
        if(confirm('ยืนยันการทำรายการ')){
            const id = $(this).data().id;
            if(id) {
                const callback = function (data) {
                    if (data.Success) {
                        seft.fetch();
                        toastr.success('บันทึกข้อมูลเสร็จสิ้น', 'Success');
                    }
                    else {
                    toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Opp!');
                    }
                };

                const failure = function (xhr, textstatus) {
                    toastr.error(xhr, 'Opp!');
                };

                const dto = {
                    roleId: id,
                    token: $("input[name='requestVerificationToken']").val(),
                    updatedBy: $('#hidden-routeinfo').data().empId
                };

                seft.service.delete(dto, callback, failure);
            }
        }
    });
}

Permission.prototype.fetch = function() {
    const dto = {
        pager: {
            pageSize: this.defaultPageSize,
            pageNumber: this.defaultPageNumber
        }
    };

    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Opp!');
    };

    this.service.fetch(dto, callback, failure);
}

Permission.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

export default Permission;
