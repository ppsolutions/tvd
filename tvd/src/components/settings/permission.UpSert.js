﻿var $ = require('jquery');
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../shared/validator';
import PermissionService from './permission.Service';
import toastr from 'toastr';

class PermissionUpSert {
    constructor(options) {
        EventEmitter.call(this);
        this.service = new PermissionService();
        this.validator = new Validator();
        this.empCreateButton = $(options.empCreateButton);

        this.roleId = $(options.roleId);
        this.roleName = $(options.roleName);
        this.isPermitInMemberRegistrationSystem = $(options.isPermitInMemberRegistrationSystem);
        this.isPermitInApproveSystem = $(options.isPermitInApproveSystem);
        this.isPermitInNewsSystem = $(options.isPermitInNewsSystem);
        this.isPermitInPointSettingSystem = $(options.isPermitInPointSettingSystem);
        this.isPermitInReportSystem = $(options.isPermitInReportSystem);
        this.isPermisInAuditReportSystem = $(options.isPermisInAuditReportSystem);
        this.isPermitInEmployeeManagementSystem = $(options.isPermitInEmployeeManagementSystem);
        this.isPermitInRoleSystem = $(options.isPermitInRoleSystem);
        this.permissionSubmitButton = $(options.permissionSubmitButton);
        this.permissionEditButton = $(options.permissionEditButton);
        this.updatedBy = $(options.updatedBy);
        this.empAccessControl = $(options.empAccessControl);
        this.empAccessContrlCheckedAll = $(options.empAccessContrlCheckedAll);
        eventBinding(this);
    }
}

inherits(PermissionUpSert, EventEmitter);

function eventBinding(from) {
    var seft = from;
    seft.permissionSubmitButton.on("click", function () {
        seft.emit('create');
    });

    seft.permissionEditButton.on("click", function () {
        seft.emit('edit');
    });

    seft.roleName.on("keyup", function (e) {
        if(seft.validator.empty(seft.roleName.val()).valid){
            seft.roleName.attr("style", "border-color:#ccc");
        }
        else {
            seft.roleName.attr("style", "border-color:red");
        }
    });

    seft.empAccessContrlCheckedAll.on('click', function() {
        const current = $(this).prop("checked");
        seft.empAccessControl.prop("checked", current);
    });
}

PermissionUpSert.prototype.valid = function () {
    this.resetError();

    this.validateEmpty(this.roleName.val(), this.roleName);

    var errorCount = this.validator.basket.length;
    if (errorCount > 0) 
    {
        //Hilight elements
        for (var i = 0; i < errorCount; i++) {
            var element = this.validator.basket[i];
            element.attr("style", "border-color:red");
        }
    }
    return (errorCount === 0);
}

PermissionUpSert.prototype.resetError = function() {
    for (var i = 0; i < this.validator.basket.length; i++) {
        var element = this.validator.basket[i];
        element.removeAttr("style", "border-color:red");
    }
    this.validator.basket = [];
}

PermissionUpSert.prototype.reset = function() {
    this.resetError();

    this.roleName.val("");
    this.isPermitInMemberRegistrationSystem.prop("checked", false);
    this.isPermitInApproveSystem.prop("checked", false);
    this.isPermitInNewsSystem.prop("checked", false);
    this.isPermitInPointSettingSystem.prop("checked", false);
    this.isPermitInReportSystem.prop("checked", false);
    this.isPermisInAuditReportSystem.prop("checked", false);
    this.isPermitInEmployeeManagementSystem.prop("checked", false);
    this.isPermitInRoleSystem.prop("checked", false);
}

PermissionUpSert.prototype.validateEmpty = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

PermissionUpSert.prototype.create = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.reset();
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.create(dto, callback, failure);
}

PermissionUpSert.prototype.edit = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.edit(dto, callback, failure);
}

export default PermissionUpSert;
