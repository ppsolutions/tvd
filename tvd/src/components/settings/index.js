﻿import toastr from 'toastr';
import Employee from './employee';
import '../shared/tvd.custom.css';
import EmployeeUpSert from './employee.UpSert';
import Permission from './permission';
import PermissionUpSert from './permission.UpSert';

$(document).ready(function(){
    const emp = new Employee({
        element: "#emp-search-result",
        searchButton: "#emp-search-button",
        searchText: "#emp-search-text",
        searchRole: "#emp-role",
        pager: {
            id: "#emp-pager",
            defaultPageSize: 15,
            defaultPageNumber: 1
        }
    });

    if(emp.element){
        let pager = {
            pageSize: 15,
            pageNumber: 1
        };
        emp.fetch(pager);
    }

    const employeeUpSert = new EmployeeUpSert({
        empCreateButton: '#emp-create',
        empEditButton: '#emp-edit',
        empCode: '#emp-empCode',
        roleId: '#emp-role',
        empCodeSearchButton : '#search-empCode-button',
        updatedBy: '#hidden-routeinfo',
    });
   
    const permission = new Permission({
        element: "#role-result",
        pager: {
            id: "#role-pager",
            defaultPageSize: 50,
            defaultPageNumber: 1
        }
    });
    if(permission.element){
        permission.fetch();
    }

    const permissionUpSert = new PermissionUpSert({
        roleId: "#roleId",
        roleName: "#roleName",
        isPermitInMemberRegistrationSystem: "#permission-member-registration-system",
        isPermitInApproveSystem: "#permission-approve-system",
        isPermitInNewsSystem: "#permission-news-system",
        isPermitInPointSettingSystem: "#permission-point-setting-system",
        isPermitInReportSystem: "#permission-report-system",
        isPermisInAuditReportSystem: "#permission-audit-system",
        isPermitInEmployeeManagementSystem: "#permission-employee-management-system",
        isPermitInRoleSystem: "#permission-in-role-system",
        permissionSubmitButton: "#permission-submit",
        permissionEditButton: "#permission-edit",
        updatedBy: '#hidden-routeinfo',
        empAccessControl: 'ul#emp-access-control > li > input[type=checkbox]',
        empAccessContrlCheckedAll: '#emp-access-control-checked-all',
    });

    permissionUpSert.on("create", function () {
        if(this.valid()) {
            let accessControl = [];
            if(this.empAccessControl &&
                this.empAccessControl.length > 0) {
                    const accessControlChecked = this.empAccessControl.filter(function(idx, item){
                        return $(item).prop("checked") === true;
                    });
                    if(accessControlChecked) {
                        accessControlChecked.filter(function(idx, item){
                            accessControl.push(parseInt($(item).attr("id")));
                        });
                    }
            }

            let dto = {
                roleId: null,
                roleName: this.roleName.val(),
                isPermitInMemberRegistrationSystem: this.isPermitInMemberRegistrationSystem.prop("checked"),
                isPermitInApproveSystem: this.isPermitInApproveSystem.prop("checked"),
                isPermitInNewsSystem: this.isPermitInNewsSystem.prop("checked"),
                isPermitInPointSettingSystem: this.isPermitInPointSettingSystem.prop("checked"),
                isPermitInReportSystem: this.isPermitInReportSystem.prop("checked"),
                isPermisInAuditReportSystem: this.isPermisInAuditReportSystem.prop("checked"),
                isPermitInEmployeeManagementSystem: this.isPermitInEmployeeManagementSystem.prop("checked"),
                isPermitInRoleSystem: this.isPermitInRoleSystem.prop("checked"),
                updatedBy: this.updatedBy.data().empId,
                token: $("input[name='requestVerificationToken']").val(),
                accessControl: accessControl
            };
            
            this.create(dto);
        }
        else {
            toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
        }
    });
    permissionUpSert.on("edit", function () {
        if(this.valid() && this.roleId) {

            let accessControl = [];
            if(this.empAccessControl &&
                this.empAccessControl.length > 0) {
                    const accessControlChecked = this.empAccessControl.filter(function(idx, item){
                        return $(item).prop("checked") === true;
                    });
                    if(accessControlChecked) {
                        accessControlChecked.filter(function(idx, item){
                            accessControl.push(parseInt($(item).attr("id")));
                        });
                    }
            }

            let dto = {
                roleId: this.roleId.val(),
                roleName: this.roleName.val(),
                isPermitInMemberRegistrationSystem: this.isPermitInMemberRegistrationSystem.prop("checked"),
                isPermitInApproveSystem: this.isPermitInApproveSystem.prop("checked"),
                isPermitInNewsSystem: this.isPermitInNewsSystem.prop("checked"),
                isPermitInPointSettingSystem: this.isPermitInPointSettingSystem.prop("checked"),
                isPermitInReportSystem: this.isPermitInReportSystem.prop("checked"),
                isPermisInAuditReportSystem: this.isPermisInAuditReportSystem.prop("checked"),
                isPermitInEmployeeManagementSystem: this.isPermitInEmployeeManagementSystem.prop("checked"),
                isPermitInRoleSystem: this.isPermitInRoleSystem.prop("checked"),
                updatedBy: this.updatedBy.data().empId,
                token: $("input[name='requestVerificationToken']").val(),
                accessControl: accessControl
            };
            
            this.edit(dto);
        }
        else {
            toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
        }
    });
});
