﻿export default class EmployeeService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchUrl = location + '/api/employee/fetch';
        this.editEmployeeRole = location + '/api/employee/role';
        this.searchEmployee = location + '/api/employee?id=';
    }

    get(employeeCode, callback, failure) {
        $.ajax({
            type: "GET",
            url: this.searchEmployee + employeeCode,
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetch(dto, callback, failure) {
        $.ajax({
                type: "POST",
                url: this.fetchUrl,
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    

    edit(dto, callback, failure)
    {
        $.ajax({
            type: "POST",
            url: this.editEmployeeRole,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    delete(dto, callback, failure)
    {
        $.ajax({
            type: "DELETE",
            url: this.editEmployeeRole,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}