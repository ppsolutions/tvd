﻿var $ = require('jquery');
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import EmployeeService from './employee.Service';
import toastr from 'toastr';

class EmployeeUpSert {
    constructor(options) {
        EventEmitter.call(this);
        this.service = new EmployeeService();
        this.empCreateButton = $(options.empCreateButton);
        this.empEditButton = $(options.empEditButton);
        this.empCodeSearchButton = $(options.empCodeSearchButton);
        this.empCode = $(options.empCode);
        this.updatedBy = $(options.updatedBy);
        this.roleId = $(options.roleId);
        eventBinding(this);
    }
}

inherits(EmployeeUpSert, EventEmitter);

function eventBinding(from) {
    var seft = from;

    seft.empCodeSearchButton.on("click", function() {
        seft.searchEmpCode(seft.empCode.val());
    });

    seft.empCreateButton.on("click", function () {

        const dto = {
            employeeCode: seft.empCode.val(),
            roleId: seft.roleId.val(),
            updatedBy: seft.updatedBy.data().empId,
            token: $("input[name='requestVerificationToken']").val(),
        };

        seft.edit(dto);
    });

    seft.empEditButton.on("click", function () {

        const dto = {
            employeeCode: seft.empCode.val(),
            roleId: seft.roleId.val(),
            updatedBy: seft.updatedBy.data().empId,
            token: $("input[name='requestVerificationToken']").val(),
            accessControl: accessControl
        };

        seft.edit(dto);
    });

    
}

EmployeeUpSert.prototype.edit = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.edit(dto, callback, failure);
}

EmployeeUpSert.prototype.searchEmpCode = function(empCode) {

    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            var item = data.ResponseData;
            console.log(item);
            $('#emp-prefix').val(item.PrefixName);
            $('#emp-firstName').val(item.FirstName);
            $('#emp-lastName').val(item.LastName);
            $('#emp-phone').val(item.ContactNumber);
            $('#emp-email').val(item.EmailAddress);
        }
        else {
            let errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
    };

    this.service.get(empCode, callback, failure);
}


export default EmployeeUpSert;
