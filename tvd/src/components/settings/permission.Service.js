﻿export default class PermissionService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.searchUrl = location + '/api/roles';
        this.upsertRoleUrl = location + '/api/role';
        this.editPermissionUrl = location + '/api/role/permission';
    }

    fetch(dto, callback, failure) {
        $.ajax({
                type: "POST",
                url: this.searchUrl,
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    delete(dto, callback, failure) {
        $.ajax({
                type: "DELETE",
                url: this.upsertRoleUrl,
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }
    
    create(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.upsertRoleUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    edit(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.editPermissionUrl,
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}