﻿import EmployeeResultTemplate from './searchEmployeeResult.hbs';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import EmployeeService from './employee.Service';
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import DomSelectorValidator from '../shared/domSelectorValidator';
class Employee {
    constructor(options) {
        if (!validateElement($(options.element)).valid) return;

        EventEmitter.call(this);

        this.service = new EmployeeService();
        this.template = EmployeeResultTemplate;
        this.pagerTemplate = PagerTemplate;

        this.element = $(options.element);
        this.searchButton = $(options.searchButton);
        this.searchText = $(options.searchText);
        this.searchRole = $(options.searchRole);

        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.defaultPageNumber = options.pager.defaultPageNumber;

       
        eventBinding(this);
    }
}

function validateElement(opt) {
    var selectorValidator = new DomSelectorValidator(opt);
    var valid = selectorValidator.validate();

    return valid;
}

inherits(Employee, EventEmitter);

function eventBinding(empFrom) {

    const seft = empFrom;

    seft.searchButton.on("click", function() {
        let pager = {
            pageSize: seft.defaultPageSize,
            pageNumber: seft.defaultPageNumber
        };
        seft.fetch(pager);
    });

    

    $(".card-block").on("click", "#emp-search-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        window.location.href =  '/setting/employee/edit?id=' + id;
    });

    $(".card-block").on("click", "#emp-search-result > tr > td > a[name=delete]", function(){

        if(confirm('ยืนยันการทำรายการ')){
            const id = $(this).data().id;
            if(id) {
                const callback = function (data) {
                    if (data.Success) {
                        let pager = {
                            pageSize: seft.defaultPageSize,
                            pageNumber: seft.defaultPageNumber
                        };
                        seft.fetch(pager);
                    }
                    else {
                        let errorMsgFromServer = data.Message;
                        if (errorMsgFromServer.length === 0) {
                            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
                        } else {
                            toastr.error(errorMsgFromServer, 'Oop!');                       
                        }
                    }
                };

                const failure = function (xhr, textstatus) {
                    toastr.error(xhr, 'Oop!');
                };

                const dto = {
                    employeeCode: id,
                    token: $("input[name='requestVerificationToken']").val(),
                    updatedBy: $("#hidden-routeinfo").data().empId
                };

                seft.service.delete(dto, callback, failure);
            }
        }
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        var pager = {
            pageSize: seft.defaultPageSize,
            pageNumber: $(this).data().pn
        };
        seft.fetch(pager);  
    });
}

Employee.prototype.fetch = function(pager) {
    const dto = {
        keyword: this.searchText.val(),
        roleId: this.searchRole.val() === "" ? null : this.searchRole.val(),
        pager: pager
    };

    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager, data.ReturnUrl);
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
    };

    this.service.fetch(dto, callback, failure);
}

Employee.prototype.renderTemplateWithData = function (data, pager, url) {
    const result = {
        items: data,
        url: url
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};


export default Employee;
