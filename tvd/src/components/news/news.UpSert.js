﻿var $ = require('jquery');
var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../shared/validator';
import NewsService from './news.Service';
import toastr from 'toastr';

class NewsUpSert {
    constructor(options) {
        EventEmitter.call(this);
        this.service = new NewsService();
        this.validator = new Validator();

        this.newsCreateButton = $(options.newsCreateButton);
        this.newsEditButton = $(options.newsEditButton);
        this.newsId = $(options.newsId);
        this.newsTitle = $(options.newsTitle);
        this.newsContent = $(options.newsContent);
        this.newsType = $(options.newsType);
        this.newsStatus = $(options.newsStatus);
        this.validFrom = $(options.validFrom);
        this.validTo = $(options.validTo);
        eventBinding(this);
    }
}

inherits(NewsUpSert, EventEmitter);

function eventBinding(from) {
    const seft = from;

    seft.newsCreateButton.on("click", function () {
        seft.emit('create');
    });

    seft.newsEditButton.on("click", function () {
        seft.emit('edit');
    });

    seft.newsTitle.on("keyup", function (e) {
        if(seft.validator.empty(seft.newsTitle.val()).valid){
            seft.newsTitle.attr("style", "border-color:#ccc");
        }
        else {
            seft.newsTitle.attr("style", "border-color:red");
        }
    });

    seft.newsType.on("change", function(){
        if($(this).val() === "0") {
            seft.newsType.attr("style", "border-color:red");
        }
        else {
            seft.newsType.attr("style", "border-color:#ccc");
        }
    });

    seft.newsStatus.on("change", function(){
        if($(this).val() === "0") {
            seft.newsStatus.attr("style", "border-color:red");
        }
        else {
            seft.newsStatus.attr("style", "border-color:#ccc");
        }
    });

    window.CKEDITOR.replace('news-content');
}

NewsUpSert.prototype.valid = function () {
    this.resetError();

    this.validateEmpty(this.newsTitle.val(), this.newsTitle);
    this.validateEmpty(CKEDITOR.instances['news-content'].getData(), this.newsContent);
    this.validateType(this.newsType.val(), this.newsType);
    this.validateStatus(this.newsStatus.val(), this.newsStatus);
    this.validateEmpty(this.validFrom.val(), this.validFrom);
    this.validateEmpty(this.validTo.val(), this.validTo);

    var errorCount = this.validator.basket.length;
    if (errorCount > 0) 
    {
        for (var i = 0; i < errorCount; i++) {
            var element = this.validator.basket[i];
            if (element !==  this.newsContent) {
                element.attr("style", "border-color:red");
            }
        }
    }
    return (errorCount === 0);
}

NewsUpSert.prototype.resetError = function() {
    for (var i = 0; i < this.validator.basket.length; i++) {
        var element = this.validator.basket[i];
        element.removeAttr("style", "border-color:red");
    }
    this.validator.basket = [];
}

NewsUpSert.prototype.reset = function() {
    this.resetError();

    this.newsTitle.val("");
    this.newsContent.val("");
    CKEDITOR.instances['news-content'].setData('');
    this.newsType.val(0);
    this.newsStatus.val(-1);
    this.validFrom.val("");
    this.validTo.val("");
}

NewsUpSert.prototype.validateType = function (str, element) {
    return this.validator.selected(str).valid ? true : this.validator.basket.push(element);
}

NewsUpSert.prototype.validateEmpty = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

NewsUpSert.prototype.validateStatus = function (str, element) {
    if(str === "DRAFT" || 
       str === "PUBLISH") {
       return true;
    }
    this.validator.basket.push(element);
}

NewsUpSert.prototype.create = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.reset();
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.create(dto, callback, failure);
}

NewsUpSert.prototype.edit = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length === 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    seft.service.edit(dto, callback, failure);
}

export default NewsUpSert;
