const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import NewsTemplate from './news.hbs';
import NewsViewTemplate from './newsview.hbs';
import PagerTemplate from '../shared/pager.hbs';
import NewsService from './news.Service';
import toastr from 'toastr';

class News {
    constructor(options) {
        EventEmitter.call(this);
        this.template = NewsTemplate;
        this.pagerTemplate = PagerTemplate;
        this.newsViewTemplate = NewsViewTemplate;
        this.service = new NewsService();
        this.options = $.extend(true, options, {
            newsSelectedAll: "#news-selected-all",
            newsDeleteAll: "#news-delete-all",
            newsType: options.newsType,
            newsTable: "#news-member",
            newsResult: "#news-member-result",
            newsFilter: "#news-filter",
            newsFilterType: "#news-type-filter",
            pager: {
                id: "#news-member-pager"
            }
        });
        this.pageTypeId = this.options.pageTypeId;
        this.element = $(this.options.newsResult);
        this.pager = $(this.options.pager.id);
        eventBinding(this);
    }
}

inherits(News, EventEmitter);

function eventBinding(from) {
    const seft  =from;

    $(seft.options.newsDeleteAll).on("click", function () {
        if(confirm('ยืนยันการทำรายการ')) {
            let newsIds = [];
            $(seft.options.newsResult).find("input[type=checkbox]").each(function (e) {
                var checked = $(this).is(":checked");
                if (checked) {
                    var newsId = $(this).data().id;
                    newsIds.push(newsId);
                }
            });

            if(newsIds.length > 0) {
                seft.deleteNews(newsIds);
            }
        }
    });

    $(seft.options.newsSelectedAll).on("click", function () {
        var val = $(this).is(':checked');
        if (val) {
            $(seft.options.newsTable).find("input[type=checkbox]").prop("checked", val);
        }
        else {
            $(seft.options.newsTable).find("input[type=checkbox]").prop("checked", val);
        }
    });

    $(seft.options.newsTable).on("click", "#news-member-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        window.location.href =  '/news/edit?id=' + id;
    });

    $(seft.options.newsTable).on("click", "#news-member-result > tr > td > a[name=delete]", function() {
        if(confirm('ยืนยันการทำรายการ')) {
            const id = $(this).data().id;
            if(id) {
                seft.deleteNews([id]);
            }
        }
    });

    $(seft.options.pager.id).on("click", "ul#paging > li > a", function () {
        var newsType = seft.options.pageTypeId === 64 ? $(seft.options.newsFilterType).val() : seft.options.newsType;
        
        const dto = {
            newsType: newsType,
            filterType: $(seft.options.newsFilter).val() === "0" ? null : $(seft.options.newsFilter).val(),
            pager: {
                pageSize: seft.options.pager.defaultPageSize,
                pageNumber: $(this).data().pn
            }
        };
        seft.fetch(dto);
    });

    $(seft.options.newsFilter).on("change", function () {
        var newsType = seft.options.pageTypeId === 64 ? $(seft.options.newsFilterType).val() : seft.options.newsType;
        const dto = {
            newsType: newsType,
            filterType: $(this).val() == "DRAFT" ? 0 : $(this).val() == "PUBLISH" ? 1 : null,
            pager: {
                pageSize: seft.options.pager.defaultPageSize,
                pageNumber: 1
            }
        };
        seft.fetch(dto);
    });

    $(seft.options.newsFilterType).on("change", function() {
        var newsType = seft.options.pageTypeId === 64 ? $(seft.options.newsFilterType).val() : seft.options.newsType;

        const dto = {
            newsType: newsType,
            filterType: $(this).val() == "DRAFT" ? 0 : $(this).val() == "PUBLISH" ? 1 : null,
            pager: {
                pageSize: seft.options.pager.defaultPageSize,
                pageNumber: 1
            }
        };
        seft.fetch(dto);
    });
}

News.prototype.deleteNews = function(id) {

    let seft = this;

    const callback = function (data) {
        if (data.Success) {
            var newsType = seft.options.pageTypeId === 64 ? $(seft.options.newsFilterType).val() : seft.options.newsType;

            let dto = {
                newsType: newsType,
                filterType: $(seft.options.newsFilter).val() == "0" ? null : $(seft.options.newsFilter).val(),
                pager: {
                    pageSize: seft.options.pager.defaultPageSize,
                    pageNumber: seft.options.pager.defaultPageNumber
                }
            }

            seft.fetch(dto);
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');          
            }
        }
    };

    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    const dto = {
        newsIds: id,
        token: $("input[name='requestVerificationToken']").val(),
        updatedBy: $("#hidden-routeinfo").data().empId
    };

    this.service.delete(dto, callback, failure);
}


News.prototype.fetch = function(dto) {

    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
    };

    this.service.fetch(dto, callback, failure);
}


News.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };
    if (this.pageTypeId === 64) {
        this.element.html(this.newsViewTemplate(result));
    } else {
        this.element.html(this.template(result));       
    }
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

export default News;