﻿export default class NewsService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.getNewsUrl = location + '/api/news/fetch';
        this.newsUrl = location + '/api/news';
    }

    fetch(dto, callback, failure) {
        $.ajax({
                type: "POST",
                url: this.getNewsUrl,
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    delete(dto, callback, failure) {
        $.ajax({
                type: "DELETE",
                url: this.newsUrl,
                headers: {
                    "token": dto.token
                },
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    create(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.newsUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    edit(dto, callback, failure) {
        $.ajax({
            type: "PUT",
            url: this.newsUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

}