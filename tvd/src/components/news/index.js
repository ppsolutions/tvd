﻿import News from './news';
import NewsUpSert from './news.UpSert';
//import froalaEditor from 'froala-editor';
import toastr from 'toastr';

$(document).ready(function (){

    function getNewsType(pageTypeId) {
        let newsType = 0;

        if(pageTypeId == 7) { newsType = 1; }
        if(pageTypeId == 8) { newsType = 2; }
        if(pageTypeId == 9) { newsType = 3; }
        if(pageTypeId == 13) { newsType = 4; }
        if (pageTypeId === 64) {
            newsType = $("#news-type-filter").val();
        }
        return newsType;
    }

    if($("#hidden-routeinfo").data() == undefined)
        return;

    const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
    const updatedBy = $("#hidden-routeinfo").data().empId;
    const token = $("input[name='requestVerificationToken']").val();
    let options = { };

    if(pageTypeId === 7 ||
       pageTypeId === 8 || 
       pageTypeId === 9 || 
       pageTypeId === 13 || 
        pageTypeId === 64
     ) {
        options = {
            newsSelectedAll: null,
            newsType: getNewsType(pageTypeId),
            newsTable: null,
            newsResult: null,
            newsFilter: null,
            pager: {
                id: null,
                defaultPageSize: 15,
                defaultPageNumber: 1
            },
            pageTypeId: pageTypeId,
            updatedBy: updatedBy,
            token: token
        };
        const news = new News(options);
        const dto = {
            newsType: getNewsType(pageTypeId),
            filterType: null,
            pager: {
                pageSize: options.pager.defaultPageSize,
                pageNumber: options.pager.defaultPageNumber
            }
        };
        news.fetch(dto);
    }

    if(pageTypeId === 14 || 
       pageTypeId === 15) {
        $('#datetimepicker1 input').datepicker({
            format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });

        $('#datetimepicker2 input').datepicker({
            format: 'dd/mm/yyyy',
            multidate: false,
            todayHighlight: true
        });

        options = {
            newsId: null,
            newsTitle: "#news-title",
            newsType: "#news-type",
            newsStatus: "#news-stat",
            newsContent: "#news-content",
            newsCreateButton: "#news-create-button",
            newsEditButton: "#news-edit-button",
            validFrom: '#valid-from',
            validTo: '#valid-to',
            updatedBy: updatedBy,
            token: token
        };
        
        const newsUpSert = new NewsUpSert(options);
        newsUpSert.on("create", function () {
            if(this.valid()) {
                let dto = {
                    newsId: null,
                    title: this.newsTitle.val().trim(),
                    content: CKEDITOR.instances['news-content'].getData(),
                    startDate: this.validFrom.val(),
                    endDate: this.validTo.val(),
                    newsType: this.newsType.val(),
                    status: this.newsStatus.val() === "DRAFT" ? 0 : this.newsStatus.val() === "PUBLISH" ? 1 : null,
                    updatedBy: updatedBy,
                    token: token
                };
                
                this.create(dto);
            }
            else {
                toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
            }
        });

        newsUpSert.on("edit", function () {
            if(this.valid()) {
                let dto = {
                    newsId: $("#news-id").val(),
                    title: this.newsTitle.val().trim(),
                    content: CKEDITOR.instances['news-content'].getData(),
                    startDate: this.validFrom.val(),
                    endDate: this.validTo.val(),
                    newsType: this.newsType.val(),
                    status: this.newsStatus.val() === "DRAFT" ? 0 : this.newsStatus.val() === "PUBLISH" ? 1 : null,
                    updatedBy: updatedBy,
                    token: token
                };
                
                this.edit(dto);
            }
            else {
                toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
            }
        });
    }


});