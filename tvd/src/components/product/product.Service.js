export default class ProductService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchproductinfoUrl = location + '/api/lookup/fetchproductinfo';
    }

    searchCode(dto, callback, failure) {
        $.ajax({
            type: dto.method,
            url: this.fetchproductinfoUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}