﻿export default class RuleService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchUrl = location + '/api/point/rule/fetch';
        this.updateStatusUrl = location + '/api/point/rule/updatestatus';
        this.saveUrl = location + '/api/point/rule/upsert';
        this.exportUrl = location + '/api/point/rule/export';
    }

    export(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.exportUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    delete(dto, callback, failure) {
        $.ajax({
                type: "PUT",
                url: this.updateStatusUrl,
                headers: {
                    "token": dto.token
                },
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    save(dto, callback, failure) {
        $.ajax({
                type: dto.method,
                url: this.saveUrl,
                headers: {
                    "token": dto.token
                },
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

}