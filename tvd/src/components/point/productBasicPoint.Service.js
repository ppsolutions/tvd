﻿export default class BasicPointService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchUrl = location + '/api/point/productInfo/fetch';
        this.exportUrl = location + '/api/point/productInfo/export';
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

   

    export(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.exportUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}