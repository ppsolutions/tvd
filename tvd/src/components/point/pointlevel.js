const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import PointService from './point.Service';
import toastr from 'toastr';
import Validator from '../shared/validator';

class Point {
    constructor(options) {
        EventEmitter.call(this);
       
        this.service = new PointService();
        this.pointLevel = $(options.pointLevel);
        this.validator = new Validator();
        this.updateRequirePointBtn = $(options.updateRequirePointBtn);
        this.updatePricePerPointBtn = $(options.updatePricePerPointBtn);
		this.expireDate = $(options.expireDate);
		this.expireYear = $(options.expireYear);
        this.minPurchase = $(options.minPurchase);

        eventBinding(this);
    }
}

inherits(Point, EventEmitter);

function eventBinding(from) {
    const seft  = from;
    $(seft.updateRequirePointBtn).on("click",
        function() {
            seft.emit('editRequirePoint');
        });

    $(seft.updatePricePerPointBtn).on("click",
        function() {
            seft.emit('editPricePerPoint');
    });

    $(seft.minPurchase).on('keypress', function(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );

        if(!seft.validator.positiveNumber(key).valid) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) 
                theEvent.preventDefault();
        }
    });
}

Point.prototype.edit = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success) {
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
    };

    seft.service.edit(dto, callback, failure);
}
export default Point;