const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import BasicPointService from './productBasicPoint.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import BasicPointTemplate from './productBasicPoint.hbs';
import ExportService from '../shared/export';

class BasicPoint {
    constructor(options) {
        EventEmitter.call(this);
        this.template = BasicPointTemplate;
        this.pagerTemplate = PagerTemplate;
        this.service = new BasicPointService();

        //view
        this.token = options.token;
        this.by = options.by;
        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.exportService = new ExportService();
        this.code = $(options.code);
        this.table = $(options.table);
        this.submit = $(options.submit);
        this.exportButton = $(options.exportButton);
        eventBinding(this);
    }
}

inherits(BasicPoint, EventEmitter);

function eventBinding(from) {
    const seft  = from;
   
    $(seft.code).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length === 0 ? null : $(this).val(),
                pager: {
                    pageSize: seft.defaultPageSize,
                    pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            };
            seft.fetch(dto);
        }
    });

    $(seft.submit).on("click", function (e) {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: $(this).data().pn
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

   
    seft.exportButton.on('click', function(){
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            token: seft.token,
            by: seft.by
        };
        seft.export(dto);
    });
}

BasicPoint.prototype.fetch = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

BasicPoint.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

BasicPoint.prototype.export = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            const fields = ['ProductInfo', 'Detail','PricePerUnit', 'RegularPoint', 'SilverPoint','GoldPoint','PlatinumPoint'];
            try {
                let items = data.ResponseData;
                seft.exportService.download({ data: items, fields: fields }, 'exportBasicPoint.csv');
            } catch (err) {
                toastr.error(err, 'Oop!');
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };

    this.service.export(dto, callback, failure);
}
    
export default BasicPoint;