﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
import Validator from '../shared/validator';
import RewardService from './reward.Service';
import toastr from 'toastr';
import ProductService from '../product/product.Service';
import '../../../node_modules/bootstrap-3-typeahead';

class RewardUpSert {
    constructor(options) {
        EventEmitter.call(this);
        this.service = new RewardService();
        this.productService = new ProductService();
        this.validator = new Validator();

        this.rewardId = $(options.rewardId);
        this.code = $(options.code);
        this.description = $(options.description);
        //this.rewardType = $(options.rewardType);
        this.rewardPointOnly = $(options.rewardPointOnly);
        this.rewardPoint = $(options.rewardPoint);
        this.rewardMoney = $(options.rewardMoney);
        this.rewardSave = $(options.rewardSave);
        this.rewardSearchCodeButton = $(options.rewardSearchCodeButton);
        this.recStatus = $(options.recStatus);
        this.rewardName = $("#reward-name");
        this.clearBtn = $("#reward-search-clear-button");
        eventBinding(this);
    }
}

inherits(RewardUpSert, EventEmitter);

function eventBinding(from) {
    const seft = from;

    seft.rewardSave.on("click", function () {
        seft.emit('save');
    });

    seft.clearBtn.on("click", function(){
        seft.code.val('');
        seft.rewardName.val('');
    });

    seft.code.on("keyup", function (e) {
        if(seft.validator.empty(seft.code.val()).valid){
            seft.code.attr("style", "border-color:#ccc");
        }
        else {
            seft.code.attr("style", "border-color:red");
        }
    });

    seft.rewardName.typeahead({
        minLength: 3,
        displayText: function (item) {
            return item.Name;
        },
        source: function (query, process) {
            return $.get('/api/lookup/productInfo?query=' + query, function (data) {
                return process(data.ResponseData);
            });
        },
        autoSelect: true,
        fitToElement: true
    });
    seft.rewardName.change(function() {
        var current = seft.rewardName.typeahead("getActive");
        if (current) {

            seft.code.val(current.Code);
            //// Some item from your model is active!
            //if (current.name == $input.val()) {
            //    // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            //} else {
            //    // This means it is only a partial match, you can either add a new item
            //    // or take the active if you don't want new items
            //}
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            seft.code.val('');
        }
    });

    seft.description.on("keyup", function (e) {
        if(seft.validator.empty(seft.description.val()).valid){
            seft.description.attr("style", "border-color:#ccc");
        }
        else {
            seft.description.attr("style", "border-color:red");
        }
    });

    //seft.rewardType.on("change", function(){
    //    if($(this).val() === "2") {
    //        seft.rewardMoney.removeAttr("disabled");
    //    }
    //    else {
    //        seft.rewardMoney.val('');
    //        seft.rewardMoney.attr('disabled', true);
    //        seft.rewardMoney.attr("style", "border-color:#ccc");
    //    }
    //});

    seft.rewardPoint.on("keyup", function (e) {
        if(seft.validator.empty(seft.rewardPoint.val()).valid){
            seft.rewardPoint.attr("style", "border-color:#ccc");
        }
        else {
            seft.rewardPoint.attr("style", "border-color:red");
        }
    });

    seft.rewardMoney.on("keyup", function (e) {

        //if(seft.rewardType.val() != '2') { return false; }

        if(seft.validator.empty(seft.rewardMoney.val()).valid){
            seft.rewardMoney.attr("style", "border-color:#ccc");
        }
        else {
            seft.rewardMoney.attr("style", "border-color:red");
        }
    });

    seft.rewardSearchCodeButton.on("click", function () {
        seft.emit('rewardSearchCode');
    });
    
}


RewardUpSert.prototype.valid = function () {
    this.resetError();

    this.validateEmpty(this.code.val(), this.code);
    if(this.rewardId.val() !== undefined){
        this.validateEmpty(this.description.val(), this.description);
    }else{
        this.validateEmpty(this.rewardName.val(), this.rewardName);
    }
    this.validateEmpty(this.rewardPoint.val(), this.rewardPoint);
    this.validateEmpty(this.rewardPointOnly.val(), this.rewardPointOnly);
    this.validateEmpty(this.rewardMoney.val(), this.rewardMoney);
    //if(this.rewardType.val() == '2') {
    //    this.validateEmpty(this.rewardMoney.val(), this.rewardMoney);
    //}

    var errorCount = this.validator.basket.length;
    if (errorCount > 0) 
    {
        for (var i = 0; i < errorCount; i++) {
            var element = this.validator.basket[i];
            element.attr("style", "border-color:red");
        }
    }
    return (errorCount === 0);
}

RewardUpSert.prototype.resetError = function() {
    for (var i = 0; i < this.validator.basket.length; i++) {
        var element = this.validator.basket[i];
        element.removeAttr("style", "border-color:red");
    }
    this.validator.basket = [];
}

RewardUpSert.prototype.reset = function() {
    this.resetError();
    this.code.val('');
    this.description.val('');
    //this.rewardType.val(1);
    this.rewardPointOnly.val('');
    this.rewardPoint.val('');
    this.rewardMoney.val('');
    //this.rewardMoney.attr('disabled', true);
    this.rewardMoney.attr("style", "border-color:#ccc");
    this.recStatus.val(1);
}

RewardUpSert.prototype.validateEmpty = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

RewardUpSert.prototype.save = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success && data.ResponseData) {
            if(seft.rewardId.val() === undefined) {
                seft.reset();
            }
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            let errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');          
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };

    let rewardDto = {
        rewardId: seft.rewardId.val() != undefined ? seft.rewardId.val() : null,
        code: seft.code.val(),
        description: seft.rewardId.val() != undefined ? seft.description.val() : seft.rewardName.val(),
        //rewardType: seft.rewardType.val(),
        pointOnly: seft.rewardPointOnly.val(),
        point: seft.rewardPoint.val(),
        money: seft.rewardMoney.val(), //.length == 0 ? 0 : seft.rewardMoney.val(),
        userId: dto.by,
        token: dto.token,
        recStatus: seft.recStatus.val(),
        method: seft.rewardId.val() != undefined ? 'PUT' : 'POST',
    }

    seft.service.save(rewardDto, callback, failure);
}

RewardUpSert.prototype.searchCode = function (dto) {
    var seft = this;

    seft.rewardSearchCodeButton.html('Loding..');
    seft.rewardSearchCodeButton.attr('style', 'width:125.75px;');

    const callback = function (data) {
        if (data.Success && 
            data.ResponseData) {
            seft.description.val(data.ResponseData.Name);
        }
        else {
            let errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');          
            }

            seft.reset();
        }
        seft.rewardSearchCodeButton.html('ค้นหาข้อมูลสินค้า');
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');

        seft.rewardSearchCodeButton.html('ค้นหาข้อมูลสินค้า');
    };

    let productDto = {
        code: seft.code.val(),
        by: dto.by,
        token: dto.token,
        method: 'POST',
    }

    seft.productService.searchCode(productDto, callback, failure);
}

export default RewardUpSert;
