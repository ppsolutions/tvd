﻿export default class RewardService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchRewardUrl = location + '/api/point/reward/fetch';
        this.updateRewardStatusUrl = location + '/api/point/reward/updatestatus';
        this.saveRewardUrl = location + '/api/point/reward/upsert';
        this.exportRewardUrl = location + '/api/point/reward/export';
    }

    export(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.exportRewardUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchRewardUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    delete(dto, callback, failure) {
        $.ajax({
                type: "PUT",
                url: this.updateRewardStatusUrl,
                headers: {
                    "token": dto.token
                },
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }

    save(dto, callback, failure) {
        $.ajax({
                type: dto.method,
                url: this.saveRewardUrl,
                headers: {
                    "token": dto.token
                },
                data: JSON.stringify(dto),
                xhrFields: { withCredentials: true },
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if (callback !== undefined) {
                        callback(data);
                    }
                },
                error: function (xhr, status) {
                    if (failure !== undefined) {
                        failure(xhr, status);
                    }
                },
                timeout: 9999
        });
    }
}