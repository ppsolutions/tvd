const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
const moment = require('moment');

import GamificationService from './gamification.Service';
import toastr from 'toastr';
import Validator from '../shared/validator';

class GamificationUpsert {
	constructor(options) {
		EventEmitter.call(this);
		this.validator = new Validator();
		this.service = new GamificationService();

		//view
		this.token = options.token;
		this.by = options.by;

		//create
		this.createBtn = $(options.createBtn);
		this.gamecode = $(options.gamecode);
		this.desc = $(options.desc);
		this.applyTo = $(options.applyTo);
		this.point = $(options.point);
		this.validFrom = $(options.validFrom);
		this.validTo = $(options.validTo);

		//edit
		this.updateBtn = $(options.updateBtn);
		this.gameId = $(options.gameId);
		this.recStatus = $(options.recStatus);
		eventBinding(this);
	}
}

inherits(GamificationUpsert, EventEmitter);

function eventBinding(from) {
	const seft = from;
	$(seft.createBtn).on("click", function (e) {
		if (seft.valid()) {
			let dto = {
				gamecode: seft.gamecode.val(),
				description: seft.desc.val(),
				recStatus: seft.recStatus.val(),
				applyTo: seft.applyTo.val(),
				point: seft.point.val(),
				validFrom: moment(seft.validFrom.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
				validTo: moment(seft.validTo.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
				updatedBy: seft.by,
				token: seft.token
			};

			seft.create(dto);
		}
		else {
			toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
		}
	});

	$(seft.updateBtn).on("click", function (e) {
		let dto = {
			gameId: seft.gameId.val(),
			gamecode: seft.gamecode.text(),
			description: seft.desc.val(),
			recStatus: seft.recStatus.val(),
			applyTo: seft.applyTo.val(),
			point: seft.point.val(),
			validFrom: moment(seft.validFrom.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
			validTo: moment(seft.validTo.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
			updatedBy: seft.by,
			token: seft.token
		};

		seft.edit(dto);
	});

}

GamificationUpsert.prototype.create = function (dto) {
	var seft = this;
	const callback = function (data) {
		if (data.Success) {
			seft.reset();
			toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
		}
		else {
			let errorMsgFromServer = data.Message;
			if (errorMsgFromServer.length === 0) {
				toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
			} else {
				toastr.error(errorMsgFromServer, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		let errorMsgFromServer = xhr.responseText;
		if (errorMsgFromServer.length === 0) {
			toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
		} else {
			toastr.error(errorMsgFromServer, 'Oop!');
		}
	};

	seft.service.create(dto, callback, failure);
}

GamificationUpsert.prototype.edit = function (dto) {
	var seft = this;
	const callback = function (data) {
		if (data.Success) {
			toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
		}
		else {
			var errorMsgFromServer = data.Message;
			if (errorMsgFromServer.length == 0) {
				toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
			} else {
				toastr.error(errorMsgFromServer, 'Oop!');
			}
		}
	};

	const failure = function (xhr, textstatus) {
		var errorMsgFromServer = xhr.responseText;
		if (errorMsgFromServer.length == 0) {
			toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
		} else {
			toastr.error(errorMsgFromServer, 'Oop!');
		}
	};

	seft.service.edit(dto, callback, failure);
}

GamificationUpsert.prototype.valid = function () {
	this.resetError();
	this.validateEmpty(this.gamecode.val(), this.gamecode);
	this.validateEmpty(this.desc.val(), this.desc);
	this.validateEmpty(this.point.val(), this.point);
	this.validateEmpty(this.validFrom.val(), this.validFrom);
	this.validateEmpty(this.validTo.val(), this.validTo);

	var errorCount = this.validator.basket.length;
	if (errorCount > 0) {
		//Hilight elements
		for (var i = 0; i < errorCount; i++) {
			var element = this.validator.basket[i];
			element.attr("style", "border-color:red");
		}
	}
	return (errorCount === 0);
}

GamificationUpsert.prototype.resetError = function () {
	for (var i = 0; i < this.validator.basket.length; i++) {
		var element = this.validator.basket[i];
		element.removeAttr("style", "border-color:red");
	}
	this.validator.basket = [];
}

GamificationUpsert.prototype.reset = function () {
	this.resetError();
	this.gamecode.val("");
	this.desc.val("");
	this.point.val("");
	this.validFrom.val("");
	this.validTo.val("");
}

GamificationUpsert.prototype.validateEmpty = function (str, element) {
	return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

export default GamificationUpsert;