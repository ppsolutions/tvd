﻿export default class UploadMemberPointService {
	constructor() {
		const location = 'http://' + window.location.host;
		this.uploadMemberPointUrl = location + '/api/point/uploadmemberpoint';
		this.insertMemberPointUrl = location + '/api/point/massmemberpointupdate';
	}

	uploadMemberPoint(dto, callback, failure) {
		$.ajax({
			type: "POST",
			url: this.uploadMemberPointUrl,
			headers: {
				"token": dto.token
			},
			data: dto.data,
			contentType: false,
			processData: false,
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			},
			timeout: 9999
		});
	}

	updateMemberPoint(dto, callback, failure) {
		$.ajax({
			type: "PUT",
			url: this.insertMemberPointUrl,
			data: JSON.stringify(dto),
			xhrFields: { withCredentials: true },
			dataType: "json",
			contentType: "application/json",
			success: function (data) {
				if (callback !== undefined) {
					callback(data);
				}
			},
			error: function (xhr, status) {
				if (failure !== undefined) {
					failure(xhr, status);
				}
			}
		});
	}
}