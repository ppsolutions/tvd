﻿import Point from './pointlevel';
import toastr from 'toastr';
import Reward from './reward';
import RewardUpSert from './reward.UpSert';
import Gamification from './gamification';
import GamificationUpsert from './gamification.Upsert';
import RuleUpSert from './rule.UpSert';
import Rule from './rule';
import Promotion from './promotion';
import Campaign from './campaign/campaign';
import SearchPromotion from './promotion/searchPromotion';
import PromotionDetail from './promotionDetail/promotionDetail';
import SearchCampaign from './campaign/searchCampaign';
import BasicPoint from './productBasicPoint';
import CampaignDetail from './campaign/campaignDetail';
import UploadMemberPoint from './uploadmemberpoint';

$(document).ready(function () {
	"use strict";

	if ($("#hidden-routeinfo").data() == undefined)
		return;

	const pageTypeId = $("#hidden-routeinfo").data().pageTypeId;
	const by = $("#hidden-routeinfo").data().empId;
	const token = $("input[name='requestVerificationToken']").val();

	const campaignDetail = new CampaignDetail({
		element: '#promo-in-campaign-element',
		promoInCampaignElement: '#promo-in-campaign-table',
		promoInCampaign: '#promo-in-campaign-result',
		promoInCampaignPager: {
			id: "#promo-in-campaign-pager",
			defaultPageSize: 15,
			defaultPageNumber: 1
		},
		ruleSelectedAll: '#rule-selected-all',
		ruleSearchModalButton: '#rule-search-modal-button',
		ruleCode: "#rule-code-text",
		ruleStatus: "#rule-status-dd",
		ruleCalculateType: null,
		ruleSearchButton: "#rule-search-button",
		ruleAddButton: '#rule-add-button',
		ruleReult: "#rule-add-result",
		ruleElement: "#rule-add-table",
		rulePager: {
			id: "#rule-add-pager",
			defaultPageSize: 20,
			defaultPageNumber: 1
		},
		by: by,
		token: token,
		promoId: $('#promoId').val(),
		promoCode: $('#promoCode').val(),
		rulePromoElement: '#rule-campaign-table',
		rulePromoResult: '#rule-campaign-result',
		rulePromoPager: {
			id: "#rule-campaign-pager",
			defaultPageSize: 15,
			defaultPageNumber: 1
		},
		ruleSaveDate: '#rule-campaign-save-date-button',
	});
	if (campaignDetail && campaignDetail.promoInCampaignElement) {
		const promoId = $('#promoId').val();
		const promoCode = $('#promoCode').val();
		campaignDetail.fetchPromoInCampaign({
			id: promoId,
			token: token,
			promotionType: 2,
			by: by,
			pager: {
				pageSize: campaignDetail.promoInCampaignDefaultPageSize,
				pageNumber: 1
			},
		});

		campaignDetail.fetchRuleInCampaign({
			code: promoCode,
			token: token,
			promotionType: 2,
			by: by,
			pager: {
				pageSize: campaignDetail.rulePromoPagerDefaultPageSize,
				pageNumber: 1
			},
		});
	}

	const promotionDetail = new PromotionDetail({
		element: '#product-in-promo-element',
		productInPromotionElement: '#product-in-promo-table',
		productInPromotion: '#product-in-promo-result',
		productInPromotionPager: {
			id: "#product-in-promo-pager",
			defaultPageSize: 15,
			defaultPageNumber: 1
		},
		ruleSelectedAll: '#rule-selected-all',
		ruleSearchModalButton: '#rule-search-modal-button',
		ruleCode: "#rule-code-text",
		ruleStatus: "#rule-status-dd",
		ruleCalculateType: null,
		ruleSearchButton: "#rule-search-button",
		ruleAddButton: '#rule-add-button',
		ruleReult: "#rule-add-result",
		ruleElement: "#rule-add-table",
		rulePager: {
			id: "#rule-add-pager",
			defaultPageSize: 20,
			defaultPageNumber: 1
		},
		by: by,
		token: token,
		promoId: $('#promoId').val(),
		promoCode: $('#promoCode').val(),
		rulePromoElement: '#rule-promo-table',
		rulePromoResult: '#rule-promo-result',
		rulePromoPager: {
			id: "#rule-promo-pager",
			defaultPageSize: 15,
			defaultPageNumber: 1
		}
	});
	if (promotionDetail && promotionDetail.productInPromotionElement) {
		const promoId = $('#promoId').val();
		const promoCode = $('#promoCode').val();

		promotionDetail.fetchProductInPromotion({
			id: promoId,
			token: token,
			promotionType: 1,
			by: by,
			pager: {
				pageSize: promotionDetail.productInPromotionDefaultPageSize,
				pageNumber: 1
			}
		});

		promotionDetail.fetchRuleInPromotion({
			code: promoCode,
			token: token,
			promotionType: 1,
			by: by,
			pager: {
				pageSize: promotionDetail.rulePromoPagerDefaultPageSize,
				pageNumber: 1
			}
		});
	}

	if (pageTypeId === 18 || pageTypeId === 19) {
		const options = {
			pointLevel: ".point-level",
			updateRequirePointBtn: "#update-require-point-btn",
			updatePricePerPointBtn: "#update-price-per-point-btn",
			expireDate: '#expire-date',
			expireYear: '#expire-year',
            minPurchase: '#min-purchase',
		};

		$('#datetimepicker input').datepicker({
			format: 'dd/mm/yyyy',
			multidate: false,
			todayHighlight: true
		});

		const point = new Point(options);
		point.on("editRequirePoint", function () {
			let items = [];

			this.pointLevel.each(function () {
				let item = {};

				item.PointLevelId = parseInt(this.dataset.levelId);
				item.RequiredPoint = parseInt($(this).find("input")[0].value);
				item.RecModifiedBy = by;
				items.push(item);
			});

			let dto = {
				updatedLevels: items,
				token: token,
				updatedBy: by
			};

			this.edit(dto);
		});

		point.on("editPricePerPoint", function () {
			let items = [];

			this.pointLevel.each(function () {
				let item = {};

				item.PointLevelId = parseInt(this.dataset.levelId);
				item.PricePerPoint = parseFloat($(this).find("input")[0].value);
				item.RecModifiedBy = by;
				items.push(item);
			});

			let dto = {
				updatedLevels: items,
				token: token,
				updatedBy: by,
				expireYear: this.expireYear.find("option:selected").val(),
                minPurchase: this.minPurchase.val()
			};
			//expireDate: this.expireDate.val(),
			this.edit(dto);
		});
	}
	else if (pageTypeId === 16) {
		const reward = new Reward({
			code: "#reward-code-text",
			recStatus: "#reward-status-dd",
			submit: "#reward-button",
			element: "#reward-result",
			table: "#reward-list",
			exportButton: '#reward-export',
			pager: {
				id: "#reward-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});
		reward.fetch({
			code: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			token: token,
			by: by
		});
	}
	else if (pageTypeId === 17 || pageTypeId === 28) {
		let rewardDto = {
			rewardId: '#reward-id',
			code: '#reward-code',
			name: '#reward-name',
			description: '#reward-description',
			//rewardType: '#reward-type',
			rewardPointOnly: '#reward-point-only',
			rewardPoint: '#reward-point',
			rewardMoney: '#reward-money',
			rewardSave: '#reward-save',
			rewardSearchCodeButton: '#reward-search-code-button',
			recStatus: '#reward-status'
		};

		const rewardUpsert = new RewardUpSert(rewardDto);
		rewardUpsert.on("save", function () {
			if (this.valid()) {
				$.extend(true, rewardDto, { token: token, by: by });
				this.save(rewardDto);
			}
			else {
				toastr.error('ข้อมูลไม่ถูกต้องกรุณาตรวจสอบข้อมูลใหม่อีกครั้ง', 'Oop!');
			}
		});
		rewardUpsert.on("rewardSearchCode", function () {
			this.searchCode({ token: token, by: by });
		});

	}
	else if (pageTypeId === 25) {
		const gamification = new Gamification({
			code: "#game-code-text",
			recStatus: "#game-status",
			applyTo: "#game-apply",
			submit: "#game-button",
			element: "#game-result",
			table: "#game-list",
			exportButton: '#game-export',
			pager: {
				id: "#game-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		gamification.fetch({
			code: null,
			recStatus: null,
			applyTo: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			}
		});
	}
	else if (pageTypeId === 26 || pageTypeId === 29) {
		//Gamification - create/edit

		$('#datetimepicker1 input').datepicker({
			format: 'dd/mm/yyyy',
			multidate: false,
			todayHighlight: true
		});

		$('#datetimepicker2 input').datepicker({
			format: 'dd/mm/yyyy',
			multidate: false,
			todayHighlight: true
		});

		const gamification = new GamificationUpsert({
			gameId: '#game-id',
			gamecode: '#gamecode',
			recStatus: "#game-status",
			desc: '#game-detail',
			applyTo: '#game-apply',
			point: '#game-points',
			validFrom: '#valid-from',
			validTo: '#valid-to',
			createBtn: '#game-create-btn',
			updateBtn: '#game-update-btn',
			by: by,
			token: token
		});
	}
	else if (pageTypeId === 21 || pageTypeId === 30) {
		// Rule Settings
		let ruleUpSertDto = {
			ruleId: '#rule-id',
			ruleCode: '#rule-code',
			ruleDetail: '#rule-detail',
			rulePointLevel: '#rule-point-level',
			rulePaymentType: '#rule-payment-type',
			ruleCardType: '#rule-card-type',
			ruleBankType: '#rule-bank-type',
			ruleIsAddPoint: '#rule-is-add-point',
			ruleIsAddTime: '#rule-is-add-time',
			ruleIsChangeBase: '#rule-is-change-base',
			rulePointCount: '#rule-point-count',
			rulePointCountUnit: '#rule-point-count-unit',
			rulePerItem: '#rule-per-item',
			rulePerOrder: '#rule-per-order',
			ruleFromDate: '#rule-from-date',
			ruleToDate: '#rule-to-date',
			ruleSaveBtn: '#rule-save-btn',
			recStatus: '#rule-status'
		};
		const ruleUpSert = new RuleUpSert(ruleUpSertDto);
		ruleUpSert.on("save", function () {
			if (this.valid()) {
				$.extend(true, ruleUpSertDto, { token: token, by: by });
				this.save(ruleUpSertDto);
			}
		});
	}
	else if (pageTypeId === 20) {
		const rule = new Rule({
			code: "#rule-code-text",
			recStatus: "#rule-status-dd",
			calculateType: '#rule-calculate-type-dd',
			submit: "#rule-button",
			element: "#rule-result",
			table: "#rule-list",
			exportButton: '#rule-export',
			pager: {
				id: "#rule-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		rule.fetch({
			code: null,
			calculateType: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			token: token,
			by: by
		});

	}
	else if (pageTypeId === 23) {
		const promo = new Promotion({
			code: "#promo-code-text",
			recStatus: "#promo-status-dd",
			submit: "#promo-button",
			createPromo: '#promo-create-button',
			rules: "#promo-rule-dd",
			element: "#promo-result",
			table: "#promo-table",
			pager: {
				id: "#promo-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		promo.fetch({
			code: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			promotionType: 1,
			token: token,
			by: by,
			promoRuleId: null,
		});
	}
	else if (pageTypeId === 42) {
		const searchPromotion = new SearchPromotion({
			code: "#promo-code-text",
			type: 1, // promotion
			recStatus: "#promo-status-dd",
			submit: "#promo-button",
			element: "#promo-result",
			table: "#promo-table",
			pager: {
				id: "#promo-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		searchPromotion.fetch({
			code: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			token: token,
			by: by
		});
	}
	else if (pageTypeId === 24) {
		const campaign = new Campaign({
			code: "#promo-campaign-code-text",
			recStatus: "#promo-campaign-status-dd",
			submit: "#promo-campaign-button",
			createPromo: '#promo-campaign-create-button',
			element: "#promo-campaign-result",
			table: "#promo-campaign-table",
			pager: {
				id: "#promo-campaign-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token,
			rules: "#promo-campaign-rule-dd",
		});

		campaign.fetch({
			code: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			promotionType: 2,
			token: token,
			by: by,
			promoRuleId: null
		});
	}
	else if (pageTypeId === 43) {
		const searchCampaign = new SearchCampaign({
			code: "#promo-campaign-code-text",
			recStatus: "#promo-campaign-status-dd",
			submit: "#promo-campaign-button",
			element: "#promo-campaign-result",
			table: "#promo-campaign-table",
			pager: {
				id: "#promo-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		searchCampaign.fetch({
			code: null,
			recStatus: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			token: token,
			by: by
		});
	}
	else if (pageTypeId === 22) {
		//PointBasicPage
		const basicPoint = new BasicPoint({
			code: "#product-search-text",
			submit: "#product-search-button",
			element: "#points-result",
			table: "#points-table",
			exportButton: "#basicpoint-export",
			pager: {
				id: "#points-pager",
				defaultPageSize: 15,
				defaultPageNumber: 1
			},
			by: by,
			token: token
		});

		basicPoint.fetch({
			code: null,
			pager: {
				pageSize: 15,
				pageNumber: 1
			},
			token: token,
			by: by
		});
	}
	else if (pageTypeId === 66) {
		const uploadMemberPoint = new UploadMemberPoint({
			uploadFileForm: "#uploadFileForm",
			uploadButton: "#btn-upload",
			saveButton: "#btn-save",
			fileUpload: "#uploadMemberPoint",
			fileUploadName: 'uploadMemberPoint',
			tableResult: "#table-result",
			resultList: "#result-list",
			by: by,
			token: token
		});

		uploadMemberPoint.tableResult.hide();
		uploadMemberPoint.saveButton.attr('disabled', 'disabled');
	}

});