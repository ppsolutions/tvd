const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');

import toastr from 'toastr';
import PagerTemplate from '../../shared/pager.hbs';
import PromotionDetailService from './promotionDetail.Service';
import ProductInPromotionTemplate from '../promotionItems.hbs';
import DomSelectorValidator from '../../shared/domSelectorValidator';
import RuleService from '../rule.Service';
import RuleTemplate from './promotionDetailSearchRule.hbs';
import RuleInPromotionTemplate from './promotionDetailRuleInPromotion.hbs';

export default class PromotionDetail {
    constructor(options) {
        if (!validateElement($(options.element)).valid) return;

        EventEmitter.call(this);
        this.pagerTemplate = PagerTemplate;
        this.productInPromotionTemplate = ProductInPromotionTemplate;
        this.promotionDetailService = new PromotionDetailService();
        this.token = options.token;
        this.by = options.by;
        this.productInPromotionElement = $(options.productInPromotionElement);
        this.productInPromotion = $(options.productInPromotion);
        this.productInPromotionPager = $(options.productInPromotionPager.id);
        this.productInPromotionDefaultPageSize = options.productInPromotionPager.defaultPageSize;
        
        this.ruleSelectedAll = $(options.ruleSelectedAll);
        this.ruleSearchModalButton = $(options.ruleSearchModalButton);
        this.ruleService = new RuleService();
        this.ruleTemplate = RuleTemplate;
        this.ruleCode = $(options.ruleCode);
        this.ruleStatus = $(options.ruleStatus);
        //this.ruleCalculateType = $(options.ruleCalculateType);
        this.ruleSearchButton = $(options.ruleSearchButton);
        this.ruleAddButton = $(options.ruleAddButton);
        this.ruleReult = $(options.ruleReult);
        this.ruleReultId = options.ruleReult;
        this.ruleElement = $(options.ruleElement);
        this.rulePager = $(options.rulePager.id);
        this.rulePagerId = options.rulePager.id;
        this.rulePagerDefaultPageSize = options.rulePager.defaultPageSize;
        this.ruleMarkSelectedAll = false;
        this.ruleSelectedIds = [];
        this.promoId = options.promoId;
        this.promoCode = options.promoCode;

        this.ruleInPromotionTemplate = RuleInPromotionTemplate;
        this.rulePromoElement = $(options.rulePromoElement);
        this.rulePromoResult = $(options.rulePromoResult);
        this.rulePromoPager = $(options.rulePromoPager);
        this.rulePromoPagerId = options.rulePromoPager.id;
        this.rulePromoPagerDefaultPageSize = options.rulePromoPager.defaultPageSize;

        eventBinding(this);
    }
}

inherits(PromotionDetail, EventEmitter);

function validateElement(opt) {
    var selectorValidator = new DomSelectorValidator(opt);
    var valid = selectorValidator.validate();

    // if (!valid.valid)
    //     console.log(valid.message);

    return valid;
}

function eventBinding(from) {
    var seft = from;

    $(seft.ruleCode).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length == 0 ? null : $(this).val(),
                recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
                calculateType: null,
                pager: {
                  pageSize: seft.rulePagerDefaultPageSize,
                  pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            };
            seft.fetchRule(dto);
        }
    });

    $(seft.ruleSearchButton).on("click", function (e) {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
              pageSize: seft.rulePagerDefaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetchRule(dto);
    });

    $(seft.ruleSearchModalButton).on("click", function (e) {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
              pageSize: seft.rulePagerDefaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetchRule(dto);
    });

    $(seft.rulePagerId).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
                pageSize: seft.rulePagerDefaultPageSize,
                pageNumber: $(this).data().pn
            }
        };
        seft.fetchRule(dto);
    });

    $(seft.ruleSelectedAll).on("click", function () {
        var val = $(this).is(':checked');
        if (val) {
            $(seft.ruleElement).find("input[type=checkbox]").prop("checked", val);
            seft.ruleMarkSelectedAll = true;
        }
        else {
            $(seft.ruleElement).find("input[type=checkbox]").prop("checked", val);
            seft.ruleMarkSelectedAll = false;
            seft.ruleSelectedIds = [];
        }
    });

    $(seft.ruleAddButton).on("click", function () {
        if(confirm('ยืนยันการทำรายการ')) {
            let dto = {
                ruleIds:[],
                selectedAll: false,
                token: seft.token,
                by: seft.by,
                promoId: seft.promoId,
                promotionType: 1
            };

            if(seft.ruleMarkSelectedAll === false) {
                dto.selectedAll = false;
                $(seft.ruleReultId).find("input[type=checkbox]").each(function (e) {
                    var checked = $(this).is(":checked");
                    if (checked) {
                        var ruleId = $(this).data().id;
                        dto.ruleIds.push(ruleId);
                    }
                });
            }
            else {
                dto.selectedAll = true;
            }

            seft.addRuleInPromotion(dto);
        }
    });

    $(seft.rulePromoElement).on("click", "#rule-promo-result > tr > td > a[name=delete]", function() {
        if(confirm('ยืนยันการทำรายการ')) {
            const id = $(this).data().id;
            if(id) {
                seft.removeRuleInPromotion({
                    promoRuleId: id,
                    token: seft.token,
                    by: seft.by,
                });
            }
        }
    });

    $(seft.rulePromoElement).on("change", "#rule-promo-result > tr > td > select[id=rule-promo-order]", function() {
        const promoRuleId = $(this).data().promoRuleId;
        const ruleOrder = $(this).val();
        if(promoRuleId && ruleOrder) {
            seft.updateRuleOrder({
                promoRuleId: promoRuleId,
                ruleOrder: ruleOrder,
                token: seft.token,
                by: seft.by,
            });
        }
    });
}

PromotionDetail.prototype.removeRuleInPromotion = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.fetchRuleInPromotion({
                //id: seft.promoId,
                code: seft.promoCode,
                token: seft.token,
                promotionType: 1,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.promotionDetailService.removeRuleInPromotion(dto, callback, failure);
}

PromotionDetail.prototype.fetchRule = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.renderRuleTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.ruleService.fetch(dto, callback, failure);
}

PromotionDetail.prototype.fetchProductInPromotion = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderProductInPromotionTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    seft.promotionDetailService.fetchProductInPromotion(dto, callback, failure);
}

PromotionDetail.prototype.fetchRuleInPromotion = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderRuleInPromotionTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    seft.promotionDetailService.fetchRuleInPromotion(dto, callback, failure);
}

PromotionDetail.prototype.renderProductInPromotionTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.productInPromotion.html(this.productInPromotionTemplate(result));
    if(this.productInPromotionPager) {
        this.productInPromotionPager.html(this.pagerTemplate(pager));
    }
};

PromotionDetail.prototype.renderRuleTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.ruleReult.html(this.ruleTemplate(result));
    if(pager) {
        this.rulePager.html(this.pagerTemplate(pager));
    }
};

PromotionDetail.prototype.addRuleInPromotion = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            $('#exampleModal').modal('hide');
            seft.fetchRuleInPromotion({
                //id: seft.promoId,
                code: seft.promoCode,
                token: seft.token,
                promotionType: 1,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.promotionDetailService.addRuleInPromotion(dto, callback, failure);
}

PromotionDetail.prototype.renderRuleInPromotionTemplateWithData = function (data, pager) {
    const result = {
        by_items: data,
        by_dropdowns: data
    };

    this.rulePromoResult.html(this.ruleInPromotionTemplate(result));
    if(pager) {
        this.rulePromoPager.html(this.pagerTemplate(pager));
    }
};

PromotionDetail.prototype.updateRuleOrder = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.fetchRuleInPromotion({
                //id: seft.promoId,
                code: seft.promoCode,
                token: seft.token,
                promotionType: 1,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.promotionDetailService.updateRuleOrder(dto, callback, failure);
}