﻿var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');
var moment = require('moment');

import Validator from '../shared/validator';
import RuleService from './rule.Service';
import toastr from 'toastr';

class RuleUpSert {
    constructor(options) {
        EventEmitter.call(this);
        this.service = new RuleService();
        this.validator = new Validator();

        this.ruleId = $(options.ruleId);
        this.ruleCode = $(options.ruleCode);
        this.ruleDetail = $(options.ruleDetail);
        this.rulePointLevel = $(options.rulePointLevel);
        this.rulePaymentType = $(options.rulePaymentType);
        this.ruleCardType = $(options.ruleCardType);
        this.ruleBankType = $(options.ruleBankType);
        this.ruleIsAddPoint = $(options.ruleIsAddPoint);
        this.ruleIsAddTime = $(options.ruleIsAddTime);
        this.ruleIsChangeBase = $(options.ruleIsChangeBase);
        this.rulePointCount = $(options.rulePointCount);
        this.rulePointCountUnit = $(options.rulePointCountUnit);
        this.rulePerItem = $(options.rulePerItem);
        this.rulePerOrder = $(options.rulePerOrder);
        this.ruleFromDate = $(options.ruleFromDate);
        this.ruleToDate = $(options.ruleToDate);
        this.ruleSaveBtn = $(options.ruleSaveBtn);
        this.recStatus = $(options.recStatus);
        eventBinding(this);
    }
}

inherits(RuleUpSert, EventEmitter);

function eventBinding(from) {
    const seft = from;

    seft.ruleFromDate.datepicker({
		format: 'dd/mm/yyyy',
		multidate: false,
        todayHighlight: true
    });
    
    seft.ruleFromDate.on('change', function(e){
        //$('#rule-to-date input').data("DateTimePicker").minDate(e.date);
        seft.ruleFromDate.attr("style", "border-color:#ccc");
    });

	seft.ruleToDate.datepicker({
		format: 'dd/mm/yyyy',
        multidate: false,
        todayHighlight: true,
        useCurrent: false
    });
    
    seft.ruleToDate.on('change', function(e){
        //$('#rule-from-date input').data("DateTimePicker").maxDate(e.date);
        seft.ruleToDate.attr("style", "border-color:#ccc");
    });

    seft.ruleSaveBtn.on("click", function () {
        seft.emit('save');
    });

    seft.ruleCode.on("keyup", function (e) {
        if(seft.validator.empty(seft.ruleCode.val()).valid){
            seft.ruleCode.attr("style", "border-color:#ccc");
        }
        else {
            seft.ruleCode.attr("style", "border-color:red");
        }
    });

    seft.ruleDetail.on("keyup", function (e) {
        if(seft.validator.empty(seft.ruleDetail.val()).valid){
            seft.ruleDetail.attr("style", "border-color:#ccc");
        }
        else {
            seft.ruleDetail.attr("style", "border-color:red");
        }
    });

    seft.ruleIsAddPoint.on("click", function(){
        seft.rulePointCountUnit.html('แต้ม');
    });

    seft.ruleIsAddTime.on("click", function(){
        seft.rulePointCountUnit.html('เท่า');
    });

    seft.ruleIsChangeBase.on("click", function(){
        seft.rulePointCountUnit.html('บาทต่อ 1 แต้ม');
    });

    seft.rulePointCount.on("keyup", function (e) {
        if(seft.validator.empty(seft.rulePointCount.val()).valid &&
           seft.validator.positiveNumber(seft.rulePointCount.val()).valid){
            seft.rulePointCount.attr("style", "border-color:#ccc");
        }
        else {
            seft.rulePointCount.attr("style", "border-color:red");
        }
    });

    seft.ruleFromDate.on("keydown", function (e) {
        return false;
    });

    seft.ruleToDate.on("keydown", function (e) {
        return false;
    });
}

RuleUpSert.prototype.valid = function () {
    this.resetError();

    this.validateEmpty(this.ruleCode.val(), this.ruleCode);
    this.validateEmpty(this.ruleDetail.val(), this.ruleDetail);

    if(this.ruleIsAddPoint.prop('checked') === false &&
       this.ruleIsAddTime.prop('checked') === false &&
       this.ruleIsChangeBase.prop('checked') === false) {
       this.validator.basket.push(this.ruleIsAddPoint)
       this.validator.basket.push(this.ruleIsAddTime)
       this.validator.basket.push(this.ruleIsChangeBase)
    }

    this.validatePositiveNumber(this.rulePointCount.val(), this.rulePointCount);

    if(this.rulePerItem.prop('checked') === false &&
        this.rulePerOrder.prop('checked') === false) {
        this.validator.basket.push(this.rulePerItem)
        this.validator.basket.push(this.rulePerOrder)
     }

    this.validateEmpty(this.ruleFromDate.val(), this.ruleFromDate);
    this.validateEmpty(this.ruleToDate.val(), this.ruleToDate);

    var errorCount = this.validator.basket.length;
    if (errorCount > 0) 
    {
        for (var i = 0; i < errorCount; i++) {
            var element = this.validator.basket[i];
            element.attr("style", "border-color:red");
        }
    }
    return (errorCount === 0);
}

RuleUpSert.prototype.resetError = function() {
    for (var i = 0; i < this.validator.basket.length; i++) {
        var element = this.validator.basket[i];
        element.removeAttr("style", "border-color:red");
    }
    this.validator.basket = [];
}

RuleUpSert.prototype.reset = function() {
    this.resetError();
    this.ruleCode.val('');
    this.ruleDetail.val('');
    this.rulePointLevel.val(0);
    this.rulePaymentType.val(0);
    this.ruleCardType.val(0);
    this.ruleBankType.val(0);
    this.ruleIsAddPoint.prop('checked', false);
    this.ruleIsAddTime.prop('checked', false);
    this.ruleIsChangeBase.prop('checked', false);
    this.rulePointCount.val('');
    this.rulePointCountUnit.val('');
    this.rulePerItem.prop('checked', false);
    this.rulePerOrder.prop('checked', false);
    this.ruleFromDate.val('');
    this.ruleToDate.val('');
    this.recStatus.val(1);
}

RuleUpSert.prototype.validateEmpty = function (str, element) {
    return this.validator.empty(str).valid ? true : this.validator.basket.push(element);
}

RuleUpSert.prototype.validatePositiveNumber = function (str, element) {
    return this.validator.positiveNumber(str).valid ? true : this.validator.basket.push(element);
}

RuleUpSert.prototype.save = function (dto) {
    var seft = this;
    const callback = function (data) {
        if (data.Success && data.ResponseData) {
            if(seft.ruleId.val() === undefined) {
                seft.reset();
            }
            toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
        }
        else {
            let errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length === 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');                       
            }
        }
    };

    const failure = function (xhr, textstatus) {
        let errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length === 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };

    let calculationType;
    if(seft.ruleIsAddPoint.prop('checked')) {
        calculationType = 1;
    }
    if(seft.ruleIsAddTime.prop('checked')) {
        calculationType = 2;
    }
    if(seft.ruleIsChangeBase.prop('checked')) {
        calculationType = 3;
    }

    let ruleDto = {
        ruleId: seft.ruleId.val() != undefined ? seft.ruleId.val() : null,
        code: seft.ruleCode.val(),
        description: seft.ruleDetail.val(),
        pointLevelId: seft.rulePointLevel.val(),
        paymentTypeId: seft.rulePaymentType.val(),
        cardTypeId: seft.ruleCardType.val() == 0 ? null : seft.ruleCardType.val(),
        bankId: seft.ruleBankType.val() == 0 ? null : seft.ruleBankType.val(),
        calculationType: calculationType,
        receivedPoint: seft.rulePointCount.val(),
        isPerItem: seft.rulePerItem.prop('checked'),
        isPerOrder: seft.rulePerOrder.prop('checked'),
		validFrom: moment(seft.ruleFromDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
        validTo: moment(seft.ruleToDate.val(), "DD/MM/YYYY").format("YYYY-MM-DD"),
        recStatus: seft.recStatus.val(),
        by: dto.by,
        token: dto.token,
        method: seft.ruleId.val() !== undefined ? 'PUT' : 'POST',
    }

    seft.service.save(ruleDto, callback, failure);
}

export default RuleUpSert;
