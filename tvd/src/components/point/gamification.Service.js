﻿export default class GamificationService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.gamification = location + '/api/point/gamification';
        this.fetchGamification = location + '/api/point/gamification/fetch';
        this.exportUrl = location + '/api/point/gamification/export';
    }

    create(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.gamification,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    edit(dto, callback, failure) {
        $.ajax({
            type: "PUT",
            url: this.gamification,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchGamification,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    delete(dto, callback, failure){
        $.ajax({
            type: "DELETE",
            url: this.gamification,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    export(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.exportUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}