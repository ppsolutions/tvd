const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import GamificationService from './gamification.Service';
import toastr from 'toastr';
import PagerTemplate from '../shared/pager.hbs';
import GameTemplate from './gamification.hbs';
import ExportService from '../shared/export';

class Gamification {
    constructor(options) {
        EventEmitter.call(this);
        this.template = GameTemplate;
        this.pagerTemplate = PagerTemplate;
        this.service = new GamificationService();

        //view
        this.token = options.token;
        this.by = options.by;
        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.exportService = new ExportService();
        this.code = $(options.code);
        this.applyTo = $(options.applyTo);
        this.recStatus = $(options.recStatus);
        this.table = $(options.table);
        this.submit = $(options.submit);
        this.exportButton = $(options.exportButton);
        eventBinding(this);
    }
}

inherits(Gamification, EventEmitter);

function eventBinding(from) {
    const seft  = from;
   
    $(seft.recStatus).on("change", function () {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
            recStatus: seft.recStatus.val() === "-1" ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            }
        };
        seft.fetch(dto);
    });

    $(seft.applyTo).on("change", function () {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
            recStatus: seft.recStatus.val() === "-1" ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            }
        };
        seft.fetch(dto);
    });

    $(seft.code).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: seft.code.val().length === 0 ? null : seft.code.val(),
                applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
                recStatus: seft.recStatus.val() === "-1" ? null : seft.recStatus.val(),
                pager: {
                    pageSize: seft.defaultPageSize,
                    pageNumber: 1
                }
            };
            seft.fetch(dto);
        }
    });

    $(seft.submit).on("click", function (e) {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
            recStatus: seft.recStatus.val() === "-1" ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            }
        };
        seft.fetch(dto);
    });

    $(seft.table).on("click", "#game-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        window.location.href =  '/point/gamificationedit?id=' + id;
    });

    $(seft.table).on("click", "#game-result > tr > td > a[name=delete]", function() {
        if(confirm('ยืนยันการทำรายการ')) {
            const id = $(this).data().id;
            if(id) {
                seft.delete(id);
            }
        }
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
            recStatus: seft.recStatus.val() === "" ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: $(this).data().pn
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    seft.exportButton.on('click', function(){
        const dto = {
            code: seft.code.val().length === 0 ? null : seft.code.val(),
            applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
            recStatus: seft.recStatus.val() === "-1" ? null : seft.recStatus.val()
        };
        seft.export(dto);
    });
}

Gamification.prototype.fetch = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

Gamification.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};

Gamification.prototype.delete = function(id) {
    
    let seft = this;
    
    const callback = function (data) {
        if (data.Success) {
            seft.fetch({
                code: seft.code.val().length === 0 ? null : seft.code.val(),
                applyTo: seft.applyTo.val() === "0" ? null : seft.applyTo.val(),
                recStatus: seft.recStatus.val() === "" ? null : seft.recStatus.val(),
                pager: {
                    pageSize: seft.defaultPageSize,
                    pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            });
        }
        else {
            var errorMsgFromServer = data.Message;
            if (errorMsgFromServer.length == 0) {
                toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
            } else {
                toastr.error(errorMsgFromServer, 'Oop!');          
            }
        }
    };
    
    const failure = function (xhr, textstatus) {
        var errorMsgFromServer = xhr.responseText;
        if (errorMsgFromServer.length == 0) {
            toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
        } else {
            toastr.error(errorMsgFromServer, 'Oop!');
        }
    };
    
    const dto = {
        gameId: id,
        updatedBy: seft.by,
        token: seft.token
    };
    
    this.service.delete(dto, callback, failure);
}
Gamification.prototype.export = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            const fields = ['GamificationPointId', 'GamificationCode','Description', 'ApplyTo', 'IncreasePoint','ValidFrom','ValidTo',  'RecStatus'];
            try {
                let items = data.ResponseData;
                seft.exportService.download({ data: items, fields: fields }, 'exportGame.csv');
            } catch (err) {
                toastr.error(err, 'Oop!');
            }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };

    this.service.export(dto, callback, failure);
}
    
export default Gamification;