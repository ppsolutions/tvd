﻿export default class CampaignDetailService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchPromoInCampaignUrl = location + '/api/point/promotion/searchpromotionincampaign';
        this.addRuleInPromotionUrl = location + '/api/point/promotion/addruleinpromotion';
        this.fetchRuleInPromotionUrl = location + '/api/point/promotion/searchruleinpromotion';
        this.removeRuleInPromotionUrl = location + '/api/point/promotion/updateruleinpromotion';
        this.saveCampaignDateUrl = location + '/api/point/promotion/savecampaigndate';
        this.updateRuleOrderUrl = location + '/api/point/promotion/updateruleorder';
    }

    fetchPromoInCampaign(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchPromoInCampaignUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    addRuleInPromotion(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.addRuleInPromotionUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
    
    fetchRuleInCampaign(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchRuleInPromotionUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    removeRuleInPromotion(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.removeRuleInPromotionUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    saveDate(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.saveCampaignDateUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    updateRuleOrder(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.updateRuleOrderUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }
}