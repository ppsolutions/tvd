const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');

import toastr from 'toastr';
import PagerTemplate from '../../shared/pager.hbs';
import SearchCampaignService from './searchCampaign.Service';
import SearchCampaignTemplate from './searchCampaign.hbs';

export default class SearchCampaign {
    constructor(options) {
        EventEmitter.call(this);
        this.pagerTemplate = PagerTemplate;
        this.template = SearchCampaignTemplate;
        this.service = new SearchCampaignService();
        this.token = options.token;
        this.by = options.by;
        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.pagerId = options.pager.id;
        this.defaultPageSize = options.pager.defaultPageSize;
        this.table = $(options.table);

        this.code = $(options.code);
        this.recStatus = $(options.recStatus);
        this.submit = $(options.submit);

        eventBinding(this);
    }
}

inherits(SearchCampaign, EventEmitter);

function eventBinding(from) {
    var seft = from;

    $(seft.recStatus).on("change", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: $(this).val() == -1 ? null : $(this).val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.code).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length == 0 ? null : $(this).val(),
                recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
                pager: {
                  pageSize: seft.defaultPageSize,
                  pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            };
            seft.fetch(dto);
        }
    });

    $(seft.submit).on("click", function (e) {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.pagerId).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: $(this).data().pn
            }
        };
        seft.fetch(dto);
    });
}

SearchCampaign.prototype.fetch = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

SearchCampaign.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};