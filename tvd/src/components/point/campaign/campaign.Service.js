﻿export default class CampaignService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchPromotionUrl = location + '/api/point/promotion/fetch';
        this.fetchRuleUrl = location + '/api/point/promotion/fetchrule';
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchPromotionUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

    fetchRule(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchRuleUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

}