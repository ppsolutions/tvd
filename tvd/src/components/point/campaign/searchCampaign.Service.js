﻿export default class SearchCampaignService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.fetchCampaignUrl = location + '/api/point/promotion/searchcampaign';
    }

    fetch(dto, callback, failure) {
        $.ajax({
            type: "POST",
            url: this.fetchCampaignUrl,
            headers: {
                "token": dto.token
            },
            data: JSON.stringify(dto),
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

}