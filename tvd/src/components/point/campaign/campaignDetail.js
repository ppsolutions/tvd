const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');

import toastr from 'toastr';
import PagerTemplate from '../../shared/pager.hbs';
import CampaignDetailService from './campaignDetail.Service';
import PromoInCampaignTemplate from './promotionItems.hbs';
import DomSelectorValidator from '../../shared/domSelectorValidator';
import RuleService from '../rule.Service';
import RuleTemplate from './campaignDetailSearchRule.hbs';
import RuleInPromotionTemplate from './campaignDetailRuleInPromotion.hbs';

export default class CampaignDetail {
    constructor(options) {
        if (!validateElement($(options.element)).valid) return;

        EventEmitter.call(this);
        this.pagerTemplate = PagerTemplate;
        this.promoInCampaignTemplate = PromoInCampaignTemplate;
        this.campaignDetailService = new CampaignDetailService();
        this.token = options.token;
        this.by = options.by;
        this.promoInCampaignElement = $(options.promoInCampaignElement);
        this.promoInCampaign = $(options.promoInCampaign);
        this.promoInCampaignPager = $(options.promoInCampaignPager.id);
        this.promoInCampaignDefaultPageSize = options.promoInCampaignPager.defaultPageSize;
        
        this.ruleSelectedAll = $(options.ruleSelectedAll);
        this.ruleSearchModalButton = $(options.ruleSearchModalButton);
        this.ruleService = new RuleService();
        this.ruleTemplate = RuleTemplate;
        this.ruleCode = $(options.ruleCode);
        this.ruleStatus = $(options.ruleStatus);
        //this.ruleCalculateType = $(options.ruleCalculateType);
        this.ruleSearchButton = $(options.ruleSearchButton);
        this.ruleAddButton = $(options.ruleAddButton);
        this.ruleReult = $(options.ruleReult);
        this.ruleReultId = options.ruleReult;
        this.ruleElement = $(options.ruleElement);
        this.rulePager = $(options.rulePager.id);
        this.rulePagerId = options.rulePager.id;
        this.rulePagerDefaultPageSize = options.rulePager.defaultPageSize;
        this.ruleMarkSelectedAll = false;
        this.ruleSelectedIds = [];
        this.promoId = options.promoId;
        this.promoCode = options.promoCode;

        this.ruleInPromotionTemplate = RuleInPromotionTemplate;
        this.rulePromoElement = $(options.rulePromoElement);
        this.rulePromoResult = $(options.rulePromoResult);
        this.rulePromoPager = $(options.rulePromoPager);
        this.rulePromoPagerId = options.rulePromoPager.id;
        this.rulePromoPagerDefaultPageSize = options.rulePromoPager.defaultPageSize;

        this.ruleSaveDate = $(options.ruleSaveDate);

        eventBinding(this);
    }
}

inherits(CampaignDetail, EventEmitter);

function validateElement(opt) {
    const selectorValidator = new DomSelectorValidator(opt);
    const valid = selectorValidator.validate();

    return valid;
}

function eventBinding(from) {
    const seft = from;

    $(seft.ruleSaveDate).on('click', function(e) {
        const st = $('#valid-from').val();
        const en = $('#valid-to').val();
        if(st && en) {
            seft.saveDate(st, en, seft.promoCode);
        }
    });

	$('#datetimepicker1 input').datepicker({
		format: 'dd/mm/yyyy',
        multidate: false,
        todayHighlight: true
    });

	$('#datetimepicker2 input').datepicker({
		format: 'dd/mm/yyyy',
        multidate: false,
        todayHighlight: true
    });

    $(seft.ruleCode).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length == 0 ? null : $(this).val(),
                recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
                calculateType: null,
                pager: {
                  pageSize: seft.rulePagerDefaultPageSize,
                  pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            };
            seft.fetchRule(dto);
        }
    });

    $(seft.ruleSearchButton).on("click", function (e) {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
              pageSize: seft.rulePagerDefaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetchRule(dto);
    });

    $(seft.ruleSearchModalButton).on("click", function (e) {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
              pageSize: seft.rulePagerDefaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetchRule(dto);
    });

    $(seft.rulePagerId).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.ruleCode.val().length == 0 ? null : seft.ruleCode.val(),
            recStatus: seft.ruleStatus.val() == -1 ? null : seft.ruleStatus.val(),
            calculateType: null,
            pager: {
                pageSize: seft.rulePagerDefaultPageSize,
                pageNumber: $(this).data().pn
            }
        };
        seft.fetchRule(dto);
    });

    $(seft.ruleSelectedAll).on("click", function () {
        var val = $(this).is(':checked');
        if (val) {
            $(seft.ruleElement).find("input[type=checkbox]").prop("checked", val);
            seft.ruleMarkSelectedAll = true;
        }
        else {
            $(seft.ruleElement).find("input[type=checkbox]").prop("checked", val);
            seft.ruleMarkSelectedAll = false;
            seft.ruleSelectedIds = [];
        }
    });

    $(seft.ruleAddButton).on("click", function () {
        if(confirm('ยืนยันการทำรายการ')) {
            let dto = {
                ruleIds:[],
                selectedAll: false,
                token: seft.token,
                by: seft.by,
                promoId: seft.promoId,
                promotionType: 2
            };

            if(seft.ruleMarkSelectedAll === false) {
                dto.selectedAll = false;
                $(seft.ruleReultId).find("input[type=checkbox]").each(function (e) {
                    var checked = $(this).is(":checked");
                    if (checked) {
                        var ruleId = $(this).data().id;
                        dto.ruleIds.push(ruleId);
                    }
                });
            }
            else {
                dto.selectedAll = true;
            }

            seft.addRuleInPromotion(dto);
        }
    });

    $(seft.rulePromoElement).on("click", "#rule-campaign-result > tr > td > a[name=delete]", function() {
        if(confirm('ยืนยันการทำรายการ')) {
            const id = $(this).data().id;
            if(id) {
                seft.removeRuleInPromotion({
                    promoRuleId: id,
                    token: seft.token,
                    by: seft.by,
                });
            }
        }
    });

    $(seft.rulePromoElement).on("change", "#rule-campaign-result > tr > td > select[id=rule-promo-order]", function() {
        const promoRuleId = $(this).data().promoRuleId;
        const ruleOrder = $(this).val();
        if(promoRuleId && ruleOrder) {
            seft.updateRuleOrder({
                promoRuleId: promoRuleId,
                ruleOrder: ruleOrder,
                token: seft.token,
                by: seft.by
            });
        }
    });
}

CampaignDetail.prototype.saveDate = function(st, en, id) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            if(data.ResponseData === 1) {
            // seft.fetchRuleInCampaign({
            //     id: seft.promoId,
            //     token: seft.token,
            //     promotionType: 2,
            //     by: seft.by,
            //     pager: {
            //         pageSize: seft.rulePromoPagerDefaultPageSize,
            //         pageNumber: 1
            //     },
            // });
                toastr.success('DONE!');
            }
            else {
                toastr.failure('Fail!');
            }
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    const dto = {
        startDate: st,
        endDate: en,
        code: id,
        by: seft.by, 
        token: seft.token
    };
    this.campaignDetailService.saveDate(dto, callback, failure);
}

CampaignDetail.prototype.removeRuleInPromotion = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.fetchRuleInCampaign({
                //id: seft.promoId,
                code: seft.promoCode,
                token: seft.token,
                promotionType: 2,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.campaignDetailService.removeRuleInPromotion(dto, callback, failure);
}

CampaignDetail.prototype.fetchRule = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.renderRuleTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.ruleService.fetch(dto, callback, failure);
}

CampaignDetail.prototype.fetchPromoInCampaign = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderPromoInCampaignTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    seft.campaignDetailService.fetchPromoInCampaign(dto, callback, failure);
}

CampaignDetail.prototype.fetchRuleInCampaign = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderRuleInPromotionTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    seft.campaignDetailService.fetchRuleInCampaign(dto, callback, failure);
}

CampaignDetail.prototype.renderPromoInCampaignTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };
    if(result.items) {
    this.promoInCampaign.html(this.promoInCampaignTemplate(result));
    }
    if(pager) {
        this.promoInCampaignPager.html(this.pagerTemplate(pager));
    }
};

CampaignDetail.prototype.renderRuleTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.ruleReult.html(this.ruleTemplate(result));
    if(pager) {
        this.rulePager.html(this.pagerTemplate(pager));
    }
};

CampaignDetail.prototype.addRuleInPromotion = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            $('#exampleModal').modal('hide');
            seft.fetchRuleInCampaign({
                code: seft.promoCode,
                token: seft.token,
                promotionType: 2,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.campaignDetailService.addRuleInPromotion(dto, callback, failure);
}

CampaignDetail.prototype.renderRuleInPromotionTemplateWithData = function (data, pager) {
    const result = {
        by_items: data,
        by_dropdowns: data
    };

    if(result.by_items) {
        this.rulePromoResult.html(this.ruleInPromotionTemplate(result));
    }
    if(pager) {
        this.rulePromoPager.html(this.pagerTemplate(pager));
    }
};

CampaignDetail.prototype.updateRuleOrder = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            seft.fetchRuleInCampaign({
                //id: seft.promoId,
                code: seft.promoCode,
                token: seft.token,
                promotionType: 2,
                by: seft.by,
                pager: {
                    pageSize: seft.rulePromoPagerDefaultPageSize,
                    pageNumber: 1
                },
            });
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.campaignDetailService.updateRuleOrder(dto, callback, failure);
}