﻿const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import toastr from 'toastr';
import UploadMemberPointService from './uploadmemberpoint.Service';
import PagerTemplate from '../shared/pager.hbs';
import UploadMemberPointTemplate from './uploadmemberpoint.hbs';

export default class UploadMemberPoint {
	constructor(options) {
		EventEmitter.call(this);

		this.template = UploadMemberPointTemplate;
		this.pagerTemplate = PagerTemplate;

		this.service = new UploadMemberPointService();
		this.token = options.token;
		this.by = options.by;
		//this.element = $(options.element);
		//this.pager = $(options.pager.id);
		//this.defaultPageSize = options.pager.defaultPageSize;
		this.uploadFileForm = $(options.uploadFileForm);
		this.uploadButton = $(options.uploadButton);
		this.saveButton = $(options.saveButton);
		this.fileUpload = $(options.fileUpload);
		this.tableResult = $(options.tableResult);
		this.resultList = $(options.resultList);

		this.fileUploadName = options.fileUploadName;
		this.files = null;

		eventBinding(this);
	}
}

inherits(UploadMemberPoint, EventEmitter);

function eventBinding(uploadMemberPoint) {
	const seft = uploadMemberPoint;

	seft.fileUpload.on('change', function (event) {
		seft.files = event.target.files;
	})

	seft.saveButton.on('click', function () {
		const raw = seft.resultList.data('rawdata');

		let dto = []
		for (var i = 0; i < raw.length; i++) {
			let obj = {
				PersonId: raw[i].MemberId,
				Points: raw[i].Point,
				ActionType: raw[i].ActionType,
				UpdatedBy: seft.by,
				Token: seft.token,
				Remark: raw[i].Remark
			}
			dto.push(obj);
		}

		const callback = function (data) {
			if (data.Success) {
				seft.resultList.html('');
				seft.saveButton.attr('disabled', 'disabled');
				toastr.success('ระบบได้ทำการบันทึกข้อมูลแล้วค่ะ', 'Success');
			}
		};

		const failure = function (xhr, textstatus) {
			toastr.error(xhr.statusText, 'Oop!');
		};

		seft.service.updateMemberPoint(dto, callback, failure);
	});

	seft.uploadFileForm.on('submit', function (event) {

		event.stopPropagation();
		event.preventDefault();

		if (seft.files !== null) {
			var data = new FormData();
			data.append(seft.fileUploadName, seft.files[0]);

			const dto = {
				token: seft.token,
				data: data
			};

			const callback = function (data) {
				if (data.Success) {
					const result = {
						items: data.ResponseData,
					};
					const html = seft.template(result);
					if (result.items.length > 0) {
						seft.tableResult.show();

						seft.resultList.html(html);
						seft.resultList.data('rawdata', data.ResponseData);

						let canUpdatePoint = false;

						for (var i = 0; i < result.items.length; i++) {
							if (result.items[i].Note !== '') {
								canUpdatePoint = false;
								break;
							}
						}

						if (canUpdatePoint) {
							seft.saveButton.removeAttr('disabled');
						}
					}
				}
			};

			const failure = function (xhr, textstatus) {
				toastr.error(xhr.statusText, 'Oop!');
			};

			seft.service.uploadMemberPoint(dto, callback, failure);
		}
	});
}