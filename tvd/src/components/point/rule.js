

const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import toastr from 'toastr';
import RuleService from './rule.Service';
import PagerTemplate from '../shared/pager.hbs';
import RuleTemplate from './rule.hbs';
import fs from 'browserify-fs';
import ExportService from '../shared/export';

export default class Rule {
    constructor(options) {
        EventEmitter.call(this);
        this.template = RuleTemplate;
        this.pagerTemplate = PagerTemplate;
        this.service = new RuleService();
        this.token = options.token;
        this.by = options.by;
        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.table = $(options.table);
        this.exportService = new ExportService();
        this.exportButton = $(options.exportButton);
        this.code = $(options.code);
        this.calculateType = $(options.calculateType);
        this.recStatus = $(options.recStatus);
        this.calculateType = $(options.calculateType);
        this.submit = $(options.submit);

        eventBinding(this);
    }
}

inherits(Rule, EventEmitter);

function eventBinding(from) {
    var seft = from;

    $(seft.recStatus).on("change", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: $(this).val() == -1 ? null : $(this).val(),
            calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.calculateType).on("change", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            calculateType: $(this).val() == 0 ? null : $(this).val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.code).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length == 0 ? null : $(this).val(),
                recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
                calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
                pager: {
                  pageSize: seft.defaultPageSize,
                  pageNumber: 1
                },
                token: seft.token,
                by: seft.by
            };
            seft.fetch(dto);
        }
    });

    $(seft.submit).on("click", function (e) {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    $(seft.table).on("click", "#rule-result > tr > td > a[name=edit]", function(){
        const id = $(this).data().id;
        window.location.href =  '/point/ruleedit?id=' + id;
    });

    $(seft.table).on("click", "#rule-result > tr > td > a[name=detail]", function(){
        const id = $(this).data().id;
        window.location.href =  '/point/ruledetail?id=' + id;
    });

    $(seft.table).on("click", "#rule-result > tr > td > a[name=delete]", function() {
        if(confirm('ยืนยันการทำรายการ')) {
            const id = $(this).data().id;
            if(id) {
                seft.delete(id);
            }
        }
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: $(this).data().pn
            },
            token: seft.token,
            by: seft.by
        };
        seft.fetch(dto);
    });

    seft.exportButton.on('click', function(){
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
            token: seft.token,
            by: seft.by
        };
        seft.export(dto);
    });
}

Rule.prototype.fetch = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

Rule.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.element.html(this.template(result));
     if(pager) {
         this.pager.html(this.pagerTemplate(pager));
     }
};
    
Rule.prototype.delete = function(id) {
        let seft = this;
    
        const callback = function (data) {
            if (data.Success) {
                seft.fetch({
                    code: seft.code.val().length == 0 ? null : seft.code.val(),
                    recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
                    calculateType: seft.calculateType.val() == 0 ? null : seft.calculateType.val(),
                    pager: {
                      pageSize: seft.defaultPageSize,
                      pageNumber: 1
                    },
                    token: seft.token,
                    by: seft.by
                });
            }
            else {
                var errorMsgFromServer = data.Message;
                if (errorMsgFromServer.length == 0) {
                    toastr.error('ไม่สามารถทำรายการได้กรุณาทดลองใหม่อีกครั้ง', 'Oop!');
                } else {
                    toastr.error(errorMsgFromServer, 'Oop!');          
                }
            }
        };
    
        const failure = function (xhr, textstatus) {
            toastr.error(xhr.statusText, 'Oop!');
        };
    
        const dto = {
            ruleId: id,
            recStatus: -1,
            userId: seft.by,
            token: seft.token
        };
    
        this.service.delete(dto, callback, failure);
}

Rule.prototype.export = function(dto) {
    const seft = this;
    const callback = function (data) {
        if (data.Success) {
            const fields = ['RuleId', 'Code', 'Description','PointLevel','PaymentType','CardType','BankType','CalculationType', 'ReceivedPoint','Per', 'ValidFrom','ValidTo',  'RecStatus'];
                try {
                    let items = data.ResponseData;
                    seft.exportService.download({ data: items, fields: fields }, 'exportRule.csv');
                } catch (err) {
                    toastr.error(err, 'Oop!');
                }
        }
    };

    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };

    this.service.export(dto, callback, failure);
}