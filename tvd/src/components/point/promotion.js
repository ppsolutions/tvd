
const EventEmitter = require('events').EventEmitter;
const inherits = require('inherits');
import toastr from 'toastr';
import PromotionService from './promotion.Service';
import PagerTemplate from '../shared/pager.hbs';
import PromotionTemplate from './promotion.hbs';
import PromotionRuleTemplate from './promotionRules.hbs';

export default class Promotion {
    constructor(options) {
        EventEmitter.call(this);
        this.template = PromotionTemplate;
        this.ruleTemplate = PromotionRuleTemplate;
        this.pagerTemplate = PagerTemplate;
        this.service = new PromotionService();
        this.token = options.token;
        this.by = options.by;
        this.element = $(options.element);
        this.pager = $(options.pager.id);
        this.defaultPageSize = options.pager.defaultPageSize;
        this.table = $(options.table);
        this.tableId = options.table;
        this.createPromo = $(options.createPromo);
        this.code = $(options.code);
        this.recStatus = $(options.recStatus);
        this.submit = $(options.submit);
        this.rules = $(options.rules);
        eventBinding(this);
    }
}

inherits(Promotion, EventEmitter);

function eventBinding(from) {
    var seft = from;

    $(seft.createPromo).on('click', function(){
        window.location = '/point/promotion/create';
    });

    $(seft.tableId).on("click", "#promo-result > tr > td > a[name=promo-rule-detail]", function() {
        var id = $(this).data().id;
        var index = $(this).data().index;
        $(this).find('i').toggleClass('glyphicon-minus icon-minus');
        if($(this).find('i').attr('class') == 'glyphicon glyphicon-plus icon-plus') {
            $(this).closest('tr').next().hide();
        }
        else {
            $(this).closest('tr').next().show();
        }

        seft.fetchRule({
            promoId: id,
            token: seft.token,
            by: seft.by
        });
    });

    $(seft.rules).on("change", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            promotionType: 1,
            token: seft.token,
            by: seft.by,
            promoRuleId: $(this).val() == undefined ? null : $(this).val()
        };
        seft.fetch(dto);
    });

    $(seft.recStatus).on("change", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: $(this).val() == -1 ? null : $(this).val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            promotionType: 1,
            token: seft.token,
            by: seft.by,
            promoRuleId: seft.rules.val() == undefined ? null : seft.rules.val()
        };
        seft.fetch(dto);
    });

    $(seft.code).on("keyup", function (e) {
        if (e.keyCode == 13) {
            const dto = {
                code: $(this).val().length == 0 ? null : $(this).val(),
                recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
                pager: {
                  pageSize: seft.defaultPageSize,
                  pageNumber: 1
                },
                promotionType: 1,
                token: seft.token,
                by: seft.by,
                promoRuleId: seft.rules.val() == undefined ? null : seft.rules.val()
            };
            seft.fetch(dto);
        }
    });

    $(seft.submit).on("click", function (e) {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            pager: {
              pageSize: seft.defaultPageSize,
              pageNumber: 1
            },
            promotionType: 1,
            token: seft.token,
            by: seft.by,
            promoRuleId: seft.rules.val() == undefined ? null : seft.rules.val()
        };
        seft.fetch(dto);
    });

    $(seft.pager).on("click", "ul#paging > li > a", function () {
        const dto = {
            code: seft.code.val().length == 0 ? null : seft.code.val(),
            recStatus: seft.recStatus.val() == -1 ? null : seft.recStatus.val(),
            pager: {
                pageSize: seft.defaultPageSize,
                pageNumber: 1
            },
            token: seft.token,
            by: seft.by,
            promotionType: 1,
            promoRuleId: seft.rules.val() == undefined ? null : seft.rules.val()
        };
        seft.fetch(dto);
    });
}

Promotion.prototype.fetch = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            seft.renderTemplateWithData(data.ResponseData, data.Pager);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetch(dto, callback, failure);
}

Promotion.prototype.fetchRule = function(dto) {
    
    const seft = this;

    const callback = function (data) {
        if (data.Success) {
            const result = {
                items: data.ResponseData,
            };
            const html = seft.ruleTemplate(result);
            $(seft.tableId).find('ul[id='+dto.promoId+']').html(html);
        }
    };
    
    const failure = function (xhr, textstatus) {
        toastr.error(xhr.statusText, 'Oop!');
    };
    
    this.service.fetchRule(dto, callback, failure);
}

Promotion.prototype.renderTemplateWithData = function (data, pager) {
    const result = {
        items: data,
    };

    this.element.html(this.template(result));
    if(pager) {
        this.pager.html(this.pagerTemplate(pager));
    }
};
