﻿export default class PointService {
    constructor() {
        const location = 'http://' + window.location.host;
        this.updatePointLevel = location + '/api/points/level';
    }

    edit(dto, callback, failure) {
        $.ajax({
            type: "PUT",
            url: this.updatePointLevel,
            data: JSON.stringify(dto),
            headers: {
                "token": dto.token
            },
            xhrFields: { withCredentials: true },
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (callback !== undefined) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                if (failure !== undefined) {
                    failure(xhr, status);
                }
            },
            timeout: 9999
        });
    }

}