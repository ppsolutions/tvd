﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace tvd.Helper
{
    public static class QueryStringBuilder
    {
        public static string BuildSearchResultQuery(string name, string idCard, string phoneNumber, string memberId)
        {
            var collection = new NameValueCollection();
            if (!string.IsNullOrEmpty(name))
            {
                collection.Add("name", name.Trim());
            }

			if (!string.IsNullOrEmpty(idCard))
			{
				collection.Add("idCard", idCard.Trim());
			}

			if (!string.IsNullOrEmpty(phoneNumber))
            {
                collection.Add("phoneNumber", phoneNumber.Trim());
            }

            if (!string.IsNullOrEmpty(memberId))
            {
                collection.Add("memberId", memberId.Trim());
            }

            return collection.Count == 0 ? string.Empty : ToQueryString(collection);
        }

        private static string ToQueryString(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                    from value in nvc.GetValues(key)
                    select $"{HttpUtility.UrlEncode(key)}={HttpUtility.UrlEncode(value)}")
                .ToArray();
            return "?" + string.Join("&", array);
        }
    }
}