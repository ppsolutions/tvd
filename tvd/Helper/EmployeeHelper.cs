﻿using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services;
using tvd.Services.Interfaces;

namespace tvd.Helper
{
    public static class EmployeeHelper
    {
        private static readonly IDBHelper DbHelper = new DBHelper();
        private static readonly IEmployeeRepository EmployeeRepository = new EmployeeRepository(DbHelper);
        private static readonly  IUrlGeneratorService UrlGeneratorService = new UrlGeneratorService();
        public static EmployeeInfo GetEmployee(string employeeCode)
        {
            var employee = EmployeeRepository.GetEmployee(employeeCode);
            return employee != null
                ? new EmployeeInfo(employee, UrlGeneratorService.GetSettingsEmployeeDetailPageUrl(employeeCode))
                : null;

        }

    }
}