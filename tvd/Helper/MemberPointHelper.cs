﻿using System;
using System.Collections.Generic;
using System.Linq;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;

namespace tvd.Helper
{
    public static class MemberPointHelper
    {
        private static int DaysBeforeExpired = 30;
        public static bool ValidateMemberPointIsEnough(List<MemberPoint> points, int usedPoints)
        {
            var currentActivePoints = GetActivePoints(points);
            return currentActivePoints >= Math.Abs(usedPoints);
        }
        public static int GetActivePoints(List<MemberPoint> points)
        {
            //Active points
            var activePoints = points.Where(o => o.RecStatus == RecStatus.ACTIVE);
            //Get minus point
            var minusPoint = points
                .Where(o => o.Point < 0)
                .Sum(o => o.Point);

            //Get positive point that not expired
            var positivePoints = activePoints
                .Where(o => o.Point >= 0)
                .Sum(o => o.Point);

            //subtract by minus point
            positivePoints += minusPoint;

            if (positivePoints < 0) positivePoints = 0;
            return positivePoints;
        }

        public static int GetNearlyExpirePoints(List<MemberPoint> points)
        {
            var nearlyExpirePoint = points
                .Where(o => o.RecStatus == RecStatus.ACTIVE &&
                            o.RecExpiredWhen.HasValue &&
                            o.RecExpiredWhen.Value.Subtract(DateTime.Now.Date).TotalDays <= DaysBeforeExpired &&
                            o.Point >= 0)
                .Sum(o => o.Point);
            return nearlyExpirePoint;
        }

    }
}