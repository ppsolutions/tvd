﻿using System.Web;
using System.Web.Mvc;
using tvd.model.Models.Request;

namespace tvd.ModelBinder
{
    public class SearchCriteriaModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext context, ModelBindingContext bindingContext)
        {
            var nameParam = bindingContext.ValueProvider.GetValue("name");
			var idCardParam = bindingContext.ValueProvider.GetValue("idCard");
			var phoneNumberParam = bindingContext.ValueProvider.GetValue("phoneNumber");
            var memberIdParam = bindingContext.ValueProvider.GetValue("memberId");
            var queryString = context.RequestContext.HttpContext.Request.QueryString;

            var searchCriteria = new SearchCriteria();

            if (nameParam != null)
            {
                searchCriteria.Name = HttpUtility.HtmlDecode(nameParam.AttemptedValue);
            }
			if (idCardParam != null)
			{
				searchCriteria.IdCard = HttpUtility.HtmlDecode(idCardParam.AttemptedValue);
			}
			if (phoneNumberParam != null)
            {
                searchCriteria.Phone = HttpUtility.HtmlDecode(phoneNumberParam.AttemptedValue);
            }

            if (memberIdParam != null)
            {
                searchCriteria.MemberId = HttpUtility.HtmlDecode(memberIdParam.AttemptedValue);
            }

            return searchCriteria;
        }
    }
}