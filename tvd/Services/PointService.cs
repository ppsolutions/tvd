﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Point;
using tvd.model.Provider;
using tvd.model.Repositories.Interfaces;
using tvd.Models;
using tvd.Services.Interfaces;
using tvd.ViewModel;
using tvd.model.Models.CommonDTO.Rule;

namespace tvd.Services
{
	public class PointService : BaseService, IPointService
	{
		private readonly IPointRepository _pointRepository;
		private readonly IUrlGeneratorService _urlGeneratorService;

		public PointService(IPointRepository customerPointRepository, IUrlGeneratorService urlGeneratorService)
		{
			_pointRepository = customerPointRepository;
			_urlGeneratorService = urlGeneratorService;
		}

        public void UpdatePointLevelAndExpireDate(List<PointLevel> levels,  int? expireYear, decimal? minPurchase, string by)
		{
			foreach (var level in levels)
			{
				_pointRepository.UpdatePointLevel(level);
			}

			if(expireYear.HasValue && minPurchase.HasValue)
			{
				_pointRepository.UpdatePointConfig(expireYear, minPurchase, by);
			}
		}

		public PointViewModel CreatePointViewModel(IContextProvider contextProvider, int? id)
		{
			var vm = new PointViewModel
			{
				RewardCreateUrl = _urlGeneratorService.GetRewardCreateUrl(),
				RuleCreateUrl = _urlGeneratorService.GetRuleCreateUrl(),
				GamificationCreateUrl = _urlGeneratorService.GetGamificationCreateUrl(),
				Sidebar = CreateSidebar(contextProvider),
				SubBreadcrumb = CreateBreadcrumb(contextProvider, id),
				SubPageTitle = GetSubPageTitle((PageTypeId)contextProvider.GetPageTypeId())
			};
			return vm;
		}


		protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
		{
			var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
			var bc = new List<BreadcrumbItem>();
			bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
			bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "กำหนดคะแนน", Url = "" });

			if (pageTypeId == PageTypeId.UploadMemberPoint)
			{
				bc.Add(new BreadcrumbItem() { Name = "ปรับปรุงคะแนนสมาชิก" });
			}
			else if (pageTypeId == PageTypeId.PointRewardPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนนแลกของกำนัล", Url = _urlGeneratorService.GetRewardPageUrl() });
			}
			else if (pageTypeId == PageTypeId.PointRewardCreatePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนนแลกของกำนัล", Url = _urlGeneratorService.GetRewardPageUrl() });
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "เพิ่มของกำนัล", Url = _urlGeneratorService.GetRewardCreateUrl() });
			}
			else if (pageTypeId == PageTypeId.PointRewardEditPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนนแลกของกำนัล", Url = _urlGeneratorService.GetRewardPageUrl() });
				bc.Add(new BreadcrumbItem() { Name = "แก้ไขของกำนัล" });
			}

			else if (pageTypeId == PageTypeId.PointLevelPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนนเลื่อนขั้น", Url = _urlGeneratorService.GetPointLevelUrl() });
			}
			else if (pageTypeId == PageTypeId.PointBasicLevelPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ตั้งค่าคะแนนแบบพื้นฐาน", Url = _urlGeneratorService.GetPointLevelUrl() });
			}
			else if (pageTypeId == PageTypeId.PointRulePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ตั้งกฏการคำนวณ", Url = _urlGeneratorService.GetRuleUrl() });
			}
			else if (pageTypeId == PageTypeId.PointRuleCreatePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ตั้งกฏการคำนวณ", Url = _urlGeneratorService.GetRuleUrl() });
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "เพิ่มกฏการคำนวณ", Url = _urlGeneratorService.GetRuleCreateUrl() });
			}
			else if (pageTypeId == PageTypeId.PointRuleEditPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ตั้งกฏการคำนวณ", Url = _urlGeneratorService.GetRuleUrl() });
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "แก้ไขกฏการคำนวณ", Url = _urlGeneratorService.GetRuleEditUrl(id.Value) });
			}
			else if (pageTypeId == PageTypeId.PointBasicPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนนพื้นฐาน", Url = _urlGeneratorService.GetBasicPointUrl() });
			}
			else if (pageTypeId == PageTypeId.PointPromotionPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "สำหรับ Promotion", Url = _urlGeneratorService.GetPromotionUrl() });
			}
			else if (pageTypeId == PageTypeId.PointCampaignPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "สำหรับ Campaign", Url = _urlGeneratorService.GetCampaignUrl() });
			}
			else if (pageTypeId == PageTypeId.PointGamificationPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนน Gamification", Url = _urlGeneratorService.GetGamificationUrl() });
			}
			else if (pageTypeId == PageTypeId.PointGamificationCreatePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนน Gamification", Url = _urlGeneratorService.GetGamificationUrl() });
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "เพิ่มการคำนวณคะแนน", Url = _urlGeneratorService.GetGamificationCreateUrl() });
			}
			else if (pageTypeId == PageTypeId.PointGamificationEditPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "คะแนน Gamification", Url = _urlGeneratorService.GetGamificationUrl() });
				bc.Add(new BreadcrumbItem() { Name = "แก้ไขการคำนวณคะแนน" });
			}
			else if (pageTypeId == PageTypeId.PointPromotionCreatePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "สำหรับ Promotion", Url = _urlGeneratorService.GetPromotionUrl() });
				bc.Add(new BreadcrumbItem() { Name = "ค้นหาโปรโมชั่น" });
			}
			else if (pageTypeId == PageTypeId.PointCampaignCreatePage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "สำหรับ Campaign", Url = _urlGeneratorService.GetCampaignUrl() });
				bc.Add(new BreadcrumbItem() { Name = "ค้นหาแคมเปญ" });
			}
			else if (pageTypeId == PageTypeId.PointRuleDetailPage)
			{
				bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ตั้งกฏการคำนวณ", Url = _urlGeneratorService.GetRuleUrl() });
				bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "รายละเอียดกฏการคำนวณ", Url = "" });
			}


			return bc;
		}

		protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
		{
			var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

			var sb = new List<SidebarItem>();

			sb.Add(new SidebarItem() { IsHeader = true, Name = "ตั้งค่าคะแนนแบบพื้นฐาน", IsActive = IsActive(pageTypeId, PageTypeId.PointBasicLevelPage), MenuId = 3, ReferMenuId = 0, Url = _urlGeneratorService.GetBasicPointLevelUrl() });
			sb.Add(new SidebarItem() { IsHeader = true, Name = "คะแนนพื้นฐาน", IsActive = IsActive(pageTypeId, PageTypeId.PointBasicPage), MenuId = 5, ReferMenuId = 0, Url = _urlGeneratorService.GetBasicPointUrl() });
			sb.Add(new SidebarItem() { IsHeader = true, Name = "คะแนนเลื่อนขั้น", IsActive = IsActive(pageTypeId, PageTypeId.PointLevelPage), MenuId = 2, ReferMenuId = 0, Url = _urlGeneratorService.GetPointLevelUrl() });

			sb.Add(new SidebarItem()
			{
				IsHeader = true,
				Name = "ปรับปรุงคะแนนสมาชิก",
				IsActive = IsActive(pageTypeId, new List<PageTypeId> {
						PageTypeId.UploadMemberPoint
					}),
				MenuId = 9,
				ReferMenuId = 0,
				Url = _urlGeneratorService.GetUploadMemberPointUrl()
			});

			sb.Add(new SidebarItem()
			{
				IsHeader = true,
				Name = "คะแนนแลกของกำนัล",
				IsActive = IsActive(pageTypeId, new List<PageTypeId> {
						PageTypeId.PointRewardPage,
						PageTypeId.PointRewardCreatePage,
						PageTypeId.PointRewardEditPage
					}),
				MenuId = 1,
				ReferMenuId = 0,
				Url = _urlGeneratorService.GetRewardPageUrl()
			});

			sb.Add(new SidebarItem()
			{
				IsHeader = true,
				Name = "คะแนน Gamification",
				IsActive = IsActive(pageTypeId, new List<PageTypeId> {
						PageTypeId.PointGamificationPage,
						PageTypeId.PointGamificationCreatePage,
						PageTypeId.PointGamificationEditPage
					}),
				MenuId = 8,
				ReferMenuId = 0,
				Url = _urlGeneratorService.GetGamificationUrl()
			});

			sb.Add(new SidebarItem()
			{
				IsHeader = true,
				Name = "ตั้งกฏการคำนวณ",
				IsActive = IsActive(pageTypeId, new List<PageTypeId> {
						PageTypeId.PointRuleCreatePage,
						PageTypeId.PointRuleEditPage,
						PageTypeId.PointRulePage,
						PageTypeId.PointRuleDetailPage
					}),
				MenuId = 4,
				ReferMenuId = 0,
				Url = _urlGeneratorService.GetRuleUrl()
			});
						
			

			//sb.Add(new SidebarItem()
			//{
			//    IsHeader = true,
			//    Name = "สำหรับ Promotion",
			//    IsActive = IsActive(pageTypeId, new List<PageTypeId> {
			//            PageTypeId.PointPromotionPage,
			//            PageTypeId.PointPromotionCreatePage,
			//            PageTypeId.PointPromotionDetailPage
			//        }),
			//    MenuId = 6,
			//    ReferMenuId = 0,
			//    Url = _urlGeneratorService.GetPromotionUrl()
			//});

			sb.Add(new SidebarItem()
			{
				IsHeader = true,
				Name = "ตั้งกฎสำหรับ Campaign",
				IsActive = IsActive(pageTypeId, new List<PageTypeId> {
						PageTypeId.PointCampaignPage,
						PageTypeId.PointCampaignCreatePage,
						PageTypeId.PointCampaignDetailPage
					}),
				MenuId = 7,
				ReferMenuId = 0,
				Url = _urlGeneratorService.GetCampaignUrl()
			});


			return sb;
		}

		public RewardInfo FetchRewardInfo(int rewardId)
		{
			return _pointRepository.FetchRewardInfo(rewardId);
		}

		public PointRuleInfo FetchPointRuleInfo(int ruleId)
		{
			return _pointRepository.FetchPointRuleInfo(ruleId);
		}

		public PromotionInfo FetchPromotionInfo(string id)
		{
			return _pointRepository.FetchPromotionInfo(id);
		}

		public PromotionInfo FetchCampaignInfo(string code)
		{
			return _pointRepository.FetchCampaignInfo(code);
		}


		public IList<PromotionInfo> FetchRuleUsed(int ruleId)
		{
			return _pointRepository.FetchRuleUsed(ruleId);
		}

		public RedeemPoint GetRedeemPoint(string code)
		{
			return _pointRepository.GetRedeemPoint(code);
		}

		public IList<Rule> FetchRuleUsed()
		{
			return _pointRepository.FetchRuleUsed();
		}
	}
}