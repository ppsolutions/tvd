﻿using System.Web.Mvc;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class CommonPropertiesServiceSet : ICommonPropertiesServiceSet
    {
        private IDependencyResolver resolver = DependencyResolver.Current;

        public IUrlGeneratorService UrlGeneratorService { get; private set; }

        public CommonPropertiesServiceSet()
        {
            UrlGeneratorService = resolver.GetService<IUrlGeneratorService>();
        }
    }
}