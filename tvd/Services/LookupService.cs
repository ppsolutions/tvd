﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.CommonDTO.Rule;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.model.V;
using tvd.Services.Interfaces;

namespace tvd.Services
{
	public class LookupService : ILookupService
	{
		private readonly ILookupRepository _lookupRepository;
		private readonly IPointRepository _memberPointRepository;
		private readonly IRoleRepository _roleRepository;
		private readonly INewsRepository _newsRepository;
		private readonly IRuleRepository _ruleRepository;
		public LookupService(ILookupRepository lookupRepository,
			IPointRepository pointRepository,
			IRoleRepository roleRepository,
			INewsRepository newsRepository,
			IRuleRepository ruleRepository)
		{
			_lookupRepository = lookupRepository;
			_memberPointRepository = pointRepository;
			_roleRepository = roleRepository;
			_newsRepository = newsRepository;
			_ruleRepository = ruleRepository;
		}

		public IEnumerable<City> GetCities()
		{
			return _lookupRepository.GetCities();
		}

		public IEnumerable<Title> GetTitles()
		{
			return _lookupRepository.GetTitles();
		}

		public IEnumerable<Question> GetQuestions()
		{
			return _lookupRepository.GetQuestions();
		}

		public IEnumerable<AddressType> GetAddressTypes()
		{
			return _lookupRepository.GetAddressTypes();
		}

		public IEnumerable<PersonType> GetPersonTypes()
		{
			return _lookupRepository.GetPersonTypes();
		}

		public IEnumerable<PointLevel> GetPointLevels()
		{
			return _lookupRepository.GetPointLevels();
		}

		public IEnumerable<Role> GetRoles()
		{
			return _roleRepository.GetRoles();
		}

		public IEnumerable<RedeemPointsType> GetRedeemPointsTypes()
		{
			return _memberPointRepository.GetRedeemPointsTypes();
		}

		public IEnumerable<NewsStatus> GetNewsStatuses()
		{
			return _newsRepository.GetNewsStatuses();
		}

		public IEnumerable<Rule> GetRules()
		{
			return _ruleRepository.GetRules();
		}

		public News GetNews(int newsId)
		{
			return _newsRepository.GetNews(newsId);
		}

		public IEnumerable<Bank> GetBanks()
		{
			return _lookupRepository.GetBanks();
		}

		public IEnumerable<CardType> GetCardTypes()
		{
			return _lookupRepository.GetCardTypes();
		}

		public IEnumerable<PaymentType> GetPaymentType()
		{
			return _lookupRepository.GetPaymentType();
		}

		public IEnumerable<ApplyType> GetApplyTypes()
		{
			return _lookupRepository.GetApplyTypes();
		}

		public ProductInfo GetProductInfoFromView(string code)
		{
			return _lookupRepository.GetProductInfoFromView(code);
		}

		public IEnumerable<ProductInfo> GetProductInfoFromName(string name)
		{
			return _lookupRepository.GetProductInfoFromName(name);
		}

		public PointConfig GetPointConfig()
		{
			return _memberPointRepository.GetPointConfig();
		}
	}
}
