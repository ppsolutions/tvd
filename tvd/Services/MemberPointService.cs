﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using tvd.Helper;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class MemberPointService : IMemberPointService
    {
        private readonly IPointRepository _pointRepository;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IApproveHelper _approveHelper;
        private readonly ILogHelper _logHelper;
        private readonly IMemberRepository _memberRepository;
        private readonly ILookupRepository _lookupRepository;

        public MemberPointService(IPointRepository customerPointRepository, IUrlGeneratorService urlGeneratorService,
            IApproveHelper approveHelper, ILogHelper logHelper, IMemberRepository memberRepository, ILookupRepository lookupRepository)
        {
            _pointRepository = customerPointRepository;
            _urlGeneratorService = urlGeneratorService;
            _approveHelper = approveHelper;
            _logHelper = logHelper;
            _memberRepository = memberRepository;
            _lookupRepository = lookupRepository;
        }

        public int GetCurrentMemberPoints(int personId)
        {
            var customerPoints = _pointRepository.GetMemberPoints(personId, false).ToList();
            return MemberPointHelper.GetActivePoints(customerPoints);
        }

        public MemberPointDetail GetMemberPointDetail(int personId)
        {
            //Get customer point
            var customerPoint = _pointRepository
                .GetMemberPoints(personId, true).ToList();

            //Get pending points
            var pendingPoint = customerPoint.Where(o => o.RecStatus == RecStatus.PENDING &&
                                                        o.Point >= 0)
                .Sum(o => o.Point);

            //Get active point
            var activePoint = MemberPointHelper.GetActivePoints(customerPoint);

            //Nearly expired
            var nearlyExpirePoint = MemberPointHelper.GetNearlyExpirePoints(customerPoint);

            //Level
            var levelName = _pointRepository.GetMemberPointLevel(personId).LevelName;


            var pointDetail = new MemberPointDetail
            {
                CurrentPoint = activePoint,
                NearlyExpirePoint = nearlyExpirePoint,
                PendingPoint = pendingPoint,
                LevelName = levelName
            };

            return pointDetail;
        }

        public UpdateResult InsertMemberPoints(int personId, int points, string createdBy, ActionType type, string remark)
        {
            if (type != ActionType.ADD && type != ActionType.DELETE) return new UpdateResult { ValidationMessage = "No action type" };

            if (type == ActionType.DELETE)
            {
                if (!ValidateMemberPointIsEnough(personId, points))
                {
                    return new UpdateResult { ValidationMessage = "คะแนนสะสมไม่เพียงพอ" };
                }
            }

            var memberPoint = new MemberPoint
            {
                PersonId = personId,
                RecCreatedBy = createdBy,
                Type = UpdatePointType.ADJUST,
				Remark = remark
            };
            //Assign member points
            switch (type)
            {
                case ActionType.ADD:
                    memberPoint.Point = Math.Abs(points);
                    break;
                case ActionType.DELETE:
                    memberPoint.Point = Math.Abs(points) * -1;
                    break;

            }

            if (memberPoint.Point > 0)
            {
				int expireYear = _pointRepository.GetPointConfig().ExpireYear ?? 0;
				int calculateExpireYear = 0;
				if (expireYear == 0)
				{
					calculateExpireYear = 9999;
				}
				else
				{
					calculateExpireYear = DateTime.Now.AddYears((expireYear - 1)).Year;
				}

				var pointExpiredDate = new DateTime(calculateExpireYear, 12, 31, 23, 59, 59);
				memberPoint.RecExpiredWhen = pointExpiredDate;
            }

            CheckAutoApproveAndUpdateMemberpoint(memberPoint);

            return new UpdateResult(true);
        }

        public UpdateResult InsertRedeemPointTransaction(int redeemPointId, int personId, RedeemPointsType type, string createdBy)
        {
            var redeemPoint = _pointRepository.GetRedeemPoint(redeemPointId);

            if (redeemPoint == null || redeemPoint.RecStatus != RecStatus.ACTIVE)
            {
                return new UpdateResult(false, "ไม่พบสินค้าที่ต้องการแลก");
            }

            var usedsPoints = type == RedeemPointsType.POINT_AND_PAID ? redeemPoint.RequiredPoint : redeemPoint.RequiredPointOnly;
            if (!ValidateMemberPointIsEnough(personId, usedsPoints))
            {
                return new UpdateResult { ValidationMessage = "คะแนนสะสมไม่เพียงพอ" };
            }

            var transaction = new RedeemPointTransaction
            {
                PersonId = personId,
				PersonCode = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE).Code,
                RecCreatedBy = EmployeeHelper.GetEmployee(createdBy),
                RecStatus = RecStatus.ACTIVE,
                Type = type,
                RedeemPoint = redeemPoint
            };

            CheckApproveAndInsertRedeemPoint(transaction);
            return new UpdateResult(true);
        }

        public UpdateResult UpdateMemberPointLevel(int personId, int pointLevel, int usedPoints, string modifiedBy)
        {
            var cuurentPointLevel = _pointRepository.GetMemberPointLevel(personId);
            if (!ValidateMemberPointIsEnough(personId, usedPoints))
            {
                return new UpdateResult { ValidationMessage = "คะแนนสะสมไม่เพียงพอ" };
            }

            if (!ValidateUpdatePointLevelIsChanged(pointLevel, cuurentPointLevel))
            {
                return new UpdateResult { ValidationMessage = "สมาชิกอยู่ระดับนี้อยู่แล้ว" };
            }

            var activePointLevel = _pointRepository.GetPointLevel().FirstOrDefault(o => o.PointLevelId == pointLevel);
            if (activePointLevel == null)
            {
                return new UpdateResult { ValidationMessage = "ไม่พบระดับสมาชิกที่ต้องการอัพเดท" };
            }

            CheckAutoApproveAndUpdateMemberPointLevel(personId, usedPoints, modifiedBy, activePointLevel, cuurentPointLevel);

            return new UpdateResult(true);
        }

        public void UpdateMemberPointStatus(int memberPointId, RecStatus status, string updatedBy)
        {
            _pointRepository.UpdateMemberPointStatus(memberPointId, status, updatedBy);
        }

        #region Private
        private bool ValidateMemberPointIsEnough(int personId, int usedPoints)
        {
            var currentActivePoints = _pointRepository.GetMemberPoints(personId, false).ToList();
            return MemberPointHelper.ValidateMemberPointIsEnough(currentActivePoints, usedPoints);
        }

        private bool ValidateUpdatePointLevelIsChanged(int updatedPointLevel, PointLevel currentPointLevel)
        {
            return currentPointLevel.PointLevelId != updatedPointLevel;
        }


        private void CheckAutoApproveAndUpdateMemberpoint(MemberPoint memberPoint)
        {
            //If update point is auto aprrove, need to update rec_status
            var isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.UPDATE_REWARD_POINT);
            memberPoint.RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING;
            InsertMemberPointsToStorage(memberPoint, isAutoApprove);
        }

        private void InsertMemberPointsToStorage(MemberPoint memberPoint, bool isAutoApprove)
        {
            var employee = EmployeeHelper.GetEmployee(memberPoint.RecCreatedBy);
			var preFix = memberPoint.Point > 0 ? "เพิ่ม" : "ลด";
			var icon = memberPoint.Point > 0 ? "+" : "";
			var person = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE);
			//Log
			_logHelper.Log(() =>
                {
                    var memberPointId = _pointRepository.InsertMemberPoint(memberPoint);
                    if (!isAutoApprove)
                    {
                        var json = new
                        {
                            Person = person,
                            Description = $"{preFix} {Math.Abs(memberPoint.Point):##,###} คะแนน",
                            Source = employee
                        };
                        _approveHelper.InsertPendingObject(memberPointId, json, ApproveType.UPDATE_REWARD_POINT, memberPoint.RecCreatedBy);
                    }
                },
                new AuditLog
                {
                    AuditActionType = AuditActionType.MEMBERSHIP_POINT_UPDATE,
                    ObjectId = memberPoint.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
                    NewValue = memberPoint.Point,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = memberPoint.PersonId,
							Detail = preFix + " "  + memberPoint.Point,
							UpdatePoint = icon + " " + memberPoint.Point,
                            RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(memberPoint.PersonId, false).ToList()),
                            Name = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE).FullName,
                            Remark = EnumHelper.GetEnumDescription(memberPoint.Type),
                            Source = employee
                        }),
                    RecCreatedBy = memberPoint.RecCreatedBy
                });
        }


        private void CheckAutoApproveAndUpdateMemberPointLevel(int personId, int usedPoints, string modifiedBy, PointLevel pointLevel, PointLevel currentPointLevel)
        {
            //If update point is auto aprrove, need to update rec_status
            var isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.UPGRADE_MEMBER_LEVEL);

            var employee = EmployeeHelper.GetEmployee(modifiedBy);
            var member = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE);


            //Log
            _logHelper.Log(() =>
            {
                //Check auto approve
                if (isAutoApprove)
                {
                    var memberPoint = new MemberPoint
                    {
                        PersonId = personId,
                        RecCreatedBy = modifiedBy,
                        Point = Math.Abs(usedPoints) * -1,
                        Type = UpdatePointType.UPGRADE,
                        RecStatus = RecStatus.ACTIVE
                    };
                    //Deduct member point                
                    InsertMemberPointsToStorage(memberPoint, true);
                    _pointRepository.UpsertMemberPointLevel(personId, pointLevel.PointLevelId);
                }
                else
                {

                    //insert into pending list
                    var json = new MemberPointLevel
                    {
                        PersonId = personId,
						PersonCode = member.Code,
						Point = Math.Abs(usedPoints),
                        PointLevelId = pointLevel.PointLevelId,
                        LevelName = pointLevel.LevelName,
                        Source = employee
                    };
                    _approveHelper.InsertPendingObject(personId, json, ApproveType.UPGRADE_MEMBER_LEVEL, modifiedBy);
                }
            },
           new AuditLog
           {
               AuditActionType = AuditActionType.MEMBERSHIP_LEVEL_UPGRADE_APPROVED,
               ObjectId = personId,
               Source = Source.MEMBERSHIP_WEBSITE,
               RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
               OldValue = currentPointLevel.PointLevelId,
               NewValue = pointLevel.PointLevelId,
               Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = personId,
                            Level = pointLevel.LevelName,
                            LevelId = pointLevel.PointLevelId,
                            Name = member.FullName,
                            Source = employee
                        }),
               RecCreatedBy = modifiedBy

           });

        }

        private void CheckApproveAndInsertRedeemPoint(RedeemPointTransaction transaction)
        {
            var isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.PRODUCT_REDEMPTION);
            var product = _lookupRepository.GetProduct(transaction.RedeemPoint.ProductCode);
            //Log
            _logHelper.Log(() =>
            {
                //Check auto approve
                if (isAutoApprove)
                {
                    //Deduct member point    
                    var usedsPoints = transaction.Type == RedeemPointsType.POINT_AND_PAID ? transaction.RedeemPoint.RequiredPoint : transaction.RedeemPoint.RequiredPointOnly;
                    var memberPoint = new MemberPoint
                    {
                        PersonId = transaction.PersonId,
                        Point = usedsPoints * -1,
                        ProductCode = transaction.RedeemPoint.ProductCode,
                        RecCreatedBy = transaction.RecCreatedBy.EmployeeCode,
                        RecStatus = RecStatus.ACTIVE,
                        Type = UpdatePointType.REDEMPTION
                    };

                    InsertMemberPointsToStorage(memberPoint, true);
                    _pointRepository.InsertRedeemPointTransaction(transaction);

                }
                else
                {
                    _approveHelper.InsertPendingObject(transaction.PersonId, transaction, ApproveType.PRODUCT_REDEMPTION,
               transaction.RecCreatedBy.EmployeeCode);
                }
            },
                new AuditLog
                {
                    AuditActionType = AuditActionType.REWARD_REDEMPTION,
                    ObjectId = transaction.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
                    OldValue = "Redemption",
                    NewValue = transaction,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = transaction.PersonId,
							Detail = $"แลกสินค้า {transaction.RedeemPoint.ProductCode} {product?.ProductName}",
							Product = $"{transaction.RedeemPoint.ProductCode} {product?.ProductName}",
                            Money = transaction.Type == RedeemPointsType.POINT_ONLY ? 0 : transaction.RedeemPoint.RequiredMoney,
							UpdatePoint = transaction.Type == RedeemPointsType.POINT_ONLY ? transaction.RedeemPoint.RequiredPointOnly : transaction.RedeemPoint.RequiredPoint,
							RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(transaction.PersonId, false).ToList()),
							Source = transaction.RecCreatedBy
                        }),
                    RecCreatedBy = transaction.RecCreatedBy.EmployeeCode
                });

        }
        #endregion
    }
}