﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface IApproveService
    {
		void Insert(Approve approve);
        UpdateResult ApprovePendingRequest(int approveId, string updatedBy);

        void RejectPendingRequest(int approveId, string updatedBy, ApproveType type);
        ResultSet<Approve> GetApproveList(ApproveType approveType, int? approveId, RecStatus? status, int pageSize, int pageNumber);
		ApproveSetting GetApproveSetting();
		void InsertApproveSetting(ApproveSetting approveSetting);
		void UpdateApproveSetting(ApproveSetting approveSetting);
        ApprovalViewModel CreateApprovalViewModel(IContextProvider contextProvider);
    }
}
