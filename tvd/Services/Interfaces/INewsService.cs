﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface INewsService
    {
        IEnumerable<News> GetNews(NewsType type, int? filterType);
        ResultSet<News> GetNews(NewsType type, int? filterType, int pageSize, int pageNumber);
        int InsertNews(News news);
        void UpdateNews(News news);
        void DeleteNews(int[] ids, int recModifiedBy);
        NewsViewModel CreateNewsViewModel(IContextProvider contextProvider, int? newsId = null);
    }
}
