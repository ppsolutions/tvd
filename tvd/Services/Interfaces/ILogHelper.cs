﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.CommonDTO.Audit;

namespace tvd.Services.Interfaces
{
	public interface ILogHelper
	{
		void Log(Action actionMethod, AuditLog auditLog);
	}
}
