﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.CommonDTO.Rule;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;
using tvd.model.V;

namespace tvd.Services.Interfaces
{
    public interface ILookupService
    {
        IEnumerable<City> GetCities();
        IEnumerable<Title> GetTitles();
        IEnumerable<Question> GetQuestions();
        IEnumerable<AddressType> GetAddressTypes();
        IEnumerable<PersonType> GetPersonTypes();
        IEnumerable<PointLevel> GetPointLevels();
        PointConfig GetPointConfig();
        IEnumerable<Role> GetRoles();
        IEnumerable<RedeemPointsType> GetRedeemPointsTypes();
        IEnumerable<NewsStatus> GetNewsStatuses();
        IEnumerable<Rule> GetRules();
        News GetNews(int newsId);
        IEnumerable<Bank> GetBanks();
        IEnumerable<CardType> GetCardTypes();
        IEnumerable<PaymentType> GetPaymentType();
        IEnumerable<ApplyType> GetApplyTypes();
        ProductInfo GetProductInfoFromView(string code);
        IEnumerable<ProductInfo> GetProductInfoFromName(string productName);

	}

}
