﻿using System;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Enum;

namespace tvd.Services.Interfaces
{
	public interface IApproveHelper
	{
		void IsNeedApprove(Action actionMethod, Approve approve);

	    bool IsAutoApprove(ApproveType approve);

	    Approve GetApprove(int approveId);

	    void InsertPendingObject(int objectId, object pendingObject, ApproveType approve, string createdBy);

	}
}
