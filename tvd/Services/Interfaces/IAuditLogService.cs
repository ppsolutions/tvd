﻿using System;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.Enum;
using tvd.Models;

namespace tvd.Services.Interfaces
{
    public interface IAuditLogService
	{
		void Insert(AuditLog auditLog);

	    ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate,
	        DateTime? toDate,int? roleId, int? objectId, string value, int pageSize, int pageNumber);

		ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate,
			DateTime? toDate, int? roleId, int? objectId, string value);

		ResultSet<AuditLog> GetAuditLogs(DateTime? fromDate, DateTime? toDate);


		void InsertViewHistory(int? personId, PageTypeId pageId, PageTypeId? subPageId, string visitedBy);

        ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int paegId, int pageSize, int pageNumber);
		ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int paegId);

		ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? daysBeforeExpire, int pageSize, int pageNumber);
		ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? daysBeforeExpire);

		ResultSet<MemberPointExpireReport> GetMemberPointExpiryReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate, int pageSize, int pageNumber);
		ResultSet<MemberPointExpireReport> GetMemberPointExpiryReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate);
	}
}
