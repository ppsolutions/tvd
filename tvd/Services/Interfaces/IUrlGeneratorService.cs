﻿using tvd.model.Models.Enum;

namespace tvd.Services.Interfaces
{
    public interface IUrlGeneratorService
    {
        string GetSearchUrl();
        string GetSearchResultUrl(string queryString);
        string GetMemberDetail(int personId, Source source = Source.MEMBERSHIP_WEBSITE);
        string GetLoginUrl();
        string GetMemberPageUrl();
        string GetRewardPageUrl();
        string GetApproveMemberPageUrl();
        string GetApproveAutoSettingPageUrl();
        string GetApproveNewMemberPageUrl();
        string GetApproveUpdateMemberPageUrl();
        string GetApproveBlockMemberPageUrl();
        string GetApproveCancelMemberPageUrl();
        string GetApproveUpdatePointPageUrl();
        string GetApproveRedemptionPageUrl();
        string GetApproveUpgradeLevelPageUrl();
        string GetNewsMemberPageUrl();
        string GetSettingsEmployeePageUrl();
        string GetSettingsPermissionPageUrl();
        string GetSettingsEmployeeCreatePageUrl();
        string GetSettingsEmployeeEditPageUrl(string employeeCode);
        string GetSettingsEmployeeDetailPageUrl(string employeeCode);
        string GetSettingsPermissionCreatePageUrl();
        string GetSettingsPermissionEditPageUrl(int id);
        string GetNewsPromotionPageUrl();
        string GetNewsMemberRulePageUrl();
        string GetNewsMemberBenefitPageUrl();
        string GetNewsCreatePageUrl();
        string GetNewsEditPageUrl(int newsId);
        string GetBasicPointUrl();
        string GetBasicPointLevelUrl();
        string GetCampaignUrl();
        string GetGamificationUrl();
        string GetGamificationCreateUrl();
        string GetPointLevelUrl();
        string GetPromotionUrl();
        string GetRuleUrl();
        string GetRewardCreateUrl();
        string GetRuleCreateUrl();
        string GetRewardEditUrl();
        string GetRuleEditUrl(int ruleId);
        string GetPromotionCreateUrl();
        string GetReportNewMemberPageUrl();
        string GetReportUpgradeMemberUrl();
        string GetReportCancelMember();
        string GetReportBlockMember();
        string GetReportChangePassword();
        string GetReportRedemptionUrl();
        string GetReportHistoryMember();
        string GetReportHistory();
        string GetReportMemberPointUrl();
        string GetReportMemberPointNearlyExpireUrl();
        string GetReportUpdateMemberPointUrl();
        string GetReportCancelMemberPointUrl();
        string GetNewsMainUrl();
		string GetUploadMemberPointUrl();
    }
}
