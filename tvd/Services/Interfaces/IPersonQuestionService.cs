﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.Services
{
    public interface IPersonQuestionService
    {
        [Obsolete]
        void UpdatePersonQuestions(List<PersonQuestion> questions);
    }
}
