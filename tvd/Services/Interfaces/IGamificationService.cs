﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Point;

namespace tvd.Services.Interfaces
{
    public interface IGamificationService
    {
        ResultSet<GamificationPoint> GetGamificationPoints(string code, int? applyTo, int? recStatus, int? pageSize, int? pagegNumber);
        void CreateGamification(GamificationPoint newGame);
        void UpdateGamificationPoint(GamificationPoint game);
        void UpdateGamificationStatus(GamificationPoint game);

        GamificationPoint GetGamification(string gameCode);
        GamificationPoint GetGamification(int gameId);
    }
}
