﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Provider;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface IReportService
    {
        ReportViewModel CreateSettingViewModel(IContextProvider contextProvider);
    }
}
