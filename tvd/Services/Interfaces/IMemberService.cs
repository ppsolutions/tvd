﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.Models;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface IMemberService
    {
       
        MemberInfo GetMemberInfo(int personId);
        Person GetPersonFromContactNumber(string contactNumber);
		int GetMemberId(string memberCode);
        IEnumerable<MemberInfo> GetMemberInfoList(string name, string idCard, string phoneNumber, string memberId);

        ResultSet<MemberInfo> GetMemberInfoList(string name, string idCard, string phoneNumber, string memberId, int pageSize, int pageNumber);
            
        IEnumerable<SearchNameResult> GetPersonName(string name);
        Member GetMemberDetail(int personId, Source source);
        int MergeCustomer(Person customerBase, List<int> mergeCustomerIds);
        int CreateMember(Person person, PersonAddressGroup addressGroup, List<PersonQuestion> questions, string[] mappingDatas, string createdBy);

        //[Obsolete]
        //void UpdateMember(Person person, PersonAddressGroup addressGroup, List<PersonQuestion> questions);

        UpdateResult UpdatePassword(int personId, string oldPassword, string newPassword, string updatedBy);
        UpdateResult UpdateBasicInfo(Person person);
        UpdateResult UpdateAddress(PersonAddressGroup addressGroup);
        UpdateResult UpdateQuestions(List<PersonQuestion> questions);
        void BlockUser(int personId, string updatedBy);
		void ActiveUser(int personId, string updatedBy);
		void DeleteUser(int personId, string updatedBy);
        //void UpdateUserStatus(int personId, RecStatus status, int updatedBy);
        List<MemberMapping> GetMergeUsers(int personId);
        void MergeMember(MemberMapping memberMapping, int updateBy);
		//void ApproveMember(int approveId, int updatedBy);
		RegisterViewModel CreateRegisterViewModel(IContextProvider contextProvider);

	}
}