﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Provider;
using tvd.Models;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface IEmployeeService
    {
		ResultSet<Employee> GetEmployees(string criteria, int? roleId, int pageSize, int pageNumber);
		Employee GetEmployee(string employeeCode);
		Employee Authenticate(string userName, string password);               
        bool SetCookie(IContextProvider contextProvider, Employee employee, bool isRemember);
        bool Logout(IContextProvider contextProvider);
        UserSettingViewModel CreateSettingViewModel(IContextProvider contextProvider);
        IList<AccessControl> GetAccessControlUser(string employeeCode);
        IList<AccessControl> GetAccessControlRole(int roleId);
    }
}
