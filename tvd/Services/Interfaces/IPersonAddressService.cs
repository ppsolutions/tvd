﻿using tvd.model.Models.CommonDTO.Address;

namespace tvd.Services
{
    public interface IPersonAddressService
    {
        PersonAddressGroup GetPersonAddressGroup(int personId);

        void AddPersonAddressGroup(PersonAddressGroup addressGroup);

        void UpdatePersonAddress(PersonAddressGroup addressGroup);
    }
}
