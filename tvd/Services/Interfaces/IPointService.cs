﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Point;
using tvd.model.Provider;
using tvd.ViewModel;

namespace tvd.Services.Interfaces
{
    public interface IPointService
    {
        void UpdatePointLevelAndExpireDate(List<PointLevel> levels, int? expireYear, decimal? minPurchase, string by);
        PointViewModel CreatePointViewModel(IContextProvider contextProvider, int? id = null);
        RewardInfo FetchRewardInfo(int rewardId);
        PointRuleInfo FetchPointRuleInfo(int ruleId);
        PromotionInfo FetchPromotionInfo(string id);
        PromotionInfo FetchCampaignInfo(string code);
        IList<PromotionInfo> FetchRuleUsed(int ruleId);
        RedeemPoint GetRedeemPoint(string code);
        IList<tvd.model.Models.CommonDTO.Rule.Rule> FetchRuleUsed();
    }
    
}