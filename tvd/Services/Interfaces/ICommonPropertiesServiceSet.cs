﻿namespace tvd.Services.Interfaces
{
    public interface ICommonPropertiesServiceSet
    {
        IUrlGeneratorService UrlGeneratorService { get; }
    }
}
