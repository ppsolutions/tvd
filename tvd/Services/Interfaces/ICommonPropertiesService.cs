﻿using System.Web;
using tvd.model.Models.UserData;

namespace tvd.Services.Interfaces
{
    public interface ICommonPropertiesService
    {
        void SetCommonProperties(object viewmodel, HttpContextBase contextBase, IUserData userData);
    }
}
