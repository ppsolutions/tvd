﻿using tvd.model.Models.CommonDTO.Employee;

namespace tvd.Services.Interfaces
{
    public interface IRoleService
    {
        void InsertRoleAndPermission(RolePermission permission);
        void UpdateRolePermisson(RolePermission permission);
        void DeleteRole(Role role);
        RolePermission GetRolePermission(int roleId);
        void AssignRoleToUser(string employeeCode, int roleId, string updatedBy);
        void RemoveRoleFromUser(string employeeCode, string updatedBy);
        void AssignAccessControlToUser(string employeeCode, int[] controlIds, string updatedBy);
        void AssignAccessControlToRole(int roleId, int[] controlIds, string updatedBy);
    }
}
