﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.V;

namespace tvd.Services.Interfaces
{
    public interface IProductService
    {
        ResultSet<ProductInfo> GetProductInfos(string code, int? pageSize, int? pageNumber);

        ResultSet<ProductInfo> GetProductInfosWithBasicPoints(string code, int? pageSize, int? pageNumber);
    }
}
