﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;

namespace tvd.Services.Interfaces
{
    public interface IMemberPointService
    {
        MemberPointDetail GetMemberPointDetail(int personId);
        int GetCurrentMemberPoints(int personId);
        UpdateResult InsertMemberPoints(int personId, int points, string createdBy, ActionType type, string remark);        
        UpdateResult UpdateMemberPointLevel(int personId, int pointLevel, int usedPoints, string modifiedBy);        
        UpdateResult InsertRedeemPointTransaction(int redeemPointId, int personId, RedeemPointsType type, string createdBy);
        void UpdateMemberPointStatus(int memberPointId, RecStatus status, string updatedBy);
        
    }
}
