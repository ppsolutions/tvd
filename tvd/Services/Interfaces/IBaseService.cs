﻿using System.Collections.Generic;
using tvd.Models;

namespace tvd.Services.Interfaces
{
    public interface IBaseService
    {
        IList<Breadcrumb> BuildBreadcrumb();
    }
}
