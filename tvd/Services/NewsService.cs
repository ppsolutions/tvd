﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;
using tvd.model.Provider;
using tvd.ViewModel;
using tvd.Models;
using System.Web;
using tvd.model.Models.CommonDTO;
using tvd.urlmapping.Configuration;

namespace tvd.Services
{
    public class NewsService : BaseService, INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly ILookupService _lookupService;

        public NewsService(INewsRepository newsRepository,
            IUrlGeneratorService urlGeneratorService,
            ILookupService lookupService)
        {
            _newsRepository = newsRepository;
            _urlGeneratorService = urlGeneratorService;
            _lookupService = lookupService;
        }

        public IEnumerable<News> GetNews(NewsType type, int? filterType)
        {
            //Decode content
            var news = _newsRepository.GetNews(type, filterType);

            return news.Select(o =>
            {
                o.Content = WebUtility.HtmlDecode(o.Content);
                return o;
            });
        }

        public ResultSet<News> GetNews(NewsType type, int? filterType, int pageSize, int pageNumber)
        {
            //Decode content
            var news = _newsRepository.GetNews(type, filterType, pageSize, pageNumber);
            return news;
        }

        public int InsertNews(News news)
        {
            var newsId = 0;
            if (news != null)
            {
                news.Title = WebUtility.HtmlEncode(news.Title);
                news.Content = WebUtility.HtmlEncode(news.Content);
                newsId = _newsRepository.InsertNews(news);
            }
            return newsId;
        }

        public void UpdateNews(News news)
        {
            if (news != null)
            {
                news.Title = WebUtility.HtmlEncode(news.Title);
                news.Content = WebUtility.HtmlEncode(news.Content);
                _newsRepository.UpdateNews(news);
            }

        }

        public void DeleteNews(int[] ids, int recModifiedBy)
        {
            if (ids != null && ids.Any())
            {
                //TODO: add recCreateBY
                _newsRepository.DeleteNews(ids, recModifiedBy);
            }
        }

        public NewsViewModel CreateNewsViewModel(IContextProvider contextProvider, int? newsId)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
            var vm = new NewsViewModel();
            vm.Sidebar = CreateSidebar(contextProvider);
            vm.SubBreadcrumb = CreateBreadcrumb(contextProvider, newsId);
            vm.NewsStatus = _lookupService.GetNewsStatuses().ToList();
            vm.CreateNewsUrl = _urlGeneratorService.GetNewsCreatePageUrl();
            vm.SubPageTitle = GetSubPageTitle((PageTypeId)contextProvider.GetPageTypeId());
            vm.NewsId = newsId.HasValue ? newsId.Value : 0;
            vm.NewsInfo = newsId.HasValue ? _lookupService.GetNews(newsId.Value) : null;
            return vm;
        }

        protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
            var bc = new List<BreadcrumbItem>();
            if (pageTypeId == PageTypeId.NewsMemberPage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ข่าวสารสมาชิก", Url = _urlGeneratorService.GetNewsMemberPageUrl() });
            }

            if (pageTypeId == PageTypeId.NewsPromotionPage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ข่าวสารโปรโมชัน", Url = _urlGeneratorService.GetNewsPromotionPageUrl() });
            }

            if (pageTypeId == PageTypeId.NewsMemberRulePage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "กฎระเบียบสมาชิก", Url = _urlGeneratorService.GetNewsMemberRulePageUrl() });
            }

            if (pageTypeId == PageTypeId.NewsMemberBenefitPage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "สิทธิประโยชน์", Url = _urlGeneratorService.GetNewsMemberBenefitPageUrl() });
            }

            if (pageTypeId == PageTypeId.NewsCreatePage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(GetBreadcrumbItemFromReferer(GetPageReferer(contextProvider.GetContext())));
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "เพิ่มข่าวสาร", Url = _urlGeneratorService.GetNewsCreatePageUrl() });
            }

            if (pageTypeId == PageTypeId.NewsEditPage)
            {
                bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
                bc.Add(GetBreadcrumbItemFromReferer(GetPageReferer(contextProvider.GetContext())));
                bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "แก้ไขข่าวสาร", Url = _urlGeneratorService.GetNewsEditPageUrl(id.Value) });
            }

            return bc;
        }

        private BreadcrumbItem GetBreadcrumbItemFromReferer(PageTypeId pageTypeId)
        {
            var bcItem = new BreadcrumbItem();

            if (pageTypeId == PageTypeId.NewsMemberPage)
            {
                bcItem.HasUrl = true;
                bcItem.Name = "ข่าวสารสมาชิก";
                bcItem.Url = _urlGeneratorService.GetNewsMemberPageUrl();
            }
            if (pageTypeId == PageTypeId.NewsPromotionPage)
            {
                bcItem.HasUrl = true;
                bcItem.Name = "ข่าวสารโปรโมชัน";
                bcItem.Url = _urlGeneratorService.GetNewsPromotionPageUrl();
            }
            if (pageTypeId == PageTypeId.NewsMemberRulePage)
            {
                bcItem.HasUrl = true;
                bcItem.Name = "กฎระเบียบสมาชิก";
                bcItem.Url = _urlGeneratorService.GetNewsMemberRulePageUrl();
            }
            if (pageTypeId == PageTypeId.NewsMemberBenefitPage)
            {
                bcItem.HasUrl = true;
                bcItem.Name = "สิทธิประโยชน์";
                bcItem.Url = _urlGeneratorService.GetNewsMemberBenefitPageUrl();
            }

            return bcItem;
        }

        private PageTypeId GetPageReferer(HttpContextBase contextBase)
        {
            var pageTypeId = PageTypeId.Unknow;
            if (contextBase.Request.UrlReferrer != null)
            {
                var path = contextBase.Request.UrlReferrer.AbsolutePath;
                var config = RewriteRulesConfigurationSection.Configuration.UrlMapping;
                var rules = config.UrlMappings.Cast<UrlMapping>();
                var direct = rules.FirstOrDefault(a => string.Compare(a.PageLocation, path, true) == 0);
                if (direct != null)
                {
                    return (PageTypeId)direct.MappingType;
                }
            }
            return pageTypeId;
        }

        protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

            var sb = new List<SidebarItem>();
            
            sb.Add(new SidebarItem() { IsHeader = true, Icon = "glyphicon glyphicon-exclamation-sign", Name = "ข่าวสาร", IsActive = false, MenuId = 1, ReferMenuId = 0 });
            sb.Add(new SidebarItem()
            {
                IsHeader = false,
                Name = "ดูและค้นหา",
                IsActive = IsActive(pageTypeId, PageTypeId.NewsMainPage),
                MenuId = 0,
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetNewsMainUrl()
            });
            sb.Add(new SidebarItem() { IsHeader = false, Icon = null, Name = "ข่าวสารสมาชิก", IsActive = IsActive(pageTypeId, PageTypeId.NewsMemberPage), MenuId = 0, ReferMenuId = 1, Url = _urlGeneratorService.GetNewsMemberPageUrl() });
            sb.Add(new SidebarItem() { IsHeader = false, Icon = null, Name = "ข่าวสารโปรโมชั่น", IsActive = IsActive(pageTypeId, PageTypeId.NewsPromotionPage), MenuId = 0, ReferMenuId = 1, Url = _urlGeneratorService.GetNewsPromotionPageUrl() });
            sb.Add(new SidebarItem() { IsHeader = true, Icon = "glyphicon glyphicon-thumbs-up", Name = "ข้อกำหนดสมาชิก", IsActive = false, MenuId = 2, ReferMenuId = 0 });
            sb.Add(new SidebarItem() { IsHeader = false, Icon = null, Name = "กฏระเบียบสมาชิก", IsActive = IsActive(pageTypeId, PageTypeId.NewsMemberRulePage), MenuId = 0, ReferMenuId = 2, Url = _urlGeneratorService.GetNewsMemberRulePageUrl() });
            sb.Add(new SidebarItem() { IsHeader = false, Icon = null, Name = "สิทธิประโยชน์สมาชิก", IsActive = IsActive(pageTypeId, PageTypeId.NewsMemberBenefitPage), MenuId = 0, ReferMenuId = 2, Url = _urlGeneratorService.GetNewsMemberBenefitPageUrl() });

            return sb;
        }

        private bool IsActive(PageTypeId pageTypeId1, PageTypeId pageTypeId2)
        {
            return (pageTypeId1 == pageTypeId2);
        }
    }
}