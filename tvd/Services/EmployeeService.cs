﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;
using tvd.Models;
using tvd.ViewModel;
using tvd.urlmapping.Configuration;

namespace tvd.Services
{
    public class EmployeeService : BaseService, IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IUrlGeneratorService _urlGeneratorService;
        public EmployeeService(IEmployeeRepository employeeRepository, IUrlGeneratorService urlGenerator)
        {
            _employeeRepository = employeeRepository;
            _urlGeneratorService = urlGenerator;
        }

        public ResultSet<Employee> GetEmployees(string criteria, int? roleId, int pageSize, int pageNumber)
        {
            return _employeeRepository.GetEmployees(criteria, roleId, pageSize, pageNumber);
        }
        public Employee GetEmployee(string employeeCode)
        {
            return _employeeRepository.GetEmployee(employeeCode);
        }

        public IList<AccessControl> GetAccessControlUser(string employeeCode)
        {
            var userAccessControl = _employeeRepository.GetAccessControlUser(employeeCode);
            var result = new List<AccessControl>();
            foreach(var c in userAccessControl)
            {
                var a = new AccessControl();
                a.IsChecked = c.RecStatus == 1 ? true : false;
                a.PageTypeId = c.PageTypeId;
                a.PageTypeName = (PageTypeId)c.PageTypeId;
                result.Add(a);
            }
            return result;
        }

        public IList<AccessControl> GetAccessControlRole(int roleId)
        {
            var roleAccessControl = _employeeRepository.GetAccessControlRole(roleId);
            var result = new List<AccessControl>();
            foreach (var c in roleAccessControl)
            {
                var a = new AccessControl();
                a.IsChecked = c.RecStatus == 1 ? true : false;
                a.PageTypeId = c.PageTypeId;
                a.PageTypeName = (PageTypeId)c.PageTypeId;
                result.Add(a);
            }
            return result;
        }

        public Employee Authenticate(string userName, string password)
        {
            userName = userName.Trim();
            //password = PasswordHasher.EncryptString(password.Trim());
            return _employeeRepository.Authenticate(userName, password);
        }

        public bool SetCookie(IContextProvider contextProvider, Employee employee, bool isRemember)
        {
            var userSerializer = JsonConvert.SerializeObject(employee.EmployeeCode);
            var ticket = new FormsAuthenticationTicket(1,
                                employee.EmployeeCode,
                                DateTime.Now,
                                DateTime.Now.AddYears(1),
                                isRemember,
                                userSerializer);
            var encToken = FormsAuthentication.Encrypt(ticket);
            var context = contextProvider.GetContext();
            var authenCookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authenCookie != null)
            {
                context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
            }
			
			authenCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encToken);
            authenCookie.HttpOnly = true;
			//authenCookie.Secure = FormsAuthentication.RequireSSL;

			if (isRemember && ticket.IsPersistent)
            {
                authenCookie.Expires = ticket.Expiration;
            }
            context.Response.Cookies.Set(authenCookie);

            return true;
        }

        public bool Logout(IContextProvider contextProvider)
        {
            try
            {
                contextProvider.GetContext().Session.Abandon();
                FormsAuthentication.SignOut();
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie.Expires = DateTime.Now.AddYears(-2);
                contextProvider.GetContext().Response.Cookies.Add(cookie);
            }
            catch
            {

            }

            return true;
        }

        protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
            var bc = new List<BreadcrumbItem> { new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" } };

            switch (pageTypeId)
            {
                case PageTypeId.SettingEmployeePage:
                    bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "พนักงาน", Url = _urlGeneratorService.GetSettingsEmployeePageUrl() });
                    break;
                case PageTypeId.SettingEmployeeCreatePage:
                    bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "พนักงาน", Url = _urlGeneratorService.GetSettingsEmployeePageUrl() });
                    bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "ลงทะเบียนพนักงาน", Url = _urlGeneratorService.GetSettingsEmployeeCreatePageUrl()});
                    break;
                case PageTypeId.SettingEmployeeEditPage:
                    bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "พนักงาน", Url = _urlGeneratorService.GetSettingsEmployeePageUrl() });
                    bc.Add(new BreadcrumbItem() { Name = "แก้ไขพนักงาน" });
                    break;
                case PageTypeId.SettingEmployeeDetailPage:
                    bc.Add(new BreadcrumbItem() { HasUrl = true, Name = "พนักงาน", Url = _urlGeneratorService.GetSettingsEmployeePageUrl() });
                    bc.Add(new BreadcrumbItem() { Name = "รายละเอียดพนักงาน" });
                    break;
                case PageTypeId.SettingPermissionPage:
                    bc.Add(new BreadcrumbItem()
                    {
                        HasUrl = true,
                        Name = "หน้าที่การใช้งาน",
                        Url = _urlGeneratorService.GetSettingsPermissionPageUrl()
                    });
                    break;
                case PageTypeId.SettingPermissionCreatePage:
                    bc.Add(new BreadcrumbItem()
                    {
                        HasUrl = true,
                        Name = "หน้าที่การใช้งาน",
                        Url = _urlGeneratorService.GetSettingsPermissionPageUrl()
                    });
                    bc.Add(new BreadcrumbItem()
                    {
                        HasUrl = true,
                        Name = "เพิ่มหน้าที่การใช้งาน",
                        Url = _urlGeneratorService.GetSettingsPermissionCreatePageUrl()
                    });
                    break;
                case PageTypeId.SettingPermissionEditPage:
                    bc.Add(new BreadcrumbItem()
                    {
                        HasUrl = true,
                        Name = "หน้าที่การใช้งาน",
                        Url = _urlGeneratorService.GetSettingsPermissionPageUrl()
                    });
                    bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "แก้ไขหน้าที่การใช้งาน" });
                    break;
            }
            return bc;

        }

        protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

            var sb = new List<SidebarItem>
            {
                new SidebarItem
                {
                    IsHeader = true,
                    Name = "พนักงาน",
                    IsActive = IsActive(pageTypeId,
                        new List<PageTypeId>
                        {
                            PageTypeId.SettingEmployeePage,
                            PageTypeId.SettingEmployeeCreatePage,
                            PageTypeId.SettingEmployeeEditPage,
                            PageTypeId.SettingEmployeeDetailPage
                        }),
                    MenuId = 1,
                    ReferMenuId = 0,
                    Url = _urlGeneratorService.GetSettingsEmployeePageUrl()
                },
                new SidebarItem
                {
                    IsHeader = true,
                    Name = "หน้าที่การใช้งาน",
                    IsActive = IsActive(pageTypeId,
                        new List<PageTypeId>
                        {
                            PageTypeId.SettingPermissionPage,
                            PageTypeId.SettingPermissionCreatePage,
                            PageTypeId.SettingPermissionEditPage,
                            PageTypeId.SettingPermissionEditPage
                        }),
                    MenuId = 2,
                    ReferMenuId = 0,
                    Url = _urlGeneratorService.GetSettingsPermissionPageUrl()
                }
            };

            return sb;
        }

        public UserSettingViewModel CreateSettingViewModel(IContextProvider contextProvider)
        {
            var vm = new UserSettingViewModel
            {
                Sidebar = CreateSidebar(contextProvider),
                SubBreadcrumb = CreateBreadcrumb(contextProvider, null),
                EmployeeUrl = _urlGeneratorService.GetSettingsEmployeePageUrl(),
                EmployeeCreateUrl = _urlGeneratorService.GetSettingsEmployeeCreatePageUrl(),
                PermissionUrl = _urlGeneratorService.GetSettingsPermissionPageUrl(),
                PermissionCreateUrl = _urlGeneratorService.GetSettingsPermissionCreatePageUrl(),
                AccessControlList = this.GetAccessControl()
            };
            return vm;
        }

        private IList<AccessControl> GetAccessControl()
        {
            var list = new List<AccessControl>();
            var urlRules = RewriteRulesConfigurationSection.Configuration;

            foreach (UrlMapping mapping in urlRules.UrlMapping.UrlMappings)
            {
                var accessControl = new AccessControl()
                {
                    PageTypeId =mapping.MappingType,
                    PageTypeName = (PageTypeId)mapping.MappingType,
                    IsChecked = false
                };
                if (accessControl.PageTypeId != 1)
                {
                    list.Add(accessControl);
                }
            }

            return list;
        }
    }
}