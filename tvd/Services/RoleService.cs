﻿using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public void InsertRoleAndPermission(RolePermission rolePermission)
        {
            if(rolePermission?.Role == null) return;

            //Add role name to get role id then insert permission id  related with new role
            var roleId = _roleRepository.InsertRole(rolePermission.Role);
            if (roleId == 0) return;

            rolePermission.Role.RoleId = roleId;
            _roleRepository.InsertRolePermission(rolePermission);
        }

        public void UpdateRolePermisson(RolePermission permission)
        {
            if(permission?.Role == null) return;
            //Set role permission to active
            permission.RecStatus = RecStatus.ACTIVE;
            _roleRepository.UpdateRolePermission(permission); 
        }

        public void DeleteRole(Role role)
        {
            if(role == null) return;
            role.RecStatus = RecStatus.DELETE;
            _roleRepository.UpdateRole(role);
        }

        public RolePermission GetRolePermission(int roleId)
        {
            return _roleRepository.GetRolePermission(roleId);
        }

        public void AssignRoleToUser(string employeeCode, int roleId, string updatedBy)
        {
            _roleRepository.UpdateRoleToUser(employeeCode, roleId, RecStatus.ACTIVE, updatedBy);
        }

        public void RemoveRoleFromUser(string employeeCode, string updatedBy)
        {
            _roleRepository.UpdateRoleToUser(employeeCode, null, RecStatus.DELETE, updatedBy);
        }

        public void AssignAccessControlToUser(string employeeCode, int[] controlIds, string updatedBy)
        {
            _roleRepository.AssignAccessControlToUser(employeeCode, controlIds, updatedBy);
        }

        public void AssignAccessControlToRole(int roleId, int[] controlIds, string updatedBy)
        {
            _roleRepository.AssignAccessControlToRole(roleId, controlIds, updatedBy);
        }
    }
}