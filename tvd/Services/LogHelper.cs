﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Audit;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class LogHelper : ILogHelper
    {
        private readonly IAuditLogService _auditLogService;

        public LogHelper(IAuditLogService auditLogService)
        {
            _auditLogService = auditLogService;
        }

        public void Log(Action actionMethod, AuditLog auditLog)
        {
            actionMethod.Invoke();
            _auditLogService.Insert(auditLog);
        }
    }
}