﻿using System;
using System.Linq;
using Newtonsoft.Json;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;

namespace tvd.Services
{
	public class ApproveHelper : IApproveHelper
	{
		private readonly IApproveRepository _approveRepository;
		public ApproveHelper(IApproveRepository approveRepository)
		{
		    _approveRepository = approveRepository;
		}

		public void IsNeedApprove(Action actionMethod, Approve approve)
		{
			//if (isAutoApprove)
			//{
			//	actionMethod.Invoke();
			//}
			//else
			//{
			//	_approveService.Insert(approve);
			//}
		}

	    public bool IsAutoApprove(ApproveType approve)
	    {
	        var settings = _approveRepository.GetApproveSetting();
	        switch (approve)
	        {
	            case ApproveType.UNKNOWN:
	                return false;
	            case ApproveType.NEW_MEMBER:
	                return settings.IsAutoApproveNewMembership;
	            case ApproveType.UPDATE_MEMEBER_INFO:
	                return settings.IsAutoApproveChangeMembershipInfo;
	            case ApproveType.BLOCK_MEMBER:
	                return false;
	            case ApproveType.CANCEL_MEMBER:
	                return settings.IsAutoApproveCancelMembership;
	            case ApproveType.UPDATE_REWARD_POINT:
	                return settings.IsAutoApproveChangeMembershipPoint;
	            case ApproveType.PRODUCT_REDEMPTION:
	                return settings.IsAutoApproveRewardRedeemMembership;
	            case ApproveType.UPGRADE_MEMBER_LEVEL:
	                return settings.IsAutoApproveLevelUpMembership;
	            default:
	                return true;
	        }
	    }

	    public Approve GetApprove(int approveId)
	    {
	        return _approveRepository.GetApproveList(approveId, ApproveType.UNKNOWN, null).FirstOrDefault();
        }
        

	    public void InsertPendingObject(int objectId, object pendingObject, ApproveType approve, string createdBy)
	    {
            //convert pending object to json
	        var jsonObject = JsonConvert.SerializeObject(pendingObject);
            var pendingAppoveObject = new Approve();
	        pendingAppoveObject.ApproveType = approve;
	        pendingAppoveObject.ObjectId = objectId;
            pendingAppoveObject.ObjectValue = jsonObject;
	        pendingAppoveObject.RecCreatedBy = createdBy;
	        _approveRepository.Insert(pendingAppoveObject);

	    }
	}
}