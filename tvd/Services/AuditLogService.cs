﻿using System;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.Enum;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;
using System.Linq;
using tvd.Models;
using tvd.model.Helpers;

namespace tvd.Services
{
	public class AuditLogService : IAuditLogService
	{
		private readonly IAuditLogRepository _auditLogRepository;
		private readonly ILookupRepository _lookupRepository;
		private readonly IUrlGeneratorService _urlGeneratorService;
		private readonly IMemberRepository _memberRepository;

		public AuditLogService(IAuditLogRepository auditLogRepository
							   , ILookupRepository lookupRepository
							   , IUrlGeneratorService urlGeneratorService
							   , IMemberRepository memberRepository)
		{
			_auditLogRepository = auditLogRepository;
			_lookupRepository = lookupRepository;
			_urlGeneratorService = urlGeneratorService;
			_memberRepository = memberRepository;
		}

		public void Insert(AuditLog auditLog)
		{
			_auditLogRepository.Insert(auditLog);
		}

		public ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate, DateTime? toDate, int? roleId, int? objectId, string value, int pageSize, int pageNumber)
		{
			var list = _auditLogRepository.GetAuditLogs(type
														, status
														, createBy
														, fromDate
														, toDate?.Date.AddDays(1).AddMilliseconds(-1)
														, roleId
														, objectId
														, value
														, pageSize
														, pageNumber);
			list.ToList().ForEach(o =>
			{
				o.ObjectCode = o.ObjectId == 0 ? string.Empty : _memberRepository.GetMember(o.ObjectId, Source.MEMBERSHIP_WEBSITE).Code;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.ObjectId);
			});
			return list;
		}

		public ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate, DateTime? toDate, int? roleId, int? objectId, string value)
		{
			var list = _auditLogRepository.GetAuditLogs(type
														, status
														, createBy
														, fromDate
														, toDate?.Date.AddDays(1).AddMilliseconds(-1)
														, roleId
														, objectId
														, value);
			list.ToList().ForEach(o =>
			{
				o.ObjectCode = o.ObjectId == 0 ? string.Empty : _memberRepository.GetMember(o.ObjectId, Source.MEMBERSHIP_WEBSITE).Code;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.ObjectId);
			});
			return list;
		}

		public ResultSet<AuditLog> GetAuditLogs(DateTime? fromDate, DateTime? toDate)
		{
			var list = _auditLogRepository.GetAuditLogs(fromDate, toDate?.Date.AddDays(1).AddMilliseconds(-1));
			return new ResultSet<AuditLog>(list, list.Count());
		}


		public void InsertViewHistory(int? personId, PageTypeId pageId, PageTypeId? subPageId, string visitedBy)
		{
			_auditLogRepository.InsertViewHistory(personId, (int)pageId, (int?)subPageId, visitedBy);
		}

		public ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int pageId, int pageSize, int pageNumber)
		{
			var list = _auditLogRepository.GetHistory(fromDate, toDate?.Date.AddDays(1).AddMilliseconds(-1), pageId, pageSize, pageNumber);

			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId.HasValue ? _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code : string.Empty;
				o.TitleName = o.TitleId.HasValue ? titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName : "";
				o.EmployeeURL = _urlGeneratorService.GetSettingsEmployeeDetailPageUrl(o.EmployeeCode);
				o.MemberURL = o.PersonId.HasValue ? _urlGeneratorService.GetMemberDetail(o.PersonId.Value) : "";
				o.SubPageName = o.SubPageId.HasValue ? EnumHelper.GetEnumDescription((PageTypeId)o.SubPageId.Value) : "";
			});

			return list;
		}

		public ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int pageId)
		{
			var list = _auditLogRepository.GetHistory(fromDate, toDate?.Date.AddDays(1).AddMilliseconds(-1), pageId);

			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId.HasValue ? _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code : string.Empty;
				o.TitleName = o.TitleId.HasValue ? titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName : "";
				o.EmployeeURL = _urlGeneratorService.GetSettingsEmployeeDetailPageUrl(o.EmployeeCode);
				o.MemberURL = o.PersonId.HasValue ? _urlGeneratorService.GetMemberDetail(o.PersonId.Value) : "";
				o.SubPageName = o.SubPageId.HasValue ? EnumHelper.GetEnumDescription((PageTypeId)o.SubPageId.Value) : "";
			});



			return list;
		}

		public ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? daysBeforeExpire, int pageSize, int pageNumber)
		{
			var list = _auditLogRepository.GetMemberPointReport(minPoint, maxPoint, daysBeforeExpire, pageSize, pageNumber);
			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId == 0 ? string.Empty : _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code;
				o.TitleName = titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.PersonId);
			});

			return list;
		}

		public ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? daysBeforeExpire)
		{
			var list = _auditLogRepository.GetMemberPointReport(minPoint, maxPoint, daysBeforeExpire);
			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId == 0 ? string.Empty : _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code;
				o.TitleName = titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.PersonId);
			});

			return list;
		}

		public ResultSet<MemberPointExpireReport> GetMemberPointExpiryReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate, int pageSize, int pageNumber)
		{
			var list = _auditLogRepository.GetMemberPointExpireReport(minPoint, maxPoint, startDate, endDate, pageSize, pageNumber);
			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId == 0 ? string.Empty : _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code;
				o.TitleName = titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.PersonId);
				o.ExpireDetail = o.ExpiredDate.HasValue ? this.GetExpireDetail(o.ExpiredDate.Value, DateTime.Today) : "ไม่ระบุข้อมูลหมดอายุได้";

			});

			return list;
		}

		public ResultSet<MemberPointExpireReport> GetMemberPointExpiryReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate)
		{
			var list = _auditLogRepository.GetMemberPointExpireReport(minPoint, maxPoint, startDate, endDate);
			//Mapping Title Name
			var titles = _lookupRepository.GetTitles().ToList();
			list.ToList().ForEach(o =>
			{
				o.PersonCode = o.PersonId == 0 ? string.Empty : _memberRepository.GetMember(o.PersonId, Source.MEMBERSHIP_WEBSITE).Code;
				o.TitleName = titles.FirstOrDefault(x => x.TitleId == o.TitleId)?.TitleName;
				o.MemberURL = _urlGeneratorService.GetMemberDetail(o.PersonId);
				o.ExpireDetail = o.ExpiredDate.HasValue ? this.GetExpireDetail(o.ExpiredDate.Value, DateTime.Today) : "ไม่ระบุข้อมูลหมดอายุได้";
			});

			return list;
		}


		private string GetExpireDetail(DateTime startDate, DateTime endDate)
		{
			var date = startDate;
			var now = endDate;

			if(date < now)
			{
				return "หมดอายุแล้ว";
			}

			var years = date.Year - now.Year;
			if (date.Month < now.Month || (date.Month == now.Month && date.Day < now.Day))
			{
				if(years > 0)
				{
					years--;
				}
			}
			var months = date.Month - now.Month;
			if (date.Day < now.Day)
			{
				months--;
			}
			if (months < 0)
			{
				months += 12;
			}
			var lastMonthDate = now.AddMonths(-1);
			var daysInLastMonth = DateTime.DaysInMonth(lastMonthDate.Year, lastMonthDate.Month);
			var days =
				date.Day -
					(now.Day > daysInLastMonth
						? daysInLastMonth
						: now.Day);
			if (days < 0)
			{
				days += daysInLastMonth;
			}

			return $"{years} ปี {months} เดือน {days} วัน";
		}
	}
}