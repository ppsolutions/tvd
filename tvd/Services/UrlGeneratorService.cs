﻿using System;
using tvd.model.Models.Enum;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class UrlGeneratorService : IUrlGeneratorService
    {
        public string GetSearchResultUrl(string queryString) => $"/searchResult/index{queryString}";
        public string GetMemberDetail(int personId, Source source) => $"/memberDetail/index?personId={personId}&source={(int)source}";
        public string GetSearchUrl() => "/search/index";
        public string GetLoginUrl() => "/login";
        public string GetMemberPageUrl() => "/member/index";
        public string GetRewardPageUrl() => "/point/rewardlist";
        public string GetApproveMemberPageUrl() => "/approval/newmember";
        public string GetApproveAutoSettingPageUrl() => "/approval/autosetting";
        public string GetApproveNewMemberPageUrl() => "/approval/newmember";
        public string GetApproveUpdateMemberPageUrl() => "/approval/updatemember";
        public string GetApproveBlockMemberPageUrl() => "/approval/blockmember";
        public string GetApproveCancelMemberPageUrl() => "/approval/cancelmember";
        public string GetApproveUpdatePointPageUrl() => "/approval/updatepoint";
        public string GetApproveRedemptionPageUrl() => "/approval/redemption";
        public string GetApproveUpgradeLevelPageUrl() => "/approval/upgradelevel";
        public string GetNewsMemberPageUrl() => "/news/member";
        public string GetSettingsEmployeePageUrl() => "/setting/employee";
        public string GetSettingsPermissionPageUrl() => "/setting/permission";
        public string GetSettingsEmployeeCreatePageUrl() => "/setting/employee/create";
        public string GetSettingsEmployeeEditPageUrl(string employeeCode) => $"/setting/employee/edit?id={employeeCode}";
        public string GetSettingsEmployeeDetailPageUrl(string employeeCode) => $"/setting/employee/detail?id={employeeCode}";
        public string GetSettingsPermissionCreatePageUrl() => "/setting/permission/create";
        public string GetSettingsPermissionEditPageUrl(int id) => $"/setting/permission/edit/{id}";
        public string GetNewsPromotionPageUrl() => "/news/promotion";
        public string GetNewsMemberRulePageUrl() => "/news/memberrules";
        public string GetNewsMemberBenefitPageUrl() => "/news/memberbenefits";
        public string GetNewsCreatePageUrl() => "/news/create";
        public string GetNewsEditPageUrl(int newsId) => $"/news/edit?id={newsId}";
        public string GetBasicPointUrl() => "/point/basicpoint";
        public string GetBasicPointLevelUrl() => "/point/basicpointlevel";
        public string GetCampaignUrl() => "/point/campaign";
        public string GetGamificationUrl() => "/point/gamification";
        public string GetPointLevelUrl() => "/point/pointlevel";
        public string GetPromotionUrl() => "/point/promotion";
        public string GetRuleUrl() => "/point/rule";
        public string GetRewardCreateUrl() => "/point/rewardcreate";
        public string GetRewardEditUrl() => "/point/rewardedit";
        public string GetRuleCreateUrl() => "/point/rulecreate";
        public string GetRuleEditUrl(int ruleId) => $"/point/ruleedit?id={ruleId}";

        public string GetGamificationCreateUrl() => "/point/gamificationcreate";
        public string GetPromotionCreateUrl() => "/point/promotion/create";
        public string GetReportNewMemberPageUrl() => "/report/newmember";
        public string GetReportUpgradeMemberUrl() => "/report/upgrademember";

        public string GetReportCancelMember() => "/report/cancelmember";

        public string GetReportBlockMember() => "/report/blockmember";

        public string GetReportChangePassword() => "/report/changepassword";

        public string GetReportRedemptionUrl() => "/report/redemption";

        public string GetReportHistoryMember() => "/report/MemberHistory";
        public string GetReportHistory() => "/report/history";

        public string GetReportMemberPointUrl() => "/report/memberPoint";

        public string GetReportMemberPointNearlyExpireUrl() => "/report/memberPointExpire";

        public string GetReportUpdateMemberPointUrl() => "/report/updateMemberPoint";

        public string GetReportCancelMemberPointUrl() => "/report/cancelMemberPoint";

        public string GetNewsMainUrl() => "/news/index";

		public string GetUploadMemberPointUrl() => "/point/uploadMemberPoint";
	}
}