﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using tvd.Helper;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.Models;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Services
{
	public class MemberService : BaseService, IMemberService
	{
		private readonly IMemberRepository _memberRepository;
		private readonly IPersonQuestionRepository _personQuestionRepository;
		private readonly IApproveHelper _approveHelper;
		private readonly IPersonAddressService _addressService;
		private readonly IUrlGeneratorService _urlGeneratorService;
		private readonly ILogHelper _logHelper;
		private readonly IMemberPointService _memberPointService;

		public MemberService(IMemberRepository memberRepository,
			IPersonAddressService addressService, IPersonQuestionRepository personQuestionRepository,
			IApproveHelper approveHelper, IUrlGeneratorService urlGeneratorService,
			ILogHelper logHelper, IMemberPointService memberPointService)
		{
			_memberRepository = memberRepository;
			_addressService = addressService;
			_personQuestionRepository = personQuestionRepository;
			_approveHelper = approveHelper;
			_urlGeneratorService = urlGeneratorService;
			_logHelper = logHelper;
			_memberPointService = memberPointService;
		}

		public Person GetPersonFromContactNumber(string contactNumber)
		{
			var members = _memberRepository.GetMembers(string.Empty, string.Empty, contactNumber, string.Empty);
			return members.FirstOrDefault();
		}

		public IEnumerable<MemberInfo> GetMemberInfoList(string name, string idCard, string phoneNumber, string memberId)
		{
			name = string.IsNullOrEmpty(name) ? null : name.Trim();
			idCard = string.IsNullOrEmpty(idCard) ? null : idCard.Trim();
			phoneNumber = string.IsNullOrEmpty(phoneNumber) ? null : phoneNumber.Trim();
			memberId = string.IsNullOrEmpty(memberId) ? null : memberId.Trim();

			IEnumerable<Person> persons;

			if (!string.IsNullOrEmpty(name) && 
				string.IsNullOrEmpty(idCard) && 
				string.IsNullOrEmpty(phoneNumber) && 
				string.IsNullOrEmpty(memberId))
			{
				persons = _memberRepository.GetMembers(name);
			}
			else
			{
				persons = _memberRepository.GetMembers(name == "" ? null : name, idCard, phoneNumber, memberId);
			}
			return (from person in persons
					let points = _memberPointService.GetCurrentMemberPoints(person.PersonId)
					select new MemberInfo(person, points)).ToList();
		}

		public ResultSet<MemberInfo> GetMemberInfoList(string name, string idCard, string phoneNumber, string memberId, int pageSize, int pageNumber)
		{
			name = name == "" ? null : name.Trim();
			phoneNumber = phoneNumber == "" ? null : phoneNumber.Trim();
			memberId = memberId == "" ? null : memberId.Trim();
			idCard = idCard == "" ? null : idCard.Trim();

			var persons = _memberRepository.GetMembers(name == "" ? null : name, idCard, phoneNumber, memberId, pageSize, pageNumber);

			var memberInfos = new ResultSet<MemberInfo>
			{
				TotalItems = persons.TotalItems
			};
			foreach (var person in persons)
			{
				var memberPoint = 0;
				if (person.PersonId != 0)
				{
					//Mapping active point if user is in the system.
					memberPoint = _memberPointService.GetCurrentMemberPoints(person.PersonId);
				}
				memberInfos.Add(new MemberInfo(person, memberPoint));
			}
			return memberInfos;
		}


		public int CreateMember(Person person, PersonAddressGroup addressGroup, List<PersonQuestion> questions, string[] mappingDatas, string createdBy)
		{
			//TODO: Begin  transaciton
			//who created?
			var employee = EmployeeHelper.GetEmployee(createdBy);
			var personId = 0;
			var isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.NEW_MEMBER);

			//If newmember is auto aprrove, need to update rec_status
			person.RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING;

			//Hash password
			person.EncryptedPassword = PasswordHasher.EncryptString(person.Password);

			var auditLog = new AuditLog
			{
				AuditActionType = AuditActionType.NEW_MEMBERSHIP_APPROVED,
				Source = Source.MEMBERSHIP_WEBSITE,
				RecStatus = person.RecStatus,
				OldValue = "New person",
				NewValue = person,
				Description = JsonConvert.SerializeObject(
						new
						{
							Name = person.FullName,
							Source = employee
						}),
				RecCreatedBy = createdBy
			};


			//Log before insert person
			_logHelper.Log(() =>
				{
					personId = _memberRepository.CreateUser(person);
					auditLog.ObjectId = personId;
				}, auditLog);


			if (personId <= 0) return 0;

			//Update password
			_memberRepository.UpdateUserPassword(personId, person.EncryptedPassword, createdBy);

			if (!isAutoApprove)
			{
				//If not auto approve, insert into pending approve list
				var json = new
				{
					PersonId = personId,
					Name = person.FullName,
					Source = employee
				};
				_approveHelper.InsertPendingObject(personId, json, ApproveType.NEW_MEMBER, createdBy);
			}


			//Create address
			//Assign person Id
			addressGroup.CurrentAddress.PersonId = personId;
			addressGroup.CurrentAddress.RecCreatedBy = createdBy;
			addressGroup.ShipmentAddress.PersonId = personId;
			addressGroup.ShipmentAddress.RecCreatedBy = createdBy;
			addressGroup.InvoiceAddress.PersonId = personId;
			addressGroup.InvoiceAddress.RecCreatedBy = createdBy;

			_addressService.AddPersonAddressGroup(addressGroup);

			//Create questions
			//Assing person id
			questions = questions.Select(o =>
			{
				o.PersonId = personId;
				o.RecCreatedBy = createdBy;
				return o;
			}).ToList();

			foreach (var question in questions)
			{
				_personQuestionRepository.InsertPersonQuestion(question);
			}

			_memberPointService.UpdateMemberPointLevel(personId, 1, 0, createdBy);

			if (mappingDatas != null && mappingDatas.Length > 0)
			{
				foreach (string data in mappingDatas)
				{
					string[] items = data.Split(new char[] { '_' }, System.StringSplitOptions.RemoveEmptyEntries);
					MemberMapping memberMapping = new MemberMapping()
					{
						PersonId = personId,
						MapWithId = items[0],
						Source = (Source)Convert.ToInt32(items[1]),
						PointTransfered = Convert.ToInt32(items[2]),
						RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING
					};

					if (isAutoApprove)
					{
						this.MergeMember(memberMapping, Convert.ToInt32(createdBy));
					}
					else
					{
						_approveHelper.InsertPendingObject(personId, memberMapping, ApproveType.MERGE_MEMBER, createdBy);
					}
				}
			}
			return personId;
		}

		public UpdateResult UpdatePassword(int personId, string oldPassword, string newPassword, string updatedBy)
		{
			var encryptPassword = PasswordHasher.EncryptString(newPassword);
			//Validate old password
			var oldEncryptPwd = PasswordHasher.EncryptString(oldPassword);
			var person = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE);
			if (oldEncryptPwd != person.EncryptedPassword)
			{
				return new UpdateResult(false, "รหัสผ่านเก่าไม่ถูกต้อง");
			}


			var isautoApprove = _approveHelper.IsAutoApprove(ApproveType.UPDATE_MEMEBER_INFO);
			var employee = EmployeeHelper.GetEmployee(updatedBy);
			//Log
			_logHelper.Log(() =>
			{
				if (isautoApprove)
				{
					_memberRepository.UpdateUserPassword(personId, encryptPassword, updatedBy);
				}
				else
				{

					var json = new
					{
						Password = encryptPassword,
						Description = "change password",
						Type = UpdateMember.SECURITY,
						Source = employee
					};
					_approveHelper.InsertPendingObject(personId, json, ApproveType.UPDATE_MEMEBER_INFO, updatedBy);
				}
			},
				new AuditLog
				{
					AuditActionType = AuditActionType.CHANGE_PASSWORD,
					ObjectId = personId,
					Source = Source.MEMBERSHIP_WEBSITE,
					RecStatus = isautoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
					OldValue = "Change password",
					NewValue = "",
					Description = JsonConvert.SerializeObject(
					new
					{
						PersonId = personId,
						Name = person.FullName,
						Source = employee
					}),
					RecCreatedBy = updatedBy
				});

			return new UpdateResult(true);
		}

		public UpdateResult UpdateBasicInfo(Person person)
		{
			bool isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.UPDATE_MEMEBER_INFO);
			var employee = EmployeeHelper.GetEmployee(person.RecModifiedBy);
			var oldEmployeeData = _memberRepository.GetMember(person.PersonId, Source.MEMBERSHIP_WEBSITE);

			oldEmployeeData.EncryptedPassword = string.Empty;
			person.EncryptedPassword = oldEmployeeData.EncryptedPassword;
			person.Source = oldEmployeeData.Source;
			person.Code = oldEmployeeData.Code;
			person.ReferenceId = oldEmployeeData.ReferenceId;
			person.RecCreatedBy = oldEmployeeData.RecCreatedBy;
			
			_logHelper.Log(() =>
			{
				if (isAutoApprove)
				{
					_memberRepository.UpdateUser(person);
				}
				else
				{
					var json = new ApproveMemberDetail
					{
						Person = person,
						Description = "change basic information",
						Type = UpdateMember.BASICINFO,
						Source = employee
					};
					_approveHelper.InsertPendingObject(person.PersonId, json, ApproveType.UPDATE_MEMEBER_INFO, person.RecModifiedBy);
				}
			},
			 new AuditLog
			 {
				 AuditActionType = AuditActionType.UPDATE_MEMBERSHIP_INFO,
				 ObjectId = person.PersonId,
				 Source = Source.MEMBERSHIP_WEBSITE,
				 RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
				 OldValue = JsonConvert.SerializeObject(oldEmployeeData),
				 NewValue = JsonConvert.SerializeObject(person),
				 Description = JsonConvert.SerializeObject(
					new
					{
						PersonId = person.PersonId,
						Name = person.FullName,
						Source = employee
					}),
				 RecCreatedBy = employee.EmployeeCode
			 });
			return new UpdateResult(true);

		}

		public UpdateResult UpdateAddress(PersonAddressGroup addressGroup)
		{
			bool isAutoApprove = _approveHelper.IsAutoApprove(ApproveType.UPDATE_MEMEBER_INFO);
			var employee = EmployeeHelper.GetEmployee(addressGroup.CurrentAddress.RecModifiedBy);
			var person = _memberRepository.GetMember(addressGroup.CurrentAddress.PersonId, Source.MEMBERSHIP_WEBSITE);

			_logHelper.Log(() =>
			{
				if (isAutoApprove)
				{
					_addressService.UpdatePersonAddress(addressGroup);
				}
				else
				{
					var json = new ApproveMemberDetail
					{
						AddressGroup = addressGroup,
						Description = "change address",
						Type = UpdateMember.ADDRESS,
						Source = employee
					};
					_approveHelper.InsertPendingObject(addressGroup.CurrentAddress.PersonId, json, ApproveType.UPDATE_MEMEBER_INFO, addressGroup.CurrentAddress.RecModifiedBy);
				}
			},
			new AuditLog
			{
				AuditActionType = AuditActionType.UPDATE_MEMBERSHIP_ADDRESS,
				ObjectId = addressGroup.CurrentAddress.PersonId,
				Source = Source.MEMBERSHIP_WEBSITE,
				RecStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING,
				OldValue = "change address",
				NewValue = JsonConvert.SerializeObject(addressGroup),
				Description = JsonConvert.SerializeObject(
					new
					{
						PersonId = person.PersonId,
						Name = person.FullName,
						Source = employee
					}),
				RecCreatedBy = employee.EmployeeCode
			});

			return new UpdateResult(true);
		}

		public UpdateResult UpdateQuestions(List<PersonQuestion> questions)
		{
			if (_approveHelper.IsAutoApprove(ApproveType.UPDATE_MEMEBER_INFO))
			{
				foreach (var question in questions)
				{
					_personQuestionRepository.UpdatePersonQuestion(question);
				}
			}
			else
			{

				var q = questions.First();
				var json = new ApproveMemberDetail
				{
					Questions = questions,
					Description = "change question",
					Type = UpdateMember.QUESTION,
					Source = EmployeeHelper.GetEmployee(q.RecModifiedBy)
				};
				_approveHelper.InsertPendingObject(q.PersonId, json, ApproveType.UPDATE_MEMEBER_INFO, q.RecModifiedBy);
			}
			return new UpdateResult(true);
		}

		public void BlockUser(int personId, string updatedBy)
		{
			CheckApproveAndUpdateMemberStatus(personId, updatedBy, RecStatus.INACTIVE, ApproveType.BLOCK_MEMBER, AuditActionType.BLOCK_MEMBERSHIP);
		}

		public void ActiveUser(int personId, string updatedBy)
		{
			CheckApproveAndUpdateMemberStatus(personId, updatedBy, RecStatus.ACTIVE, ApproveType.ACTIVE_MEMBER, AuditActionType.ACTIVE_MEMBERSHIP);
		}

		public void DeleteUser(int personId, string updatedBy)
		{
			CheckApproveAndUpdateMemberStatus(personId, updatedBy, RecStatus.DELETE, ApproveType.CANCEL_MEMBER, AuditActionType.CANCEL_MEMBERSHIP);
		}

		private void UpdateUserStatus(int personId, RecStatus status, string updatedBy)
		{
			_memberRepository.UpdateUserStatus(personId, status, updatedBy);
		}

		public IEnumerable<SearchNameResult> GetPersonName(string name)
		{
			var persons = _memberRepository.GetMembers(name);
			//Grouping user by name
			var results = persons.GroupBy(
				p => p.FullName,
				p => p,
				(key, g) => new SearchNameResult
				{
					Name = key,
					Count = g.Count(),
					//Decide navigate url to result list or member detail
					Url = g.Count() == 1 ? _urlGeneratorService.GetMemberDetail(g.First().PersonId == 0 ? Convert.ToInt32(g.First().ReferenceId) : g.First().PersonId, g.First().Source)
										: _urlGeneratorService.GetSearchResultUrl(QueryStringBuilder.BuildSearchResultQuery(key, "", "", ""))
				});
			return results;

		}

		public int MergeCustomer(Person customerBase, List<int> mergeCustomerIds)
		{
			//TODO: merge customer
			return customerBase.PersonId;

		}

		public Member GetMemberDetail(int personId, Source source)
		{
			var userInfo = _memberRepository.GetMember(personId, source);
			if (userInfo != null)
			{
				//Get password
				userInfo.Password = string.IsNullOrEmpty(userInfo.EncryptedPassword) ?
										string.Empty : PasswordHasher.DecryptString(userInfo.EncryptedPassword);

				var customerPoint = new MemberPointDetail();
				var addressGroup = new PersonAddressGroup();
				var questions = new List<PersonQuestion>();
				var memberMapping = new List<MemberMapping>();
				if (source == Source.MEMBERSHIP_WEBSITE)
				{
					customerPoint = _memberPointService.GetMemberPointDetail(userInfo.PersonId);
					addressGroup = _addressService.GetPersonAddressGroup(userInfo.PersonId);
					questions = _personQuestionRepository.GetPersonQuestions(userInfo.PersonId).ToList();
					memberMapping = personId == 0 ? new List<MemberMapping>() : GetMergeUsers(Convert.ToInt32(userInfo.SearchId));
				}
				var customer = new Member(userInfo, customerPoint, addressGroup, questions, memberMapping);
				return customer;
			}

			return null;
		}

		public MemberInfo GetMemberInfo(int personId)
		{
			var person = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE);
			if (person == null) return null;
			var points = _memberPointService.GetCurrentMemberPoints(personId);
			return new MemberInfo(person, points);
		}

		public List<MemberMapping> GetMergeUsers(int personId)
		{
			List<MemberMapping> memberMappingList = _memberRepository.GetMergeUsers(personId);

			foreach (MemberMapping item in memberMappingList)
			{
				var person = _memberRepository.GetMember(item.MapWithId, item.Source);
				switch (item.Source)
				{
					case Source.MEMBERSHIP_WEBSITE: item.ReferenceId = person.Code; break;
					case Source.CALL_CENTER:
					case Source.TVDIS_POS:
						item.ReferenceId = person.ReferenceId; break;
				}

			}

			return memberMappingList;
		}

		public void MergeMember(MemberMapping memberMapping, int updateBy)
		{
			_memberRepository.MergeMember(memberMapping, updateBy);
		}

		public int GetMemberId(string memberCode)
		{
			return _memberRepository.FetchMemberId(memberCode);
		}

		public RegisterViewModel CreateRegisterViewModel(IContextProvider contextProvider)
		{
			var vm = new RegisterViewModel()
			{
				SubPageTitle = GetSubPageTitle((PageTypeId)contextProvider.GetPageTypeId()),
				SubBreadcrumb = CreateBreadcrumb(contextProvider, null),
				Sidebar = CreateSidebar(contextProvider)
			};

			return vm;
		}

		protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
		{
			var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
			var bc = new List<BreadcrumbItem>();
			bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });


			return bc;
		}
		protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
		{
			var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

			var sb = new List<SidebarItem>();

			return sb;
		}

		private void CheckApproveAndUpdateMemberStatus(int personId, string updatedBy, RecStatus updatedStatus, ApproveType approveType, AuditActionType auditActionType)
		{
			var isAutoApprove = _approveHelper.IsAutoApprove(approveType);
			var recStatus = isAutoApprove ? RecStatus.ACTIVE : RecStatus.PENDING;
			var auditType = auditActionType;

			var currentMember = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE);
			var employee = EmployeeHelper.GetEmployee(updatedBy);

			//Log
			_logHelper.Log(() =>
				{
					if (isAutoApprove)
					{
						UpdateUserStatus(personId, updatedStatus, updatedBy);
					}
					else
					{
						var person = _memberRepository.GetMember(personId, Source.MEMBERSHIP_WEBSITE);
						var json = new
						{
							PersonId = personId,
							Name = person.FullName,
							Source = employee
						};
						_approveHelper.InsertPendingObject(personId, json, approveType, updatedBy);
					}
				},
				new AuditLog
				{
					AuditActionType = auditType,
					ObjectId = personId,
					Source = Source.MEMBERSHIP_WEBSITE,
					RecStatus = recStatus,
					OldValue = EnumHelper.GetEnumDescription(currentMember.RecStatus),
					NewValue = EnumHelper.GetEnumDescription(updatedStatus),
					Description = JsonConvert.SerializeObject(
					new
					{
						PersonId = personId,
						Name = currentMember.FullName,
						RecStatus = EnumHelper.GetEnumDescription(updatedStatus),
						Source = employee
					}),
					RecCreatedBy = updatedBy
				});

		}

	}
}