﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class GamificationService : IGamificationService
    {
        private readonly IGamificationPointRepository _gamificationPointRepository;
        public GamificationService(IGamificationPointRepository gamificationPoint)
        {
            _gamificationPointRepository = gamificationPoint;
        }

        public ResultSet<GamificationPoint> GetGamificationPoints(string code, int? applyTo, int? recStatus, int? pageSize, int? pagegNumber)
        {
            return _gamificationPointRepository.GetGamificationPoints(code, applyTo, recStatus, pageSize, pagegNumber);
        }

        public void CreateGamification(GamificationPoint newGame)
        {
            _gamificationPointRepository.InsertGamificationPoint(newGame);
        }

        public void UpdateGamificationPoint(GamificationPoint game)
        {
            _gamificationPointRepository.UpdateGamificationPoint(game);
        }

        public GamificationPoint GetGamification(string gameCode)
        {
            return _gamificationPointRepository.GetGamificationPoint(gameCode);
        }

        public GamificationPoint GetGamification(int gameId)
        {
            return _gamificationPointRepository.GetGamificationPoint(gameId);
        }

        public void UpdateGamificationStatus(GamificationPoint game)
        {
            _gamificationPointRepository.UpdateGamificationStatus(game.GamificationPointId, game.RecStatus, game.RecModifiedBy);
        }
    }
}