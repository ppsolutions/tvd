﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Helpers;
using tvd.model.Provider;
using tvd.Models;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Services
{
    public class ReportService : BaseService, IReportService
    {
        private readonly IUrlGeneratorService _urlGeneratorService;

        public ReportService(IUrlGeneratorService urlGeneratorService)
        {
            _urlGeneratorService = urlGeneratorService;
        }
        public ReportViewModel CreateSettingViewModel(IContextProvider contextProvider)
        {
            var vm = new ReportViewModel
            {
                
                Sidebar = CreateSidebar(contextProvider),
                SubBreadcrumb = CreateBreadcrumb(contextProvider, null),
                SubPageTitle = GetSubPageTitle((PageTypeId)contextProvider.GetPageTypeId())
            };
            return vm;
        }

        protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
            var bc = new List<BreadcrumbItem>();
            bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "หน้าหลัก", Url = "" });
            bc.Add(new BreadcrumbItem() { HasUrl = false, Name = "รายงาน", Url = "" });

            if (pageTypeId == PageTypeId.ReportNewMember || pageTypeId == PageTypeId.ReportBlockMember || pageTypeId == PageTypeId.ReportCancelMember || pageTypeId == PageTypeId.ReportUpgradeMember)
            {
                bc.Add(new BreadcrumbItem() { Name = "รายงานเกี่ยวกับสมาชิก" });
            }
            else if (pageTypeId == PageTypeId.ReportMemberPoint || pageTypeId == PageTypeId.ReportNearlyExpiredPoint || pageTypeId == PageTypeId.ReportRedemption || pageTypeId == PageTypeId.ReportUpdateMemberPoint || pageTypeId == PageTypeId.ReportCancelMemberPoint)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานเกี่ยวกับคะแนน" });
            }
            if (pageTypeId == PageTypeId.ReportNewMember)
            {
                bc.Add(new BreadcrumbItem() { Name = "รายงานการลงทะเบียนสมาชิก", Url = _urlGeneratorService.GetReportNewMemberPageUrl() });
            }
            else if (pageTypeId == PageTypeId.ReportBlockMember)
            {
                bc.Add(new BreadcrumbItem() { Name = "รายงานการบล็อคสมาชิก", Url = _urlGeneratorService.GetReportBlockMember() });

            }
            else if (pageTypeId == PageTypeId.ReportCancelMember)
            {
                bc.Add(new BreadcrumbItem() { Name = "รายงานกายกเลิกสมาชิก", Url = _urlGeneratorService.GetReportCancelMember() });

            }
            else if (pageTypeId == PageTypeId.ReportUpgradeMember)
            {
                bc.Add(new BreadcrumbItem() { Name = "รายงานการเลื่อนระดับสมาชิก", Url = _urlGeneratorService.GetReportUpgradeMemberUrl() });

            }
            else if (pageTypeId == PageTypeId.ReportChangePassword)
            {
                bc.Add(new BreadcrumbItem { Name = "การเปลี่ยนรหัสผ่านสมาชิก", Url = _urlGeneratorService.GetReportChangePassword() });
            }
            else if (pageTypeId == PageTypeId.ReportHistoryMember)
            {
                bc.Add(new BreadcrumbItem { Name = "การเรียกดูทะเบียนสมาชิก", Url = _urlGeneratorService.GetReportHistoryMember() });
            }
            else if (pageTypeId == PageTypeId.ReportHistory)
            {
                bc.Add(new BreadcrumbItem { Name = "การเรียกดูรายงาน", Url = _urlGeneratorService.GetReportHistory() });
            }
            else if (pageTypeId == PageTypeId.ReportRedemption)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานการแลกคะแนนสะสม", Url = _urlGeneratorService.GetReportRedemptionUrl() });

            }
            else if (pageTypeId == PageTypeId.ReportMemberPoint)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานคะแนนสะสมทั้งหมด", Url = _urlGeneratorService.GetReportMemberPointUrl() });
            }
            else if (pageTypeId == PageTypeId.ReportNearlyExpiredPoint)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานคะแนนสะสมใกล้ครบอายุ", Url = _urlGeneratorService.GetReportMemberPointNearlyExpireUrl() });
            }
            else if(pageTypeId == PageTypeId.ReportUpdateMemberPoint)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานการปรับปรุงคะแนน", Url = _urlGeneratorService.GetReportUpdateMemberPointUrl() });               
            }
            else if(pageTypeId == PageTypeId.ReportCancelMemberPoint)
            {
                bc.Add(new BreadcrumbItem { Name = "รายงานการยกเลิกคะแนน", Url = _urlGeneratorService.GetReportCancelMemberPointUrl() });               
            }
            return bc;
        }

        protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

            var sb = new List<SidebarItem>();

            sb.Add(new SidebarItem
            {
                IsHeader = true,
                Name = "รายงานเกี่ยวกับสมาชิก",
                IsActive = false,
                MenuId = 1,
                ReferMenuId = 0
            });

            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportNewMember),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportNewMember),
                MenuId = 2,
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetReportNewMemberPageUrl()
            });
            sb.Add(new SidebarItem
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportUpgradeMember),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportUpgradeMember),
                MenuId = 3,
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetReportUpgradeMemberUrl()
            });
            sb.Add(new SidebarItem
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportCancelMember),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportCancelMember),
                MenuId = 4,
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetReportCancelMember()
            });
            sb.Add(new SidebarItem
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportBlockMember),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportBlockMember),
                MenuId = 5,
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetReportBlockMember()
            });
            //sb.Add(new SidebarItem
            //{
            //    Name = EnumHelper.GetEnumDescription(PageTypeId.ReportChangePassword),
            //    IsActive = IsActive(pageTypeId, PageTypeId.ReportChangePassword),
            //    MenuId = 6,
            //    ReferMenuId = 1,
            //    Url = _urlGeneratorService.GetReportChangePassword()
            //});

            sb.Add(new SidebarItem
            {
                IsHeader = true,
                Name = "รายงานเกี่ยวกับคะแนน",
                IsActive = false,
                MenuId = 8,
                ReferMenuId = 0
            });
            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportRedemption),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportRedemption),
                MenuId = 9,
                ReferMenuId = 8,
                Url = _urlGeneratorService.GetReportRedemptionUrl()
            });
            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportMemberPoint),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportMemberPoint),
                MenuId = 10,
                ReferMenuId = 8,
                Url = _urlGeneratorService.GetReportMemberPointUrl()
            });
            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportNearlyExpiredPoint),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportNearlyExpiredPoint),
                MenuId = 14,
                ReferMenuId = 8,
                Url = _urlGeneratorService.GetReportMemberPointNearlyExpireUrl()
            });
            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportUpdateMemberPoint),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportUpdateMemberPoint),
                MenuId = 15,
                ReferMenuId = 8,
                Url = _urlGeneratorService.GetReportUpdateMemberPointUrl()
            });
            sb.Add(new SidebarItem()
            {
                Name = EnumHelper.GetEnumDescription(PageTypeId.ReportCancelMemberPoint),
                IsActive = IsActive(pageTypeId, PageTypeId.ReportCancelMemberPoint),
                MenuId = 16,
                ReferMenuId = 8,
                Url = _urlGeneratorService.GetReportCancelMemberPointUrl()
            });
            sb.Add(new SidebarItem()
            {
                IsHeader = true,
                Name = "ประวัติการดู",
                IsActive = false,
                MenuId = 11,
                ReferMenuId = 0
            });
            sb.Add(new SidebarItem()
            {
                Name = "ทะเบียนสมาชิก",
                IsActive = IsActive(pageTypeId, PageTypeId.ReportHistoryMember),
                MenuId = 12,
                ReferMenuId = 11,
                Url = _urlGeneratorService.GetReportHistoryMember()
            });
            sb.Add(new SidebarItem()
            {
                Name = "รายงาน",
                IsActive = IsActive(pageTypeId, PageTypeId.ReportHistory),
                MenuId = 13,
                ReferMenuId = 11,
                Url = _urlGeneratorService.GetReportHistory()
            });



            return sb;
        }
    }
}