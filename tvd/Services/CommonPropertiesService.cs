﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;
using tvd.model.Models.UserData;
using tvd.model.Repositories.Interfaces;
using tvd.Services.Interfaces;
using tvd.ViewModel;

namespace tvd.Services
{
    public class CommonPropertiesService : ICommonPropertiesService
    {
        private readonly ICommonPropertiesServiceSet ServiceSet;
        private readonly IApproveRepository _approveRepository;
        public CommonPropertiesService(ICommonPropertiesServiceSet serviceSet, IApproveRepository approveRepository) : base()
        {
            ServiceSet = serviceSet;
            _approveRepository = approveRepository;
        }

        public void SetCommonProperties(object viewmodel, HttpContextBase contextBase, IUserData userData)
        {
            if (Valid(viewmodel, userData))
            {
                BasePageViewModel vm = viewmodel as BasePageViewModel;

                SetHeader(vm, userData, contextBase);
                SetBreadCrumbs(vm, userData);
                SetNotificationGroup(vm);
            }
        }

        private bool Valid(object vm, IUserData userData)
        {
            return userData != null && vm != null && (vm as BasePageViewModel) != null;
        }

        private void SetHeader(BasePageViewModel vm, IUserData userData, HttpContextBase contextBase)
        {
            vm.MemberPageUrl = ServiceSet.UrlGeneratorService.GetSearchResultUrl("");
            vm.RewardPageUrl = ServiceSet.UrlGeneratorService.GetRewardPageUrl();
            vm.ApprovalPageUrl = ServiceSet.UrlGeneratorService.GetApproveMemberPageUrl();
            vm.NewsPageUrl = ServiceSet.UrlGeneratorService.GetNewsMainUrl();
            vm.ReportPageUrl = ServiceSet.UrlGeneratorService.GetReportNewMemberPageUrl();
            vm.SettingsEmployeePageUrl = ServiceSet.UrlGeneratorService.GetSettingsEmployeePageUrl();
            vm.SettingsPermissionPageUrl = ServiceSet.UrlGeneratorService.GetSettingsPermissionPageUrl();
        }

        private void SetBreadCrumbs(BasePageViewModel vm, IUserData userData)
        {

        }

        private void SetNotificationGroup(BasePageViewModel vm)
        {
            var results = _approveRepository.GetApproveList(null, ApproveType.UNKNOWN, RecStatus.INACTIVE, 999, 1);
            var notificationGroup = new NotificationGroup();
            notificationGroup.Items = new List<NotificationItem>();

            vm.NotificationGroup = notificationGroup;
            
            //Grouping by approve type
            var grouping = results.ToList().GroupBy(o => o.ApproveType).OrderBy(o =>(int)o.Key);

            foreach (var group in grouping)
            {
                var item = new NotificationItem
                {
                    Count = group.Count()
                };

                switch (@group.Key)
                {
                    case ApproveType.NEW_MEMBER:
                        item.Text = "คำขอสมาชิกใหม่";
                        item.Url = "/approval/newmember";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.UPDATE_MEMEBER_INFO:
                        item.Text = "คำขอการเปลี่ยนข้อมูลสมาชิก";
                        item.Url = "/approval/updatemember";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.BLOCK_MEMBER:
                        item.Text = "คำขอการบล็อคสมาชิก";
                        item.Url = "/approval/blockmember";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.CANCEL_MEMBER:
                        item.Text = "คำขอการยกเลิกสมาชิก";
                        item.Url = "/approval/cancelmember";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.UPDATE_REWARD_POINT:
                        item.Text = "คำขอการเปลี่ยนคะแนน";
                        item.Url = "/approval/updatepoint";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.PRODUCT_REDEMPTION:
                        item.Text = "คำขอการใช้แลกสินค้า";
                        item.Url = "/approval/redemption";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;
                    case ApproveType.UPGRADE_MEMBER_LEVEL:
                        item.Text = "คำขอการเลื่อนระดับ";
                        item.Url = "/approval/upgradelevel";
                        item.Count = group.Count();
                        notificationGroup.Items.Add(item);
                        break;                   
                }
            }
            notificationGroup.AllPendings = grouping.Sum(o => o.Count());
        }
    }
}