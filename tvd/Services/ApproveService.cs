﻿using Autofac;
using System.Collections.Generic;
using System.Linq;
using System.Web.DynamicData;
using Newtonsoft.Json;
using tvd.Helper;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Provider;
using tvd.model.Repositories.Interfaces;
using tvd.Models;
using tvd.Services.Interfaces;
using tvd.ViewModel;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Repositories;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Employee;

namespace tvd.Services
{
    public class ApproveService : BaseService, IApproveService, IStartable
    {
        private readonly IApproveRepository _approveRepository;

        public ApproveSetting ApproveSettingCache;
        private readonly IUrlGeneratorService _urlGeneratorService;
        private readonly IMemberRepository _memberRepository;
        private readonly IPointRepository _pointRepository;
        private readonly IPersonQuestionRepository _personQuestionRepository;
        private readonly IPersonAddressRepository _personAddressRepository;
        private readonly ILookupRepository _lookupRepository;
        private readonly ILogHelper _logHelper;
        public ApproveService(IApproveRepository approveRepository,
            IUrlGeneratorService urlGeneratorService,
            IMemberRepository memberRepository,
            IPointRepository pointRepository,
            IPersonQuestionRepository personQuestionRepository,
            IPersonAddressRepository personAddressRepository,
            ILookupRepository lookupRepository,
            ILogHelper logHelper)
        {
            _approveRepository = approveRepository;
            _urlGeneratorService = urlGeneratorService;
            _memberRepository = memberRepository;
            _pointRepository = pointRepository;
            _personQuestionRepository = personQuestionRepository;
            _personAddressRepository = personAddressRepository;
            _lookupRepository = lookupRepository;
            _logHelper = logHelper;
        }

        public UpdateResult ApprovePendingRequest(int approveId, string updatedBy)
        {
            var approve = _approveRepository.GetApprove(approveId);
            if (approve == null || approve.RecStatus == RecStatus.ACTIVE) return new UpdateResult { IsValid = true };
            var employee = EmployeeHelper.GetEmployee(updatedBy);
            var person = _memberRepository.GetMember(approve.ObjectId, Source.MEMBERSHIP_WEBSITE);
            UpdateResult updateResult;
            switch (approve.ApproveType)
            {
                case ApproveType.NEW_MEMBER:
                    updateResult = NewMember(approve, approveId, employee, updatedBy);
                    SendMail(employee.Email, "APPROVE_NEWMEMBER", "ApproveNewMember", updateResult, new{});
                    break;
                case ApproveType.UPDATE_MEMEBER_INFO:
                    updateResult = UpdateMember(approveId, approve.ObjectId, approve.ObjectValue, updatedBy, employee,
                        person);
                    SendMail(employee.Email, "UPDATE_MEMBER_INFO", "UpdateMemberInfo", updateResult, 
                    new 
                    {
                        MemberName = employee.FullName
                    });

                    break;
                case ApproveType.BLOCK_MEMBER:
                    updateResult = BlockMember(approve, approveId, employee, updatedBy);
                    SendMail(employee.Email, "BLOCK_MEMBER", "BlockMember", updateResult, new { });
                    break;

                case ApproveType.CANCEL_MEMBER:
                    updateResult = CancelMember(approve, approveId, employee, updatedBy);
                    SendMail(employee.Email, "CANCEL_MEMBER", "CancelMember", updateResult, new { });
                    break;

                case ApproveType.UPDATE_REWARD_POINT:
                    updateResult = UpdateMemberPoint(approve, approveId, employee, updatedBy);
                    SendMail(employee.Email, "UPDATE_REWARD_POINT", "UpdateRewardPoint", updateResult, new { });
                    break;

                case ApproveType.UPGRADE_MEMBER_LEVEL:
                    updateResult = UpdateMemberPointLevelAndCutPoints(approve, approveId, updatedBy, employee);
                    SendMail(employee.Email, "UPGRADE_MEMBER_LEVEL", "UpgradeMember", updateResult, new { });
                    break;

                case ApproveType.PRODUCT_REDEMPTION:
                    updateResult = UpdateProductRedemptionAndCutPoints(approve, approveId, updatedBy, employee);
                    SendMail(employee.Email, "PRODUCT_REDEMPTION", "ProductRedemption", updateResult, new { });
                    break;

                default:
                    return new UpdateResult(false, "ไม่ค้นพบคำขออนุมัติที่ท่านเลือกในระบบ");
            }

            return updateResult;
        }

        public void RejectPendingRequest(int approveId, string updatedBy, ApproveType type)
        {
            if (type == ApproveType.UPDATE_REWARD_POINT)
            {
                var employee = EmployeeHelper.GetEmployee(updatedBy);
                var approve = _approveRepository.GetApprove(approveId);
                var memberPointId = approve.ObjectId;
                var memberPoint = _pointRepository.GetMemberPoint(memberPointId);
                //Log
                _logHelper.Log(() =>
                    {
                        _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.DELETE);
                    },
                    new AuditLog
                    {
                        AuditActionType = AuditActionType.MEMBERSHIP_POINT_UPDATE,
                        ObjectId = memberPoint.PersonId,
                        Source = Source.MEMBERSHIP_WEBSITE,
                        RecStatus = RecStatus.DELETE,
                        NewValue = memberPoint.Point,
                        Description = JsonConvert.SerializeObject(
                            new
                            {
                                PersonId = memberPoint.PersonId,
                                UpdatePoint = memberPoint.Point,
                                RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(memberPoint.PersonId, false).ToList()),
                                Name = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE).FullName,
                                Remark = EnumHelper.GetEnumDescription(memberPoint.Type),
                                Source = employee
                            }),
                        RecCreatedBy = memberPoint.RecCreatedBy
                    });
            }
            else
            {
                _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.DELETE);
            }
        }

        public ResultSet<Approve> GetApproveList(ApproveType approveType, int? approveId, RecStatus? status, int pageSize, int pageNumber)
        {
            //convert object value to specific type
            var approveList = _approveRepository.GetApproveList(approveId, approveType, status, pageSize, pageNumber);
            return approveList;
        }


        public void Insert(Approve approve)
        {
            _approveRepository.Insert(approve);
        }

        public ApproveSetting GetApproveSetting()
        {
            return _approveRepository.GetApproveSetting();
        }

        public void InsertApproveSetting(ApproveSetting approveSetting)
        {
            _approveRepository.InsertApproveSetting(approveSetting);
            this.ApproveSettingCache = approveSetting;
        }

        public void UpdateApproveSetting(ApproveSetting approveSetting)
        {
            _approveRepository.UpdateApproveSetting(approveSetting);
            this.ApproveSettingCache = approveSetting;
        }

        public ApprovalViewModel CreateApprovalViewModel(IContextProvider contextProvider)
        {
            var vm = new ApprovalViewModel
            {
                Sidebar = CreateSidebar(contextProvider),
                SubBreadcrumb = CreateBreadcrumb(contextProvider, null),
                SubPageTitle = GetSubPageTitle((PageTypeId)contextProvider.GetPageTypeId())
            };
            //vm.SubBreadcrumb = CreateBreadcrumb(contextProvider, null);
            return vm;
        }

        public void Start()
        {
            ApproveSettingCache = new ApproveSetting();
            ApproveSettingCache = GetApproveSetting();
        }

        protected override IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();
            var bc = new List<BreadcrumbItem>();
            bc.Add(new BreadcrumbItem { Name = "หน้าหลัก" });
            bc.Add(new BreadcrumbItem { Name = "การอนุมัติ" });
            if (pageTypeId == PageTypeId.ApproveAutoSetting)
            {
                bc.Add(new BreadcrumbItem { HasUrl = true, Name = "การคั้งค่าการอนุมัติ", Url = _urlGeneratorService.GetApproveAutoSettingPageUrl() });

            }
            else if (pageTypeId == PageTypeId.ApproveNewMemberPage || pageTypeId == PageTypeId.ApproveBlockMemberPage || pageTypeId == PageTypeId.ApproveCancelMemberPage || pageTypeId == PageTypeId.ApproveUpdateMemberPage)
            {
                bc.Add(new BreadcrumbItem { Name = "เกี่ยวกับสมาชิก" });
                switch (pageTypeId)
                {
                    case PageTypeId.ApproveNewMemberPage:
                        bc.Add(new BreadcrumbItem { Name = "อนุมัติสมาชิกใหม่", HasUrl = true, Url = _urlGeneratorService.GetApproveNewMemberPageUrl() });
                        break;
                    case PageTypeId.ApproveBlockMemberPage:
                        bc.Add(new BreadcrumbItem { Name = "คำขออนุมัติบล็อคมาชิก", HasUrl = true, Url = _urlGeneratorService.GetApproveBlockMemberPageUrl() });
                        break;
                    case PageTypeId.ApproveCancelMemberPage:
                        bc.Add(new BreadcrumbItem { Name = "คำขออนุมัติยกเลิกสมาชิก", HasUrl = true, Url = _urlGeneratorService.GetApproveCancelMemberPageUrl() });
                        break;
                    case PageTypeId.ApproveUpdateMemberPage:
                        bc.Add(new BreadcrumbItem { Name = "อนุมัติการเปลี่ยนแปลงข้อมูลสมาชิก", HasUrl = true, Url = _urlGeneratorService.GetApproveUpdateMemberPageUrl() });
                        break;
                }
            }
            else if (pageTypeId == PageTypeId.ApproveRedemptionPage || pageTypeId == PageTypeId.ApproveUpdatePointPage || pageTypeId == PageTypeId.ApproveUpgradeLevelPage)
            {
                bc.Add(new BreadcrumbItem { Name = "เกี่ยวกับคะแนน" });
                switch (pageTypeId)
                {
                    case PageTypeId.ApproveRedemptionPage:
                        bc.Add(new BreadcrumbItem { Name = "คำขออนุมัติแลกสินค้า", HasUrl = true, Url = _urlGeneratorService.GetApproveRedemptionPageUrl() });
                        break;
                    case PageTypeId.ApproveUpdatePointPage:
                        bc.Add(new BreadcrumbItem { Name = "คำขออนุมัติเปลี่ยนคะแนน", HasUrl = true, Url = _urlGeneratorService.GetApproveUpdatePointPageUrl() });
                        break;
                    case PageTypeId.ApproveUpgradeLevelPage:
                        bc.Add(new BreadcrumbItem { Name = "คำขออนุมัติเลื่อนระดับ", HasUrl = true, Url = _urlGeneratorService.GetApproveUpgradeLevelPageUrl() });
                        break;
                }
            }
            return bc;

        }

        protected override IList<SidebarItem> CreateSidebar(IContextProvider contextProvider)
        {
            var pageTypeId = (PageTypeId)contextProvider.GetPageTypeId();

            var sb = new List<SidebarItem>();
            sb.Add(new SidebarItem
            {
                IsHeader = true,
                Name = "คำขออนุมัติเกี่ยวกับสมาชิก",
                IsActive = false,
                MenuId = 1,
                IconImageUrl = "/src/images/approve_page/approve_member.png"
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "สมาชิกใหม่",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveNewMemberPage),
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetApproveNewMemberPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การเปลี่ยนข้อมูลสมาชิก",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveUpdateMemberPage),
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetApproveUpdateMemberPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การบล็อคสมาชิก",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveBlockMemberPage),
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetApproveBlockMemberPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การยกเลิกสมาชิก",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveCancelMemberPage),
                ReferMenuId = 1,
                Url = _urlGeneratorService.GetApproveCancelMemberPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = true,
                Name = "คำขออนุมัติเกี่ยวกับคะแนน",
                IsActive = false,
                MenuId = 2,
                IconImageUrl = "/src/images/approve_page/approve_point.png"
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การเปลี่ยนคะแนน",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveUpdatePointPage),
                ReferMenuId = 2,
                Url = _urlGeneratorService.GetApproveUpdatePointPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การใช้แลกสินค้า",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveRedemptionPage),
                ReferMenuId = 2,
                Url = _urlGeneratorService.GetApproveRedemptionPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "การใช้เลื่อนระดับ",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveUpgradeLevelPage),
                ReferMenuId = 2,
                Url = _urlGeneratorService.GetApproveUpgradeLevelPageUrl()
            });
            sb.Add(new SidebarItem
            {
                IsHeader = true,
                Name = "การตั้งค่าการอนุมัติ",
                MenuId = 3,
                IconImageUrl = "/src/images/approve_page/settings.png"
            });
            sb.Add(new SidebarItem
            {
                IsHeader = false,
                Name = "ตั้งค่าการอนุมัติ",
                IsActive = IsActive(pageTypeId, PageTypeId.ApproveAutoSetting),
                ReferMenuId = 3,
                Url = _urlGeneratorService.GetApproveAutoSettingPageUrl()
            });


            return sb;
        }


        private UpdateResult UpdateMember(int approveId, int personId, string json, string updatedBy, EmployeeInfo employee, Person person)
        {
            var obj = JsonConvert.DeserializeObject<ApproveMemberDetail>(json);
            UpdateMember type = obj.Type;
            switch (type)
            {
                case model.Models.Enum.UpdateMember.ADDRESS:
                    {
                        PersonAddressGroup add = obj.AddressGroup;
                        _personAddressRepository.UpdatePersonAddress(add.CurrentAddress);
                        _personAddressRepository.UpdatePersonAddress(add.InvoiceAddress);
                        _personAddressRepository.UpdatePersonAddress(add.ShipmentAddress);
                        break;
                    }
                case model.Models.Enum.UpdateMember.BASICINFO:
                    {
                        Person updatedPerson = obj.Person;
                        _memberRepository.UpdateUser(updatedPerson);
                        break;
                    }
                case model.Models.Enum.UpdateMember.QUESTION:
                    {
                        List<PersonQuestion> questions = obj.Questions;
                        foreach (var question in questions)
                        {
                            _personQuestionRepository.UpdatePersonQuestion(question);
                        }
                        break;
                    }
                case model.Models.Enum.UpdateMember.SECURITY:
                    {
                        //Log
                        _logHelper.Log(() =>
                            {
                                string newPassword = obj.Password.ToString();
                                _memberRepository.UpdateUserPassword(personId, newPassword, updatedBy);
                            },
                            new AuditLog
                            {
                                AuditActionType = AuditActionType.CHANGE_PASSWORD,
                                ObjectId = personId,
                                Source = Source.MEMBERSHIP_WEBSITE,
                                RecStatus = RecStatus.ACTIVE,
                                OldValue = "Change password",
                                NewValue = "",
                                Description = JsonConvert.SerializeObject(
                                    new
                                    {
                                        PersonId = personId,
                                        Name = person.FullName,
                                        Source = employee
                                    }),
                                RecCreatedBy = updatedBy
                            });
                        break;
                    }

            }

            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);
            return new UpdateResult(true);

        }

        private UpdateResult UpdateProductRedemptionAndCutPoints(Approve approve, int approveId, string updatedBy, EmployeeInfo employee)
        {
            var transaction = JsonConvert.DeserializeObject<RedeemPointTransaction>(approve.ObjectValue);
            //check member point is enough to cut
            var usedsPoints = transaction.Type == RedeemPointsType.POINT_AND_PAID ?
                transaction.RedeemPoint.RequiredPoint : transaction.RedeemPoint.RequiredPointOnly;

            if (!ValidateMemberPointIsEnough(transaction.PersonId, usedsPoints))
            {
                return new UpdateResult { ValidationMessage = "คะแนนสะสมไม่เพียงพอ" };
            }

            var product = _lookupRepository.GetProduct(transaction.RedeemPoint.ProductCode);
            var memberPoint = new MemberPoint
            {
                PersonId = transaction.PersonId,
                Point = usedsPoints * -1,
                RecCreatedBy = transaction.RecCreatedBy.EmployeeCode,
                RecStatus = RecStatus.ACTIVE,
                ProductCode = transaction.RedeemPoint.ProductCode,
                Type = UpdatePointType.REDEMPTION
            };

            //Log
            _logHelper.Log(() =>
            {
                _pointRepository.InsertRedeemPointTransaction(transaction);
            },
                new AuditLog
                {
                    AuditActionType = AuditActionType.REWARD_REDEMPTION,
                    ObjectId = transaction.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    OldValue = "Redemption",
                    NewValue = transaction,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = transaction.PersonId,
                            Product = $"{transaction.RedeemPoint.ProductCode} {product?.ProductName}",
                            Money = transaction.Type == RedeemPointsType.POINT_ONLY ? 0 : transaction.RedeemPoint.RequiredMoney,
                            Point = transaction.Type == RedeemPointsType.POINT_ONLY ? transaction.RedeemPoint.RequiredPointOnly : transaction.RedeemPoint.RequiredPoint,
                            Source = transaction.RecCreatedBy
                        }),
                    RecCreatedBy = updatedBy
                });

            //Log cut member point
            _logHelper.Log(() =>
                {
                    _pointRepository.InsertMemberPoint(memberPoint);
                },
                new AuditLog
                {
                    AuditActionType = AuditActionType.MEMBERSHIP_POINT_UPDATE,
                    ObjectId = memberPoint.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    NewValue = memberPoint.Point,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = memberPoint.PersonId,
                            UpdatePoint = memberPoint.Point,
                            RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(memberPoint.PersonId, false).ToList()),
                            Name = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE).FullName,
                            Remark = EnumHelper.GetEnumDescription(memberPoint.Type),
                            Source = employee
                        }),
                    RecCreatedBy = updatedBy
                });


            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);


            return new UpdateResult(true);
        }

        private UpdateResult UpdateMemberPointLevelAndCutPoints(Approve approve, int approveId, string updatedBy,
            EmployeeInfo employee)
        {
            var personId = approve.ObjectId;
            var currentPointLevel = _pointRepository.GetMemberPointLevel(personId);
            var person = _memberRepository.GetMember(approve.ObjectId, Source.MEMBERSHIP_WEBSITE);
            //Update memberpoint  / update point level
            //Validate point is enough
            var mb = JsonConvert.DeserializeObject<MemberPointLevel>(approve.ObjectValue);
            if (!ValidateMemberPointIsEnough(personId, mb.Point))
            {
                return new UpdateResult { ValidationMessage = "ไม่สามารถอนุมัติเพราะคะแนนไม่เพียงพอ" };
            }

            //Log
            _logHelper.Log(() =>
            {
                //Check auto approve
                _pointRepository.UpsertMemberPointLevel(mb.PersonId, mb.PointLevelId);

            },
           new AuditLog
           {
               AuditActionType = AuditActionType.MEMBERSHIP_LEVEL_UPGRADE_APPROVED,
               ObjectId = personId,
               Source = Source.MEMBERSHIP_WEBSITE,
               RecStatus = RecStatus.ACTIVE,
               OldValue = currentPointLevel.PointLevelId,
               NewValue = mb.PointLevelId,
               Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = personId,
                            Level = mb.LevelName,
                            LevelId = mb.PointLevelId,
                            Name = person.FullName,
                            Source = employee
                        }),
               RecCreatedBy = updatedBy

           });

            //Log cut member point
            var memberPoint = new MemberPoint
            {
                PersonId = personId,
                Point = mb.Point * -1,
                RecCreatedBy = updatedBy,
                RecStatus = RecStatus.ACTIVE,
                Type = UpdatePointType.UPGRADE
            };
            _logHelper.Log(() =>
                {
                    _pointRepository.InsertMemberPoint(memberPoint);
                },
                new AuditLog
                {
                    AuditActionType = AuditActionType.MEMBERSHIP_POINT_UPDATE,
                    ObjectId = personId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    NewValue = memberPoint.Point,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = memberPoint.PersonId,
                            UpdatePoint = memberPoint.Point,
                            RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(memberPoint.PersonId, false).ToList()),
                            Name = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE).FullName,
                            Remark = EnumHelper.GetEnumDescription(memberPoint.Type),
                            Source = employee
                        }),
                    RecCreatedBy = updatedBy
                });


            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);
            return new UpdateResult(true);
        }

        private UpdateResult UpdateMemberPoint(Approve approve, int approveId, EmployeeInfo employee, string updatedBy)
        {
            var memberPointId = approve.ObjectId;
            var memberPoint = _pointRepository.GetMemberPoint(memberPointId);

            if (memberPoint.Point < 0 && !ValidateMemberPointIsEnough(memberPoint.PersonId, memberPoint.Point))
            {
                return new UpdateResult { ValidationMessage = "ไม่สามารถอนุมัติเพราะคะแนนไม่เพียงพอ" };
            }

            //Log
            _logHelper.Log(() =>
                {
                    _pointRepository.UpdateMemberPointStatus(memberPointId, RecStatus.ACTIVE, updatedBy);
                },
                new AuditLog
                {
                    AuditActionType = AuditActionType.MEMBERSHIP_POINT_UPDATE,
                    ObjectId = memberPoint.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    NewValue = memberPoint.Point,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = memberPoint.PersonId,
                            UpdatePoint = memberPoint.Point,
                            RamainPoint = MemberPointHelper.GetActivePoints(_pointRepository.GetMemberPoints(memberPoint.PersonId, false).ToList()),
                            Name = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE).FullName,
                            Remark = EnumHelper.GetEnumDescription(memberPoint.Type),
                            Source = employee
                        }),
                    RecCreatedBy = memberPoint.RecCreatedBy
                });
            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);


            return new UpdateResult(true);
        }

        private UpdateResult NewMember(Approve approve, int approveId, EmployeeInfo employee, string updatedBy)
        {
            var person = _memberRepository.GetMember(approve.ObjectId, Source.MEMBERSHIP_WEBSITE);

            //Log before insert person
            _logHelper.Log(actionMethod: () =>
                {

                    _memberRepository.UpdateUserStatus(approve.ObjectId, RecStatus.ACTIVE, updatedBy);
                },
                auditLog: new AuditLog
                {
                    AuditActionType = AuditActionType.NEW_MEMBERSHIP_APPROVED,
                    ObjectId = person.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    OldValue = "Approve new person",
                    NewValue = person,
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            Name = person.FullName,
                            Source = employee
                        }),
                    RecCreatedBy = updatedBy
                });
            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);

            return new UpdateResult(true);
        }

        private UpdateResult BlockMember(Approve approve, int approveId, EmployeeInfo employee, string updatedBy)
        {
            //Log
            var person = _memberRepository.GetMember(approve.ObjectId, Source.MEMBERSHIP_WEBSITE);
            _logHelper.Log(() =>
                     {
                         _memberRepository.UpdateUserStatus(approve.ObjectId, RecStatus.INACTIVE, updatedBy);
                     },
                     new AuditLog
                     {
                         AuditActionType = AuditActionType.BLOCK_MEMBERSHIP_APPROVED,
                         ObjectId = person.PersonId,
                         Source = Source.MEMBERSHIP_WEBSITE,
                         RecStatus = RecStatus.ACTIVE,
                         OldValue = EnumHelper.GetEnumDescription(person.RecStatus),
                         NewValue = EnumHelper.GetEnumDescription(RecStatus.INACTIVE),
                         Description = JsonConvert.SerializeObject(
                             new
                             {
                                 PersonId = person.PersonId,
                                 Name = person.FullName,
                                 RecStatus = EnumHelper.GetEnumDescription(RecStatus.INACTIVE),
                                 Source = employee
                             }),
                         RecCreatedBy = updatedBy
                     });
            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);

            return new UpdateResult(true);
        }

        private UpdateResult CancelMember(Approve approve, int approveId, EmployeeInfo employee, string updatedBy)
        {
            //Log
            var person = _memberRepository.GetMember(approve.ObjectId, Source.MEMBERSHIP_WEBSITE);
            _logHelper.Log(() =>
                {
                    _memberRepository.UpdateUserStatus(approve.ObjectId, RecStatus.DELETE, updatedBy);
                },
                new AuditLog
                {
                    AuditActionType = AuditActionType.CANCEL_MEMBERSHIP_APPROVED,
                    ObjectId = person.PersonId,
                    Source = Source.MEMBERSHIP_WEBSITE,
                    RecStatus = RecStatus.ACTIVE,
                    OldValue = EnumHelper.GetEnumDescription(person.RecStatus),
                    NewValue = EnumHelper.GetEnumDescription(RecStatus.DELETE),
                    Description = JsonConvert.SerializeObject(
                        new
                        {
                            PersonId = person.PersonId,
                            Name = person.FullName,
                            RecStatus = EnumHelper.GetEnumDescription(RecStatus.DELETE),
                            Source = employee
                        }),
                    RecCreatedBy = updatedBy
                });
            _approveRepository.UpdateAppoveStatus(approveId, updatedBy, RecStatus.ACTIVE);

            return new UpdateResult(true);
        }
        private bool ValidateMemberPointIsEnough(int personId, int usedsPoints)
        {
            var currentActivePoints = _pointRepository.GetMemberPoints(personId, false).ToList();
            return MemberPointHelper.ValidateMemberPointIsEnough(currentActivePoints, usedsPoints);
        }

        private void SendMail<T>(string receipient, string subject, string template, UpdateResult updareResult, T model)
        {
            if (updareResult.IsValid && !string.IsNullOrEmpty(receipient))
            {
                EmailService.Instance.SendMail("test@test.com", receipient, subject, template, model);
            }
        }
    }
}