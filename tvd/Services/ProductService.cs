﻿using System;
using System.Linq;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Enum;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.model.V;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class ProductService : IProductService
    {
        private readonly IPointRepository _pointRepository;
        private readonly ILookupRepository _lookupRepository;

        public ProductService(IPointRepository pointRepository, ILookupRepository lookupRepository)
        {
            _pointRepository = pointRepository;
            _lookupRepository = lookupRepository;
        }
        public ResultSet<ProductInfo> GetProductInfos(string code, int? pageSize, int? pageNumber)
        {
            return _pointRepository.GetProductInfos(code, pageSize, pageNumber);
        }

        public ResultSet<ProductInfo> GetProductInfosWithBasicPoints(string code, int? pageSize, int? pageNumber)
        {
            var products = GetProductInfos(code, pageSize, pageNumber);
            //Calculate point from unit price
            var basicPointConfig = _lookupRepository.GetPointLevels().ToList();

            if (basicPointConfig.Any())
            {
                var regularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var silverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var goldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var platinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();

                var productInfos = products.ToList().Select(p =>
                {
                    p.RegularPoint = Convert.ToInt32(double.Parse(p.UnitPrice) / (regularPoint * 1.0));
                    p.SilverPoint = Convert.ToInt32(double.Parse(p.UnitPrice) / (silverPoint * 1.0));
                    p.GoldPoint = Convert.ToInt32(double.Parse(p.UnitPrice) / (goldPoint * 1.0));
                    p.PlatinumPoint = Convert.ToInt32(double.Parse(p.UnitPrice) / (platinumPoint * 1.0));
                    return p;
                });

                return new ResultSet<ProductInfo>(productInfos, products.TotalItems);
            }
            return products;
        }
    }
}

/*
using System;
using System.Collections.Generic;
using System.Linq;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.Enum;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;
using tvd.model.V;
using tvd.Services.Interfaces;

namespace tvd.Services
{
    public class ProductService : IProductService
    {
        private readonly IPointRepository _pointRepository;
        private readonly ILookupRepository _lookupRepository;

        public ProductService(IPointRepository pointRepository, ILookupRepository lookupRepository)
        {
            _pointRepository = pointRepository;
            _lookupRepository = lookupRepository;
        }
        public ResultSet<ProductInfo> GetProductInfos(string code, int? pageSize, int? pageNumber)
        {
            return _pointRepository.GetProductInfos(code, pageSize, pageNumber);
        }

        public ResultSet<ProductInfo> GetProductInfosWithBasicPoints(string code, int? pageSize, int? pageNumber)
         {
            var products = GetProductInfos(code, pageSize, pageNumber);
            //Calculate point from unit price
            var basicPointConfig = _lookupRepository.GetPointLevels().ToList();

            if (basicPointConfig.Any())
            {
                var regularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var silverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var goldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
                var platinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();

                var productInfos = products.ToList().Select(p =>
                {
                    p.RegularPoint = Convert.ToInt32(p.UnitPrice / regularPoint);
                    p.SilverPoint = Convert.ToInt32(p.UnitPrice / silverPoint);
                    p.GoldPoint = Convert.ToInt32(p.UnitPrice / goldPoint);
                    p.PlatinumPoint = Convert.ToInt32(p.UnitPrice / platinumPoint);
                    return p;
                });

                return new ResultSet<ProductInfo>(productInfos, products.TotalItems);
            }
            return products;
        }
    }
}

*/
