﻿using System.Collections.Generic;
using tvd.model.Helpers;
using tvd.model.Provider;
using tvd.Models;

namespace tvd.Services
{
    public abstract class BaseService
    {
        protected abstract IList<BreadcrumbItem> CreateBreadcrumb(IContextProvider contextProvider, int? id);
        protected abstract IList<SidebarItem> CreateSidebar(IContextProvider contextProvider);

        protected bool IsActive(PageTypeId pageTypeId1, PageTypeId pageTypeId2)
        {
            return (pageTypeId1 == pageTypeId2);
        }

        protected bool IsActive(PageTypeId currentPageType, List<PageTypeId> referPages)
        {
            return referPages.Contains(currentPageType);
        }

        protected string GetSubPageTitle(PageTypeId currentPageTypeId)
        {
            switch (currentPageTypeId)
            {
                case PageTypeId.NewsCreatePage: return "เพิ่มข่าวสาร";
                case PageTypeId.NewsEditPage: return "แก้ไขข่าวสาร";
                case PageTypeId.ApproveAutoSetting: return "ตั้งค่าการอนุมัติ";
                case PageTypeId.PointRewardPage: return "คะแนนแลกของกำนัล";
                case PageTypeId.PointLevelPage: return "คะแนนเลื่อนขั้น";
                case PageTypeId.PointBasicLevelPage: return "ตั้งค่าคะแนนแบบพื้นฐาน";
                case PageTypeId.PointRulePage: return "ตั้งกฏการคำนวณ";
                case PageTypeId.PointBasicPage: return "คะแนนพื้นฐาน";
                case PageTypeId.PointPromotionPage: return "สำหรับ Promotion";
                case PageTypeId.PointPromotionDetailPage: return "Promotion คะแนน";
                case PageTypeId.PointCampaignPage: return "สำหรับ Campaign";
                case PageTypeId.PointCampaignDetailPage: return "Campaign คะแนน";
                case PageTypeId.PointGamificationPage: return "คะแนน Gamification";
                case PageTypeId.ApproveNewMemberPage: return "อนุมัติสมาชิกใหม่";
                case PageTypeId.ApproveUpdateMemberPage: return "อนุมัติการเปลี่ยนแปลงข้อมูลสมาชิก";
                case PageTypeId.ApproveBlockMemberPage: return "คำขออนุมัติบล็อคสมาชิก";
                case PageTypeId.ApproveCancelMemberPage: return "คำขออนุมัติยกเลิกสมาชิก";
                case PageTypeId.ApproveUpdatePointPage: return "คำขออนุมัติเปลี่ยนคะแนน";
                case PageTypeId.ApproveRedemptionPage: return "คำขออนุมัติแลกสินค้า";
                case PageTypeId.ApproveUpgradeLevelPage: return "คำขออนุมัติเลื่อนระดับ";
                case PageTypeId.ReportNewMember: return "รายงานการลงทะเบียนสมาชิก";
                case PageTypeId.ReportBlockMember: return "รายงานการบล็อคสมาชิก";
                case PageTypeId.ReportCancelMember: return "รายงานการยกเลิกสมาชิก";
                case PageTypeId.ReportUpgradeMember: return "รายงานการเลื่อนระดับสมาชิก";
				case PageTypeId.UploadMemberPoint: return "ปรับปรุงคะแนนสมาชิก";
                default: return EnumHelper.GetEnumDescription(currentPageTypeId);
            }
        }
    }
}