﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Repositories;

namespace tvd.Services
{
    public class PersonQuestionService : IPersonQuestionService
    {
        private readonly IPersonQuestionRepository _personQuestionRepository;
        public PersonQuestionService(IPersonQuestionRepository personQuestionRepository)
        {
            _personQuestionRepository = personQuestionRepository;
        }
        public void UpdatePersonQuestions(List<PersonQuestion> questions)
        {
            foreach (var question in questions)
            {
                _personQuestionRepository.UpdatePersonQuestion(question);

            }
        }
    }
}