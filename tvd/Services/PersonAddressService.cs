﻿using System.Collections.Generic;
using System.Linq;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.Enum;
using tvd.model.Repositories;

namespace tvd.Services
{
    public class PersonAddressService : IPersonAddressService
    {
        private readonly IPersonAddressRepository _addressRepository;

        public PersonAddressService(IPersonAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        public PersonAddressGroup GetPersonAddressGroup(int personId)
        {
            //initial person address group
            var addressGroup = new PersonAddressGroup();
            //get active address
            var allAddress = _addressRepository.GetPersonAddress(personId).ToList();
            var currentAddress =
                allAddress.FirstOrDefault(o => o.AddressTypeId == AddressType.CURRENT &&
                                               o.RecStatus == RecStatus.ACTIVE);
            addressGroup.CurrentAddress = currentAddress;

            //get shipping address
            var shippingAddress = allAddress.Where(o => o.AddressTypeId == AddressType.SHIPMENT)
                .OrderByDescending(o => (int)o.RecStatus).FirstOrDefault();
            addressGroup.ShipmentAddress = shippingAddress;


            var invocieAddress = allAddress.Where(o => o.AddressTypeId == AddressType.INVOICE)
                .OrderByDescending(o => (int)o.RecStatus).FirstOrDefault();
            addressGroup.InvoiceAddress = invocieAddress;
            return addressGroup;
        }

        public void AddPersonAddressGroup(PersonAddressGroup addressGroup)
        {
            //Create address
            var currentAddress = addressGroup.CurrentAddress;
            _addressRepository.InsertPersonAddress(currentAddress);

            //Create shipment address
            var shipmentAddress = addressGroup.ShipmentAddress;
            _addressRepository.InsertPersonAddress(shipmentAddress);


            //Create invoice address
            var invoiceAddress = addressGroup.InvoiceAddress;
            _addressRepository.InsertPersonAddress(invoiceAddress);

        }

        public void UpdatePersonAddress(PersonAddressGroup addressGroup)
        {
            //Create address
            var currentAddress = addressGroup.CurrentAddress;
            if (currentAddress != null)
            {
                _addressRepository.UpdatePersonAddress(currentAddress);
            }
            //Create shipment address
            var shipmentAddress = addressGroup.ShipmentAddress;
            if (shipmentAddress != null)
            {
                _addressRepository.UpdatePersonAddress(shipmentAddress);
            }

            //Create invoice address
            var invoiceAddress = addressGroup.InvoiceAddress;
            if (invoiceAddress != null)
            {
                _addressRepository.UpdatePersonAddress(invoiceAddress);
            }
        }
    }
}