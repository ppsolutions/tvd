﻿
using System;
using System.Net.Mail;
using System.Web.Hosting;
using FluentEmail;

namespace tvd.Services
{
    public class EmailService
    {
        private static EmailService instance;

        private EmailService() { }

        public static EmailService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailService();
                }
                return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender">email's sender</param>
        /// <param name="receipient">email's receipient</param>
        /// <param name="subject">subject email</param>
        /// <param name="templateName">razor view name</param>
        /// <param name="model">view model to used in razor view</param>
        /// <returns></returns>
        public void SendMail<T>(string sender, string receipient, string subject, string templateName, T model)
        {
            var smtp = new SmtpClient();
            smtp.EnableSsl = smtp.DeliveryMethod == SmtpDeliveryMethod.Network;
            
            var templateDir  = HostingEnvironment.MapPath($"~/Views/EmailTemplate/{templateName}.cshtml");

            var email = new Email(smtp, new RazorRenderer(), sender);
            email.To(receipient);
            email.Subject(subject);
            email.UsingTemplateFromFile(templateDir, model);

            //var email = Email
            //    .From(sender)
            //    .To(receipient)
            //    .Subject(subject)
            //    .UsingTemplateFromFile(templateDir, model);

            email.SendAsync(((o, args) => Console.WriteLine("Send success")));
        }

        public void SendMail(string sender, string receipient, string subject, string body)
        {
            var smtp = new SmtpClient();
            smtp.EnableSsl = smtp.DeliveryMethod == SmtpDeliveryMethod.Network;

            var email = new Email(smtp, new RazorRenderer(), sender);
            email.To(receipient);
            email.Subject(subject);
            email.Body(body);

            //var email = Email
            //    .From(sender)
            //    .To(receipient)
            //    .Subject(subject)
            //    .UsingTemplateFromFile(templateDir, model);

            email.SendAsync(((o, args) => Console.WriteLine("Send success")));
        }
    }
}