﻿using System.Web.Mvc;

namespace tvd.Security
{
    public abstract class UserPermission : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }
}