﻿using tvd.Models;

namespace tvd.Http
{
    public interface IRequestInfo
    {
        string Domain { get; }
        PageTypeId PageTypeId { get; }
        string TargetUrl { get; }
    }
}
