﻿using System;
using System.Web;
using System.Web.Mvc;
using tvd.Http.Repository;

namespace tvd.Http
{
    public class MappingHttpModule
    {
        public void OnBeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            if (application == null)
                return;

            var httpRepository = DependencyResolver.Current.GetService<IHttpRepository>();
            var contextBase = new HttpContextWrapper(application.Context);
            var requestInfo = httpRepository.FetchRequestInfo(contextBase);
            if(requestInfo != null)
            {
                contextBase.Items["IRequestInfo"] = requestInfo;
            }
        }
    }
}