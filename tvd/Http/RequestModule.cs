﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using tvd.model.Provider;
using tvd.urlmapping.Configuration;
using tvd.urlmapping.Rules;

namespace tvd.Http
{
    public class RequestModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            var mappingHttpModule = new MappingHttpModule();

            context.BeginRequest += OnBeginRequest;
            context.BeginRequest += mappingHttpModule.OnBeginRequest;
            context.BeginRequest += RewriteUrl;
        }

        void OnBeginRequest(object sender, EventArgs e)
        {
            var contextProvider = DependencyResolver.Current.GetService<IContextProvider>();
            var contextBase = contextProvider.GetContext();
            var request = contextBase.Request;
            if (!request.IsAjaxRequest())
            {
                var path = request.Url.AbsolutePath.ToLower();
                var rawUrl = request.RawUrl.ToLower();
                if (path.EndsWith("/")
                    && rawUrl.Contains(""))
                {
                    contextBase.Response.Redirect("/login");
                }
            }
        }

        private IRequestInfo GetRequestInfo(HttpContextBase contextBase)
        {
            return contextBase.Items["IRequestInfo"] as IRequestInfo ?? null;
        }

        void RewriteUrl(object sender, EventArgs eventArgs)
        {
            var application = sender as HttpApplication;
            if (application == null)
                return;

            var contextBase = new HttpContextWrapper(application.Context);
            var requestInfo = GetRequestInfo(contextBase);
            var requestPath = contextBase.Request.Url.AbsolutePath.ToLower();
            var mappingTypeId = contextBase.Items["pageTypeId"] as int? ?? 0;
            if (requestInfo != null &&
                mappingTypeId > 0 &&
                !contextBase.Request.IsAjaxRequest())
            {
                var urlRules = RewriteRulesConfigurationSection.Configuration;
                var providers = new Dictionary<string, RequestRuleBase>();
                foreach (RewriteRule rule in urlRules.Rules)
                {
                    var selectedProvider = SelectProvider(rule, mappingTypeId);

                    if (string.IsNullOrEmpty(rule.Expression) ||
                        Regex.IsMatch(requestPath, rule.Expression))
                    {
                        if(!providers.ContainsKey(selectedProvider))
                            providers.Add(selectedProvider, CreateProvider(selectedProvider, new object[] { contextBase }));
                    }
                    if (providers.Any())
                    {
                        var requestRule = providers[selectedProvider];
                        if (requestRule != null)
                        {
                            var result = requestRule.RedirectUrl();
                            if (result.Success)
                            {
                                switch (rule.RewriteAction)
                                {
                                    case RewriteAction.WritePath:
                                        contextBase.RewritePath(result.NewLocation, false);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private string SelectProvider(RewriteRule rule, int mappingTypeId)
        {
            return rule.Provider;
        }

        private RequestRuleBase CreateProvider(string type, object[] arguments)
        {
            try
            {
                var providerType = Type.GetType(type, true, false);
                return (RequestRuleBase)Activator.CreateInstance(providerType, arguments);
            }
            catch
            {
                return null;
            }
        }

        public void Dispose()
        {

        }
    }
}