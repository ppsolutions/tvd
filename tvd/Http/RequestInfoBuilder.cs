﻿using tvd.Models;

namespace tvd.Http
{
    public class RequestInfoBuilder
    {
        public IRequestInfo Build(string host, PageTypeId pageTypeId, string requestUrl)
        {
            var req = new RequestInfo(host, pageTypeId, requestUrl);

            return req;
        }
    }
}