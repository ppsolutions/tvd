﻿using tvd.Models;

namespace tvd.Http
{
    public class RequestInfo : IRequestInfo
    {
        public RequestInfo(string host, PageTypeId pageTypeId, string targetUrl)
        {
            this.Domain = host;
            this.PageTypeId = pageTypeId;
            this.TargetUrl = targetUrl;
        }

        public string Domain
        {
            get;
            private set;
        }

        public PageTypeId PageTypeId
        {
            get;
            private set;
        }

        public string TargetUrl
        {
            get;
            private set;
        }
    }
}