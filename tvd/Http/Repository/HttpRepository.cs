﻿using System.Linq;
using System.Web;
using tvd.Models;
using tvd.urlmapping.Configuration;

namespace tvd.Http.Repository
{
    public class HttpRepository : IHttpRepository
    {
        public IRequestInfo FetchRequestInfo(HttpContextBase contextBase)
        {
            var host = this.GetHost(contextBase);
            var pageTypeId = this.GetPageTypeId(contextBase);
            var requestTargetUrl = this.GetTargetUrl(contextBase);
            var builder = new RequestInfoBuilder();

            return builder.Build(host, pageTypeId, requestTargetUrl);
        }

        private string GetHost(HttpContextBase context)
        {
            string request = context.Request.Url.AbsolutePath.ToLower();

            var host = context.Request.Url.DnsSafeHost;

            return host;
        }

        private PageTypeId GetPageTypeId(HttpContextBase context)
        {
            var requestAbsolutePath = context.Request.Url.AbsolutePath.ToLower();
            var pageTypeId = PageTypeId.Unknow;
            var config = RewriteRulesConfigurationSection.Configuration.UrlMapping;
            var rules = config.UrlMappings.Cast<UrlMapping>();
            var direct = rules.FirstOrDefault(a => string.Compare(a.PageLocation, requestAbsolutePath, true) == 0);
            if (direct != null)
            {
                pageTypeId = (PageTypeId)direct.MappingType;                     
                if (context.Items["pageTypeId"] == null ||
                    context.Items["pageTypeId"].ToString() == "0")
                {
                    context.Items["pageTypeId"] = direct.MappingType;
                }
            }
            return pageTypeId;
        }

        private string GetTargetUrl(HttpContextBase context)
        {
            var requestAbsolutePath = context.Request.Url.AbsolutePath.ToLower();

            var config = RewriteRulesConfigurationSection.Configuration.UrlMapping;
            var rules = config.UrlMappings.Cast<UrlMapping>();
            var direct = rules.FirstOrDefault(a => string.Compare(a.PageLocation, requestAbsolutePath, true) == 0);
            if (direct != null)
            {
                if (context.Items["requestTargetUrl"] == null)
                {
                    context.Items["requestTargetUrl"] = string.IsNullOrEmpty(direct.RequestTarget) ? config.DefaultLocation : direct.RequestTarget;
                }
                return direct.RequestTarget;
            }
            return null;
        }
    }
}