﻿using System.Web;

namespace tvd.Http.Repository
{
    public interface IHttpRepository
    {
        IRequestInfo FetchRequestInfo(HttpContextBase contextBase);
    }
}