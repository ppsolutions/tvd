﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tvd.model.Provider;
using tvd.Services.Interfaces;

namespace tvd.Http
{
    public class AuthorizationHandlerHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += PreRequestHandlerExecute;
        }

        protected void PreRequestHandlerExecute(object sender, EventArgs e)
        {
            var contextProvider = DependencyResolver.Current.GetService<IContextProvider>();
            var context = contextProvider.GetContext();

            if (IsValidRequest(context) && !context.Request.IsAjaxRequest())
            {
                SetUserContext(contextProvider.GetContext());
            }
        }

        protected bool IsValidRequest(HttpContextBase context)
        {
            // to prevent the module from being executed multiple times for extensionless URLs like 
            if (context.Handler == null ||
                context.Handler.GetType().Name.Contains("TransferRequestHandler") ||
                context.Session == null)
            {
                return false;
            }

            return true;
        }

        private void SetUserContext(HttpContextBase contextBase)
        {
            var authenCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            if (authenCookie != null)
            {
                if (contextBase.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
					var employeeService = DependencyResolver.Current.GetService<IEmployeeService>();
					var ticket = FormsAuthentication.Decrypt(contextBase.Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                    var employeeCode = JsonConvert.DeserializeObject<string>(ticket.UserData);
					var userInfo = employeeService.GetEmployee(employeeCode);
                    if (userInfo != null)
                    {
                        var employeeInRole = userInfo.EmployeeInRole;
                        if(employeeInRole != null)
                        {
                            var roles = employeeInRole.GetRoles();
                            contextBase.User = new GenericPrincipal(new FormsIdentity(ticket), roles);
                        }
                        contextBase.Items["UserInfo"] = userInfo;

                        if(!AllowAccess(contextBase, employeeService, employeeInRole.Role.RoleId))
                        {
                            contextBase.Response.Redirect("~/permission");
                        }
                    }
                }
            }
        }

        public void Dispose()
        {

        }

        private bool AllowAccess(HttpContextBase contextBase, IEmployeeService employeeService, int roleId)
        {
            var allow = false;
            var mappingTypeId = contextBase.Items["pageTypeId"] as int? ?? 0;
            var accessControlRoles = employeeService.GetAccessControlRole(roleId);
            if(accessControlRoles?.Where(a => a.PageTypeId == mappingTypeId).FirstOrDefault() != null 
                || contextBase.Request.RawUrl.Contains("permission")
                || contextBase.Request.RawUrl.ToLower().Contains("login"))
            {
                allow = true;
            }
            return allow;
        }
    }
}