﻿using tvd.Factory.Interfaces;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Request;

namespace tvd.Factory
{
    public class RoleFactory : IRoleFactory
    {
        public Role CreateRole(UpdateRoleRequest request)
        {
            return new Role
            {
                RoleId = request.RoleId,
                RecModifiedBy = request.UpdatedBy
            };
        }

        public RolePermission CreateRolePermission(AddRoleRequest request)
        {
            var rolePermission = new RolePermission
            {
                Role = new Role
                {
                    RoleName = request.RoleName,
                    RecCreatedBy = request.UpdatedBy
                },
                IsPermitInReportSystem = request.IsPermitInReportSystem,
                IsPermisInAuditReportSystem = request.IsPermisInAuditReportSystem,
                IsPermitInEmployeeManagementSystem = request.IsPermisInEmployeeManagementSystem,
                IsPermitInRoleSystem = request.IsPermisInRoleSystem,
                IsPermitInNewsSystem = request.IsPermitInNewsSystem,
                IsPermitInMemberRegistrationSystem = request.IsPermitInMemberRegistrationSystem,
                IsPermitInPointSettingSystem = request.IsPermitInPointSettingSystem,
                IsPermitInApproveSystem = request.IsPermitInApproveSystem,
                RecCreatedBy = request.UpdatedBy
            };

            return rolePermission;
        }

        public RolePermission CreateRolePermission(UpsertRolePermissionRequest request)
        {
            var rolePermission = new RolePermission
            {
                Role = new Role
                {
                    RoleId = request.RoleId.HasValue ? request.RoleId.Value : 0,
                    RoleName = request.RoleName
                },
                IsPermitInReportSystem = request.IsPermitInReportSystem,
                IsPermisInAuditReportSystem = request.IsPermisInAuditReportSystem,
                IsPermitInEmployeeManagementSystem = request.IsPermitInEmployeeManagementSystem,
                IsPermitInRoleSystem = request.IsPermitInRoleSystem,
                IsPermitInNewsSystem = request.IsPermitInNewsSystem,
                IsPermitInMemberRegistrationSystem = request.IsPermitInMemberRegistrationSystem,
                IsPermitInPointSettingSystem = request.IsPermitInPointSettingSystem,
                IsPermitInApproveSystem = request.IsPermitInApproveSystem,
                RecStatus = model.Models.Enum.RecStatus.ACTIVE
            };

            if (request.RoleId.HasValue)
            {
                rolePermission.Role.RecModifiedBy = request.UpdatedBy;
                rolePermission.RecModifiedBy = request.UpdatedBy;
            }
            else
            {
                rolePermission.Role.RecCreatedBy = request.UpdatedBy;
                rolePermission.RecCreatedBy = request.UpdatedBy;

            }

            return rolePermission;
        }
    }
}