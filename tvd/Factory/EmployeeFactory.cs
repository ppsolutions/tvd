﻿using System;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Employee;

namespace tvd.Factory
{
    public class EmployeeFactory : IEmployeeFactory
    {
        public Employee Create(EmployeeUpSertDto request)
        {
            if (!string.IsNullOrEmpty(request.EmployeeCode))
            {
                return CreateUpdateObject(request);
            }

            return CreateInsertObject(request);
        }

        private Employee CreateInsertObject(EmployeeUpSertDto request)
        {
            var emp = new Employee
            {
                EmployeeCode = request.EmployeeCode,
                //FirstName =  request.FirstName,
                //LastName = request.LastName,
                //ContactNumber = request.Phone,
                //EmailAddress = request.Email,
                EmployeeInRole = new RolePermission
                {
                    Role = new Role
                    {
                        RoleId = request.RoleId,
                        RecCreatedBy =  request.UpdatedBy
                    },
					RecCreatedBy = request.UpdatedBy
					
                }
            };
            return emp;
        }

        private Employee CreateUpdateObject(EmployeeUpSertDto request)
        {
            var emp = new Employee
            {
                EmployeeCode = request.EmployeeCode,
                //FirstName = request.FirstName,
                //LastName = request.LastName,
                //ContactNumber = request.Phone,
                //EmailAddress = request.Email,
                EmployeeInRole = new RolePermission
                {
                    Role = new Role
                    {
                        RoleId = request.RoleId,
                        RecModifiedBy = request.UpdatedBy
                    }

                }
            };
            return emp;
        }
    }
}