﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.Request;

namespace tvd.Factory.Interfaces
{
    public interface IApproveFactory
    {
        ApproveSetting Create(UpdateApproveSettingRequest request);
    }
}
