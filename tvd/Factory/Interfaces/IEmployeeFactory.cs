﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Employee;

namespace tvd.Factory.Interfaces
{
    public interface IEmployeeFactory
    {
        Employee Create(EmployeeUpSertDto request);
    }
}
