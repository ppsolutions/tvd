﻿using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Request;

namespace tvd.Factory.Interfaces
{
    public interface INewsFactory
    {
        News CreateNews(UpsertNewsRequest request);
    }
}
