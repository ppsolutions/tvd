﻿using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Request;

namespace tvd.Factory.Interfaces
{
    public interface IRoleFactory
    {
        Role CreateRole(UpdateRoleRequest request);

        RolePermission CreateRolePermission(AddRoleRequest request);

        RolePermission CreateRolePermission(UpsertRolePermissionRequest request);
    }
}
