﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.Factory.Interfaces
{
    public interface IRegisterFactory
    {
        Person CreatePerson(RegisterRequest request);
        PersonAddressGroup CreateAddressGroup(RegisterRequest request);
        List<PersonQuestion> CreatePersonQuestions(RegisterRequest request);
    }
}
