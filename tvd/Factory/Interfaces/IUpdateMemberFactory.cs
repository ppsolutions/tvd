﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Request;

namespace tvd.Factory.Interfaces
{
    public interface IUpdateMemberFactory
    {
        Person CreatePerson(UpdateMemberRequest request);
        PersonAddressGroup CreateAddressGroup(UpdateMemberRequest request);
        PersonAddressGroup CreateAddressGroup(UpdateAddressRequest request);
        List<PersonQuestion> CreatePersonQuestions(UpdateQuestionRequest request);
    }
}
