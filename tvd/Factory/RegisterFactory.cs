﻿using System;
using System.Collections.Generic;
using tvd.Factory.Interfaces;
using tvd.model.Models;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;

namespace tvd.Factory
{
	public class RegisterFactory : IRegisterFactory
	{
		public Person CreatePerson(RegisterRequest request)
		{
			var isLegalPerson = request.SelectedPerson;
			return isLegalPerson ? CreateLegalPerson(request) : CreateCompany(request);
		}



		public PersonAddressGroup CreateAddressGroup(RegisterRequest request)
		{
			var currentAdress = new PersonAddress
			{
				Address1 = request.CurrentAddress,
				Address2 = string.Empty,
				City = new City
				{ CityId = request.CurrentAddressProvince },
				PostalCode = request.CurrentAddressPostalCode,
				AddressTypeId = AddressType.CURRENT,
				RecCreatedBy = request.UpdatedBy,
				RecStatus = RecStatus.INACTIVE
			};

			var shipmentAddress = request.ShipmentUseCurrentAddress
				? CopyAddress(currentAdress, AddressType.SHIPMENT)
				: new PersonAddress
				{
					Address1 = request.ShipmentAddress,
					Address2 = string.Empty,
					City = new City
					{ CityId = request.ShipmentProvince.Value },
					PostalCode = request.ShipmentPostalCode,
					AddressTypeId = AddressType.SHIPMENT,
					RecCreatedBy = request.UpdatedBy

				};
			shipmentAddress.AddressTypeId = AddressType.SHIPMENT;

			PersonAddress invoiceAddress;
			if (request.InvoiceUseCurrentAddress)
			{
				invoiceAddress = CopyAddress(currentAdress, AddressType.INVOICE);
			}
			else if (request.InvoiceUseShipmentAddress)
			{
				invoiceAddress = CopyAddress(shipmentAddress, AddressType.INVOICE);
			}
			else
			{
				invoiceAddress = new PersonAddress
				{
					Address1 = request.InvoiceAddress,
					Address2 = string.Empty,
					City = new City
					{ CityId = request.InvoiceProvince.Value },
					PostalCode = request.InvoicePostalCode,
					AddressTypeId = AddressType.INVOICE,
					RecCreatedBy = request.UpdatedBy
				};
			}
			var addressGroup = new PersonAddressGroup
			{
				CurrentAddress = currentAdress,
				ShipmentAddress = shipmentAddress,
				InvoiceAddress = invoiceAddress

			};

			return addressGroup;
		}

		public List<PersonQuestion> CreatePersonQuestions(RegisterRequest request)
		{
			var questions = new List<PersonQuestion>();
			questions.Add(new PersonQuestion
			{
				Question = new Question
				{
					QuestionId = request.FirstQuestion
				},
				Answer = request.FirstAnswer,
				RecCreatedBy = request.UpdatedBy
			});

			if(request.SecondQuestion != 0)
			{
				questions.Add(new PersonQuestion
				{
					Question = new Question
					{
						QuestionId = request.SecondQuestion
					},
					Answer = request.SecondAnswer,
					RecCreatedBy = request.UpdatedBy
				});
			}

			return questions;
		}

		private Person CreateLegalPerson(RegisterRequest request)
		{
			var person = new Person
			{
				Source = Source.MEMBERSHIP_WEBSITE,
				IsLegalPerson = true,
				TitleId = request.PersonTitle ?? 0,
				FirstName = request.FirstName,
				LastName = request.LastName,
				BirthDate = ConvertDate(request.DOB),
				IsSpecificYear = request.IsSpecificYear,
				ContactNumber = request.Phone,
				UserName = request.Email,
				Password = request.Password,
				EmailAddress = request.Email,
				RecCreatedBy = request.UpdatedBy
			};
			return person;
		}

		private Person CreateCompany(RegisterRequest request)
		{
			var person = new Person
			{
				Source = Source.MEMBERSHIP_WEBSITE,
				IsLegalPerson = true,
				TitleId = request.PersonTitle ?? 0,
				FirstName = request.FirstName,
				LastName = request.LastName,
				BirthDate = ConvertDate(request.DOB),
				ContactNumber = request.Phone,
				UserName = request.Email,
				Password = request.Password,
				EmailAddress = request.Email,
				RecCreatedBy = request.UpdatedBy
			};
			return person;
		}

		private PersonAddress CopyAddress(PersonAddress add, AddressType overrideType)
		{
			var copyObj = new PersonAddress
			{
				Address1 = add.Address1,
				Address2 = add.Address2,
				City = new City
				{ CityId = add.City.CityId },
				PostalCode = add.PostalCode,
				AddressTypeId = overrideType,
				RecCreatedBy = add.RecCreatedBy
			};
			return copyObj;
		}

		private DateTime ConvertDate(string date)
		{
			string[] dates = date.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
			return new DateTime(Convert.ToInt32(dates[2]), Convert.ToInt32(dates[1]), Convert.ToInt32(dates[0]));
		}
	}
}