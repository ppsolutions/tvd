﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.Factory.Interfaces;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Models.Request;

namespace tvd.Factory
{
    public class UpdateMemberFactory : IUpdateMemberFactory
    {
        public Person CreatePerson(UpdateMemberRequest request)
        {
            var isLegalPerson = (RegisterType)request.RegisterType == RegisterType.Person;
			var person = new Person
			{
				PersonId = request.PersonId,
				IsLegalPerson = isLegalPerson,
				TitleId = isLegalPerson ? request.PersonTitle : request.CorporateTitle,
				FirstName = isLegalPerson ? request.FirstName : request.BusinessName,
				LastName = isLegalPerson ? request.LastName : "",
				BirthDate = ConvertDate(request.DateOfBirth),
				IsSpecificYear = request.IsSpecificYear,
                ContactNumber = request.ContactNumber,
                UserName = request.Email,
                EmailAddress = request.Email,
                RecModifiedBy = request.UpdatedBy,
				RecStatus = (RecStatus)request.RecStatus
            };
            return person;
        }

        public PersonAddressGroup CreateAddressGroup(UpdateMemberRequest request)
        {
            //PersonAddress shipping = null;
            //PersonAddress invoice = null;

            //if (request.ShipmentAddress != null)
            //{
            //    shipping = new PersonAddress
            //    {
            //        Address1 = request.ShipmentAddress.Address1,
            //        Address2 = request.ShipmentAddress.Address2,
            //        City = new City
            //            {CityId = request.ShipmentAddress.CityId},
            //        PostalCode = request.ShipmentAddress.PostalCode,
            //        AddressTypeId = AddressType.ShippingaddressType.Value

            //    };
            //}

            //if (request.InvoiceAddress != null)
            //{
            //    invoice = new PersonAddress
            //    {
            //        Address1 = request.InvoiceAddress.Address1,
            //        Address2 = request.InvoiceAddress.Address2,
            //        City = new City
            //            {CityId = request.InvoiceAddress.CityId},
            //        PostalCode = request.InvoiceAddress.PostalCode,
            //        AddressTypeId = AddressType.InvoiceAddressType.Value
            //    };
            //}
            //var addressGroup = new PersonAddressGroup
            //{
            //    ShipmentAddress = shipping,
            //    InvoiceAddress = invoice
            //};

            //return addressGroup;
            return null;
        }

        public PersonAddressGroup CreateAddressGroup(UpdateAddressRequest request)
        {
            var currentAdress = new PersonAddress
            {
				PersonAddressId = request.CurrentAddressId,
                Address1 = request.CurrentAddress,
                Address2 = string.Empty,
                City = new City
                    { CityId = request.CurrentAddressProvince },
                PostalCode = request.CurrentAddressPostalCode,
                AddressTypeId = AddressType.CURRENT,
                RecModifiedBy = request.UpdatedBy,
                RecStatus = RecStatus.ACTIVE,
                PersonId = request.PersonId
            };

            var shipmentAddress = request.ShipmentUseCurrentAddress
                ? CopyAddress(currentAdress, AddressType.SHIPMENT)
                : new PersonAddress
                {
					PersonAddressId = request.ShipmentAddressId,
                    Address1 = request.ShipmentAddress,
                    Address2 = string.Empty,
                    City = new City
                        { CityId = request.ShipmentProvince.Value },
                    PostalCode = request.ShipmentPostalCode,
                    AddressTypeId = AddressType.SHIPMENT,
                    RecModifiedBy = request.UpdatedBy,
                    PersonId = request.PersonId,
                    RecStatus = RecStatus.ACTIVE

                };
            shipmentAddress.AddressTypeId = AddressType.SHIPMENT;

            PersonAddress invoiceAddress;
            if (request.InvoiceUseCurrentAddress)
            {
                invoiceAddress = CopyAddress(currentAdress, AddressType.INVOICE);
            }
            else if (request.InvoiceUseShipmentAddress)
            {
                invoiceAddress = CopyAddress(shipmentAddress, AddressType.INVOICE);
            }
            else
            {
                invoiceAddress = new PersonAddress
                {
					PersonAddressId = request.InvoiceAddressId,
                    Address1 = request.InvoiceAddress,
                    Address2 = string.Empty,
                    City = new City
                        { CityId = request.InvoiceProvince.Value },
                    PostalCode = request.InvoicePostalCode,
                    AddressTypeId = AddressType.INVOICE,
                    RecModifiedBy = request.UpdatedBy,
                    PersonId = request.PersonId,
                    RecStatus = RecStatus.ACTIVE
                };
            }
            var addressGroup = new PersonAddressGroup
            {
                CurrentAddress = currentAdress,
                ShipmentAddress = shipmentAddress,
                InvoiceAddress = invoiceAddress
            };

            return addressGroup;
        }

        public List<PersonQuestion> CreatePersonQuestions(UpdateQuestionRequest request)
        {
            var question = new List<PersonQuestion>
            {
                new PersonQuestion
                {
                    Answer = request.FirstAnswer,
                    PersonId = request.PersonId,
                    PersonQuestionId = request.FirstPersonQuestionId,
                    Question = new Question {QuestionId = request.FirstQuestionId},
                    RecModifiedBy = request.UpdatedBy,
                    RecStatus = RecStatus.ACTIVE
                },
                new PersonQuestion
                {
                    Answer = request.SecondAnswer,
                    PersonId = request.PersonId,
                    PersonQuestionId = request.SecondPersonQuestionId,
                    Question = new Question {QuestionId = request.SecondQuestionId},
                    RecModifiedBy = request.UpdatedBy,
                    RecStatus = RecStatus.ACTIVE
                }
            };

            return question;
        }

        private PersonAddress CopyAddress(PersonAddress add, AddressType overrideType)
        {
            var copyObj = new PersonAddress
            {
                Address1 = add.Address1,
                Address2 = add.Address2,
                City = new City
                    { CityId = add.City.CityId },
                PostalCode = add.PostalCode,
                AddressTypeId = overrideType,
                RecModifiedBy = add.RecModifiedBy,
                PersonId = add.PersonId,
                RecStatus = add.RecStatus
            };
            return copyObj;
        }

		private DateTime ConvertDate(string date)
		{
			string[] dates = date.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
			return new DateTime(Convert.ToInt32(dates[0]), Convert.ToInt32(dates[1]), Convert.ToInt32(dates[2]));
		}
	}
}