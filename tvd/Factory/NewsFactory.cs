﻿using System;
using tvd.Factory.Interfaces;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;
using tvd.model.Models.Request;

namespace tvd.Factory
{
    public class NewsFactory : INewsFactory
    {

        public News CreateNews(UpsertNewsRequest request)
        {
            var upsertNews = new News{
                
                Title = request.Title,
                Content = request.Content,
                Type = request.NewsType,
                Status = (NewsStatus)request.Status,
                StartDate = DateTime.ParseExact(request.StartDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture),
                EndDate = DateTime.ParseExact(request.EndDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture),
            };

            if (request.NewsId.HasValue)
            {
                upsertNews.NewsId = request.NewsId.Value;
                upsertNews.Order = request.Order;
                upsertNews.RecModifiedBy = request.UpdatedBy;
            }
            else
            {
                upsertNews.RecCreatedBy = request.UpdatedBy;
            }

            return upsertNews;
        }
    }
}