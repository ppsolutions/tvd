﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tvd.Factory.Interfaces;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.Request;

namespace tvd.Factory
{
    public class ApproveFactory : IApproveFactory
    {
        public ApproveSetting Create(UpdateApproveSettingRequest request)
        {
            return new ApproveSetting
            {
                ApproveSettingId = request.ApproveSettingId,
                IsAutoApproveCancelMembership = request.IsAutoApproveCancelMembership,
                IsAutoApproveChangeMembershipInfo = request.IsAutoApproveChangeMembershipInfo,
                IsAutoApproveChangeMembershipPoint = request.IsAutoApproveChangeMembershipPoint,
                IsAutoApproveLevelUpMembership = request.IsAutoApproveLevelUpMembership,
                IsAutoApproveNewMembership = request.IsAutoApproveNewMembership,
                IsAutoApproveRewardRedeemMembership = request.IsAutoApproveRewardRedeemMembership,
                PointRetensionPeriod = request.PointRetensionPeriod,
                MembershipRetensionPeriod = request.MembershipRetensionPeriod,
                RecModifiedBy = request.UpdatedBy
            };
        }
    }
}