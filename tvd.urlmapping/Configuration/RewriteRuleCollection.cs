﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public class RewriteRuleCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RewriteRule();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var rule = (RewriteRule)element;

            return string.Concat(rule.Expression.GetHashCode(), "|", rule.Provider.GetHashCode());
        }

        public RewriteRule this[int index]
        {
            get { return (RewriteRule)BaseGet(index); }
        }
    }
}
