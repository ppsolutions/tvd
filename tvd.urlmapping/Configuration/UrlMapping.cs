﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public class UrlMapping : ConfigurationElement
    {
        [ConfigurationProperty("mappingType", IsKey = true, IsRequired = true)]
        public int MappingType
        {
            get { return (int)this["mappingType"]; }
        }

        [ConfigurationProperty("requestTarget", IsRequired = true)]
        public string RequestTarget
        {
            get { return (string)this["requestTarget"]; }
        }

        [ConfigurationProperty("requestParameters")]
        public string RequestParameters
        {
            get { return (string)this["requestParameters"]; }
        }

        [ConfigurationProperty("pageLocation")]
        public string PageLocation
        {
            get { return (string)this["pageLocation"]; }
        }
    }
}
