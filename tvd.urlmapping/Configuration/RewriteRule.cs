﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public sealed class RewriteRule : ConfigurationElement
    {
        [ConfigurationProperty("expression", IsKey = false, IsRequired = true)]
        public string Expression
        {
            get { return (string)this["expression"]; }
        }

        [ConfigurationProperty("provider", IsKey = false, IsRequired = true)]
        public string Provider
        {
            get { return (string)this["provider"]; }
        }

        [ConfigurationProperty("rewriteAction", IsKey = false, DefaultValue = RewriteAction.None)]
        public RewriteAction RewriteAction
        {
            get { return (RewriteAction)this["rewriteAction"]; }
        }

        [ConfigurationProperty("redirectTo", IsKey = false, DefaultValue = null, IsRequired = false)]
        public string RedirectTo
        {
            get { return (string)this["redirectTo"]; }
        }
    }
}
