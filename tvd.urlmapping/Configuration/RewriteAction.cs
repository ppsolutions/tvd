﻿namespace tvd.urlmapping.Configuration
{
    public enum RewriteAction
    {
        None = 0,

        Redirect = 1,

        WritePath = 2,

        WriteFile = 3,

        Write = 4,

        Moved = 5
    }
}
