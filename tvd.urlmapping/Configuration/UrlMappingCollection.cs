﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public class UrlMappingCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UrlMapping();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var urlMapping = (UrlMapping)element;
            return urlMapping.MappingType;
        }
    }
}
