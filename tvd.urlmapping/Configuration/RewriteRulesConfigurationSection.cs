﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public class RewriteRulesConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("rules", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(RewriteRuleCollection), AddItemName = "addRule", ClearItemsName = "clearRules")]
        public RewriteRuleCollection Rules
        {
            get { return (RewriteRuleCollection)this["rules"]; }
        }

        [ConfigurationProperty("urlMappingConfiguration")]
        public UrlMappingConfigurationElement UrlMapping
        {
            get { return (UrlMappingConfigurationElement)this["urlMappingConfiguration"]; }
        }

        public static RewriteRulesConfigurationSection Configuration
        {
            get { return (RewriteRulesConfigurationSection)ConfigurationManager.GetSection("rewriteConfiguration"); }
        }
    }
}
