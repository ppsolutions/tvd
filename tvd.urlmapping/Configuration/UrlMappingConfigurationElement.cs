﻿using System.Configuration;

namespace tvd.urlmapping.Configuration
{
    public class UrlMappingConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("rules", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(UrlMapping), AddItemName = "urlMapping", ClearItemsName = "clear")]
        public UrlMappingCollection UrlMappings
        {
            get { return (UrlMappingCollection)this["rules"]; }
        }

        [ConfigurationProperty("defaultLocation", IsRequired = true)]
        public string DefaultLocation
        {
            get { return (string)this["defaultLocation"]; }
        }
    }
}
