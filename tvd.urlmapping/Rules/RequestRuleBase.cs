﻿using System.Web;

namespace tvd.urlmapping.Rules
{
    public abstract class RequestRuleBase
    {
        protected readonly HttpContextBase ContextBase;
        protected RequestRuleBase(
            HttpContextBase contextBase)
        {
            this.ContextBase = contextBase;
        }

        protected abstract string ExecuteRule();

        protected virtual bool IsRuleValid()
        {
            return true;
        }

        public string RequestPath
        {
            get { return ContextBase.Request.Url.AbsolutePath.ToLower(); }
        }

        public string RequestQuery
        {
            get { return HttpUtility.UrlDecode(ContextBase.Request.QueryString.ToString()); }
        }

        public RuleResult RedirectUrl()
        {
            string newUrl = null;
            var success = false;

            if (IsRuleValid())
            {
                newUrl = ExecuteRule();
                success = true;
            }
            return new RuleResult(success, newUrl);
        }
    }
}
