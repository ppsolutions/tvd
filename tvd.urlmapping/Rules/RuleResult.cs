﻿namespace tvd.urlmapping.Rules
{
    public class RuleResult
    {
        private readonly bool success;

        private readonly string newLocation;

        public RuleResult(bool success, string newLocation)
        {
            this.success = success;
            this.newLocation = newLocation;
        }

        public bool Success
        {
            get { return success; }
        }

        public string NewLocation
        {
            get { return newLocation; }
        }

        public bool CanRewrite
        {
            get { return success && !string.IsNullOrEmpty(newLocation); }
        }
    }
}
