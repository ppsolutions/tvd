﻿using System.Web;

namespace tvd.urlmapping.Rules
{
    public class MvcDirectAccess : RequestRuleBase
    {
        public MvcDirectAccess(HttpContextBase contextBase) : base(contextBase)
        {

        }

        protected override string ExecuteRule()
        {
            var requestTargetUrl = ContextBase.Items["requestTargetUrl"] as string;
            var target = FormatRequestTargetParameter(requestTargetUrl);
            var queryString = string.Empty;

            //if (HttpContext.Current.Request.QueryString.Count != 0)
            //{
            //    queryString = HttpContext.Current.Request.QueryString.ToString();
            //}

            //if (!string.IsNullOrEmpty(queryString))
            //{
            //    if (target.Contains(RequestTargetParamNames.REQUESTQUERY))
            //    {
            //        queryString = string.Format("?{0}", queryString);
            //        target = target.Replace(RequestTargetParamNames.REQUESTQUERY, queryString);
            //    }
            //    else
            //    {
            //        target = string.Format("{0}?{1}", target, queryString);
            //    }
            //}
            //else
            //{
            //    target = target.Replace(RequestTargetParamNames.REQUESTQUERY, string.Empty);
            //}

            return target;
        }

        private string FormatRequestTargetParameter(string requestTarget)
        {
            var pageTypeId = ContextBase.Items["pageTypeId"] as int? ?? 0;
            requestTarget = requestTarget.Replace(RequestTargetParamNames.PAGETYPEID, pageTypeId.ToString());

            return requestTarget;
        }
    }
}
