﻿namespace tvd.urlmapping.Rules
{
    public static class RequestTargetParamNames
    {
        public const string REQUESTQUERY = "{REQUEST_QUERY}";
        public const string PAGETYPEID = "{PAGETYPE_ID}";
    }
}
