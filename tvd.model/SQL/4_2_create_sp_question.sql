USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[question_fetch_v1]    Script Date: 15/7/2560 1:12:28 ******/
DROP PROCEDURE [dbo].[question_fetch_v1]
GO

/****** Object:  StoredProcedure [dbo].[question_fetch_v1]    Script Date: 15/7/2560 1:12:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-10	| Create new sp
-----------------------------------------------------------------------
-- EXEC question_fetch_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[question_fetch_v1]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- Insert statements for procedure here
	SELECT	question_id
		, question_detail
		, rec_status 
	FROM	dbo.question
	WHERE	rec_status = 1
END
GO


GRANT EXECUTE ON question_fetch_v1 TO tvd_db_user