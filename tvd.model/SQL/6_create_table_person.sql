USE [tvd_db]
GO

/****** Object:  Table [dbo].[person]    Script Date: 18/7/2560 23:25:25 ******/
DROP TABLE [dbo].[person]
GO

/****** Object:  Table [dbo].[person]    Script Date: 18/7/2560 23:25:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[person_type_id] [int] NOT NULL,
	[title_id] [int] NOT NULL,
	[firstname] [nvarchar](500) NOT NULL,
	[lastname] [nvarchar](500) NULL,
	[is_legal_person] [bit] NOT NULL,
	[birthdate] [date] NOT NULL,
	[contact_number] [nvarchar](100) NOT NULL,
	[email_address] [varchar](100) NOT NULL,
	[username] [varchar](100) NULL,
	[password] [varchar](500) NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modified_when] [datetime] NULL,
	[rec_modified_by] [int] NULL,
 CONSTRAINT [PK_person] PRIMARY KEY CLUSTERED 
(
	[person_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


