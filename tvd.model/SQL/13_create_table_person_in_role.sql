USE [tvd_db]
GO

/****** Object:  Table [dbo].[person_in_role]    Script Date: 18/7/2560 22:13:16 ******/
DROP TABLE [dbo].[person_in_role]
GO

/****** Object:  Table [dbo].[person_in_role]    Script Date: 18/7/2560 22:13:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person_in_role](
	[person_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modifed_when] [datetime] NULL,
	[rec_modifed_by] [int] NULL,
 CONSTRAINT [PK_person_in_role] PRIMARY KEY CLUSTERED 
(
	[person_id] ASC
	, [role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


