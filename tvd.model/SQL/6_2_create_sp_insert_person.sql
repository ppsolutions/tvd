USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_insert_v1]    Script Date: 18/7/2560 10:02:13 ******/
DROP PROCEDURE [dbo].[person_insert_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_insert_v1]    Script Date: 18/7/2560 10:02:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.person_insert_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_insert_v1] 
	@person_type_id int
	, @title_id int
	, @firstname nvarchar(500)
	, @lastname nvarchar(500)
	, @is_legal_person bit
	, @birthdate date
	, @contact_number nvarchar(100)
	, @email_address varchar(100)
	, @by int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Insert statements for procedure here
	INSERT INTO	dbo.person
		(
			person_type_id
			, title_id
			, firstname
			, lastname
			, is_legal_person
			, birthdate
			, contact_number
			, email_address
			, username
			, rec_status
			, rec_created_by
			, rec_created_when
		)
	VALUES
		(
			@person_type_id
			, @title_id
			, @firstname
			, @lastname
			, @is_legal_person
			, @birthdate
			, @contact_number
			, @email_address
			, @email_address
			, 1
			, @by
			, GETDATE()
		)

	SELECT CAST(SCOPE_IDENTITY() AS int) AS person_id
END
GO


