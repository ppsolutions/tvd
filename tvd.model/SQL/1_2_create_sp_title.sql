USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[title_fetch_v1]    Script Date: 11/7/2560 0:42:35 ******/
DROP PROCEDURE [dbo].[title_fetch_v1]
GO

/****** Object:  StoredProcedure [dbo].[title_fetch_v1]    Script Date: 11/7/2560 0:42:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-10	| Create new sp
-----------------------------------------------------------------------
-- EXEC title_fetch_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[title_fetch_v1] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- Insert statements for procedure here
	SELECT	title_id
		, title_name
		, is_legal_person
		, rec_status 
	FROM	dbo.title
	WHERE	rec_status = 1
END
GO


GRANT EXECUTE ON title_fetch_v1 TO tvd_db_user