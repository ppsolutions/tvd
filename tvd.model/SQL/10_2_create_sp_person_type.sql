USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_type_fetch_v1]    Script Date: 12/7/2560 0:37:48 ******/
DROP PROCEDURE [dbo].[person_type_fetch_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_type_fetch_v1]    Script Date: 12/7/2560 0:37:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-10	| Create new sp
-----------------------------------------------------------------------
-- EXEC person_type_fetch_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_type_fetch_v1] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- Insert statements for procedure here
	SELECT	person_type_id
		, person_type_name
		, rec_status 
	FROM	dbo.person_type
	WHERE	rec_status = 1
END
GO

GRANT EXECUTE ON person_type_fetch_v1 TO tvd_db_user
