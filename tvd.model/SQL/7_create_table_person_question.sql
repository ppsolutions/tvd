USE [tvd_db]
GO

/****** Object:  Table [dbo].[person_question]    Script Date: 18/7/2560 10:15:16 ******/
DROP TABLE [dbo].[person_question]
GO

/****** Object:  Table [dbo].[person_question]    Script Date: 18/7/2560 10:15:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person_question](
	[person_question_id] [int] IDENTITY(1,1) NOT NULL,
	[person_id] [int] NOT NULL,
	[question_id] [int] NOT NULL,
	[answer] [nvarchar](max) NOT NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modified_when] [datetime] NULL,
	[rec_modified_by] [int] NULL,
 CONSTRAINT [PK_person_question] PRIMARY KEY CLUSTERED 
(
	[person_question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


