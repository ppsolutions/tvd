USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_password_update_v1]    Script Date: 18/7/2560 10:24:28 ******/
DROP PROCEDURE [dbo].[person_password_update_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_password_update_v1]    Script Date: 18/7/2560 10:24:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.person_password_update_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_password_update_v1] 
	@person_id int
	, @password varchar(100)
	, @by int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE	dbo.person
	SET	password = @password
		, rec_modified_by = @by
		, rec_modified_when = GETDATE()
	WHERE	person_id = @person_id
END
GO

GRANT EXECUTE ON person_password_update_v1 TO tvd_db_user