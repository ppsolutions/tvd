USE [tvd_db]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_person (person_id, person_type_id, email_address, username)]    Script Date: 8/7/2560 23:57:44 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_person (person_id, person_type_id, email_address, username)] ON [dbo].[person]
(
	[person_id] ASC,
	[person_type_id] ASC,
	[email_address] ASC,
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


