USE [tvd_db]
GO

/****** Object:  Table [dbo].[role]    Script Date: 18/7/2560 22:18:24 ******/
DROP TABLE [dbo].[role_permission]
GO

/****** Object:  Table [dbo].[role]    Script Date: 18/7/2560 22:18:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[role_permission](
	[role_id] [int] NOT NULL,
	[is_permit_in_member_registration_system] [bit] NULL,
	[is_permit_in_approve_system] [bit] NULL,
	[is_permit_in_news_system] [bit] NULL,
	[is_permit_in_point_setting_system] [bit] NULL,
	[is_permit_in_report_system] [bit] NULL,
	[is_permit_in_audit_report_system] [bit] NULL,
	[is_permit_in_employee_management_system] [bit] NULL,
	[is_permit_in_role_system] [bit] NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modifed_when] [datetime] NULL,
	[rec_modifed_by] [int] NULL,
 CONSTRAINT [PK_role_permission] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


