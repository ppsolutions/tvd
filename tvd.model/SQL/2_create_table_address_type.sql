USE [tvd_db]
GO

/****** Object:  Table [dbo].[address_type]    Script Date: 9/7/2560 0:05:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[address_type](
	[address_type_id] [int] IDENTITY(1,1) NOT NULL,
	[address_type_name] [nvarchar](255) NOT NULL,
	[rec_status] [tinyint] NOT NULL,
 CONSTRAINT [PK_address_type] PRIMARY KEY CLUSTERED 
(
	[address_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


