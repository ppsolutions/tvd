USE [tvd_db]
GO

/****** Object:  Table [dbo].[role]    Script Date: 18/7/2560 22:18:05 ******/
DROP TABLE [dbo].[role]
GO

/****** Object:  Table [dbo].[role]    Script Date: 18/7/2560 22:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [nvarchar](1000) NOT NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modifed_when] [datetime] NULL,
	[rec_modifed_by] [int] NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


