USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[address_type_fetch_v1]    Script Date: 12/7/2560 0:33:50 ******/
DROP PROCEDURE [dbo].[address_type_fetch_v1]
GO

/****** Object:  StoredProcedure [dbo].[address_type_fetch_v1]    Script Date: 12/7/2560 0:33:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-10	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.address_type_fetch_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[address_type_fetch_v1] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- Insert statements for procedure here
	SELECT	address_type_id
		, address_type_name
		, rec_status 
	FROM	dbo.address_type
	WHERE	rec_status = 1
END
GO

GRANT EXECUTE ON address_type_fetch_v1 TO tvd_user_db
