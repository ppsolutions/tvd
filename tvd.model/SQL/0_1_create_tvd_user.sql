USE [master]
GO
CREATE LOGIN [tvd_db_user] WITH PASSWORD=N'tvd_db_user1234', DEFAULT_DATABASE=[tvd_db], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [tvd_db_user]
GO
USE [tvd_db]
GO
CREATE USER [tvd_db_user] FOR LOGIN [tvd_db_user]
GO
USE [tvd_db]
GO
ALTER USER [tvd_db_user] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [tvd_db]
GO
ALTER ROLE [db_owner] ADD MEMBER [tvd_db_user]
GO
