USE [tvd_db]
GO

/****** Object:  Table [dbo].[person_address]    Script Date: 18/7/2560 11:46:25 ******/
DROP TABLE [dbo].[person_address]
GO

/****** Object:  Table [dbo].[person_address]    Script Date: 18/7/2560 11:46:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person_address](
	[person_address_id] [int] IDENTITY(1,1) NOT NULL,
	[person_id] [int] NOT NULL,
	[address_1] [nvarchar](1000) NOT NULL,
	[address_2] [nvarchar](1000) NULL,
	[city_id] [int] NOT NULL,
	[postal_code] [varchar](20) NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [datetime] NOT NULL,
	[rec_created_by] [int] NOT NULL,
	[rec_modifed_when] [datetime] NULL,
	[rec_modifed_by] [int] NULL,
 CONSTRAINT [PK_person_address] PRIMARY KEY CLUSTERED 
(
	[person_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


