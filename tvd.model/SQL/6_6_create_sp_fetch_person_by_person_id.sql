USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_fetch_by_person_id_v1]    Script Date: 18/7/2560 23:06:45 ******/
DROP PROCEDURE [dbo].[person_fetch_by_person_id_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_fetch_by_person_id_v1]    Script Date: 18/7/2560 23:06:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.person_fetch_by_person_id_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_fetch_by_person_id_v1] 
	@person_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	WITH CTE AS(
		SELECT		@person_id AS member_person_id
				, p.person_id
				, p.firstname
				, p.lastname
				, r.role_name
		FROM		dbo.person AS p
		INNER JOIN	dbo.person_in_role AS pir
		ON		p.person_id = pir.person_id
		INNER JOIN	dbo.role AS r
		ON		pir.role_id = r.role_id
		WHERE	p.person_id = (	SELECT	ISNULL(rec_modified_by, rec_created_by)
					FROM	dbo.person
					WHERE	person_id = @person_id)
					
	)



	-- Insert statements for procedure here
	SELECT		p.person_id
			, p.person_type_id
			, p.title_id
			, p.firstname
			, p.lastname
			, p.is_legal_person
			, p.birthdate
			, p.contact_number
			, p.email_address
			, p.username
			, p.password
			, p.rec_status
			, p.rec_created_when
			, p.rec_created_by
			, p.rec_modified_when
			, p.rec_modified_by
			, c.person_id AS pl_person_id
			, c.firstname AS pl_firstname
			, c.lastname AS pl_lastname
			, c.role_name AS pl_role_name
	FROM		dbo.person AS p
	INNER JOIN	CTE AS c
	ON		p.person_id = c.member_person_id
	WHERE		p.person_id = @person_id

	SELECT		pa.person_address_id
			, pa.person_id
			, pat.address_type_id
			, pa.address_1
			, pa.address_2
			, pa.city_id
			, pa.postal_code
			, pa.rec_status
			, pa.rec_created_when
			, pa.rec_created_by
			, pa.rec_modifed_when
			, pa.rec_modifed_by
	FROM		dbo.person_address_type AS pat
	INNER JOIN	dbo.person_address AS pa
	ON		pat.person_address_id = pa.person_address_id
	WHERE		pat.person_id = @person_id

	SELECT		pq.person_question_id
			, pq.person_id
			, q.question_id
			, q.question_detail
			, pq.answer
			, pq.rec_status
			, pq.rec_created_when
			, pq.rec_created_by
			, pq.rec_modified_when
			, pq.rec_modified_by
	FROM		dbo.person_question AS pq
	INNER JOIN	dbo.question AS q
	ON		pq.question_id = q.question_id
	WHERE		pq.person_id = @person_id

END
GO


