USE [tvd_db]
GO

/****** Object:  Table [dbo].[person_type]    Script Date: 8/7/2560 23:23:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person_type](
	[person_type_id] [int] IDENTITY(1,1) NOT NULL,
	[person_type_name] [nvarchar] (255) NOT NULL,
	[rec_status] [tinyint] NOT NULL,
 CONSTRAINT [PK_person_type] PRIMARY KEY CLUSTERED 
(
	[person_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


