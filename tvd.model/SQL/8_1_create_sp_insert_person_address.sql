USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_address_insert_v1]    Script Date: 18/7/2560 11:50:34 ******/
DROP PROCEDURE [dbo].[person_address_insert_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_address_insert_v1]    Script Date: 18/7/2560 11:50:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.person_address_insert_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_address_insert_v1] 
	@person_id int
	, @address_type_id int
	, @address_1 nvarchar(1000)
	, @address_2 nvarchar(1000)
	, @city_id int
	, @postal_code varchar(20)
	, @by int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @person_address_id int

	INSERT INTO	dbo.person_address
		(
			person_id
			, address_1
			, address_2
			, city_id
			, postal_code
			, rec_status
			, rec_created_when
			, rec_created_by
		)
	VALUES	(
			@person_id
			, @address_1
			, @address_2
			, @city_id
			, @postal_code
			, 1
			, GETDATE()
			, @by
		)

	SET @person_address_id = CAST(SCOPE_IDENTITY() AS int)

	INSERT INTO	dbo.person_address_type
		(
			person_id
			, person_address_id
			, address_type_id
			, rec_status
		)
	VALUES	(
			@person_id
			, @person_address_id
			, @address_type_id
			, 1
		)
END
GO

GRANT EXECUTE ON person_address_insert_v1 TO tvd_db_user