USE [tvd_db]
GO

/****** Object:  Table [dbo].[person_address_type]    Script Date: 16/7/2560 2:17:14 ******/
DROP TABLE [dbo].[person_address_type]
GO

/****** Object:  Table [dbo].[person_address_type]    Script Date: 16/7/2560 2:17:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[person_address_type](
	[person_id] [int] NOT NULL,
	[person_address_id] [int] IDENTITY(1,1) NOT NULL,
	[address_type_id] [int] NOT NULL,
	[rec_status] [tinyint] NOT NULL,
 CONSTRAINT [PK_person_address_type] PRIMARY KEY CLUSTERED 
(
	[person_id] ASC,
	[person_address_id] ASC,
	[address_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


