USE [tvd_db]
GO

/****** Object:  Table [dbo].[title]    Script Date: 17/7/2560 22:23:06 ******/
DROP TABLE [dbo].[title]
GO

/****** Object:  Table [dbo].[title]    Script Date: 17/7/2560 22:23:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[title](
	[title_id] [int] IDENTITY(1,1) NOT NULL,
	[title_name] [nvarchar](100) NOT NULL,
	[is_legal_person] [bit] NOT NULL,
	[rec_status] [tinyint] NOT NULL,
 CONSTRAINT [PK_title] PRIMARY KEY CLUSTERED 
(
	[title_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


