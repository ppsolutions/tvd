USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[city_fetch_v1]    Script Date: 12/7/2560 1:09:43 ******/
DROP PROCEDURE [dbo].[city_fetch_v1]
GO

/****** Object:  StoredProcedure [dbo].[city_fetch_v1]    Script Date: 12/7/2560 1:09:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-10	| Create new sp
-----------------------------------------------------------------------
-- EXEC city_fetch_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[city_fetch_v1] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- Insert statements for procedure here
	SELECT	city_id
		, city_name
		, rec_status 
	FROM	dbo.city
	WHERE	rec_status = 1
END
GO


GRANT EXECUTE ON city_fetch_v1 TO tvd_db_user