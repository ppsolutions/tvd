USE [tvd_db]
GO

/****** Object:  Table [dbo].[audit_log]    Script Date: 8/7/2560 23:23:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[audit_log](
	[audit_log_id] [int] IDENTITY(1,1) NOT NULL,
	[audit_action_type_id] [int] NOT NULL,
	[object_id] [uniqueidentifier] NOT NULL,
	[old_value] [nvarchar] (MAX) NOT NULL,
	[new_value] [nvarchar] (MAX) NOT NULL,
	[rec_status] [tinyint] NOT NULL,
	[rec_created_when] [uniqueidentifier] NOT NULL,
	[rec_created_by] [datetime] NOT NULL,
	[rec_modifed_when] [uniqueidentifier] NULL,
	[rec_modifed_by] [datetime] NULL
 CONSTRAINT [PK_audit_log] PRIMARY KEY CLUSTERED 
(
	[audit_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


