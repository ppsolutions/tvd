USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[person_person_question_insert_v1]    Script Date: 18/7/2560 10:24:56 ******/
DROP PROCEDURE [dbo].[person_question_insert_v1]
GO

/****** Object:  StoredProcedure [dbo].[person_person_question_insert_v1]    Script Date: 18/7/2560 10:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.person_question_insert_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[person_question_insert_v1] 
	@person_id int
	, @question_id int
	, @answer nvarchar(max)
	, @by int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	INSERT INTO	dbo.person_question
		(
			person_id
			, question_id
			, answer
			, rec_status
			, rec_created_when
			, rec_created_by
		)
	VALUES	(
			@person_id
			, @question_id
			, @answer
			, 1
			, GETDATE()
			, @by
		)
END
GO

GRANT EXECUTE ON person_question_insert_v1 TO tvd_db_user