USE [tvd_db]
GO

/****** Object:  StoredProcedure [dbo].[member_fetch_by_criteria_v1]    Script Date: 18/7/2560 23:06:45 ******/
DROP PROCEDURE [dbo].[member_fetch_by_criteria_v1]
GO

/****** Object:  StoredProcedure [dbo].[member_fetch_by_criteria_v1]    Script Date: 18/7/2560 23:06:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-----------------------------------------------------------------------
-- Name			| Date		| Comment
-----------------------------------------------------------------------
-- Pornchai Tipwataksorn| 2017-07-18	| Create new sp
-----------------------------------------------------------------------
-- EXEC dbo.member_fetch_by_criteria_v1
-----------------------------------------------------------------------

CREATE PROCEDURE [dbo].[member_fetch_by_criteria_v1]
	@firstname nvarchar(500)
	, @lastname nvarchar(500)
	, @contact_number nvarchar(100)
	, @person_id varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- Insert statements for procedure here
	SELECT		p.person_id
			, p.person_type_id
			, p.title_id
			, p.firstname
			, p.lastname
			, p.is_legal_person
			, p.birthdate
			, p.contact_number
			, p.email_address
			, p.username
			, p.password
			, p.rec_status
			, p.rec_created_when
			, p.rec_created_by
			, p.rec_modified_when
			, p.rec_modified_by
			, pl.person_id AS pl_person_id
			, pl.firstname AS pl_firstname
			, pl.lastname AS pl_lastname
			, r.role_name AS pl_role_name
	FROM		dbo.person AS p
	LEFT OUTER JOIN	dbo.person AS pl	
	ON		ISNULL(p.rec_modified_by, p.rec_created_by) = pl.person_id
	LEFT JOIN	dbo.person_in_role AS pir
	ON		pl.person_id = pir.person_id
	INNER JOIN	dbo.role AS r
	ON		pir.role_id = r.role_id
	WHERE		pl.person_id IS NOT NULL
	AND		(@person_id IS NULL OR CAST(p.person_id AS varchar) LIKE @person_id + '%')
	AND		(@firstname IS NULL OR p.firstname LIKE @firstname + '%')
	AND		(@lastname IS NULL OR p.lastname LIKE @lastname + '%')
	AND		(@contact_number IS NULL OR p.contact_number LIKE @contact_number + '%')
END
GO

GRANT EXECUTE ON member_fetch_by_criteria_v1 TO tvd_db_user
