﻿namespace tvd.model.Api.Models.Member
{
    public class PersonAddress
    {
        public AddressType AddressType { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string PostalCode { get; set; }
    }
}