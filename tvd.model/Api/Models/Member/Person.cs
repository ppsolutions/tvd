﻿namespace tvd.model.Api.Models.Member
{
    public class Person
    {
        public int? PersonTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
		public bool IsSpecificYear { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}