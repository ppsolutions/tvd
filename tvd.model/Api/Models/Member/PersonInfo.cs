﻿using System;

namespace tvd.model.Api.Models.Member
{
    public class PersonInfo
    {
        public int PersonId { get; set; }
        public string Code { get; set; }
        public int TitleId { get; set; }
        public DateTime BirthDate { get; set; }
		public bool IsSpecificYear { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public int RecStatus { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
