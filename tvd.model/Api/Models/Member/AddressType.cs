﻿using System.ComponentModel;

namespace tvd.model.Api.Models.Member
{
    public enum AddressType
    {
        [Description("ที่อยู่ปัจจุบัน")]
        CURRENT = 1,

        [Description("ที่อยู่จัดส่งสินค้า")]
        SHIPMENT = 2,

        [Description("ที่อยู่จัดส่งใบเสร็จ")]
        INVOICE = 3
    }
}