﻿namespace tvd.model.Api.Models.Member
{
    public class Corporate
    {
        public int? CorporateTitle { get; set; }
        public string CorporateName { get; set; }
        public string CorporatePhone { get; set; }
        public string CorporateEmail { get; set; }
    }
}