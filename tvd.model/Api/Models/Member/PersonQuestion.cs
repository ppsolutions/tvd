﻿namespace tvd.model.Api.Models.Member
{
    public class PersonQuestion
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
    }
}