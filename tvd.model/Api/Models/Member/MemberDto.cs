﻿using System.Collections.Generic;

namespace tvd.model.Api.Models.Member
{
    public class MemberDto
    {
        public int RequestType { get; set; }
        public int? PersonId { get; set; }
        public Person Person { get; set; }
        public Corporate Corporate { get; set; }
        public List<PersonAddress> Address { get; set; }
        public List<PersonQuestion> Question { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public int RecStatus { get; set; }
        public string CustomerRef { get; set; }
    }
}