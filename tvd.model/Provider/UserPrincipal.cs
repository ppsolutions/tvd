﻿using System.Security.Principal;

namespace tvd.model.Provider
{
    public class UserPrincipal : IPrincipal
    {
        private readonly IIdentity identity;

        public UserPrincipal(UserIdentity userIdentity)
        {
            identity = userIdentity.identity;
        }

        public IIdentity Identity
        {
            get { return identity; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}
