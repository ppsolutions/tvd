﻿using System.Security.Principal;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Provider
{
    public class UserIdentity : IIdentity
    {
        public IIdentity identity { get; set; }
        public Employee user { get; set; }

        public UserIdentity(Employee user)
        {
            identity = new GenericIdentity(user.EmployeeCode);
            this.user = user;
        }

        public string AuthenticationType
        {
            get { return identity.AuthenticationType; }
        }

        public bool IsAuthenticated
        {
            get { return identity.IsAuthenticated; }
        }

        public string Name
        {
            get { return identity.Name; }
        }
    }
}
