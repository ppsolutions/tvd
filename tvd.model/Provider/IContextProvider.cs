﻿using System.Web;

namespace tvd.model.Provider
{
    public interface IContextProvider
    {
        bool IsAuthenticated();
        HttpCookie GetHttpCookie(string cookieKey);
        string GetCookieValue(string cookieKey);
        bool HasCookie(string cookieKey);
        HttpContextBase GetContext();
        int GetPageTypeId();
    }
}
