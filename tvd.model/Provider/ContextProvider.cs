﻿using System.Web;

namespace tvd.model.Provider
{
    public class ContextProvider : IContextProvider
    {
        private HttpContextBase ContextBase;

        public ContextProvider(HttpContextBase contextBase)
        {
            this.ContextBase = contextBase;
        }

        public bool IsAuthenticated()
        {
            return ContextBase.User?.Identity?.IsAuthenticated as bool? ?? false;
        }

        public HttpCookie GetHttpCookie(string cookieKey)
        {
            return this.ContextBase.Request.Cookies[cookieKey];
        }

        public string GetCookieValue(string cookieKey)
        {
            if (this.HasCookie(cookieKey))
            {
                return GetHttpCookie(cookieKey).Value;
            }
            return null;
        }

        public bool HasCookie(string cookieKey)
        {
            return GetHttpCookie(cookieKey) != null;
        }

        public HttpContextBase GetContext()
        {
            return this.ContextBase;
        }

        public int GetPageTypeId()
        {
            return this.ContextBase.Items["pageTypeId"] as int? ?? 0;
        }
    }
}
