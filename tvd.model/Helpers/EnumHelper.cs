﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Helpers
{
	public static class EnumHelper
	{
		public static string GetEnumDescription(Enum value)
		{
			DescriptionAttribute descriptionAttribute = value.GetType()
				.GetField(value.ToString())
				.GetCustomAttributes(typeof(DescriptionAttribute), false)
				.SingleOrDefault() as DescriptionAttribute;
			return descriptionAttribute == null ? value.ToString() : descriptionAttribute.Description;
		}
	}
}
