﻿using PPSolution.Core;

namespace tvd.model.Helpers
{
    public class DBHelper : IDBHelper
    {
        private Connection _connection;


        public Connection GetConnection()
        {
            if (_connection == null)
            {
                _connection = new Connection();
            }

            return _connection;
        }
    }
}