﻿using PPSolution.Core;

namespace tvd.model.Helpers
{
    public interface IDBHelper
    {
        Connection GetConnection();
    }
}
