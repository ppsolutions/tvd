﻿namespace tvd.model.Models.UserData
{
    public interface IUserData
    {
        bool IsAuthenticated();
        model.Models.CommonDTO.Employee.Employee UserInfo { get; }
    }
}
