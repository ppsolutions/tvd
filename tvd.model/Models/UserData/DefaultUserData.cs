﻿using System.Web;
using tvd.model.Provider;

namespace tvd.model.Models.UserData
{
    public class DefaultUserData : IUserData
    {
        private readonly IContextProvider contextProvider;
        public tvd.model.Models.CommonDTO.Employee.Employee UserInfo { get; private set; }

        public DefaultUserData(IContextProvider contextProvider)
        {
            this.contextProvider = contextProvider;
            this.SetUserInfo(this.contextProvider.GetContext());
        }

        public bool IsAuthenticated()
        {
            return this.contextProvider.IsAuthenticated();
        }

        public int UserId
        {
            get
            {
                return 1;
            }
        }

        public object JsonConvert { get; private set; }

        private void SetUserInfo(HttpContextBase contextBase)
        {
            this.UserInfo = contextBase.Items["UserInfo"] as tvd.model.Models.CommonDTO.Employee.Employee;
        }
    }
}
