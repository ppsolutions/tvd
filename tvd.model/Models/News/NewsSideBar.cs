﻿namespace tvd.model.Models.News
{
    public class NewsSideBar
    {
        public string NewsMemberUrl { get; set; }
        public string NewsPromotionUrl { get; set; }
        public string NewsMemberRuleUrl { get; set; }
        public string NewsMemberBenefitUrl { get; set; }
    }
}
