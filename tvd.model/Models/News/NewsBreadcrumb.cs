﻿namespace tvd.model.Models.News
{
    public class NewsBreadcrumb
    {
        public string NewsCreateUrl { get; set; }
        public string NewsEditUrl { get; set; }
    }
}
