﻿namespace tvd.model.Models
{
    public enum AuthenticationType
    {
        Unauthenticated = 0,
        Username = 1,
        Email = 2,
        OpenID = 3,
        FacebookConnect = 4,
        TwitterConnect = 5,
        GoogleConnect = 6,
    }

    public class UserInfo
    {
        private readonly int personId;
        private readonly string email;
        private readonly string username;
        private readonly AuthenticationType authenticationType;
        private readonly bool isAuthenticated;

        public UserInfo(int personId, string email, string userName, AuthenticationType authenticationType)
        {
            this.personId = personId;
            this.email = email;
            this.username = userName;
            this.authenticationType = authenticationType;
            this.isAuthenticated = true;
        }

        public int PersonId
        {
            get
            {
                return personId;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return isAuthenticated;
            }
        }

        public AuthenticationType AuthenticationType
        {
            get
            {
                return authenticationType;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }
        }
    }
}
