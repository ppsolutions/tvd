﻿namespace tvd.model.Models
{
    public class SearchProductDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string By { get; set; }
    }
}
