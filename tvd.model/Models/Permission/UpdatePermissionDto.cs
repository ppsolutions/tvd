﻿namespace tvd.model.Models.Permission
{
    public class UpdatePermissionDto
    {
        public int RoleId { get; set; }
        public string Token { get; set; }
    }
}
