﻿using tvd.model.Models.CommonDTO.Employee;

namespace tvd.model.Models.Permission
{
    public class PermissionUpSertDto
    {
        public int? RoleId { get; set; }
        public string RoleName { get; set; }
        public string Token { get; set; }
        public int UpdatedBy { get; set; }
        public RolePermission RolePermission { get; set; }
    }
}
