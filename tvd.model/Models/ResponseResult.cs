﻿namespace tvd.model.Models
{
    public class ResponseResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object ResponseData { get; set; }
        public string ReturnUrl { get; set; }
        public Pager Pager { get; set; }
    }    
}