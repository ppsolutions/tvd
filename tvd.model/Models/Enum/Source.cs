﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
	public enum Source
	{
		[Description ("Unknown Source")]
		UNKNOWN				= 0,

		[Description("Membership Website")]
		MEMBERSHIP_WEBSITE	= 1,

		[Description("Call Center")]
		CALL_CENTER			= 2,

		[Description("TVDis POS")]
		TVDIS_POS			= 3
	}
}
