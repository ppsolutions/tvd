﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum UpdatePointType
    {
        [Description("ปรับปรุงคะแนน")]
        ADJUST = 1,
        [Description("เลื่อนระดับ")]
        UPGRADE = 2,
        [Description("แลกของรางวัล")]
        REDEMPTION =  3,
        UNKNOWN = 0
    }
}
