﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum CalculationType
	{
		[Description("เพิ่มคะแนน")]
		INCREASE_POINT		= 1,

		[Description("เพิ่มเท่า")]
		MULTIPLE_POINT		= 2,

		[Description("เปลี่ยนฐานคะแนน")]
		CHANGE_BASE_POINT	= 3
	}
}
