﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum ActionType
    {
        [Description("เพิ่มคะแนน")]
        ADD = 1,

        [Description("ลดคะแนน")]
        DELETE = -1,

        NONE = 0
    }
}