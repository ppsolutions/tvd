﻿namespace tvd.model.Models.Enum
{
    public enum PointLevelType
    {
        Regular = 1,
        Silver = 2,
        Gold = 3,
        Platinum = 4,
        All = 0
    }
}
