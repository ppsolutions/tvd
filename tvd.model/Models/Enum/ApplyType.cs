﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
	public enum ApplyType
	{
		[Description("ทั้งหมด")]
		ALL		= 0,

		[Description("Website")]
		WEBSITE	= 1,

		[Description("Shop")]
		SHOP	= 2
	}
}
