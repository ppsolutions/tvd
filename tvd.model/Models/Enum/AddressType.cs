﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
    public enum AddressType
    {
        [Description("ที่อยู่ปัจจุบัน")]
        CURRENT = 1,

        [Description("ที่อยู่จัดส่งสินค้า")]
        SHIPMENT = 2,

        [Description("ที่อยู่จัดส่งใบเสร็จ")]
        INVOICE = 3

    }
}
