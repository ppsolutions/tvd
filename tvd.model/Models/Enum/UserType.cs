﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
    public enum UserType
    {
        Customer = 1,
        Employee = 2
    }
}
