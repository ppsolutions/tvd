﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Enum
{
    public enum ApproveType
    {
		[Description("ไม่มีที่มา")]
		UNKNOWN					= 0,

		[Description("สมาชิกใหม่")]
        NEW_MEMBER				= 1,

		[Description("เปลี่ยนข้อมูลสมาชิก")]
		UPDATE_MEMEBER_INFO		= 2,

		[Description("บล็อคสมาชิก")]
		BLOCK_MEMBER			= 3,

		[Description("ยกเลิกสมาชิก")]
		CANCEL_MEMBER			= 4,

		[Description("เปลี่ยนคะแนน")]
		UPDATE_REWARD_POINT		= 5,

		[Description("แลกสินค้า")]
		PRODUCT_REDEMPTION		= 6,

		[Description("เลื่อนระดับ")]
		UPGRADE_MEMBER_LEVEL	= 7,

		[Description("รวมสมาชิกซ้ำซ้อน")]
		MERGE_MEMBER			= 8,

		[Description("ใช้งานสมาชิก")]
		ACTIVE_MEMBER			= 9,
	}
}
