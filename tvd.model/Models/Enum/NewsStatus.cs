﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum NewsStatus
    {
        [Description("แบบร่าง")]
        DRAFT = 0,

        [Description("เผยแพร่")]
        PUBLISH = 1
    }
}
