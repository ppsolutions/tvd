﻿namespace tvd.model.Models.Enum
{
    public class PointRuleType
    {
        public bool AddPoint { get; set; }
        public bool AddTime { get; set; }
        public bool ChangeBase { get; set; }
    }
}
