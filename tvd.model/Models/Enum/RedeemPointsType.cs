﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum RedeemPointsType
    {
        [Description("ใช้คะแนนเท่านั้น")]
        POINT_ONLY = 0,

        [Description("ใช้คะแนนและเพิ่มเงิน")]
        POINT_AND_PAID = 1
    }
}
