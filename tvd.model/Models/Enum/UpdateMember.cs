﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum UpdateMember
    {
        [Description("เปลี่ยนแปลงข้อมูลทั่วไป")]
        BASICINFO = 0,
        [Description("เปลี่ยนแปลงที่อยู่")]
        ADDRESS = 1,
        [Description("ความปลอดภัย")]
        SECURITY = 2,
        [Description("เปลี่ยนแปลงคำถาม")]
        QUESTION = 3

    }
}
