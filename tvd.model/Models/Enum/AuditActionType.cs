﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
	public enum AuditActionType
	{
		[Description("ไม่มีที่มา")]
		UNKNOWN = 0,

		[Description("เข้าสู่ระบบ")]
		LOG_IN = 1,

		[Description("ทะเบียนสมาชิก")]
		MEMBERSHIP = 2,

		[Description("ปรับปรุงคะแนน")]
		MEMBERSHIP_POINT_UPDATE = 3,

		[Description("แลกของกำนัล")]
		REWARD_REDEMPTION = 4,

		[Description("แลกคะแนน")]
		POINT_REDEMPTION = 5,

		[Description("อนุมัติสมาชิกใหม่")]
		NEW_MEMBERSHIP_APPROVED = 6,

		[Description("อนุมัติการเปลี่ยนแปลงข้อมูล")]
		UPDATE_MEMBERSHIP_INFO_APPROVED = 7,

		[Description("อนุมัติการบล็อคสมาชิก")]
		BLOCK_MEMBERSHIP_APPROVED = 8,

		[Description("อนุมัติการยกเลิกสมาชิก")]
		CANCEL_MEMBERSHIP_APPROVED = 9,

		[Description("อนุมัติการเปลี่ยนคะแนน")]
		UPDATE_POINT_APPROVED = 10,

		[Description("อนุมัติการแลกสินค้า")]
		PRODUCT_REDEMPTION_APPROVED = 11,

		[Description("อนุมัติการเลื่อนระดับ")]
		MEMBERSHIP_LEVEL_UPGRADE_APPROVED = 12,

		[Description("อนุมัติการตั้งค่าอนุมัติ")]
		APPROVE_SETTING_APPROVED = 13,

		[Description("ข่าวสารสมาชิก")]
		MEMBERSHIP_NEWS = 14,

		[Description("ข่าวสารโปรโมชั่น")]
		PROMOTION_NEWS = 15,

		[Description("กฏระเบียบสมาชิก")]
		MEMBERSHIP_RULE = 16,

		[Description("สิทธิประโยชน์สมาชิก")]
		MEMBERSHIP_BENEFIT = 17,

		[Description("คะแนนแลกของกำนัล")]
		POINT_REDEMPTION_REWARD = 18,

		[Description("คะแนนเลื่อนขั้น")]
		POINT_LEVEL_UPGRADE = 19,

		[Description("ตั้งค่าคะแนนแบบพื้นฐาน")]
		BASIC_SETTING_POINT = 20,

		[Description("ตั้งกฎการคำนวณ")]
		CALCULATION_SETTING_RULE = 21,

		[Description("คะแนนพื้นฐาน")]
		BASIC_POINT = 22,

		[Description("สำหรับ Promotion")]
		FOR_PROMOTION = 23,

		[Description("สำหรับ Campaign")]
		FOR_CAMPAIGN = 24,

		[Description("คะแนน Gamification")]
		GAMIFICATION_POINT = 25,

		[Description("รายงาน")]
		REPORT = 26,

		[Description("พนักงาน")]
		EMPLOYEE = 27,

		[Description("หน้าที่การใช้งาน")]
		ROLE_PERMISSION = 28,

        [Description("เปลี่ยนรหัสผ่าน")]
        CHANGE_PASSWORD = 29,

		[Description("เปลี่ยนแปลงข้อมูลสมาชิก")]
		UPDATE_MEMBERSHIP_INFO = 30,

		[Description("เปลี่ยนแปลงที่อยู่สมาชิก")]
		UPDATE_MEMBERSHIP_ADDRESS = 31,

		[Description("ยกเลิกสมาชิก")]
		BLOCK_MEMBERSHIP = 32,

		[Description("ใช้งานสมาชิก")]
		ACTIVE_MEMBERSHIP = 33,

		[Description("ยกเลิกสมาชิก")]
		CANCEL_MEMBERSHIP
	}
}