﻿using System.ComponentModel;

namespace tvd.model.Models.Enum
{
    public enum RecStatus
    {
        [Description("Delete")]
        DELETE		= -1,
        [Description("Inactive")]
        INACTIVE	= 0,
        [Description("Active")]
        ACTIVE		= 1,
        [Description("Pending")]
        PENDING		= 2,
        [Description("Unknown")]
		UNKOWN		= 99
    }
}