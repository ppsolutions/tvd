﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
	public enum Gender
	{
		[Description("หญิง")]
		FEMALE		= 0,

		[Description("ชาย")]
		MALE		= 1
	}
}
