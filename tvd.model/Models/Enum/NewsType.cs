﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Enum
{
    public enum NewsType
    {
        All = 0,
        [Description("ข่าวสารสมาชิก")]
        MemberNews = 1,
        [Description("ข่าวสารโปรโมชั่น")]
        PromotionNews = 2,
        [Description("กฏระเบียบสมาชิก")]
        MemberRule = 3,
        [Description("สิทธิประโยชน์สมาชิก")]
        MemberBenefit = 4
    }
}
