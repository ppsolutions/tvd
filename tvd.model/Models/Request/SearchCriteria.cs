﻿namespace tvd.model.Models.Request
{
    public class SearchCriteria
    {
        public string Name { get; set; }
		public string IdCard { get; set; }
        public string Phone { get; set; }
        public string MemberId { get; set; }
    }
}
