﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.CommonDTO.Enum;

namespace tvd.model.Models.Request
{
    public class UpdateApproveRequest
    {
        public ApproveType Type { get; set; }
        public int[] ApproveIds { get; set; }
        public string UpdatedBy { get; set; }
        public string Token { get; set; }
    }
}
