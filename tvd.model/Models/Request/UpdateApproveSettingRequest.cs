﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Request
{
    public class UpdateApproveSettingRequest
    {
        public int ApproveSettingId { get; set; }
        public bool IsAutoApproveNewMembership { get; set; }
        public bool IsAutoApproveChangeMembershipInfo { get; set; }
        public bool IsAutoApproveCancelMembership { get; set; }

        public bool IsAutoApproveChangeMembershipPoint { get; set; }
        public bool IsAutoApproveRewardRedeemMembership { get; set; }
        public bool IsAutoApproveLevelUpMembership { get; set; }

        /// <summary>
        /// Set up days before approve point requests expired
        /// </summary>
        public int PointRetensionPeriod { get; set; }

        /// <summary>
        /// Set up days before approve member requests expired
        /// </summary>
        public int MembershipRetensionPeriod { get; set; }

        public string UpdatedBy { get; set; }
        public string Token { get; set; }
    }
}
