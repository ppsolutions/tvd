﻿using System.Collections.Generic;

namespace tvd.model.Models.Request
{
    public class AddRoleRequest
    {
        public string UpdatedBy { get; set; }
        public string RoleName { get; set; }
        public bool IsPermitInMemberRegistrationSystem { get; set; }
        public bool IsPermitInApproveSystem { get; set; }
        public bool IsPermitInNewsSystem { get; set; }
        public bool IsPermitInPointSettingSystem { get; set; }
        public bool IsPermitInReportSystem { get; set; }
        public bool IsPermisInAuditReportSystem { get; set; }
        public bool IsPermisInEmployeeManagementSystem { get; set; }
        public bool IsPermisInRoleSystem { get; set; }
    }
}