﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class GetAuditLogRequest
    {
        public string CreatedBy { get; set; }
        public RecStatus? RecStatus { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? RoleId { get; set; }
        public int? ObjectId { get; set; }
        public int? MinPoint { get; set; }
        public int? MaxPoint { get; set; }
        public int? DaysBeforeExpire { get; set; }
		public int ExpireYear { get; set; }
        public PagerDto Pager { get; set; }
        public AuditActionType Type { get; set; }
        public string Value { get; set; }
    }
}
