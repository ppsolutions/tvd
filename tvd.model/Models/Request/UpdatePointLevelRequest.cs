﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Point;

namespace tvd.model.Models.Request
{
    public class UpdatePointLevelRequest
    {
        public List<PointLevel> UpdatedLevels { get; set; }
		public int? ExpireYear { get; set; }
        public DateTime? ExpireDate { get; set; }
        public decimal? MinPurchase { get; set; }
        public string Token { get; set; }
        public string UpdatedBy { get; set; }
    }
}
