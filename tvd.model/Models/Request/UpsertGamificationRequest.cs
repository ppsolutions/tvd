﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class UpsertGamificationRequest
    {
        public string Token { get; set; }
        public string UpdatedBy { get; set; }
        public string GameCode { get; set; }
        public string Description { get; set; }
        public ApplyType ApplyTo { get; set; }
        public int Point { get; set; }
        public RecStatus RecStatus { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public int? GameId { get; set; }
    }
}
