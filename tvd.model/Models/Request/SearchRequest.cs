﻿namespace tvd.model.Models.Request
{
    public class SearchRequest
    {
		public PagerDto Pager { get; set; }
		public string SearchName { get; set; }
		public string SearchIdCard { get; set; }
        public string SearchPhone { get; set; }
        public string SearchMember { get; set; }
    }
}
