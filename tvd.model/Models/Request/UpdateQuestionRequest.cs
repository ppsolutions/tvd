﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Models.Request
{
    public class UpdateQuestionRequest
    {
        public int FirstPersonQuestionId { get; set; }
        public int FirstQuestionId { get; set; }
        public string FirstAnswer { get; set; }
        public int SecondPersonQuestionId { get; set; }
        public int SecondQuestionId { get; set; }
        public string SecondAnswer { get; set; }

        public int PersonId { get; set; }
        public string UpdatedBy { get; set; }
        public string Token { get; set; }
    }
}