﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class UpdateMemberRequest
    {
        public int RegisterType { get; set; }
        public int PersonId { get; set; }
        public int PersonTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
		public bool IsSpecificYear { get; set; }
        public int CorporateTitle { get; set; }
        public string BusinessName { get; set; }

        public string ContactNumber { get; set; }
        public string Email { get; set; }
        
        public int FirstQuestionId { get; set; }
        public string FirstAnswer { get; set; }
        public int SecondQuestionId { get; set; }
        public string SecondAnswer { get; set; }

		public string Token { get; set; }
		public string UpdatedBy { get; set; }
		public int RecStatus { get; set; }
    }
}
