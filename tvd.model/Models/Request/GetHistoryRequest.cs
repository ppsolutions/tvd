﻿using System;

namespace tvd.model.Models.Request
{
    public class GetHistoryRequest
    {
        public int PageId { get; set; }
        public PagerDto Pager { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
