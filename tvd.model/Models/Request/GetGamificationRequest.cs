﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class GetGamificationRequest
    {
        public string Code { get; set; }
        public int? RecStatus { get; set; }
        public int? ApplyTo { get; set; }
        public PagerDto Pager { get; set; }
    }
}
