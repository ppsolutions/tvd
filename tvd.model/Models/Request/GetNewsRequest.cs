﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class GetNewsRequest
    {
        public PagerDto Pager { get; set; }
        public NewsType NewsType { get; set; }
        public int? FilterType { get; set; }
    }
}
