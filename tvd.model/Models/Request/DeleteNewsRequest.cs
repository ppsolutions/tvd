﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Request
{
    public class DeleteNewsRequest
    {
        public int[] NewsIds { get; set; }
        public int UpdatedBy { get; set; }
        public string Token { get; set; }
    }
}
