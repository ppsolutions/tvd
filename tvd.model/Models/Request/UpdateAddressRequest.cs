﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Request
{
    public class UpdateAddressRequest
    {
		public int CurrentAddressId { get; set; }
		public string CurrentAddress { get; set; }
        public int CurrentAddressProvince { get; set; }
        public string CurrentAddressPostalCode { get; set; }

        public bool ShipmentUseCurrentAddress { get; set; }
        public bool ShipmentUseNewAddress { get; set; }
		public int ShipmentAddressId { get; set; }
		public string ShipmentAddress { get; set; }
        public int? ShipmentProvince { get; set; }
        public string ShipmentPostalCode { get; set; }

        public bool InvoiceUseCurrentAddress { get; set; }
        public bool InvoiceUseShipmentAddress { get; set; }
        public bool InvoiceUseNewAddress { get; set; }
		public int InvoiceAddressId { get; set; }
		public string InvoiceAddress { get; set; }
        public int? InvoiceProvince { get; set; }
        public string InvoicePostalCode { get; set; }

        public int PersonId { get; set; }
		public string Token { get; set; }
		public string UpdatedBy { get; set; }
    }
}
