﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Request
{
	public class MergeMemberSelectedRequest
	{
		public int Id { get; set; }
		public int Source { get; set; }
	}
}
