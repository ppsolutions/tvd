﻿namespace tvd.model.Models.Request
{
    public class GetProductInfoRequest
    {
        public PagerDto Pager { get; set; }
        public string Code { get; set; }
    }
}
