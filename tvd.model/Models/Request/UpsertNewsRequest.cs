﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class UpsertNewsRequest
    {
        public string UpdatedBy { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public NewsType NewsType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int Status { get; set; }
        public int? NewsId { get; set; }
        public int? Order { get; set; }
    }
}
