﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class GetApproveRequest
    {
        public PagerDto Pager { get; set; }
        public ApproveType ApproveType { get; set; }
        public RecStatus? RecStatus { get; set; }
    }
}
