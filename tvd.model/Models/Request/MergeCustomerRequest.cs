﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Models.Request
{
    public class MergeCustomerRequest
    {
        public Person CustomerBase { get; set; }
        
        public List<int> MergeCustomerIds { get; set; }
    }
}