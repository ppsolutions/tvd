﻿using System;

namespace tvd.model.Models
{
    public class RegisterRequest
    {
        public bool SelectedPerson { get; set; }
        public bool SelectedCorporate { get; set; }

        public int? PersonTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
		public bool IsSpecificYear { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public int? CorporateTitle { get; set; }
        public string CorporateName { get; set; }
        public string CorporatePhone { get; set; }
        public string CorporateEmail { get; set; }

        public string CurrentAddress { get; set; }
        public int CurrentAddressProvince { get; set; }
        public string CurrentAddressPostalCode { get; set; }

        public bool ShipmentUseCurrentAddress { get; set; }
        public bool ShipmentUseNewAddress { get; set; }
        public string ShipmentAddress { get; set; }
        public int? ShipmentProvince { get; set; }
        public string ShipmentPostalCode { get; set; }

        public bool InvoiceUseCurrentAddress { get; set; }
        public bool InvoiceUseShipmentAddress { get; set; }
        public bool InvoiceUseNewAddress { get; set; }
        public string InvoiceAddress { get; set; }
        public int? InvoiceProvince { get; set; }
        public string InvoicePostalCode { get; set; }

        public string Password { get; set; }

        public int FirstQuestion { get; set; }
        public string FirstAnswer { get; set; }
        public int SecondQuestion { get; set; }
        public string SecondAnswer { get; set; }

        public string Token { get; set; }

		public string SelectedMergeMembers { get; set; }


		public string UpdatedBy { get; set; }
    }
}
