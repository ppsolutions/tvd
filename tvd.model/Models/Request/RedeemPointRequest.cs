﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class RedeemPointRequest
    {
        public int PersonId { get; set; }
        public int RedeemPointId { get; set; }
		public string Description { get; set; }
        public RedeemPointsType Type { get; set; }
        public string UpdatedBy { get; set; }

        public string Token { get; set; }
    }
}
