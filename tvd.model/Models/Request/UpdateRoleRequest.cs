﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.Request
{
    public class UpdateRoleRequest
    {
        public string UpdatedBy { get; set; }
        public int RoleId { get; set; }
        public string Token { get; set; }
    }
}
