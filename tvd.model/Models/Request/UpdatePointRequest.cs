﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Request
{
    public class UpdatePointRequest
    {
        //User who update point
        public string UpdatedBy { get; set; }
        public int PointLevelId { get; set; }
        //User who get point
        public int PersonId { get; set; }
        public int Points { get; set; }
        public ActionType ActionType { get; set; }
		public int ActionTypeValue { get; set; }
        public string Token { get; set; }
		public string Remark { get; set; }
    }
}