﻿namespace tvd.model.Models.Request
{
    public class UpdatePersonStatusRequest
    {
        public int PersonId { get; set; }
        public string UpdatedBy { get; set; }
		public string Token { get; set; }
		public int RecStatus { get; set; }
	}
}