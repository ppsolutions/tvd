﻿using System.Runtime.Serialization;

namespace tvd.model.Models.Request
{
    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public bool IsRemember { get; set; }
    }
}