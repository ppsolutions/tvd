﻿namespace tvd.model.Models.Request
{
    public class UpsertRolePermissionRequest
    {
        public string Token { get; set; }
        public string UpdatedBy { get; set; }
        public int? RoleId { get; set; }
        public string RoleName  { get; set; }
        public bool IsPermitInMemberRegistrationSystem { get; set; }
        public bool IsPermitInApproveSystem { get; set; }
        public bool IsPermitInNewsSystem { get; set; }
        public bool IsPermitInPointSettingSystem { get; set; }
        public bool IsPermitInReportSystem { get; set; }
        public bool IsPermisInAuditReportSystem { get; set; }
        public bool IsPermitInEmployeeManagementSystem { get; set; }
        public bool IsPermitInRoleSystem { get; set; }
        public int[] AccessControl { get; set; }
    }
}
