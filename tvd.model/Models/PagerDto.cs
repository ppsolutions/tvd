﻿namespace tvd.model.Models
{
    public class PagerDto
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}
