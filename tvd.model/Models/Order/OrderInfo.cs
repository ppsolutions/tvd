﻿namespace tvd.model.Models.Order
{
    public class OrderInfo
    {
        public string PromotionId { get; set; }
        public string CampaignId { get; set; }
		public string CampaignCode { get; set; }

        public string UnitPrice { get; set; }
        public int RegularPoint { get; set; }
        public int SilverPoint { get; set; }
        public int GoldPoint { get; set; }
        public int PlatinumPoint { get; set; }

        public int PaymentTypeId { get; set; }
        public int CardTypeId { get; set; }
        public int BankTypeId { get; set; }

        public string Customer { get; set; }
        public string GrandTotal { get; set; }
        public string CustomerName { get; set; }
        public string ShipmentAddress { get; set; }
        public string ShipmentPostalCode { get; set; }
        public string BillingAddress { get; set; }
        public string BillingPostalCode { get; set; }


    }
}
