﻿namespace tvd.model.Models
{
    public class Province
    {
        private readonly int provinceId;
        private readonly string provinceName;

        public Province(int provinceId, string provinceName)
        {
            this.provinceId = provinceId;
            this.provinceName = provinceName;
        }

        public int ProvinceId { get { return this.provinceId; } }
        public string ProvinceName { get { return this.provinceName; } }
    }
}
