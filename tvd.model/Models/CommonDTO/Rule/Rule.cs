﻿using System;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Rule
{
    public class Rule : UpdatedEntity
	{
		public int RuleId { get; set; }
		public string Description { get; set; }
		public int PointLevelId { get; set; }
		public PaymentType PaymentType { get; set; }
		public CardType CardType { get; set; }
		public Bank Bank { get; set; }
		public CalculationType CalculationType { get; set; }
		public int ReceivedPoint { get; set; }
		public bool IsPerItem { get; set; }
		public DateTime ValidFrom { get; set; }
		public DateTime ValidTo { get; set; }
        public string RuleCode { get; set; }
        public bool IsPerOrder { get; set; }
    }
}
