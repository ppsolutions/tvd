﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO
{
    public class UpdateResult
    {
        public bool IsValid { get; set; }
        public string ValidationMessage { get; set; }
        public object Value { get; set; }

        public UpdateResult()
        {
            
        }
        public UpdateResult(bool isValid)
        {
            IsValid = isValid;
        }

        public UpdateResult(bool isValid, string message)
        {
            IsValid = isValid;
            ValidationMessage = message;
        }
    }
}
