﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Member
{
    public class MemberInfo
    {
        public MemberInfo(Person.Person person, int currentActivePoints)
        {
            Person = person;
            CurrentPoints = currentActivePoints;
        }
        public Person.Person Person { get; }
        public int CurrentPoints { get; }
    }
}
