﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Member
{
    public class MemberMapping : UpdateEntity
    {
        public int PersonId { get; set; }
        public string MapWithId { get; set; }
		public string ReferenceId { get; set; }		
		public Source Source { get; set; }
		public int PointTransfered { get; set; }
    }
}
