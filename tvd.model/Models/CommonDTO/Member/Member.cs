﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Models.CommonDTO.Member
{
    /// <summary>
    /// Pesonal information + Points + Address
    /// </summary>
    public class Member
    {
        public Member(Person.Person person, MemberPointDetail memberPointDetail, PersonAddressGroup addressGroup,
            List<PersonQuestion> questions, List<MemberMapping> memberMappings)
        {
            Person = person;
            MemberPointDetail = memberPointDetail;
            AddressGroup = addressGroup;
            Questions = questions;
            MemberMappings = memberMappings;
        }	

        public Person.Person Person { get; }

        public MemberPointDetail MemberPointDetail { get; }

        public PersonAddressGroup AddressGroup { get; }

        public List<PersonQuestion> Questions { get; }

        public List<MemberMapping> MemberMappings { get; }

    }
}