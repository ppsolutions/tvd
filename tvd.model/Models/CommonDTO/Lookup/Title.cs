﻿using System;

namespace tvd.model.Models.CommonDTO.Lookup
{
    public class Title : BaseEntity
    {
        public int TitleId { get; set; }

        public string TitleName { get; set; }

        public bool IsLegalPerson { get; set; }
    }
}