﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Lookup
{
	public class CardType : BaseEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
