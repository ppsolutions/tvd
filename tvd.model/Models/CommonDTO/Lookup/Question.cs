﻿namespace tvd.model.Models.CommonDTO.Lookup
{
    public class Question : BaseEntity
    {
        public int QuestionId { get; set; }
        public string QuestionDetail { get; set; }
    }
}