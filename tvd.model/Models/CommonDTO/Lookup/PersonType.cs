﻿namespace tvd.model.Models.CommonDTO.Lookup
{
    public class PersonType : BaseEntity
    {
        public int PersonTypeId { get; set; }
        public string PersonTypeName { get; set; }
    }
}