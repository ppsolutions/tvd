﻿namespace tvd.model.Models.CommonDTO.Lookup
{
    public class City : BaseEntity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
}