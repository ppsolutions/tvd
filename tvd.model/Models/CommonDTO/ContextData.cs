﻿using System;

namespace tvd.model.Models.CommonDTO
{
    public class ContextData
    {
        public int PersonId { get; set; }
        
        public string Token { get; set; }
    }
}