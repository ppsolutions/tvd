﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace tvd.model.Models.CommonDTO
{
    public class ResultSet<T> : IEnumerable<T>
    {
        private readonly List<T> _items = new List<T>();
        public int TotalItems { get; set; }

        public ResultSet()
        {
            
        }
        public ResultSet(IEnumerable<T> items, int totalItems)
        {
            _items = items.ToList();
            TotalItems = totalItems;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Add(T item)
        {
            _items.Add(item);
        }

        public int Count()
        {
            return _items.Count;
        }

        public List<T> ToList()
        {
            return _items;
        }
    }
}
