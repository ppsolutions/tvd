﻿using Newtonsoft.Json;
using System;
using tvd.model.Helpers;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Audit
{
    public class AuditLog : UpdateEntity
    {
        public Int64 AudiitLogId { get; set; }
        public AuditActionType AuditActionType { get; set; }
        public int ObjectId { get; set; }
		public string ObjectCode { get; set; }
		public object OldValue { get; set; }
		public object NewValue { get; set; }
		public Source Source { get; set; }
        public string Description { get; set; }
		public string AuditActionTypeDescription { get { return EnumHelper.GetEnumDescription(AuditActionType); } }

		public string SourceDescription { get { return EnumHelper.GetEnumDescription(Source); } }

		public object DescriptionObject { get { return JsonConvert.DeserializeObject(Description); } }

		public object OldValueObject { get { return JsonConvert.DeserializeObject(OldValue.ToString()); } }

		public object NewValueObject { get { return JsonConvert.DeserializeObject(NewValue.ToString()); } }

		public string RecStatusDescription
		{
			get
			{
				string text = string.Empty;

				if (RecStatus == RecStatus.ACTIVE)
				{
					text = "อนุมัติแล้ว";
				}
				else if (RecStatus == RecStatus.PENDING)
				{
					text = "รออนุมัติ";
				}

				return text;
			}
		}

		public string MemberURL { get; set; }
    }
}