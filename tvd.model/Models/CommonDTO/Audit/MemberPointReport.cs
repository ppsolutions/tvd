﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Audit
{
    public class MemberPointReport
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonId { get; set; }
		public string PersonCode { get; set; }
        public string PersonName => $"{TitleName} {FirstName} {LastName}";
        public int Points { get; set; }
        public string MemberURL { get; set; }

        public DateTime? ExpiredDate { get; set; }
    }
}
