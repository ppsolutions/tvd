﻿using System;

namespace tvd.model.Models.CommonDTO.Audit
{
    public class ViewHistory
    {
        public int? PersonId { get; set; }
		public string PersonCode { get; set; }
        public int? TitleId { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PageId { get; set; }
        public int? SubPageId { get; set; }
        public string SubPageName { get; set; }
        public DateTime RecCreatedWhen { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeURL { get; set; }
        public string PersonName => $"{TitleName} {FirstName} {LastName}";
        public string MemberURL { get; set; }
    }
}
