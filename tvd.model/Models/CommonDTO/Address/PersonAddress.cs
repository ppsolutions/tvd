﻿using System;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Address
{
    public class PersonAddress : UpdateEntity
    {
        public int PersonId { get; set; }
        public int PersonAddressId { get; set; }
        public AddressType AddressTypeId { get; set; }    
        public string Address1 { get; set; }
        public string Address2 { get; set; }       
        public City City { get; set; }
        public string PostalCode { get; set; }
    }
}