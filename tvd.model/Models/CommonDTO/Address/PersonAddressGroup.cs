﻿using System.Collections.Generic;

namespace tvd.model.Models.CommonDTO.Address
{
    public class PersonAddressGroup
    {
        public PersonAddress CurrentAddress { get; set; }

        public PersonAddress ShipmentAddress { get; set; }

        public PersonAddress InvoiceAddress { get; set; }

    }
}