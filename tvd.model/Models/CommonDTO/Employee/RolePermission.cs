﻿using System;

namespace tvd.model.Models.CommonDTO.Employee
{
    public class RolePermission : UpdateEntity
    {
        public Role Role { get; set; }
        public bool IsPermitInMemberRegistrationSystem { get; set; }
        public bool IsPermitInApproveSystem { get; set; }
        public bool IsPermitInNewsSystem { get; set; }
        public bool IsPermitInPointSettingSystem { get; set; }
        public bool IsPermitInReportSystem { get; set; }
        public bool IsPermisInAuditReportSystem { get; set; }
        public bool IsPermitInEmployeeManagementSystem { get; set; }
        public bool IsPermitInRoleSystem { get; set; }

        public string[] GetRoles()
        {
            return new string[] { Role.RoleName };
        }
    }
}