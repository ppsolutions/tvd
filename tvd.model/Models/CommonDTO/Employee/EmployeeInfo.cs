﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Employee
{
    public class EmployeeInfo
    {

        public string EmployeeCode { get; set; }
        public string PrefixName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
        public string FullName { get; set; }
        public string DetailUrl { get; set; }
        public string Email { get; set; }

        public EmployeeInfo(Employee employee, string detailUrl)
        {
            EmployeeCode = employee.EmployeeCode;
            PrefixName = employee.PrefixName;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            RoleName = employee.EmployeeInRole?.Role?.RoleName ?? "";
            FullName = employee.FullName;
            DetailUrl = detailUrl;
            Email = employee.EmailAddress;
        }

        public EmployeeInfo()
        {
            
        }
    }
}
