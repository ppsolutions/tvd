﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Employee
{
    public class Employee
    {
		public string EmployeeCode { get; set; }
		public string PrefixName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public Gender Gender { get; set; }
		public int Age { get; set; }
		public string JobTitle { get; set; }
		public string ContactNumber { get; set; }
		public string EmailAddress { get; set; }
		public RolePermission EmployeeInRole { get; set; }
        public string FullName  { get; set; }
}
}