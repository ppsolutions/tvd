﻿namespace tvd.model.Models.CommonDTO.Employee
{
    public class Role : UpdateEntity
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}