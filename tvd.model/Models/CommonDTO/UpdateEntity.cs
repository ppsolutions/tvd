﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO
{
    public class UpdateEntity : BaseEntity
    {
        public string RecCreatedBy { get; set; }
        public DateTime RecCreatedWhen { get; set; }
        public string RecModifiedBy { get; set; }
        public DateTime? RecModifiedWhen { get; set; }
    }
}
