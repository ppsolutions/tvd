﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tvd.model.Models.CommonDTO.Products
{
	public class Product : BaseEntity
	{
		public string ProductId { get; set; } 

		public string ProductCode { get; set; }

		public string ProductName { get; set; }
	}
}
