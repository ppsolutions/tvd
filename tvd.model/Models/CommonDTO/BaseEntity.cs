﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO
{
    public class BaseEntity
    {
		public RecStatus RecStatus { get; set; }
    }
}