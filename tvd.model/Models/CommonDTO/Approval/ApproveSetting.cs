﻿namespace tvd.model.Models.CommonDTO.Approval
{
    public class ApproveSetting : UpdateEntity
    {
		public int ApproveSettingId { get; set; }
        public bool IsAutoApproveNewMembership { get; set; }
        public bool IsAutoApproveChangeMembershipInfo { get; set; }
        public bool IsAutoApproveCancelMembership { get; set; }

        public bool IsAutoApproveChangeMembershipPoint { get; set; }
        public bool IsAutoApproveRewardRedeemMembership { get; set; }
        public bool IsAutoApproveLevelUpMembership { get; set; }

        /// <summary>
        /// Set up days before approve point requests expired
        /// </summary>
        public int PointRetensionPeriod { get; set; }

        /// <summary>
        /// Set up days before approve member requests expired
        /// </summary>
        public int MembershipRetensionPeriod { get; set; }

    }
}
