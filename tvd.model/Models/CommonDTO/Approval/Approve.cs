﻿using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Approval
{
    public class Approve : UpdateEntity
    { 
		public long ApproveId { get; set; }
		public ApproveType ApproveType { get; set; }
		public int ObjectId { get; set; }
		public string ObjectValue { get; set; }

		public string ApproveTypeDescription { get { return EnumHelper.GetEnumDescription(ApproveType); } }
    }

	
}
