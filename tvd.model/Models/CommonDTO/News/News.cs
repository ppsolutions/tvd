﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.News
{
    public class News
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? Order { get; set; }
        public NewsType Type { get; set; }
        public NewsStatus Status { get; set; }
        public DateTime? RecModifiedWhen { get; set; }
        public string RecModifiedBy { get; set; }
        public DateTime RecCreatedWhen { get; set; }
        public string RecCreatedBy { get; set; }
        public string NewsTypeDescription { get; set; }
    }
}
