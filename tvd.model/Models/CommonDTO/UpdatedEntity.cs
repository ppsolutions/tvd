﻿using System;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Models.CommonDTO
{
    public class UpdatedEntity : BaseEntity
    {
        public PersonLite RecCreatedBy { get; set; }
        public DateTime RecCreatedWhen { get; set; }
        public int? RecModifiedBy { get; set; }
        public DateTime? RecModifiedWhen { get; set; }
        
    }
}