﻿using tvd.model.Models.CommonDTO.Employee;

namespace tvd.model.Models.CommonDTO.Point
{
    public class MemberPointLevel
    {
        public int PersonId { get; set; }
		public string PersonCode { get; set; }
        public int Point { get; set; }
        public int PointLevelId { get; set; }
        public string LevelName { get; set; }
        public EmployeeInfo Source { get; set; }
    }
}