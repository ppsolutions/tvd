﻿namespace tvd.model.Models.CommonDTO.MemberPoint
{
    public class MemberPointDetail
    {
        public int PendingPoint { get; set; }
        public int CurrentPoint { get; set; }
        public int NearlyExpirePoint { get; set; }
        public string LevelName { get; set; }
    }
}