﻿namespace tvd.model.Models.CommonDTO.Point
{
    public class PointLevel : UpdateEntity
    {
        public int PointLevelId { get; set; }
        public string LevelName { get; set; }
        public int RequiredPoint { get; set; }
		public double RequiredPaid { get; set; }
		public double PricePerPoint { get; set; }        
    }
}