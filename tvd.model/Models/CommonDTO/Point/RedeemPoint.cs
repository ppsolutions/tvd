﻿namespace tvd.model.Models.CommonDTO.Point
{
    public class RedeemPoint : UpdateEntity
	{
		public int RedeemPointId { get; set; }
		public string Description { get; set; }
		public int RequiredPointOnly { get; set; }
		public int RequiredPoint { get; set; }
		public decimal RequiredMoney { get; set; }
        public string ProductCode { get; set; }
    }
}
