﻿using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Point
{
    public class RedeemPointTransaction
    {
        public int TransactionId { get; set; }
        public int PersonId { get; set; }
		public string PersonCode { get; set; }
        public RedeemPoint RedeemPoint { get; set; }
        public EmployeeInfo RecCreatedBy { get; set; }
        public RedeemPointsType Type { get; set; }
        public RecStatus RecStatus { get; set; }
    }
}
