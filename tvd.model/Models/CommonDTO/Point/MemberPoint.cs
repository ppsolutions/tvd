﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Point
{
    public class MemberPoint
    {
        public int PersonId { get; set; }
        
        public int Point { get; set; }
        public string RecCreatedBy { get; set; }

        public DateTime RecCreatedWhen { get; set; }

        public DateTime? RecExpiredWhen { get; set; }

        public RecStatus RecStatus { get; set; }
        
        public int PendingPoint { get; set; }
        
        public UpdatePointType Type { get; set; }
        
        public string ProductCode { get; set; }

		public string Remark { get; set; }
    }
}