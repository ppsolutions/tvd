﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Point
{
	public class GamificationPoint : UpdateEntity
	{
		public int GamificationPointId { get; set; }
		public string GamificationCode { get; set; }
		public string Description { get; set; }
		public ApplyType ApplyTo { get; set; }
		public int IncreasePoint { get; set; }
		public DateTime ValidFrom { get; set; }
		public DateTime ValidTo { get; set; }
	}
}
