﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Point
{
	public class UploadMemberPoint
	{
		public int MemberId { get; set; }
		public string MemberCode { get; set; }
		public string MemberName { get; set; }
		public int Point { get; set; }
		public ActionType ActionType { get; set; }
		public string ActionTypeValue { get; set; }
		public string Remark { get; set; }
		public string Note { get; set; }
	}
}
