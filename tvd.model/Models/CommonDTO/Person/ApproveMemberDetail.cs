﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Person
{
    public class ApproveMemberDetail
    {
        public Person Person { get; set; }
        public string Password { get; set; }
        public PersonAddressGroup AddressGroup { get; set; }
        public UpdateMember Type { get; set; }
        public EmployeeInfo Source { get; set; }
        public string Description { get; set; }
        public List<PersonQuestion> Questions { get; set; }
    }
}
