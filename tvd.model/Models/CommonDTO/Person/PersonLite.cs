﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tvd.model.Models.CommonDTO.Person
{
    public class PersonLite
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => FirstName + " " + LastName;
        public string RoleName { get; set; }

    }
}