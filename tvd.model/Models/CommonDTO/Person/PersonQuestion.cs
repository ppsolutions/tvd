﻿using tvd.model.Models.CommonDTO.Lookup;

namespace tvd.model.Models.CommonDTO.Person
{
    public class PersonQuestion : UpdateEntity
    {
        public int PersonQuestionId { get; set; }
        public int PersonId { get; set; }
        public Question Question { get; set; }
        public string Answer { get; set; }
    }
}