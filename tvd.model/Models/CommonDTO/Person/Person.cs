﻿using System;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.Enum;

namespace tvd.model.Models.CommonDTO.Person
{
    public class Person : UpdateEntity
    {
		public Source Source { get; set; }

		public int PersonId { get; set; }

		public string ReferenceId { get; set; }

        public string Code { get; set; } 
        
        public bool? IsLegalPerson { get; set; }
        
        public int TitleId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public DateTime? BirthDate { get; set; }
        
		public bool? IsSpecificYear { get; set; }

        public string ContactNumber { get; set; }
        
        public string UserName { get; set; }

        public string Password { get; set; }

        public string EncryptedPassword { get; set; }
        
        public string EmailAddress { get; set; }

        public string FullName => FirstName + " " + LastName;


		public string SourceString
		{
			get
			{
				return EnumHelper.GetEnumDescription(Source);
			}
		}

		public string SearchId
		{
			get
			{
				if(this.PersonId == 0)
				{
					return this.ReferenceId;
				}
				else
				{
					return this.PersonId.ToString();
				}
			}
		}
    }
}