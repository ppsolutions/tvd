﻿namespace tvd.model.Models.Employee
{
    public class UpdateEmployeeDto
    {
        public int EmployeeId { get; set; }
        public string Token { get; set; }
        public int UpdatedBy { get; set; }
    }
}
