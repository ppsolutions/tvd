﻿namespace tvd.model.Models.Employee
{
    public class EmployeeDto
    {
        public string Keyword { get; set; }
        public int? RoleId { get; set; }
        public PagerDto Pager { get; set; }
    }
}
