﻿namespace tvd.model.Models
{
    public class EmployeeUpSertDto
    {
        public string EmployeeCode { get; set; }
        public string Token { get; set; }
        public int RoleId { get; set; }
        public string UpdatedBy { get; set; }
    }
}
