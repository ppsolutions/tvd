﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class RewardInfo
    {
        public int RewardId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public RecStatus RecStatus { get; set; }
		public int PointOnly { get; set; }
		public int Point { get; set; }
        public decimal Money { get; set; }
    }
}
