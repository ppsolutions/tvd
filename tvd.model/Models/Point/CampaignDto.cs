﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class CampaignDto
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public int? RecStatus { get; set; }
        public PromotionType PromotionType { get; set; }
        public PagerDto Pager { get; set; }
        public int By { get; set; }
        public int? PromoRuleId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
