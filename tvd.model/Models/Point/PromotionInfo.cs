﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class PromotionInfo
    {
        public int? PromotionId { get; set; }
        public string Id { get; set; }
        public string Code { get; set; }
        public string PromotionName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public RecStatus RecStatus { get; set; }
        public PromotionType PromotionType { get; set; }
    }
}
