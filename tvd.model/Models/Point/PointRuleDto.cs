﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class PointRuleDto
    {
        public string Code { get; set; }
        public int? RecStatus { get; set; }
        public int? CalculateType { get; set; }
        public PagerDto Pager { get; set; }
    }
}
