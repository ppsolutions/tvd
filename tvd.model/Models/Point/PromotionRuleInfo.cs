﻿using System;

namespace tvd.model.Models.Point
{
    public class PromotionRuleInfo
    {
        public int PromoRuleId { get; set; }
        public string RuleCode { get; set; }
        public string Description { get; set; }
        public string PointLevelName { get; set; }
        public string PaymentTypeName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PointLevelId { get; set; }
        public int PaymentTypeId { get; set; }
        public int CardTypeId { get; set; }
        public int BankTypeId { get; set; }
        public int CalculationType { get; set; }
        public bool IsPerItem { get; set; }
        public bool IsPerOrder { get; set; }
        public DateTime RuleStartDate { get; set; }
        public DateTime RuleEndDate { get; set; }
        public int ReceivedPoint { get; set; }
        public int RuleOrder { get; set; }
        public long RuleOrderNumber { get; set; }

    }
}
