﻿namespace tvd.model.Models.Point
{
    public class PointRuleUpdateStatusDto
    {
        public int RuleId { get; set; }
        public int RecStatus { get; set; }
        public string UserId { get; set; }
    }
}
