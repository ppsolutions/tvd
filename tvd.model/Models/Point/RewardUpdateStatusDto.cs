﻿namespace tvd.model.Models.Point
{
    public class RewardUpdateStatusDto
    {
        public int RewardId { get; set; }
        public int RecStatus { get; set; }
        public string UserId { get; set; }
    }
}
