﻿using System;

namespace tvd.model.Models.Point
{
    public class PointRuleInfo
    {
        public int RuleId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int PointLevelId { get; set; }
        public int PaymentTypeId { get; set; }
        public int? CardTypeId { get; set; }
        public int? BankId { get; set; }
        public int CalculationType { get; set; }
        public int ReceivedPoint { get; set; }
        public bool IsPerItem { get; set; }
        public bool IsPerOrder { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public Enum.RecStatus RecStatus { get; set; }
    }
}
