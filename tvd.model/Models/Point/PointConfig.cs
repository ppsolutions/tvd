﻿using System;

namespace tvd.model.Models.Point
{
    public class PointConfig
    {
        public DateTime? ExpireDate { get; set; }
        public decimal? MinPurchase { get; set; }
		public int? ExpireYear { get; set; }

	}
}
