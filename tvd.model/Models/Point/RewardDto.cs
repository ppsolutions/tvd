﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class RewardDto
    {
        public string Code { get; set; }
		public string Description { get; set; }
        public int? RecStatus { get; set; }
        public PagerDto Pager { get; set; }
    }
}
