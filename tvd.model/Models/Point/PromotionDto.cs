﻿using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class PromotionDto
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public int? RecStatus { get; set; }
        public PromotionType PromotionType { get; set; }
        public PagerDto Pager { get; set; }
        public string By { get; set; }
        public int? PromoRuleId { get; set; }
    }
}
