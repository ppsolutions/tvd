﻿using System;
using tvd.model.Models.Enum;

namespace tvd.model.Models.Point
{
    public class PromotionRuleDto
    {
        public string PromoId { get; set; }
        public int[] RuleIds { get; set; }
        public bool SelectedAll { get; set; }
        public string By { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public PromotionType PromotionType { get; set; }
        public int? PromoRuleId { get; set; }
        public int? RuleOrder { get; set; }
    }
}
