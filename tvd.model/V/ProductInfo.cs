﻿namespace tvd.model.V
{
    public class ProductInfo
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Active { get; set; }
        public string UnitPrice { get; set; }
        public int RegularPoint { get; set; }
        public int SilverPoint { get; set; }
        public int GoldPoint { get; set; }
        public int PlatinumPoint { get; set; }
    }
}