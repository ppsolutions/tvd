﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories.Interfaces
{
    public interface INewsRepository
    {
        IEnumerable<News> GetNews(NewsType type, int? filterType);
		News GetNews(int newsId);
        int InsertNews(News news);
        void UpdateNews(News news);
        void DeleteNews(int[] ids, int recModifiedBy);
        NewsStatus[] GetNewsStatuses();
        ResultSet<News> GetNews(NewsType type, int? filterType, int pageSize, int pageNumber);
        IEnumerable<News> GetNews(NewsType type);
    }
}
