﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Address;

namespace tvd.model.Repositories
{
    public interface IPersonAddressRepository
    {
        IEnumerable<PersonAddress> GetPersonAddress(int peresonId);
        void InsertPersonAddress(PersonAddress address);
        void UpdatePersonAddress(PersonAddress address);
    }
}
