﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;
using tvd.model.V;

namespace tvd.model.Repositories.Interfaces
{
    public interface IPointRepository
    {
        PointLevel GetMemberPointLevel(int memberId);
        void UpdatePointLevelExpireDate(DateTime? expireDate);
        IEnumerable<PointLevel> GetPointLevel();
        PointConfig GetPointConfig();
        MemberPoint GetMemberPoint(int memberPointId);
        IEnumerable<MemberPoint> GetMemberPoints(int memberId, bool isIncludePendingPoints);
        int InsertMemberPoint(MemberPoint memberPoint);
        void UpdateMemberPointStatus(int memberPointId, RecStatus status, string updatedBy);
        void UpsertMemberPointLevel(int personId, int pointLevel);
        IEnumerable<RedeemPointsType> GetRedeemPointsTypes();
        void UpdatePointLevel(PointLevel level);
        RedeemPoint GetRedeemPoint(int redeemPointId);
        RedeemPoint GetRedeemPoint(string redeemPointCode);
        void InsertRedeemPoint(RedeemPoint redeemPoint);
        void UpdateRedeemPoint(RedeemPoint redeemPoint);
        RewardInfo UpsertReward(RewardInfoDto rewardInfoDto, string by);
        int UpdateRewardStatus(RewardUpdateStatusDto rewardUpdateStatusDto, string by);
        IList<RewardInfo> FetchAllReward();
        ResultSet<RewardInfo> FetchRewards(RewardDto rewardDto);
        RewardInfo FetchRewardInfo(int rewardId);
        ResultSet<PointRuleInfo> FetchPointRule(PointRuleDto pointRuleDto);
        PointRuleInfo UpsertPointRule(PointRuleInfoDto pointRuleInfoDto, string by);
        int UpdatePointRuleStatus(PointRuleUpdateStatusDto pointRuleUpdateStatusDto, string by);
        PointRuleInfo FetchPointRuleInfo(int ruleId);
        IList<PromotionInfo> FetchViewPromotion(PromotionDto promotionDto);
        PromotionInfo FetchPromotionInfo(string id);
        IList<ProductInfo> FetchAllProductInPromotion(PromotionDto promotionDto);
        ResultSet<ProductInfo> GetProductInfos(string code, int? pageSize, int? pageNumber);
        int AddRuleInPromotion(PromotionRuleDto promotionRuleDto);
        IList<PromotionRuleInfo> FetchAllRuleInPromotion(PromotionDto promotionDto);
        int RemoveRuleInPromotion(PromotionDto promotionDto);
        IList<PromotionInfo> FetchPromotion(PromotionDto promotionDto);
        IList<PromotionRuleInfo> FetchRuleInPromotion(PromotionRuleDto promotionRuleDto);
        IList<PromotionInfo> FetchViewCampaign(PromotionDto promotionDto);
        PromotionInfo FetchCampaignInfo(string code);
        IList<PromotionInfo> FetchAllPromotionInCampaign(PromotionDto promotionDto);
        int UpdateCampaignDate(CampaignDto campaignDto);
        int InsertRedeemPointTransaction(RedeemPointTransaction transaction);
        IList<PromotionInfo> FetchRuleUsed(int ruleId);
        IEnumerable<MemberPoint> GetMemberPointsApi(int memberId, bool isIncludePendingPoints);
        string InsertMemberPointLevelApi(MemberPoint memberPoint, int updateType);
        string InsertMemberPointApi(MemberPoint memberPoint, int updateType);
        IEnumerable<RedeemPoint> GetRedeemPointsApi(string productCode);
        RedeemPoint GetRedeemPointInfoApi(int redeemPointId);
        string InsertRedeemPointTransactionApi(RedeemPointTransaction transaction);
        PointLevel GetMemberPointLevelApi(int memberId);
        IList<Models.CommonDTO.Rule.Rule> FetchRuleUsed();
        int UpdateRuleOrder(PromotionRuleDto promotionRuleDto);
        void UpdatePointConfig(int? expireYear, decimal? minPurchase, string by);
		IEnumerable<MemberPoint> GetMemberPointsApiByMemberCode(string memberCode, bool isIncludePendingPoints);

	}
}
