﻿using System.Collections.Generic;
using tvd.model.Models;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Employee;

namespace tvd.model.Repositories.Interfaces
{
    public interface IEmployeeRepository
    {
		ResultSet<Employee> GetEmployees(string criteria, int? roleId, int pageSize, int pageNumber);
		Employee GetEmployee(string employeeCode);
		Employee Authenticate(string username, string password);
        IList<AccessControl> GetAccessControlUser(string employeeCode);
        IList<AccessControl> GetAccessControlRole(int roleId);
    }
}
