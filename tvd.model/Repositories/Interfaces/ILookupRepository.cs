﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.CommonDTO.Products;
using tvd.model.Models.Enum;
using tvd.model.V;

namespace tvd.model.Repositories
{
    public interface ILookupRepository
    {
        IEnumerable<City> GetCities();
        IEnumerable<Title> GetTitles();
        IEnumerable<Question> GetQuestions();
        IEnumerable<AddressType> GetAddressTypes();
        IEnumerable<PersonType> GetPersonTypes();
        IEnumerable<PointLevel> GetPointLevels();
		IEnumerable<Bank> GetBanks();
		IEnumerable<CardType> GetCardTypes();
		IEnumerable<PaymentType> GetPaymentType();
        IEnumerable<ApplyType> GetApplyTypes();
		IEnumerable<Product> GetProducts();
		Product GetProduct(string productCode);
        ProductInfo GetProductInfoFromView(string code);
        IEnumerable<ProductInfo> GetProductInfoFromName(string name);
        Product GetProductApi(string productCode);
        DateTime? GetPointLevelExpireDate();
    }
}
