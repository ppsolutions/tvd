﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories.Interfaces
{
    public interface IApproveRepository
    {
		void Insert(Approve approve);
        void UpdateAppoveStatus(int approveId, string updatedBy, RecStatus recStatus);

        Models.CommonDTO.Approval.Approve GetApprove(int approveId);

        ResultSet<Approve> GetApproveList(int? approveId, ApproveType approveType, RecStatus? recStatus, int rowPerPage = 1, int pageNumber = 1);
		ApproveSetting GetApproveSetting();
		void InsertApproveSetting(ApproveSetting approveSetting);
		void UpdateApproveSetting(ApproveSetting approveSetting);
        string InsertApi(Approve approve);
    }
}
