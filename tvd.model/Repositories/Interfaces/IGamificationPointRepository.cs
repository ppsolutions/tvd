﻿using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories.Interfaces
{
    public interface IGamificationPointRepository
	{
		ResultSet<GamificationPoint> GetGamificationPoints(string code, int? applyTo, int? recStatus, int? pageSize, int? pageNumber);
		GamificationPoint GetGamificationPoint(int gamificationPointId);
		GamificationPoint GetGamificationPoint(string gamificationCode);
		void InsertGamificationPoint(GamificationPoint gamificationPoint);
		void UpdateGamificationPoint(GamificationPoint gamificationPoint);
        void UpdateGamificationStatus(int gamificationPointId, RecStatus status, string updatedBy);
        GamificationPoint GetGamificationInfoApi(string code);

    }
}
