﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Rule;

namespace tvd.model.Repositories.Interfaces
{
    public interface IRuleRepository
	{
		IEnumerable<Rule> GetRules();
		Rule GetRule(int ruleId);
		void InsertRule(Rule rule);
		void UpdateRule(Rule rule);
        IEnumerable<Rule> GetRulesByCodeApi(string code, int codeType);

    }
}
