﻿
using System.Collections.Generic;
using tvd.model.Models.Order;

namespace tvd.model.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        int CheckOrderId(int orderId);
        IList<OrderInfo> FetchOrderInfo(int orderId);
        OrderInfo FetchSpecificOrderInfo(int orderId);
    }
}
