﻿using System;
using System.Collections.Generic;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using API_MODEL = tvd.model.Api.Models.Member;

namespace tvd.model.Repositories.Interfaces
{
    public interface IMemberRepository
    {
        IEnumerable<Person> GetMembers(string name, string idCard, string phoneNumber, string code);

        ResultSet<Person> GetMembers(string name, string idCard, string phoneNumber, string code, int pageSize, int pageNumber);
        IEnumerable<Person> GetMembers(string name);
        Person GetMember(object id, Source memberSource);
        int CreateUser(Person user);
        int UpdateUser(Person user);
        void UpdateUserStatus(int personId, RecStatus recStatus, string updateBy);
        void UpdateUserPassword(int personId, string password, string updateBy);
        List<MemberMapping> GetMergeUsers(int personId);
        void MergeMember(MemberMapping memberMapping, int updateBy);
        string UpdateUserPasswordApi(int personId, string oldPassword, string newPassword, int updateBy);
        Tuple<string, int> MemberUpsertApi(API_MODEL.MemberDto dto, int updateBy);
        API_MODEL.PersonInfo FetchMember(string objectId);
		int FetchMemberId(string memberCode);
		API_MODEL.PersonInfo FetchMemberByMemberCode(string memberCode);

		string MemberUpdatePasswordApi(int personId, string password, string newPassword, int updateBy);
    }
}