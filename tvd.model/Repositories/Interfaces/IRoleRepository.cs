﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories.Interfaces
{
    public interface IRoleRepository
    {
        int InsertRole(Role role);
        void UpdateRole(Role role);
        IEnumerable<Role> GetRoles();
        void InsertRolePermission(RolePermission rolePermission);
        void UpdateRolePermission(RolePermission rolePermission);
        RolePermission GetRolePermission(int roleId);
        void UpdateRoleToUser(string employeeCode, int? roleId, RecStatus? status, string updatedBy);
        void AssignAccessControlToUser(string employeeCode, int[] controlIds, string updatedBy);
        void AssignAccessControlToRole(int roleId, int[] controlIds, string updatedBy);
    }
}
