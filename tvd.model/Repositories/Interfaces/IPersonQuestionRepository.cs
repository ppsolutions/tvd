﻿using System.Collections.Generic;
using tvd.model.Models.CommonDTO.Person;

namespace tvd.model.Repositories
{
    public interface IPersonQuestionRepository
    {
        IEnumerable<PersonQuestion> GetPersonQuestions(int personId);
        void InsertPersonQuestion(PersonQuestion question);
        void UpdatePersonQuestion(PersonQuestion question);
    }
}
