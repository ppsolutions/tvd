﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Models.Point;
using tvd.model.Repositories.Interfaces;
using tvd.model.V;

namespace tvd.model.Repositories
{
    public class PointRepository : IPointRepository
    {
        private readonly IDBHelper _dbHelper;

        public PointRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public PointLevel GetMemberPointLevel(int memberId)
        {
            PointLevel pointLevel = new PointLevel();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.member_point_level_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            pointLevel.PointLevelId = reader.GetInt32("point_level_id");
                                                            pointLevel.LevelName = reader.GetString("point_level_name");
                                                            pointLevel.RequiredPoint = reader.GetInt32("required_point");
                                                            pointLevel.RequiredPaid = reader.GetDouble("required_paid");
                                                            pointLevel.PricePerPoint = reader.GetNullableDouble("price_per_point") ?? 0;
                                                            pointLevel.RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status"));
                                                            pointLevel.RecCreatedBy = reader.GetString("rec_created_by");
                                                            pointLevel.RecCreatedWhen = reader.GetDateTime("rec_created_when");
                                                            pointLevel.RecModifiedBy = reader.GetNullableString("rec_modified_by");
                                                            pointLevel.RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? DateTime.MinValue;
                                                        }
                                                    },
                                                     new PPSolution.Core.Models.Parameter("@person_id", memberId));
            return pointLevel;
        }

        public IEnumerable<PointLevel> GetPointLevel()
        {
            var pointLevels = new List<PointLevel>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.point_level_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var pointLevel = new PointLevel()
                                                            {
                                                                PointLevelId = reader.GetInt32("point_level_id"),
                                                                LevelName = reader.GetString("point_level_name"),
                                                                RequiredPoint = reader.GetInt32("required_point"),
                                                                RequiredPaid = reader.GetDouble("required_paid"),
                                                                PricePerPoint = reader.GetDouble("price_per_point"),
                                                                RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status")),
                                                                RecCreatedBy = reader.GetString("rec_created_by"),
                                                                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                                                                RecModifiedBy = reader.GetNullableString("rec_modified_by"),
                                                                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? DateTime.MinValue
                                                            };
                                                            pointLevels.Add(pointLevel);
                                                        }
                                                    });
            return pointLevels;
        }
		
        public MemberPoint GetMemberPoint(int memberPointId)
        {
            MemberPoint mb = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.member_point_fetch_by_id_v1"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        mb = GeteMemberPointEntity(reader);
                    }
                }
                , new PPSolution.Core.Models.Parameter("@member_point_id", memberPointId));
            return mb;
        }

        public IEnumerable<MemberPoint> GetMemberPoints(int memberId, bool isIncludePendingPoints)
        {
            var points = new List<MemberPoint>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.member_point_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var point = GeteMemberPointEntity(reader);
                                                            points.Add(point);
                                                        }
                                                    }
                                                     , new PPSolution.Core.Models.Parameter("@person_id", memberId)
                                                    , new PPSolution.Core.Models.Parameter("@is_include_pending", isIncludePendingPoints));
            return points;
        }

        public int InsertMemberPoint(MemberPoint memberPoint)
        {
            var memberPointId = 0;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.member_point_insert_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            memberPointId = reader.GetInt32("member_point_id");
                                                        }
                                                    }
                                                    , new Parameter("@person_id", memberPoint.PersonId)
                                                    , new Parameter("@point", memberPoint.Point)
                                                    , new Parameter("@expire_date", memberPoint.RecExpiredWhen.HasValue ? (object)memberPoint.RecExpiredWhen.Value : DBNull.Value)
                                                    , new Parameter("@rec_status", (int)memberPoint.RecStatus)
                                                    , new Parameter("@type", (int)memberPoint.Type)
                                                    , new Parameter("@product_code", memberPoint.ProductCode)
													, new Parameter("@remark", memberPoint.Remark)
													, new Parameter("@by", memberPoint.RecCreatedBy));
            return memberPointId;
        }

        public void UpdateMemberPointStatus(int memberPointId, RecStatus status, string updatedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.member_point_update_status_v1"
                , new Parameter("@member_point_id", memberPointId)
                , new Parameter("@rec_status", (int)status)
                , new Parameter("@by", updatedBy));
        }

        public void UpsertMemberPointLevel(int personId, int pointLevel)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
               , "dbo.member_point_level_upsert_v1"
               , new Parameter("@person_id", personId)
               , new Parameter("@point_level_id", (int)pointLevel));

        }

        public IEnumerable<RedeemPointsType> GetRedeemPointsTypes()
        {
            return Enum.GetValues(typeof(RedeemPointsType)).Cast<RedeemPointsType>();
        }

        public void UpdatePointLevel(PointLevel level)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                        , "dbo.point_level_update_v1"
                                        , new PPSolution.Core.Models.Parameter("@point_level_id", level.PointLevelId)
                                        , new PPSolution.Core.Models.Parameter("@required_point", level.RequiredPoint == 0 ? DBNull.Value : (object)level.RequiredPoint)
                                        , new PPSolution.Core.Models.Parameter("@price_per_point", Math.Abs(level.PricePerPoint) <= 0 ? DBNull.Value : (object)level.PricePerPoint)
                                        , new PPSolution.Core.Models.Parameter("@by", level.RecModifiedBy));
        }

        private MemberPoint GeteMemberPointEntity(PPSolution.Core.Models.EntityDataReader reader)
        {
            var point = new MemberPoint
            {
                PersonId = reader.GetInt32("person_id"),
                Point = reader.GetInt32("point"),
                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                RecCreatedBy = reader.GetString("rec_created_by"),
                RecExpiredWhen = reader.GetNullableDateTime("expire_date"),
                RecStatus = (RecStatus)reader.GetInt16("rec_status"),
                Type = (UpdatePointType)reader.GetInt32("update_type"),
                ProductCode = reader.GetNullableString("product_code")
            };
            return point;
        }

        //public ResultSet<RedeemPoint> GetRedeemPoints(RewardDto rewardDto)
        //{
        //    var redeemPoints = new ResultSet<RedeemPoint>();

        //    _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
        //        , "dbo.redeem_point_fetch_v3"
        //        , (reader) =>
        //        {
        //            while (reader.Read())
        //            {
        //                var redeemPoint = GetRedeemPointEntity(reader);
        //                redeemPoints.Add(redeemPoint);

        //                if (redeemPoints.TotalItems == 0)
        //                {
        //                    redeemPoints.TotalItems = reader.GetInt32("max_rows");
        //                }
        //            }
        //        }
        //        , new PPSolution.Core.Models.Parameter("@code", rewardDto.Code)
        //        , new PPSolution.Core.Models.Parameter("@status", rewardDto.RecStatus)
        //        , new Parameter("@page_size", rewardDto.Pager?.PageSize)
        //        , new Parameter("@page_number", rewardDto.Pager?.PageNumber));
        //    return redeemPoints;
        //}

        public RedeemPoint GetRedeemPoint(int redeemPointId)
        {
            RedeemPoint redeemPoint = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.redeem_point_fetch_by_redeem_point_id_v1"
                                                    , (reader) =>
                {
                    if (reader.Read())
                    {
                        redeemPoint = GetRedeemPointEntity(reader);
                    }
                },
                                                    new PPSolution.Core.Models.Parameter("@redeem_point_id", redeemPointId));
            return redeemPoint;
        }

        public RedeemPoint GetRedeemPoint(string redeemPointCode)
        {
            RedeemPoint redeemPoint = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.redeem_point_fetch_by_redeem_point_code_v1"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        redeemPoint = GetRedeemPointEntity(reader);
                    }
                },
                new PPSolution.Core.Models.Parameter("@code", redeemPointCode));
            return redeemPoint;
        }

        public void InsertRedeemPoint(RedeemPoint redeemPoint)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.redeem_point_insert_v1"
                                                    , new PPSolution.Core.Models.Parameter("@code", redeemPoint.ProductCode)
                                                    , new PPSolution.Core.Models.Parameter("@description", redeemPoint.Description)
                                                    , new PPSolution.Core.Models.Parameter("@required_point_only", redeemPoint.RequiredPointOnly)
                                                    , new PPSolution.Core.Models.Parameter("@required_point", redeemPoint.RequiredPoint)
                                                    , new PPSolution.Core.Models.Parameter("@required_money", redeemPoint.RequiredMoney)
                                                    , new PPSolution.Core.Models.Parameter("@by", redeemPoint.RecCreatedBy));
        }

        public void UpdateRedeemPoint(RedeemPoint redeemPoint)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.redeem_point_update_v1"
                                                    , new PPSolution.Core.Models.Parameter("@redeem_point_id", redeemPoint.RedeemPointId)
                                                    , new PPSolution.Core.Models.Parameter("@code", redeemPoint.ProductCode)
                                                    , new PPSolution.Core.Models.Parameter("@description", redeemPoint.Description)
                                                    , new PPSolution.Core.Models.Parameter("@required_point_only", redeemPoint.RequiredPointOnly)
                                                    , new PPSolution.Core.Models.Parameter("@required_point", redeemPoint.RequiredPoint)
                                                    , new PPSolution.Core.Models.Parameter("@required_money", redeemPoint.RequiredMoney)
                                                    , new PPSolution.Core.Models.Parameter("@by", redeemPoint.RecModifiedBy));
        }

        public RewardInfo UpsertReward(RewardInfoDto rewardInfoDto, string by)
        {
            RewardInfo returnRewardInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[redeem_point_save_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            var result = reader.GetInt32("redeem_point_id");
                                                            if (result > 0)
                                                            {
                                                                if (returnRewardInfo == null)
                                                                    returnRewardInfo = new RewardInfo();

                                                                returnRewardInfo.RewardId = result;
                                                                returnRewardInfo.Code = reader.GetString("code");
                                                                returnRewardInfo.Description = reader.GetString("description");
                                                                returnRewardInfo.PointOnly = reader.GetInt32("required_point_only");
                                                                returnRewardInfo.Point = reader.GetInt32("required_point");
                                                                returnRewardInfo.Money = reader.GetDecimal("required_money");
                                                                returnRewardInfo.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                            }
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@redeem_point_id", rewardInfoDto.RewardId)
                                                    , new PPSolution.Core.Models.Parameter("@code", rewardInfoDto.Code)
                                                    , new PPSolution.Core.Models.Parameter("@description", rewardInfoDto.Description)
                                                    , new PPSolution.Core.Models.Parameter("@required_point_only", rewardInfoDto.PointOnly)
                                                    , new PPSolution.Core.Models.Parameter("@required_point", rewardInfoDto.Point)
                                                    , new PPSolution.Core.Models.Parameter("@required_money", rewardInfoDto.Money)
                                                    , new PPSolution.Core.Models.Parameter("@rec_status", rewardInfoDto.RecStatus)
                                                    , new PPSolution.Core.Models.Parameter("@by", by)
                                                    );

            return returnRewardInfo;
        }

        public int UpdateRewardStatus(RewardUpdateStatusDto rewardUpdateStatusDto, string by)
        {
            var returnValue = -99;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[redeem_point_update_status_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            returnValue = reader.GetInt32("redeem_point_id");
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@redeem_point_id", rewardUpdateStatusDto.RewardId)
                                                    , new PPSolution.Core.Models.Parameter("@status", rewardUpdateStatusDto.RecStatus)
                                                    , new PPSolution.Core.Models.Parameter("@by", by)
                                                    );

            return returnValue;
        }

        public IList<RewardInfo> FetchAllReward()
        {
            IList<RewardInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[redeem_point_fetch_all_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<RewardInfo>();

                                                                var rewardInfo = new RewardInfo();
                                                                rewardInfo.RewardId = reader.GetInt32("redeem_point_id");
                                                                rewardInfo.Code = reader.GetString("code");
                                                                rewardInfo.Description = reader.GetString("description");
                                                                rewardInfo.PointOnly = reader.GetInt32("required_point_only");
                                                                rewardInfo.Point = reader.GetInt32("required_point");
                                                                rewardInfo.Money = reader.GetDecimal("required_money");
                                                                rewardInfo.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                                results.Add(rewardInfo);
                                                            }
                                                        }
                                                    }
                                                    );

            return results;
        }

        public ResultSet<RewardInfo> FetchRewards(RewardDto rewardDto)
        {
            var results = new ResultSet<RewardInfo>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[redeem_point_fetch_v3]",
                                                    (reader) =>
                                                    {
                                                        if (reader == null) return;
                                                        while (reader.Read())
                                                        {
                                                            var rewardInfo = new RewardInfo();
                                                            rewardInfo.RewardId = reader.GetInt32("redeem_point_id");
                                                            rewardInfo.Code = reader.GetString("code");
                                                            rewardInfo.Description = reader.GetString("description");
                                                            rewardInfo.PointOnly = reader.GetInt32("required_point_only");
                                                            rewardInfo.Point = reader.GetInt32("required_point");
                                                            rewardInfo.Money = reader.GetDecimal("required_money");
                                                            rewardInfo.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                            results.Add(rewardInfo);

                                                            if (results.TotalItems == 0)
                                                            {
                                                                results.TotalItems = reader.GetInt32("max_rows");
                                                            }
                                                        }

                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@code", rewardDto.Code)
                                                    , new PPSolution.Core.Models.Parameter("@status", rewardDto.RecStatus)
                                                    , new Parameter("@page_size", rewardDto.Pager?.PageSize)
                                                    , new Parameter("@page_number", rewardDto.Pager?.PageNumber)
                                                    );

            return results;
        }

        public RewardInfo FetchRewardInfo(int rewardId)
        {
            RewardInfo rewardInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[redeem_point_fetch_info_by_id_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            if (rewardInfo == null)
                                                                rewardInfo = new RewardInfo();

                                                            rewardInfo.RewardId = reader.GetInt32("redeem_point_id");
                                                            rewardInfo.Code = reader.GetString("code");
                                                            rewardInfo.Description = reader.GetString("description");
                                                            rewardInfo.PointOnly = reader.GetInt32("required_point_only");
                                                            rewardInfo.Point = reader.GetInt32("required_point");
                                                            rewardInfo.Money = reader.GetDecimal("required_money");
                                                            rewardInfo.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@redeem_point_id", rewardId)
                                                    );

            return rewardInfo;
        }

        public ResultSet<PointRuleInfo> FetchPointRule(PointRuleDto pointRuleDto)
        {
            var results = new ResultSet<PointRuleInfo>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[rule_fetch_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {

                                                                var pointRuleInfo = new PointRuleInfo();
                                                                pointRuleInfo.RuleId = reader.GetInt32("rule_id");
                                                                pointRuleInfo.Code = reader.GetString("rule_code");
                                                                pointRuleInfo.CalculationType = reader.GetByte("calculation_type");
                                                                pointRuleInfo.PointLevelId =
                                                                    reader.GetInt32("point_level_id");
                                                                pointRuleInfo.PaymentTypeId =
                                                                    reader.GetInt32("payment_type_id");
                                                                pointRuleInfo.CardTypeId =
                                                                    reader.GetNullableInt32("card_type_id");
                                                                pointRuleInfo.BankId =
                                                                    reader.GetNullableInt32("bank_id");
                                                                pointRuleInfo.Description = reader.GetString("description");
                                                                pointRuleInfo.ReceivedPoint = reader.GetInt32("received_point");
                                                                pointRuleInfo.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                                pointRuleInfo.ValidFrom =
                                                                    reader.GetNullableDateTime("valid_from");
                                                                pointRuleInfo.ValidTo =
                                                                    reader.GetNullableDateTime("valid_to");
                                                                pointRuleInfo.IsPerItem = reader.GetBoolean("is_per_item");
                                                                pointRuleInfo.IsPerOrder = reader.GetBoolean("is_per_order");

                                                                results.Add(pointRuleInfo);

                                                                if (results.TotalItems == 0)
                                                                {
                                                                    results.TotalItems = reader.GetInt32("max_rows");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@code", pointRuleDto.Code)
                                                    , new Parameter("@status", pointRuleDto.RecStatus)
                                                    , new Parameter("@calculationType", pointRuleDto.CalculateType)
                                                    , new Parameter("@page_size", pointRuleDto.Pager?.PageSize)
                                                    , new Parameter("@page_number", pointRuleDto.Pager?.PageNumber)
                                                    );

            return results;
        }

        public PointRuleInfo UpsertPointRule(PointRuleInfoDto pointRuleInfoDto, string by)
        {
            PointRuleInfo pointRuleInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[rule_upsert_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            var result = reader.GetInt32("rule_id");
                                                            if (result > 0)
                                                            {
                                                                if (pointRuleInfo == null)
                                                                    pointRuleInfo = new PointRuleInfo();

                                                                pointRuleInfo.RuleId = result;
                                                                pointRuleInfo.Code = reader.GetString("rule_code");
                                                                pointRuleInfo.Description = reader.GetString("description");
                                                                pointRuleInfo.PointLevelId = reader.GetInt32("point_level_id");
                                                                pointRuleInfo.PaymentTypeId = reader.GetInt32("payment_type_id");
                                                                pointRuleInfo.CardTypeId = reader.GetNullableInt32("card_type_id");
                                                                pointRuleInfo.BankId = reader.GetNullableInt32("bank_id");
                                                                pointRuleInfo.CalculationType = reader.GetByte("calculation_type");
                                                                pointRuleInfo.ReceivedPoint = reader.GetInt32("received_point");
                                                                pointRuleInfo.IsPerItem = reader.GetBoolean("is_per_item");
                                                                pointRuleInfo.IsPerOrder = reader.GetBoolean("is_per_order");
                                                                pointRuleInfo.ValidFrom = reader.GetNullableDateTime("valid_from");
                                                                pointRuleInfo.ValidTo = reader.GetNullableDateTime("valid_to");
                                                                pointRuleInfo.RecStatus =
                                                                    (RecStatus)reader.GetInt16("rec_status");
                                                            }
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@ruleId", pointRuleInfoDto.RuleId)
                                                    , new PPSolution.Core.Models.Parameter("@code", pointRuleInfoDto.Code)
                                                    , new PPSolution.Core.Models.Parameter("@description", pointRuleInfoDto.Description)
                                                    , new PPSolution.Core.Models.Parameter("@point_level_id", pointRuleInfoDto.PointLevelId)
                                                    , new PPSolution.Core.Models.Parameter("@payment_type_id", pointRuleInfoDto.PaymentTypeId)
                                                    , new PPSolution.Core.Models.Parameter("@card_type_id", pointRuleInfoDto.CardTypeId)
                                                    , new PPSolution.Core.Models.Parameter("@bank_id", pointRuleInfoDto.BankId)
                                                    , new PPSolution.Core.Models.Parameter("@calculation_type", pointRuleInfoDto.CalculationType)
                                                    , new PPSolution.Core.Models.Parameter("@received_point", pointRuleInfoDto.ReceivedPoint)
                                                    , new PPSolution.Core.Models.Parameter("@is_per_item", pointRuleInfoDto.IsPerItem)
                                                    , new PPSolution.Core.Models.Parameter("@is_per_order", pointRuleInfoDto.IsPerOrder)
                                                    , new PPSolution.Core.Models.Parameter("@valid_from", pointRuleInfoDto.ValidFrom)
                                                    , new PPSolution.Core.Models.Parameter("@valid_to", pointRuleInfoDto.ValidTo)
                                                    , new PPSolution.Core.Models.Parameter("@rec_status", (int)pointRuleInfoDto.RecStatus)
                                                    , new PPSolution.Core.Models.Parameter("@by", by)
                                                    );

            return pointRuleInfo;
        }

        public int UpdatePointRuleStatus(PointRuleUpdateStatusDto pointRuleUpdateStatusDto, string by)
        {
            var returnValue = -99;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[rule_update_status_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            returnValue = reader.GetInt32("ruleId");
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@ruleId", pointRuleUpdateStatusDto.RuleId)
                                                    , new PPSolution.Core.Models.Parameter("@status", pointRuleUpdateStatusDto.RecStatus)
                                                    , new PPSolution.Core.Models.Parameter("@by", by)
                                                    );

            return returnValue;
        }

        public PointRuleInfo FetchPointRuleInfo(int ruleId)
        {
            PointRuleInfo pointRuleInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[rule_fetch_by_rule_id_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            if (pointRuleInfo == null)
                                                                pointRuleInfo = new PointRuleInfo();

                                                            pointRuleInfo.RuleId = reader.GetInt32("rule_id");
                                                            pointRuleInfo.Code = reader.GetString("rule_code");
                                                            pointRuleInfo.Description = reader.GetString("description");
                                                            pointRuleInfo.PointLevelId = reader.GetInt32("point_level_id");
                                                            pointRuleInfo.PaymentTypeId = reader.GetInt32("payment_type_id");
                                                            pointRuleInfo.CardTypeId = reader.GetNullableInt32("card_type_id");
                                                            pointRuleInfo.BankId = reader.GetNullableInt32("bank_id");
                                                            pointRuleInfo.CalculationType = reader.GetByte("calculation_type");
                                                            pointRuleInfo.ReceivedPoint = reader.GetInt32("received_point");
                                                            pointRuleInfo.IsPerItem = reader.GetBoolean("is_per_item");
                                                            pointRuleInfo.IsPerOrder = reader.GetBoolean("is_per_order");
                                                            pointRuleInfo.ValidFrom = reader.GetNullableDateTime("valid_from");
                                                            pointRuleInfo.ValidTo = reader.GetNullableDateTime("valid_to");
                                                            pointRuleInfo.RecStatus =
                                                                (RecStatus)reader.GetInt16("rec_status");
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@ruleId", ruleId)
                                                    );

            return pointRuleInfo;
        }

        public IList<PromotionInfo> FetchViewPromotion(PromotionDto promotionDto)
        {
            IList<PromotionInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_promotion_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionInfo>();

                                                                var promotionInfo = new PromotionInfo();
                                                                promotionInfo.Id = reader.GetString("ID");
                                                                promotionInfo.Code = reader.GetString("CODE");
                                                                promotionInfo.PromotionName = reader.GetString("PROMOTION_NAME");
                                                                promotionInfo.RecStatus = (RecStatus)(reader.GetNullableString("ENABLE_FLAG") == "Y" ? 1 : 0);
                                                                promotionInfo.CreateDate = reader.GetNullableDateTime("CREATEDATE");
                                                                results.Add(promotionInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@code", promotionDto.Code)
                                                    , new Parameter("@status", promotionDto.RecStatus)
                                                    , new Parameter("@ruleId", promotionDto.PromoRuleId)
                                                    );

            return results;
        }

        public IList<PromotionInfo> FetchViewCampaign(PromotionDto promotionDto)
        {
            IList<PromotionInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_campaign_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionInfo>();

                                                                var promotionInfo = new PromotionInfo();
                                                                promotionInfo.Id = reader.GetDouble("ID").ToString();
                                                                promotionInfo.Code = reader.GetString("CODE");
                                                                promotionInfo.PromotionName = reader.GetString("NAME");
                                                                promotionInfo.RecStatus = (RecStatus)(reader.GetNullableString("ENABLE_FLAG") == "Y" ? 1 : 0);
                                                                promotionInfo.CreateDate = reader.GetNullableDateTime("CREATEDATE");
                                                                results.Add(promotionInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@code", promotionDto.Code)
                                                    , new Parameter("@status", promotionDto.RecStatus)
                                                    );

            return results;
        }

        public PromotionInfo FetchPromotionInfo(string id)
        {
            PromotionInfo promotionInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_promotion_info_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            if (promotionInfo == null)
                                                                promotionInfo = new PromotionInfo();

                                                            promotionInfo.Id = reader.GetString("ID");
                                                            promotionInfo.Code = reader.GetString("CODE");
                                                            promotionInfo.PromotionName = reader.GetString("PROMOTION_NAME");
                                                            promotionInfo.RecStatus = (RecStatus)(reader.GetString("ENABLE_FLAG") == "Y" ? 1 : 0);
                                                            promotionInfo.CreateDate = reader.GetNullableDateTime("CREATEDATE");
                                                        }
                                                    }
                                                    , new Parameter("@id", id)
                                                    );

            return promotionInfo;
        }

        public PromotionInfo FetchCampaignInfo(string id)
        {
            PromotionInfo promotionInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_campaign_info_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null && reader.Read())
                                                        {
                                                            if (promotionInfo == null)
                                                                promotionInfo = new PromotionInfo();

                                                            promotionInfo.Id = reader.GetString("ID");
                                                            promotionInfo.Code = reader.GetString("CODE");
                                                            promotionInfo.PromotionName = reader.GetString("NAME");
                                                            promotionInfo.RecStatus = (RecStatus)(reader.GetString("ENABLE_FLAG") == "Y" ? 1 : 0);
                                                            promotionInfo.CreateDate = reader.GetNullableDateTime("CREATEDATE");
                                                            promotionInfo.StartDate = reader.GetNullableDateTime("start_date");
                                                            promotionInfo.EndDate = reader.GetNullableDateTime("end_date");
                                                        }
                                                    }
                                                    , new Parameter("@code", id)
                                                    );

            return promotionInfo;
        }

        public IList<ProductInfo> FetchAllProductInPromotion(PromotionDto promotionDto)
        {
            IList<ProductInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_all_product_in_promotion_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<ProductInfo>();

                                                                var productInfo = new ProductInfo();
                                                                productInfo.Id = reader.GetString("PRODUCTINFO");
                                                                productInfo.UnitPrice = reader.GetString("UNIT_PRICE");
                                                                productInfo.Name = reader.GetString("NAME_TH");

                                                                results.Add(productInfo);
                                                            }
                                                        }
                                                    }
                                                    , new PPSolution.Core.Models.Parameter("@id", promotionDto.Id)
                                                    );

            return results;
        }

        public IList<PromotionInfo> FetchAllPromotionInCampaign(PromotionDto promotionDto)
        {
            IList<PromotionInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_view_fetch_all_promo_in_campaign_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionInfo>();

                                                                var info = new PromotionInfo();
                                                                info.Code = reader.GetString("CODE");
                                                                info.PromotionName = reader.GetString("PROMOTION_NAME");
                                                                info.RecStatus = (RecStatus)(reader.GetString("ENABLE_FLAG") == "Y" ? 1 : 0);
                                                                info.CreateDate = reader.GetNullableDateTime("CREATEDATE");
                                                                results.Add(info);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@campaignInfo", promotionDto.Id)
                                                    );

            return results;
        }

        public ResultSet<ProductInfo> GetProductInfos(string code, int? pageSize, int? pageNumber)
        {
            var results = new ResultSet<ProductInfo>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[sp_view_fetch_all_product_v1]",
                (reader) =>
                {
                    while (reader.Read())
                    {
                        var productInfo = new ProductInfo();
                        productInfo.Code = reader.GetString("PROMOTION_CODE");
                        try
						{
							productInfo.Id = reader.GetString("PRODUCTINFO");
						}
						catch
						{
							productInfo.Id = Convert.ToString(reader.GetDouble("PRODUCTINFO"));
						}

						try
						{
							productInfo.UnitPrice = reader.GetDecimal("UNIT_PRICE").ToString();
						}
						catch
						{
							productInfo.UnitPrice = reader.GetDouble("UNIT_PRICE").ToString();
						}

						productInfo.Name = reader.GetString("NAME_TH");

                        results.Add(productInfo);

                        if (results.TotalItems == 0)
                        {
                            results.TotalItems = Convert.ToInt32(reader.GetValue("max_rows"));
                        }
                    }

                }
                , new Parameter("@code", code)
                , new Parameter("@page_number", pageNumber)
                , new Parameter("@page_size", pageSize));

            return results;
        }

        public int AddRuleInPromotion(PromotionRuleDto promotionRuleDto)
        {
            var execResult = -99;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[promotion_rule_insert_v1]",
                (reader) =>
                {
                    if (reader != null && reader.Read())
                    {
                        execResult = reader.GetInt32(0);
                    }
                }
                , new Parameter("@promoId", promotionRuleDto.PromoId)
                , new Parameter("@promoType", promotionRuleDto.PromotionType)
                , new Parameter("@allSelected", promotionRuleDto.SelectedAll)
                , new Parameter("@rules", CreateSqlDataRecords(promotionRuleDto.RuleIds))
                , new Parameter("@startDate", promotionRuleDto.StartDate)
                , new Parameter("@endDate", promotionRuleDto.EndDate)
                , new Parameter("@by", promotionRuleDto.By)
            );

            return execResult;
        }

        public DataTable CreateSqlDataRecords(IEnumerable<int> datas)
        {
            var dt = new DataTable();
            dt.Columns.Add("rule_id", typeof(int));

            foreach (var data in datas)
            {
                var dr = dt.NewRow();
                dr[0] = data;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public IList<PromotionRuleInfo> FetchAllRuleInPromotion(PromotionDto promotionDto)
        {
            IList<PromotionRuleInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[promotion_rule_list_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionRuleInfo>();

                                                                var promotionRuleInfo = new PromotionRuleInfo();
                                                                promotionRuleInfo.PromoRuleId = reader.GetInt32("promo_rule_id");
                                                                promotionRuleInfo.Description = reader.GetString("description");
                                                                promotionRuleInfo.RuleCode = reader.GetString("rule_code");
                                                                promotionRuleInfo.PointLevelName = reader.GetNullableString("point_level_name");
                                                                promotionRuleInfo.PaymentTypeName = reader.GetNullableString("payment_type_name");
                                                                promotionRuleInfo.StartDate = reader.GetNullableDateTime("start_date");
                                                                promotionRuleInfo.EndDate = reader.GetNullableDateTime("end_date");
                                                                promotionRuleInfo.PointLevelId = reader.GetInt32("point_level_id");
                                                                promotionRuleInfo.PaymentTypeId = reader.GetInt32("payment_type_id");
                                                                promotionRuleInfo.CardTypeId = reader.GetNullableInt32("card_type_id") ?? 0;
                                                                promotionRuleInfo.BankTypeId = reader.GetNullableInt32("bank_id") ?? 0;
                                                                promotionRuleInfo.CalculationType = reader.GetByte("calculation_type");
                                                                promotionRuleInfo.IsPerItem = reader.GetBoolean("is_per_item");
                                                                promotionRuleInfo.IsPerOrder = reader.GetBoolean("is_per_order");
                                                                promotionRuleInfo.RuleStartDate = reader.GetDateTime("valid_from");
                                                                promotionRuleInfo.RuleEndDate = reader.GetDateTime("valid_to");
                                                                promotionRuleInfo.ReceivedPoint = reader.GetInt32("received_point");
                                                                promotionRuleInfo.RuleOrder = reader.GetInt32("rule_order");
                                                                promotionRuleInfo.RuleOrderNumber = reader.GetInt64("rule_order_number");

                                                                results.Add(promotionRuleInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@promoCode", promotionDto.Id)
                                                    );

            return results;
        }

        public int RemoveRuleInPromotion(PromotionDto promotionDto)
        {
            var execResult = -99;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[promotion_rule_remove_v1]",
                (reader) =>
                {
                    if (reader != null && reader.Read())
                    {
                        execResult = reader.GetInt32(0);
                    }
                }
                , new Parameter("@promoRuleId", promotionDto.PromoRuleId)
                , new Parameter("@by", promotionDto.By)
            );

            return execResult;
        }

        public IList<PromotionInfo> FetchPromotion(PromotionDto promotionDto)
        {
            IList<PromotionInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[promotion_fetch_rule_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionInfo>();

                                                                var promotionInfo = new PromotionInfo();
                                                                promotionInfo.PromotionId = reader.GetNullableInt32("promo_id");
                                                                promotionInfo.Code = reader.GetString("promo_code");
                                                                promotionInfo.PromotionName = reader.GetString("promo_description");
                                                                promotionInfo.RecStatus = (RecStatus)reader.GetInt32("rec_status");
                                                                promotionInfo.CreateDate = reader.GetNullableDateTime("rec_created_when");
                                                                results.Add(promotionInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@code", promotionDto.Code)
                                                    , new Parameter("@status", promotionDto.RecStatus)
                                                    , new Parameter("@ruleId", promotionDto.PromoRuleId)
                                                    , new Parameter("@promotionType", promotionDto.PromotionType)
                                                    );

            return results;
        }

        public IList<PromotionRuleInfo> FetchRuleInPromotion(PromotionRuleDto promotionRuleDto)
        {
            IList<PromotionRuleInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[promotion_fetch_rule_detail_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionRuleInfo>();

                                                                var promotionRuleInfo = new PromotionRuleInfo();

                                                                promotionRuleInfo.Description = reader.GetString("description");
                                                                promotionRuleInfo.RuleCode = reader.GetString("rule_code");

                                                                results.Add(promotionRuleInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@promoId", promotionRuleDto.PromoId)
                                                    );

            return results;
        }

        public PointConfig GetPointConfig()
        {
            PointConfig pointConfig = new PointConfig();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[point_config_fetch_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            pointConfig.ExpireDate = reader.GetNullableDateTime("expire_date");
                                                            pointConfig.MinPurchase = reader.GetNullableDecimal("min_purchase");
															pointConfig.ExpireYear = reader.GetNullableInt32("expire_year");
														}
                                                    });
            return pointConfig;
        }

        public void UpdatePointLevelExpireDate(DateTime? expireDate)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                 , "dbo.point_level_expire_update_v1"
                                 , new PPSolution.Core.Models.Parameter("@expire_date", expireDate.HasValue ? (object)expireDate.Value : DBNull.Value));
        }

        public void UpdatePointConfig(int? expireYear, decimal? minPurchase, string by)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                 , "[dbo].[point_config_update_v1]"
                                 , new Parameter("@expire_year", expireYear)
                                 , new Parameter("@min_purchase", minPurchase)
                                 , new Parameter("@by", by)
                                 );
        }

        public int UpdateCampaignDate(CampaignDto campaignDto)
        {
            var execResult = -99;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[promotion_savedate_v1]",
                (reader) =>
                {
                    if (reader != null && reader.Read())
                    {
                        execResult = reader.GetInt32(0);
                    }
                }
                , new Parameter("@code", campaignDto.Code)
                , new Parameter("@startDate", campaignDto.StartDate)
                , new Parameter("@endDate", campaignDto.EndDate)
                , new Parameter("@by", campaignDto.By)
            );

            return execResult;
        }

        public int InsertRedeemPointTransaction(RedeemPointTransaction transaction)
        {
            var transactionId = 0;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.redeem_point_transaction_insert_v1"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        transactionId = reader.GetInt32("transaction_id");
                    }
                }
                , new Parameter("@person_id", transaction.PersonId)
                , new Parameter("@code", transaction.RedeemPoint.ProductCode)
                , new Parameter("@required_point", transaction.Type == RedeemPointsType.POINT_ONLY ? transaction.RedeemPoint.RequiredPointOnly : transaction.RedeemPoint.RequiredPoint)
                , new Parameter("@required_money", transaction.RedeemPoint.RequiredMoney)
                , new Parameter("@required_point_type_id", (int)transaction.Type)
                , new Parameter("@rec_status", (int)transaction.RecStatus)
                , new Parameter("@by", transaction.RecCreatedBy.EmployeeCode));
            return transactionId;
        }

        public IList<PromotionInfo> FetchRuleUsed(int ruleId)
        {
            IList<PromotionInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[promotion_fetch_rule_used_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<PromotionInfo>();

                                                                var promotionInfo = new PromotionInfo();
                                                                promotionInfo.PromotionId = reader.GetNullableInt32("promo_id");
                                                                promotionInfo.Code = reader.GetString("promo_code");
                                                                promotionInfo.PromotionName = reader.GetString("promo_description");
                                                                promotionInfo.RecStatus = (RecStatus)reader.GetInt32("rec_status");
                                                                promotionInfo.CreateDate = reader.GetNullableDateTime("rec_created_when");
                                                                promotionInfo.PromotionType = (PromotionType)reader.GetInt16("promo_type");
                                                                results.Add(promotionInfo);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@ruleId", ruleId)
                                                    );

            return results;
        }

        private RedeemPoint GetRedeemPointEntity(EntityDataReader reader)
        {
            var redeemPoint = new RedeemPoint()
            {
                RedeemPointId = reader.GetInt32("redeem_point_id"),
                ProductCode = reader.GetString("code"),
                Description = reader.GetString("description"),
                RequiredPointOnly = reader.GetInt32("required_point_only"),
                RequiredPoint = reader.GetInt32("required_point"),
                RequiredMoney = reader.GetDecimal("required_money"),
                RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status")),
                RecCreatedBy = reader.GetString("rec_created_by"),
                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                RecModifiedBy = reader.GetNullableString("rec_modified_by"),
                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? DateTime.MinValue
            };
            return redeemPoint;
        }

        public IEnumerable<MemberPoint> GetMemberPointsApi(int memberId, bool isIncludePendingPoints)
        {
            var points = new List<MemberPoint>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_point_fetch_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var point = new MemberPoint
                                                            {
                                                                PersonId = reader.GetInt32("person_id"),
                                                                Point = reader.GetInt32("total_active_point"),
                                                                PendingPoint = reader.GetInt32("total_pending_point"),
                                                            };
                                                            points.Add(point);
                                                        }
                                                    }
                                                     , new Parameter("@person_id", memberId)
                                                    , new Parameter("@is_include_pending", isIncludePendingPoints));
            return points;
        }

        public string InsertMemberPointLevelApi(MemberPoint memberPoint, int updateType)
        {
            var result = "999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_insert_point_level_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            result = reader.GetString(0);
                                                        }
                                                    }
                                                    , new Parameter("@person_id", memberPoint.PersonId)
                                                    , new Parameter("@point", memberPoint.Point)
                                                    , new Parameter("@expire_date", memberPoint.RecExpiredWhen)
                                                    , new Parameter("@rec_status", memberPoint.RecStatus)
                                                    , new Parameter("@type", updateType)
                                                    , new Parameter("@by", memberPoint.RecCreatedBy));
            return result;
        }

        public string InsertMemberPointApi(MemberPoint memberPoint, int updateType)
        {
            var result = "999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_insert_point_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            result = reader.GetString(0);
                                                        }
                                                    }
                                                    , new Parameter("@person_id", memberPoint.PersonId)
                                                    , new Parameter("@point", memberPoint.Point)
                                                    , new Parameter("@expire_date", memberPoint.RecExpiredWhen)
                                                    , new Parameter("@rec_status", memberPoint.RecStatus)
                                                    , new Parameter("@type", updateType)
                                                    , new Parameter("@by", memberPoint.RecCreatedBy));
            return result;
        }

        public IEnumerable<RedeemPoint> GetRedeemPointsApi(string productCode)
        {
            var redeemPoints = new List<RedeemPoint>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_redeem_point_fetch_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var redeemPoint = new RedeemPoint()
                                                            {
                                                                RedeemPointId = reader.GetInt32("redeem_point_id"),
                                                                ProductCode = reader.GetString("code"),
                                                                Description = reader.GetString("description"),
                                                                RequiredPointOnly = reader.GetInt32("required_point_only"),
                                                                RequiredPoint = reader.GetInt32("required_point"),
                                                                RequiredMoney = reader.GetDecimal("required_money"),
                                                                RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status"))
                                                            };
                                                            redeemPoints.Add(redeemPoint);
                                                        }
                                                    }
                                                    , new Parameter("@code", productCode)
                                                    );
            return redeemPoints;
        }

        public RedeemPoint GetRedeemPointInfoApi(int redeemPointId)
        {
            var redeemPoint = new RedeemPoint();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_redeem_point_fetch_by_redeem_point_by_id_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            redeemPoint.RedeemPointId = reader.GetInt32("redeem_point_id");
                                                            redeemPoint.ProductCode = reader.GetString("code");
                                                            redeemPoint.Description = reader.GetString("description");
                                                            redeemPoint.RequiredPointOnly = reader.GetInt32("required_point_only");
                                                            redeemPoint.RequiredPoint = reader.GetInt32("required_point");
                                                            redeemPoint.RequiredMoney = reader.GetDecimal("required_money");
                                                            redeemPoint.RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status"));
                                                        }
                                                    },
                                                    new Parameter("@redeem_point_id", redeemPointId));
            return redeemPoint;
        }

        public string InsertRedeemPointTransactionApi(RedeemPointTransaction transaction)
        {
            var result = "999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[sp_api_redeem_point_transaction_insert_v1]"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        result = reader.GetString(0);
                    }
                }
                , new Parameter("@person_id", transaction.PersonId)
                , new Parameter("@code", transaction.RedeemPoint.ProductCode)
                , new Parameter("@required_point", transaction.Type == RedeemPointsType.POINT_ONLY ? transaction.RedeemPoint.RequiredPointOnly : transaction.RedeemPoint.RequiredPoint)
                , new Parameter("@required_money", transaction.RedeemPoint.RequiredMoney)
                , new Parameter("@required_point_type_id", (int)transaction.Type)
                , new Parameter("@rec_status", (int)transaction.RecStatus)
                , new Parameter("@by", transaction.RecCreatedBy.EmployeeCode));
            return result;
        }

        public PointLevel GetMemberPointLevelApi(int memberId)
        {
            PointLevel pointLevel = new PointLevel();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_point_level_fetch_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            pointLevel.PointLevelId = reader.GetInt32("point_level_id");
                                                            pointLevel.LevelName = reader.GetString("point_level_name");
                                                            pointLevel.RequiredPoint = reader.GetInt32("required_point");
                                                            pointLevel.RequiredPaid = reader.GetDouble("required_paid");
                                                            pointLevel.PricePerPoint = reader.GetNullableDouble("price_per_point") ?? 0;
                                                            pointLevel.RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status"));
                                                            pointLevel.RecCreatedBy = reader.GetString("rec_created_by");
                                                            pointLevel.RecCreatedWhen = reader.GetDateTime("rec_created_when");
                                                            pointLevel.RecModifiedBy = reader.GetNullableString("rec_modified_by");
                                                            pointLevel.RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? DateTime.MinValue;
                                                        }
                                                    },
                                                     new Parameter("@person_id", memberId));
            return pointLevel;
        }

        public IList<Models.CommonDTO.Rule.Rule> FetchRuleUsed()
        {
            IList<Models.CommonDTO.Rule.Rule> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[promotion_rule_list_active_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<Models.CommonDTO.Rule.Rule>();

                                                                var rule = new Models.CommonDTO.Rule.Rule();
                                                                rule.RuleId = reader.GetInt32("rule_id");
                                                                rule.RuleCode = reader.GetString("rule_code");

                                                                results.Add(rule);
                                                            }
                                                        }
                                                    }
                                                    );

            return results;
        }

        public int UpdateRuleOrder(PromotionRuleDto promotionRuleDto)
        {
            var result = -99;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[promotion_rule_update_order_v1]"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        result = reader.GetInt32(0);
                    }
                }
                , new Parameter("@promoRuleId", promotionRuleDto.PromoRuleId)
                , new Parameter("@ruleOrder", promotionRuleDto.RuleOrder)
                , new Parameter("@by", promotionRuleDto.By));
            return result;
        }

		public IEnumerable<MemberPoint> GetMemberPointsApiByMemberCode(string memberCode, bool isIncludePendingPoints)
		{
			var points = new List<MemberPoint>();

			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "[dbo].[sp_api_member_point_fetch_by_member_code_v1]"
													, (reader) =>
													{
														while (reader.Read())
														{
															var point = new MemberPoint
															{
																Code = reader.GetString("code"),
																Point = reader.GetInt32("total_active_point"),
																PendingPoint = reader.GetInt32("total_pending_point"),
															};
															points.Add(point);
														}
													}
													 , new Parameter("@memberCode", memberCode)
													, new Parameter("@is_include_pending", isIncludePendingPoints));
			return points;
		}
	}
}