﻿using PPSolution.Core.Models;
using System.Collections.Generic;
using System.Configuration;
using tvd.model.Helpers;
using tvd.model.Repositories.Interfaces;
using tvd.model.Models.Order;
using System;

namespace tvd.model.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IDBHelper _dbHelper;

        public OrderRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public int CheckOrderId(int orderId)
        {
            var existOrderId = -1;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[sp_api_order_check_already_exists_by_order_id_v1]"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        existOrderId = Convert.ToInt32(reader.GetDouble(0));
                    }
                }
                , new Parameter("@orderId", orderId));
            return existOrderId;
        }

        public IList<OrderInfo> FetchOrderInfo(int orderId)
        {
            IList<OrderInfo> results = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_api_order_fetch_info_by_order_id_v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (results == null)
                                                                    results = new List<OrderInfo>();

                                                                var info = new OrderInfo();
                                                                info.GrandTotal = reader.GetString("GRANDTOTAL");
                                                                info.PromotionId = reader.GetString("PROMOTIONINFO");
                                                                info.CampaignId = reader.GetString("CAMPAIGN_ID");
																info.CampaignCode = reader.GetNullableString("CAMPAIGN_CODE") ?? string.Empty;
                                                                results.Add(info);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@orderId", orderId));

            return results;
        }

        public OrderInfo FetchSpecificOrderInfo(int orderId)
        {
            OrderInfo info = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                                                    "[dbo].[sp_api_order_fetch_specific_order_info__v1]",
                                                    (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if (info == null)
                                                                    info = new OrderInfo();

                                                                info.GrandTotal = reader.GetDouble("GRANDTOTAL").ToString();
                                                                info.Customer = reader.GetDouble("CUSTOMER").ToString();
                                                                info.BillingAddress = reader.GetString("billAddress");
                                                                info.ShipmentAddress = reader.GetString("shipAddress");
                                                                info.CustomerName = reader.GetString("CUSTOMERNAME");
                                                                info.ShipmentPostalCode = reader.GetString("shipmentPostalCode");
                                                                info.BillingPostalCode = reader.GetString("billingPostalCode");
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@orderId", orderId));

            return info;
        }
    }
}