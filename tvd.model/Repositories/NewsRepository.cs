﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.News;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly IDBHelper _dbHelper;

        public NewsRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<News> GetNews(NewsType type, int? filterType)
        {
            var newsList = new List<News>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.news_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var news = GetNewsEntity(reader);
                                                            newsList.Add(news);
                                                        }
                                                    },
                                                     new Parameter("@news_type", (int)type),
                                                     new Parameter("@filter_type", filterType)

                                                    );
            return newsList;
        }

        public int InsertNews(News news)
        {
            var newsId = 0;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.news_insert_v1"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        newsId = reader.GetNullableInt32("news_id") ?? 0;
                    }
                }
                , new Parameter("@title", news.Title)
                , new Parameter("@content", news.Content)
                , new Parameter("@news_type", (int)news.Type)
                , new Parameter("@rec_status", (int)news.Status)
                , new Parameter("@start_date", news.StartDate)
                , new Parameter("@end_date", news.EndDate)
                , new Parameter("@by", news.RecCreatedBy));
            return newsId;
        }

        public void UpdateNews(News news)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.news_update_v1"
                , new Parameter("@news_id", news.NewsId)
                , new Parameter("@title", news.Title)
                , new Parameter("@content", news.Content)
                , new Parameter("@order", news.Order)
                , new Parameter("@rec_status", (int)news.Status)
                , new Parameter("@news_type", (int)news.Type)
                , new Parameter("@start_date", news.StartDate)
                , new Parameter("@end_date", news.EndDate)
                , new Parameter("@by", news.RecModifiedBy));
        }

		public NewsStatus[] GetNewsStatuses()
        {
            return Enum.GetValues(typeof(NewsStatus)).Cast<NewsStatus>().ToArray();
        }

        public ResultSet<News> GetNews(NewsType type, int? filterType, int pageSize, int pageNumber)
        {
            var newsList = new ResultSet<News>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.news_fetch_v1"
                , (reader) =>
                {
                    while (reader.Read())
                    {
                        var news = GetNewsEntity(reader);
                        newsList.Add(news);
                        if (newsList.TotalItems == 0)
                        {
                            newsList.TotalItems = reader.GetInt32("max_rows");
                        }
                    }
                },
                new Parameter("@news_type", type == NewsType.All ? null : (int?)type),
                new Parameter("@filter_type", filterType),
                new Parameter("@page_size", pageSize),
                new Parameter("@page_number", pageNumber)

            );
            return newsList;
        }

        public void DeleteNews(int[] ids, int recModifiedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.news_delete_v1"
                , new Parameter("@ids", string.Join(",", ids))
                , new Parameter("@by", recModifiedBy));
        }

		public News GetNews(int newsId)
		{
			News news = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.news_fetch_by_news_id_v1"
													, (reader) =>
													{
														if(reader.Read())
														{
															news = GetNewsEntity(reader);
														}
													},
													 new Parameter("@news_id", newsId));
			return news;
		}

        private News GetNewsEntity(EntityDataReader reader)
        {
            var news = new News()
            {
                NewsId = reader.GetInt32("news_id"),
                Title = WebUtility.HtmlDecode(reader.GetString("title")),
                Content = WebUtility.HtmlDecode(reader.GetString("content")),
                Type = (NewsType)reader.GetByte("news_type"),
                StartDate = reader.GetDateTime("start_date"),
                EndDate = reader.GetDateTime("end_date"),
                Status = (NewsStatus)reader.GetInt16("rec_status"),
                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                RecCreatedBy = reader.GetString("rec_created_by"),
                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when"),
                RecModifiedBy = reader.GetNullableString("rec_modified_by"),
                Order = reader.GetNullableInt32("news_order")
            };
            return news;
        }

        public IEnumerable<News> GetNews(NewsType type)
        {
            List<News> newsList = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_news_fetch_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            if(newsList == null)
                                                            {
                                                                newsList = new List<News>();
                                                            }

                                                            var news = new News() {
                                                                NewsId = reader.GetInt32("news_id"),
                                                                Title = reader.GetNullableString("title"),
                                                                Type = (NewsType)reader.GetByte("news_type"),
                                                                NewsTypeDescription = reader.GetNullableString("news_type_desc"),
                                                                Content = reader.GetNullableString("content"),
                                                                StartDate = reader.GetDateTime("start_date"),
                                                                EndDate = reader.GetDateTime("end_date"),
                                                                Status = (NewsStatus)reader.GetInt16("rec_status"),
                                                                RecCreatedBy = reader.GetNullableString("rec_created_by"),
                                                                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                                                                RecModifiedBy = reader.GetNullableString("rec_modified_by"),
                                                                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when"),
                                                            };
                                                            newsList.Add(news);
                                                        }
                                                    },
                                                     new Parameter("@news_type", type)
                                                    );
            return newsList;
        }
    }

    
}
