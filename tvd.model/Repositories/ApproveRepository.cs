﻿using PPSolution.Core.Models;
using System;
using System.Configuration;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
	public class ApproveRepository : IApproveRepository
	{
		private readonly IDBHelper _dbHelper;

		public ApproveRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}

	    public void UpdateAppoveStatus(int approveId, string updatedBy, RecStatus recStatus)
	    {
	        _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
	            , "dbo.approve_make_approved_v1"
	            , new Parameter("@approve_id", approveId)
	            , new Parameter("@approve_value", (int)recStatus)
	            , new Parameter("@by", updatedBy));
        }

	    public Approve GetApprove(int approveId)
	    {
	        Approve approve = null;
	        _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
	            , "dbo.approve_fetch_by_id_v1"
	            , reader =>
	            {
	                if (reader.Read())
	                {
	                    approve = GetApproveEntity(reader);
	                }
	            }
	            , new Parameter("@approve_id", approveId));
	        return approve;
	    }

	    public ResultSet<Approve> GetApproveList(int? approveId, ApproveType approveType, RecStatus? recStatus, int rowPerPage = 1, int pageNumber = 1)
		{
			//var approves = new List<Approve>();
		    var approves = new ResultSet<Approve>();
		    _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
		        , "dbo.approve_fetch_v1"
		        , (reader) =>
		        {
		            while (reader.Read())
		            {
		                var approve = GetApproveEntity(reader);
		                approves.Add(approve);

		                if (approves.TotalItems == 0)
		                {
		                    approves.TotalItems = reader.GetInt32("max_rows");
		                }
		            }
		        },
		        new Parameter("@approve_type_id", approveType == ApproveType.UNKNOWN ? DBNull.Value : (object)approveType)
		        , new Parameter("@rec_status", recStatus.HasValue ? (object)recStatus : DBNull.Value)
                , new Parameter("@pageSize", rowPerPage)
		        , new Parameter("@pageNumber", pageNumber));
                                                        
			return approves;
		}

		public void Insert(Approve approve)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.approve_insert_v1"
				, new Parameter("@approve_type_id", (int)approve.ApproveType)
				, new Parameter("@object_id", approve.ObjectId)
				, new Parameter("@object_value", approve.ObjectValue)
				, new Parameter("@by", approve.RecCreatedBy));
		}

		public ApproveSetting GetApproveSetting()
		{
			ApproveSetting approveSetting = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.approve_setting_fetch_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															approveSetting = new ApproveSetting()
															{
																ApproveSettingId = reader.GetInt32("approve_setting_id"),
																IsAutoApproveNewMembership = reader.GetBoolean("auto_approve_new_membership"),
																IsAutoApproveChangeMembershipInfo = reader.GetBoolean("auto_approve_change_membership_info"),
																IsAutoApproveCancelMembership = reader.GetBoolean("auto_approve_cancel_membership"),
																MembershipRetensionPeriod = reader.GetInt32("membership_retension_period"),
																IsAutoApproveChangeMembershipPoint = reader.GetBoolean("auto_approve_change_membership_point"),
																IsAutoApproveRewardRedeemMembership = reader.GetBoolean("auto_approve_reward_redeem_membership"),
																IsAutoApproveLevelUpMembership = reader.GetBoolean("auto_approve_level_up_membership"),
																PointRetensionPeriod = reader.GetInt32("point_retension_period"),
															};
														}
													});
			return approveSetting;
		}

		public void InsertApproveSetting(ApproveSetting approveSetting)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.approve_setting_insert_v1"
				, new Parameter("@auto_approve_new_membership", approveSetting.IsAutoApproveNewMembership)
				, new Parameter("@auto_approve_change_membership_info", approveSetting.IsAutoApproveChangeMembershipInfo)
				, new Parameter("@auto_approve_cancel_membership", approveSetting.IsAutoApproveCancelMembership)
				, new Parameter("@membership_retension_period", approveSetting.MembershipRetensionPeriod)
				, new Parameter("@auto_approve_change_membership_point", approveSetting.IsAutoApproveChangeMembershipPoint)
				, new Parameter("@auto_approve_cancel_membership", approveSetting.IsAutoApproveCancelMembership)
				, new Parameter("@auto_approve_reward_redeem_membership", approveSetting.IsAutoApproveRewardRedeemMembership)
				, new Parameter("@auto_approve_level_up_membership", approveSetting.IsAutoApproveLevelUpMembership)
				, new Parameter("@point_retension_period", approveSetting.PointRetensionPeriod)
				, new Parameter("@by", approveSetting.RecCreatedBy));
		}

		public void UpdateApproveSetting(ApproveSetting approveSetting)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.approve_setting_update_v1"
				, new Parameter("@approve_setting_id", approveSetting.ApproveSettingId)
				, new Parameter("@auto_approve_new_membership", approveSetting.IsAutoApproveNewMembership)
				, new Parameter("@auto_approve_change_membership_info", approveSetting.IsAutoApproveChangeMembershipInfo)
				, new Parameter("@auto_approve_cancel_membership", approveSetting.IsAutoApproveCancelMembership)
				, new Parameter("@membership_retension_period", approveSetting.MembershipRetensionPeriod)
				, new Parameter("@auto_approve_change_membership_point", approveSetting.IsAutoApproveChangeMembershipPoint)
				, new Parameter("@auto_approve_reward_redeem_membership", approveSetting.IsAutoApproveRewardRedeemMembership)
				, new Parameter("@auto_approve_level_up_membership", approveSetting.IsAutoApproveLevelUpMembership)
				, new Parameter("@point_retension_period", approveSetting.PointRetensionPeriod)
				, new Parameter("@by", approveSetting.RecModifiedBy));
		}

		private Approve GetApproveEntity(EntityDataReader reader)
		{
			return new Approve()
			{
				ApproveId = reader.GetInt64("approve_id"),
				ApproveType = (ApproveType)reader.GetInt32("approve_type_id"),
				ObjectId = reader.GetInt32("object_id"),
				ObjectValue = reader.GetString("object_value"),
				RecStatus = (RecStatus)Convert.ToInt32(reader.GetInt16("rec_status")),
				RecCreatedBy = reader.GetString("rec_created_by"),
				RecCreatedWhen = reader.GetDateTime("rec_created_when"),
				RecModifiedBy = reader.GetNullableString("rec_modified_by"),
				RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when")
			};
		}

        public string InsertApi(Approve approve)
        {
            string result = "999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[sp_api_approve_insert_v1]"
                , (reader) =>
                {
                    if (reader.Read())
                    {
                        result = reader.GetString(0);
                    }
                }
                , new Parameter("@approve_type_id", (int)approve.ApproveType)
                , new Parameter("@object_id", approve.ObjectId)
                , new Parameter("@object_value", approve.ObjectValue)
                , new Parameter("@by", approve.RecCreatedBy));

            return result;
        }
    }
}