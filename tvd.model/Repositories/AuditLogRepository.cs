﻿using PPSolution.Core.Models;
using System;
using System.Configuration;
using Newtonsoft.Json;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Audit;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
	public class AuditLogRepository : IAuditLogRepository
	{
		private readonly IDBHelper _dbHelper;

		public AuditLogRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}
		
		public void Insert(AuditLog auditLog)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.audit_log_insert_v1"
				, new Parameter("@audit_action_type_id", (int)auditLog.AuditActionType)
				, new Parameter("@object_id", auditLog.ObjectId)
				, new Parameter("@old_value", JsonConvert.SerializeObject(auditLog.OldValue))
				, new Parameter("@new_value", JsonConvert.SerializeObject(auditLog.NewValue))
				, new Parameter("@source_id", (int)auditLog.Source)
                , new Parameter("@rec_status", (int)auditLog.RecStatus)
                , new Parameter("@description", auditLog.Description)
				, new Parameter("@by", auditLog.RecCreatedBy));
		}

	    public ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate, DateTime? toDate, int? roleId, int? objectId, string value, int pageSize, int pageNumber)
	    {
	        var logs = new ResultSet<AuditLog>();
	        _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
	            , "dbo.audit_log_fetch_v1"
                , (reader) =>
	            {
	                while (reader.Read())
	                {
	                    logs.Add(GetLogEntity(reader));

	                    if (logs.TotalItems == 0)
	                    {
	                        logs.TotalItems = reader.GetInt32("max_rows");
	                    }
	                }
	            },
	            new Parameter("@audit_action_type_id", (int)type),
	            new Parameter("@from_date", fromDate),
	            new Parameter("@to_date", toDate),
	            new Parameter("@rec_status", status),
	            new Parameter("@by", createBy),
                new Parameter("@role_id", roleId),
                new Parameter("@object_id", objectId),
                new Parameter("@new_value", value),
                new Parameter("@page_size", pageSize),
	            new Parameter("@page_number", pageNumber));
	        return logs;
        }

		public ResultSet<AuditLog> GetAuditLogs(AuditActionType type, RecStatus? status, string createBy, DateTime? fromDate, DateTime? toDate, int? roleId, int? objectId, string value)
		{
			var logs = new ResultSet<AuditLog>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.audit_log_fetch_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetLogEntity(reader));						
					}
				},
				new Parameter("@audit_action_type_id", (int)type),
				new Parameter("@from_date", fromDate),
				new Parameter("@to_date", toDate),
				new Parameter("@rec_status", status),
				new Parameter("@by", createBy),
				new Parameter("@role_id", roleId),
				new Parameter("@object_id", objectId),
				new Parameter("@new_value", value));
			return logs;
		}

		public ResultSet<AuditLog> GetAuditLogs(DateTime? fromDate, DateTime? toDate)
		{
			var logs = new ResultSet<AuditLog>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.audit_log_fetch_all_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetLogEntity(reader));
					}
				},
				new Parameter("@from_date", fromDate),
				new Parameter("@to_date", toDate));
			return logs;
		}


		private AuditLog GetLogEntity(EntityDataReader reader)
	    {
            var log = new AuditLog();
	        log.AudiitLogId = reader.GetInt64("audit_log_id");
	        log.AuditActionType = (AuditActionType)reader.GetInt32("audit_action_type_id");
	        log.NewValue = reader.GetString("new_value");
	        log.OldValue = reader.GetString("old_value");
	        log.ObjectId = reader.GetInt32("object_id");
	        log.Source = (Source)reader.GetByte("source_id");
	        log.Description = reader.GetString("description");
	        log.RecCreatedWhen = reader.GetDateTime("rec_created_when");
	        log.RecCreatedBy = reader.GetString("rec_created_by");
	        log.RecStatus = (RecStatus)reader.GetInt16("rec_status");
	        return log;

	    }

        private ViewHistory GetHistory(EntityDataReader reader)
        {
            var log = new ViewHistory();
            log.EmployeeCode = reader.GetString("rec_created_by");
            log.EmployeeName = reader.GetString("employee_name");
            log.FirstName = reader.GetNullableString("firstname");
            log.LastName = reader.GetNullableString("lastname");
            log.PageId = reader.GetInt32("page_id");
            log.PersonId = reader.GetNullableInt32("person_id");
            log.RecCreatedWhen = reader.GetDateTime("rec_created_when");
            log.SubPageId = reader.GetNullableInt32("subpage_id");
            log.TitleId = reader.GetNullableInt32("title_id");
            return log;

        }

        private MemberPointReport GetPointReport(EntityDataReader reader, bool hasExpireDate)
        {
            var log = new MemberPointReport();
            log.PersonId = reader.GetInt32("person_id");
            log.TitleId = reader.GetInt32("title_id");
            log.FirstName = reader.GetString("firstname");
            log.LastName = reader.GetString("lastname");
            log.Points = reader.GetInt32("point");
            if (hasExpireDate)
            {
                log.ExpiredDate = reader.GetDateTime("expire_date");
            }
            return log;
        }

		private MemberPointExpireReport GetPointExpireReport(EntityDataReader reader)
		{
			var log = new MemberPointExpireReport();
			log.PersonId = reader.GetInt32("person_id");
			log.TitleId = reader.GetInt32("title_id");
			log.FirstName = reader.GetString("firstname");
			log.LastName = reader.GetString("lastname");
			log.Points = reader.GetInt32("point");
			log.ExpiredDate = reader.GetDateTime("expire_date");
			return log;
		}

		public void InsertViewHistory(int? personId, int pageId, int? subPageId, string visitedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.visit_history_insert_v1"
                , new Parameter("@person_id", personId)
                , new Parameter("@page_id", pageId)
                , new Parameter("@subpage_id", subPageId)
                , new Parameter("@by", visitedBy));
        }

        public ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int pageId, int pageSize, int pageNumber)
        {
            var logs = new ResultSet<ViewHistory>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.visit_history_fetch_v1"
                , (reader) =>
                {
                    while (reader.Read())
                    {
                        logs.Add(GetHistory(reader));

                        if (logs.TotalItems == 0)
                        {
                            logs.TotalItems = reader.GetInt32("max_rows");
                        }
                    }
                },
                new Parameter("@page_id", pageId),
                new Parameter("@from_date", fromDate),
                new Parameter("@to_date", toDate),
                new Parameter("@page_size", pageSize),
                new Parameter("@page_number", pageNumber));
            return logs;
        }

		public ResultSet<ViewHistory> GetHistory(DateTime? fromDate, DateTime? toDate, int pageId)
		{
			var logs = new ResultSet<ViewHistory>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.visit_history_fetch_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetHistory(reader));
					}
				},
				new Parameter("@page_id", pageId),
				new Parameter("@from_date", fromDate),
				new Parameter("@to_date", toDate));
			return logs;
		}

		public ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? dayBeforeExpire, int pageSize, int pageNumber)
        {
            var logs = new ResultSet<MemberPointReport>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.report_member_point_fetch_v1"
                , (reader) =>
                {
                    while (reader.Read())
                    {
                        logs.Add(GetPointReport(reader, dayBeforeExpire.HasValue));

                        if (logs.TotalItems == 0)
                        {
                            logs.TotalItems = reader.GetInt32("max_rows");
                        }
                    }
                },
                new Parameter("@min_point", minPoint),
                new Parameter("@max_point", maxPoint),
                new Parameter("@days_before_expire", dayBeforeExpire),
                new Parameter("@page_size", pageSize),
                new Parameter("@page_number", pageNumber));
            return logs;
        }

		public ResultSet<MemberPointReport> GetMemberPointReport(int? minPoint, int? maxPoint, int? dayBeforeExpire)
		{
			var logs = new ResultSet<MemberPointReport>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.report_member_point_fetch_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetPointReport(reader, dayBeforeExpire.HasValue));
					}
				},
				new Parameter("@min_point", minPoint),
				new Parameter("@max_point", maxPoint),
				new Parameter("@days_before_expire", dayBeforeExpire));
			return logs;
		}

		public ResultSet<MemberPointExpireReport> GetMemberPointExpireReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate, int pageSize, int pageNumber)
		{
			var logs = new ResultSet<MemberPointExpireReport>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.report_member_point_expiry_fetch_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetPointExpireReport(reader));

						if (logs.TotalItems == 0)
						{
							logs.TotalItems = reader.GetInt32("max_rows");
						}
					}
				},
				new Parameter("@start_date", startDate),
				new Parameter("@end_date", endDate),
				new Parameter("@min_point", minPoint),
				new Parameter("@max_point", maxPoint),
				new Parameter("@page_size", pageSize),
				new Parameter("@page_number", pageNumber));
			return logs;
		}

		public ResultSet<MemberPointExpireReport> GetMemberPointExpireReport(int? minPoint, int? maxPoint, DateTime startDate, DateTime endDate)
		{
			var logs = new ResultSet<MemberPointExpireReport>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.report_member_point_expiry_fetch_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						logs.Add(GetPointExpireReport(reader));

						if (logs.TotalItems == 0)
						{
							logs.TotalItems = reader.GetInt32("max_rows");
						}
					}
				},
				new Parameter("@start_date", startDate),
				new Parameter("@end_date", endDate),
				new Parameter("@min_point", minPoint),
				new Parameter("@max_point", maxPoint));
			return logs;
		}
	}
}