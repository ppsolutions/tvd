﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDBHelper _dbHelper;

        public RoleRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public int InsertRole(Role role)
        {
            var roleId = 0;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.role_insert_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            roleId = reader.GetNullableInt32("role_id") ?? 0;
                                                        }
                                                    }
                                                    , new Parameter("@role_name", role.RoleName)
                                                  , new Parameter("@by", role.RecCreatedBy));
            return roleId;
        }

        public void UpdateRole(Role role)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                   , "dbo.role_update_v1"
                                                   , new Parameter("@role_id", role.RoleId)
                                                   , new Parameter("@role_name", role.RoleName)
                                                   , new Parameter("@by", role.RecModifiedBy)
                                                   , new Parameter("@rec_status", (int)role.RecStatus));
        }

        public IEnumerable<Role> GetRoles()
        {
            var roles = new List<Role>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.role_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var role = new Role()
                                                            {
                                                                RoleId = reader.GetInt32("role_id"),
                                                                RoleName = reader.GetString("role_name"),
                                                                RecStatus = (RecStatus)reader.GetInt16("rec_status"),
                                                                RecCreatedBy = reader.GetString("rec_created_by"),
                                                                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                                                                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when"),
                                                                RecModifiedBy = reader.GetNullableString("rec_modified_by")
                                                            };
                                                            roles.Add(role);
                                                        }
                                                    });
            return roles;
        }

        public void InsertRolePermission(RolePermission rolePermission)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                  , "dbo.role_permission_insert_v1"
                                                  , new Parameter("@role_id", rolePermission.Role.RoleId)
                                                  , new Parameter("@is_permit_in_member_registration_system", rolePermission.IsPermitInMemberRegistrationSystem)
                                                  , new Parameter("@is_permit_in_approve_system", rolePermission.IsPermitInApproveSystem)
                                                  , new Parameter("@is_permit_in_news_system", rolePermission.IsPermitInNewsSystem)
                                                  , new Parameter("@is_permit_in_point_setting_system", rolePermission.IsPermitInPointSettingSystem)
                                                  , new Parameter("@is_permit_in_report_system", rolePermission.IsPermitInReportSystem)
                                                  , new Parameter("@is_permit_in_audit_report_system", rolePermission.IsPermisInAuditReportSystem)
                                                  , new Parameter("@is_permit_in_employee_management_system", rolePermission.IsPermitInEmployeeManagementSystem)
                                                  , new Parameter("@is_permit_in_role_system", rolePermission.IsPermitInRoleSystem)
                                                  , new Parameter("@by", rolePermission.RecCreatedBy));
        }

        public void UpdateRolePermission(RolePermission rolePermission)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                  , "dbo.role_permission_update_v1"
                                                  , new Parameter("@role_id", rolePermission.Role.RoleId)
                                                  , new Parameter("@role_name", rolePermission.Role.RoleName)
                                                  , new Parameter("@is_permit_in_member_registration_system", rolePermission.IsPermitInMemberRegistrationSystem)
                                                  , new Parameter("@is_permit_in_approve_system", rolePermission.IsPermitInApproveSystem)
                                                  , new Parameter("@is_permit_in_news_system", rolePermission.IsPermitInNewsSystem)
                                                  , new Parameter("@is_permit_in_point_setting_system", rolePermission.IsPermitInPointSettingSystem)
                                                  , new Parameter("@is_permit_in_report_system", rolePermission.IsPermitInReportSystem)
                                                  , new Parameter("@is_permit_in_audit_report_system", rolePermission.IsPermisInAuditReportSystem)
                                                  , new Parameter("@is_permit_in_employee_management_system", rolePermission.IsPermitInEmployeeManagementSystem)
                                                  , new Parameter("@is_permit_in_role_system", rolePermission.IsPermitInRoleSystem)
                                                  , new Parameter("@by", rolePermission.RecModifiedBy)
                                                  , new Parameter("@rec_status", (int)rolePermission.RecStatus));
        }

        public RolePermission GetRolePermission(int roleId)
        {
            var rolePermission = new RolePermission();
			rolePermission.Role = new Role() { RoleId = roleId };

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.role_permission_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            rolePermission = GetRolePermissionEntity(reader);
                                                        }
                                                    }
                                                    , new Parameter("@role_id", roleId));
            return rolePermission;
        }

        public void UpdateRoleToUser(string employeeCode, int? roleId, RecStatus? status, string updatedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.employee_in_role_save_v1"
                , new Parameter("@employee_code", employeeCode)
                , new Parameter("@role_id", roleId.HasValue ? (object)roleId.Value : DBNull.Value)
                , new Parameter("@rec_status", status.HasValue ? (object)status.Value : DBNull.Value)
                , new Parameter("@by", updatedBy));
        }

        private RolePermission GetRolePermissionEntity(EntityDataReader reader)
        {
            return new RolePermission()
            {
                Role = new Role()
                {
                    RoleId = reader.GetInt32("role_id"),
                    RoleName = reader.GetString("r_role_name"),
                    RecStatus = (RecStatus)reader.GetInt16("r_rec_status"),
                    RecCreatedBy = reader.GetString("r_rec_created_by"),
                    RecCreatedWhen = reader.GetDateTime("r_rec_created_when"),
                    RecModifiedBy = reader.GetNullableString("r_rec_modified_by"),
                    RecModifiedWhen = reader.GetNullableDateTime("r_rec_modified_when") ?? new DateTime(1900, 1, 1)
                },
                IsPermitInMemberRegistrationSystem = reader.GetBoolean("is_permit_in_member_registration_system"),
                IsPermitInApproveSystem = reader.GetBoolean("is_permit_in_approve_system"),
                IsPermitInNewsSystem = reader.GetBoolean("is_permit_in_news_system"),
                IsPermitInPointSettingSystem = reader.GetBoolean("is_permit_in_point_setting_system"),
                IsPermitInReportSystem = reader.GetBoolean("is_permit_in_report_system"),
                IsPermisInAuditReportSystem = reader.GetBoolean("is_permit_in_audit_report_system"),
                IsPermitInEmployeeManagementSystem = reader.GetBoolean("is_permit_in_employee_management_system"),
                IsPermitInRoleSystem = reader.GetBoolean("is_permit_in_role_system"),
                RecStatus = (RecStatus)reader.GetInt16("rec_status"),
                RecCreatedBy = reader.GetString("rec_created_by"),
                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                RecModifiedBy = reader.GetNullableString("rec_modified_by"),
                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? new DateTime(1900, 1, 1)
            };
        }

        public void AssignAccessControlToUser(string employeeCode, int[] controlIds, string updatedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[sp_access_control_upsert_employee_access_v1]"
                , new Parameter("@employee_code", employeeCode)
                , new Parameter("@accessControl", CreateSqlDataRecords(controlIds))
                , new Parameter("@by", updatedBy)
            );
        }

        public DataTable CreateSqlDataRecords(IEnumerable<int> datas)
        {
            var dt = new DataTable();
            dt.Columns.Add("rule_id", typeof(int));

            foreach (var data in datas)
            {
                var dr = dt.NewRow();
                dr[0] = data;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public void AssignAccessControlToRole(int roleId, int[] controlIds, string updatedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "[dbo].[sp_access_control_upsert_role_access_v1]"
                , new Parameter("@i_roleId", roleId)
                , new Parameter("@accessControl", CreateSqlDataRecords(controlIds))
                , new Parameter("@by", updatedBy)
            );
        }
    }
}
