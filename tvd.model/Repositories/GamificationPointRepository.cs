﻿using PPSolution.Core.Models;
using System;
using System.Configuration;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
    public class GamificationPointRepository : IGamificationPointRepository
	{
		private readonly IDBHelper _dbHelper;

		public GamificationPointRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;

		}
		public GamificationPoint GetGamificationPoint(int gamificationPointId)
		{
            GamificationPoint gamificationPoint = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.gamification_point_fetch_by_gamification_point_id_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															gamificationPoint = GetGamificationPointEntity(reader);
														}
													},
													new Parameter("@gamification_point_id", gamificationPointId));
			return gamificationPoint;
		}

		public GamificationPoint GetGamificationPoint(string gamificationCode)
		{
            GamificationPoint gamificationPoint = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.gamification_point_fetch_by_gamification_code_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															gamificationPoint = GetGamificationPointEntity(reader);
														}
													},
													new Parameter("@gamification_code", gamificationCode));
			return gamificationPoint;
		}

		public ResultSet<GamificationPoint> GetGamificationPoints(string code, int? applyTo, int? recStatus, int? pageSize, int? pageNumber)
		{
			var gamificationPoints = new ResultSet<GamificationPoint>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.gamification_point_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var gamificationPoint = GetGamificationPointEntity(reader);
															gamificationPoints.Add(gamificationPoint);

														    if (gamificationPoints.TotalItems == 0)
														    {
														        gamificationPoints.TotalItems = reader.GetInt32("max_rows");
														    }
														}
													},
			                                        new Parameter("@code", code)
                                                    , new Parameter("@apply_to", applyTo)
                                                    , new Parameter("@page_size", pageSize)
                                                    , new Parameter("@page_number", pageNumber)
                                                    , new Parameter("@rec_status", recStatus));
			return gamificationPoints;
		}

		public void InsertGamificationPoint(GamificationPoint gamificationPoint)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												 , "dbo.gamification_point_insert_v1"
												 , new Parameter("@gamification_code", gamificationPoint.GamificationCode)
												 , new Parameter("@description", gamificationPoint.Description)
                                                 , new Parameter("@rec_status", gamificationPoint.RecStatus)
												 , new Parameter("@apply_id", (int)gamificationPoint.ApplyTo)
												 , new Parameter("@increase_point", gamificationPoint.IncreasePoint)
												 , new Parameter("@valid_from", gamificationPoint.ValidFrom)
												 , new Parameter("@valid_to", gamificationPoint.ValidTo)
												 , new Parameter("@by", gamificationPoint.RecCreatedBy));
		}

		public void UpdateGamificationPoint(GamificationPoint gamificationPoint)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												 , "dbo.gamification_point_update_v1"
												 , new Parameter("@gamification_point_id", gamificationPoint.GamificationPointId)
												 , new Parameter("@gamification_code", gamificationPoint.GamificationCode)
												 , new Parameter("@description", gamificationPoint.Description)
                                                 , new Parameter("@rec_status", gamificationPoint.RecStatus)
                                                 , new Parameter("@apply_id", (int)gamificationPoint.ApplyTo)
												 , new Parameter("@increase_point", gamificationPoint.IncreasePoint)
												 , new Parameter("@valid_from", gamificationPoint.ValidFrom)
												 , new Parameter("@valid_to", gamificationPoint.ValidTo)
												 , new Parameter("@by", gamificationPoint.RecModifiedBy));
		}

        public void UpdateGamificationStatus(int gamificationPointId, RecStatus status, string updatedBy)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                 , "dbo.gamification_point_update_v1"
                                                 , new Parameter("@gamification_point_id", gamificationPointId)
                                                 , new Parameter("@rec_status", (int)status)
                                                 , new Parameter("@by", updatedBy));
        }

        private GamificationPoint GetGamificationPointEntity(EntityDataReader reader)
		{
			return new GamificationPoint()
			{
				GamificationPointId = reader.GetInt32("gamification_point_id"),
				GamificationCode = reader.GetString("gamification_code"),
				Description = reader.GetString("description"),
				ApplyTo = (ApplyType)reader.GetInt32("apply_id"),
				IncreasePoint = reader.GetInt32("increase_point"),
				ValidFrom = reader.GetNullableDateTime("valid_from") ?? DateTime.MinValue,
				ValidTo = reader.GetNullableDateTime("valid_to") ?? DateTime.MaxValue,
				RecStatus = (RecStatus)reader.GetInt16("rec_status"),
				RecCreatedBy = reader.GetString("rec_created_by"),
				RecCreatedWhen = reader.GetDateTime("rec_created_when"),
				RecModifiedBy = reader.GetNullableString("rec_modified_by"),
				RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when") ?? DateTime.MinValue
			};
		}

        public GamificationPoint GetGamificationInfoApi(string code)
        {
            GamificationPoint info = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_gamification_get_info_by_code_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            while (reader.Read())
                                                            {
                                                                if(info == null)
                                                                {
                                                                    info = new GamificationPoint();
                                                                }

                                                                info.GamificationCode = reader.GetString("gamification_code");
                                                                info.ValidFrom = reader.GetDateTime("valid_from");
                                                                info.ValidTo = reader.GetDateTime("valid_to");
                                                                info.RecStatus = (RecStatus)reader.GetInt16("rec_status");
                                                                info.IncreasePoint = reader.GetInt32("increase_point");

                                                            }
                                                        }
                                                    },
                                                    new Parameter("@code", code));
            return info;
        }
    }
}
