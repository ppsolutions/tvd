﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Rule;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
	public class RuleRepository : IRuleRepository
	{
		private readonly IDBHelper _dbHelper;

		public RuleRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}

		public Rule GetRule(int ruleId)
		{
			var rule = new Rule();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.rule_fetch_by_rule_id_v1"
													, (reader) =>
													{
														if(reader.Read())
														{
															rule = GetRuleEntity(reader);
														}
													},
													new Parameter("@rule_id", ruleId));
			return rule;
		}

		public IEnumerable<Rule> GetRules()
		{
			var rules = new List<Rule>();

			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.rule_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var rule = GetRuleEntity(reader);
															rules.Add(rule);
														}
													});

			return rules;
		}

		public void InsertRule(Rule rule)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												 , "dbo.rule_insert_v1"
												 , new Parameter("@description", rule.Description)
												 , new Parameter("@point_level_id", rule.PointLevelId)
												 , new Parameter("@payment_type_id", rule.PaymentType.Id)
												 , new Parameter("@card_type_id", rule.CardType.Id)
												 , new Parameter("@bank_id", rule.Bank.Id)
												 , new Parameter("@calculation_type", (CalculationType)rule.CalculationType)
												 , new Parameter("@received_point", rule.ReceivedPoint)
												 , new Parameter("@is_per_item", rule.IsPerItem)
												 , new Parameter("@valid_from", rule.ValidFrom)
												 , new Parameter("@valid_to", rule.ValidTo)
												 , new Parameter("@by", rule.RecCreatedBy.PersonId));
		}

		public void UpdateRule(Rule rule)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												 , "dbo.rule_update_v1"
												 , new Parameter("@rule_id", rule.RuleId)
												 , new Parameter("@description", rule.Description)
												 , new Parameter("@point_level_id", rule.PointLevelId)
												 , new Parameter("@payment_type_id", rule.PaymentType.Id)
												 , new Parameter("@card_type_id", rule.CardType.Id)
												 , new Parameter("@bank_id", rule.Bank.Id)
												 , new Parameter("@calculation_type", (CalculationType)rule.CalculationType)
												 , new Parameter("@received_point", rule.ReceivedPoint)
												 , new Parameter("@is_per_item", rule.IsPerItem)
												 , new Parameter("@valid_from", rule.ValidFrom)
												 , new Parameter("@valid_to", rule.ValidTo)
												 , new Parameter("@by", rule.RecCreatedBy.PersonId));
		}

		private Rule GetRuleEntity(EntityDataReader reader)
		{
			Rule rule = new Rule();
			rule.RuleId = reader.GetInt32("rule_id");
			rule.Description = reader.GetString("description");
			rule.PointLevelId = reader.GetInt32("point_level_id");
			rule.PaymentType = new Models.CommonDTO.Lookup.PaymentType()
			{
				Id = reader.GetInt32("payment_type_id")
			};
			rule.CardType = new Models.CommonDTO.Lookup.CardType()
			{
				Id = reader.GetInt32("card_type_id")
			};
			rule.Bank = new Models.CommonDTO.Lookup.Bank()
			{
				Id = reader.GetInt32("bank_id")
			};
			rule.CalculationType = (CalculationType)reader.GetInt32("calculation_type");
			rule.ReceivedPoint = reader.GetInt32("received_point");
			rule.IsPerItem = reader.GetBoolean("is_per_item");
			rule.ValidFrom = reader.GetDateTime("valid_from");
			rule.ValidTo = reader.GetDateTime("valid_to");


			rule.RecStatus = (RecStatus)reader.GetInt16("rec_status");
			rule.RecCreatedBy = new Models.CommonDTO.Person.PersonLite()
			{
				PersonId = reader.GetInt32("rec_created_by")
			};
			rule.RecCreatedWhen = reader.GetDateTime("rec_created_when");
			rule.RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when");
			rule.RecModifiedBy = reader.GetNullableInt32("rec_modified_by") ?? 0;

			return rule;
		}

        public IEnumerable<Rule> GetRulesByCodeApi(string code, int codeType)
        {
            var rules = new List<Rule>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "sp_api_rule_fetch_by_code_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var rule = new Rule();
                                                            rule.RuleId = reader.GetInt32("rule_id");
                                                            rule.RuleCode = reader.GetString("rule_code");
                                                            rule.Description = reader.GetString("description");
                                                            rule.PointLevelId = reader.GetInt32("point_level_id");
                                                            rule.PaymentType = new Models.CommonDTO.Lookup.PaymentType()
                                                            {
                                                                Id = reader.GetInt32("payment_type_id")
                                                            };
                                                            rule.CardType = new Models.CommonDTO.Lookup.CardType()
                                                            {
                                                                Id = reader.GetInt32("card_type_id")
                                                            };
                                                            rule.Bank = new Models.CommonDTO.Lookup.Bank()
                                                            {
                                                                Id = reader.GetInt32("bank_id")
                                                            };
                                                            rule.CalculationType = (CalculationType)reader.GetByte("calculation_type");
                                                            rule.ReceivedPoint = reader.GetInt32("received_point");
                                                            rule.IsPerItem = reader.GetBoolean("is_per_item");
                                                            rule.IsPerOrder = reader.GetBoolean("is_per_order");
                                                            rule.ValidFrom = reader.GetDateTime("valid_from");
                                                            rule.ValidTo = reader.GetDateTime("valid_to");
                                                            rules.Add(rule);
                                                        }
                                                    }
                                                    , new Parameter("@code", code)
                                                    , new Parameter("@promoType", codeType)
                                                    );

            return rules;
        }
    }
}
