﻿using System;
using System.Collections.Generic;
using System.Configuration;
using PPSolution.Core.Models;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Member;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;
using API_MODEL = tvd.model.Api.Models.Member;
using System.Data;

namespace tvd.model.Repositories
{
	public class MemberRepository : IMemberRepository
	{
		private readonly IDBHelper _dbHelper;

		public MemberRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}

		public IEnumerable<Person> GetMembers(string name, string idCard, string phoneNumber, string code)
		{
			var persons = new List<Person>();
			 _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
				, "dbo.person_fetch_by_criteria_v1"
				, (reader) =>
				{
					while (reader.Read())
					{
						var person = GetPersonEntity(reader);
						persons.Add(person);
					}
					;
				}
				, new Parameter("@name_criteria", name)
				, new Parameter("@id_card", idCard)
				, new Parameter("@contact_number", phoneNumber)
				, new Parameter("@code", code));
			return persons;
		}

	    public ResultSet<Person> GetMembers(string name, string idCard, string phoneNumber, string code, int pageSize, int pageNumber)
	    {
	        var persons = new ResultSet<Person>();
	        _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
	            , "dbo.person_fetch_by_criteria_v2"
	            , (reader) =>
	            {
	                while (reader.Read())
	                {
	                    var person = GetPersonEntity(reader);
	                    persons.Add(person);

	                    if (persons.TotalItems == 0)
	                    {
	                        persons.TotalItems = reader.GetInt32("max_rows");
	                    }
	                }
	                ;
	            }
	            , new Parameter("@name_criteria", name)
				, new Parameter("@id_card", idCard)
				, new Parameter("@contact_number", phoneNumber)
	            , new Parameter("@code", code)
	            , new Parameter("@page_size", pageSize)
	            , new Parameter("@page_number", pageNumber));
	        return persons;
	    }

		public IEnumerable<Person> GetMembers(string name)
		{
			var persons = new List<Person>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.person_fetch_by_person_name_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var person = GetPersonEntity(reader);
															persons.Add(person);
														}
													}
													, new Parameter("@criteria", name));
			return persons;
		}

		public Person GetMember(object id, Source memberSource)
		{
			var person = new Person();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.person_fetch_by_id_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															person = GetPersonEntity(reader);
														}
													}
													, new Parameter("@id", id.ToString())
													, new Parameter("@source_type", (int)memberSource));
			return person;
		}

		public int CreateUser(Person user)
		{
			var personId = 0;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.person_insert_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															personId = reader.GetInt32("person_id");
														}
													}
													, new Parameter("@title_id", user.TitleId)
													, new Parameter("@firstname", user.FirstName)
													, new Parameter("@lastname", user.LastName)
													, new Parameter("@is_legal_person", user.IsLegalPerson)
													, new Parameter("@birthdate", user.BirthDate.HasValue ? (object)user.BirthDate.Value : DBNull.Value)
													, new Parameter("@is_specific_year", user.IsSpecificYear.HasValue ? (object)user.IsSpecificYear.Value : DBNull.Value)
													, new Parameter("@contact_number", user.ContactNumber)
													, new Parameter("@email_address", user.EmailAddress)
													, new Parameter("@username", user.UserName)
													, new Parameter("@rec_status", (int)user.RecStatus)
													, new Parameter("@by", user.RecCreatedBy));

			user.PersonId = personId;
			return personId;
		}

		public int UpdateUser(Person user)
		{
			int personId = user.PersonId;
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.person_update_v1"
													, new Parameter("@person_id", user.PersonId)
													, new Parameter("@title_id", user.TitleId)
													, new Parameter("@firstname", user.FirstName)
													, new Parameter("@lastname", user.LastName)
													, new Parameter("@is_legal_person", user.IsLegalPerson)
													, new Parameter("@birthdate", user.BirthDate)
													, new Parameter("@is_specific_year", user.IsSpecificYear)
													, new Parameter("@contact_number", user.ContactNumber)
													, new Parameter("@email_address", user.EmailAddress)
													, new Parameter("@by", user.RecModifiedBy)
													, new Parameter("@rec_status", (int)user.RecStatus));
			return personId;
		}

		public void UpdateUserStatus(int personId, RecStatus recStatus, string updateBy)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												   , "dbo.person_status_update_v1"
												   , new Parameter("@person_id", personId)
												   , new Parameter("@rec_status", (int)recStatus)
												   , new Parameter("@by", updateBy));
		}

		public void UpdateUserPassword(int personId, string password, string updateBy)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.person_password_update_v1"
													, new Parameter("@person_id", personId)
													, new Parameter("@password", password)
													, new Parameter("@by", updateBy));
		}

		public List<MemberMapping> GetMergeUsers(int personId)
		{
			var memberMappings = new List<MemberMapping>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.member_mapping_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															memberMappings.Add(GetMemberMappingEntity(reader));
														}
													}
													, new Parameter("@person_id", personId));
			return memberMappings;
		}

		public void MergeMember(MemberMapping memberMapping, int updateBy)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.member_mapping_insert_v1"
													, new Parameter("@person_id", memberMapping.PersonId)
													, new Parameter("@map_with_id", memberMapping.MapWithId)
													, new Parameter("@map_source", (int)memberMapping.Source)
													, new Parameter("@point_transfered", memberMapping.PointTransfered)
													, new Parameter("@by", updateBy));
		}

		private Person GetPersonEntity(EntityDataReader reader)
		{
			var person = new Person();

			person.Source = (Source)((int)reader.GetByte("source_type"));
			person.PersonId = reader.GetNullableInt32("person_id") ?? 0;
            //person.ReferenceId = Convert.ToString(reader.GetNullableDouble("reference_id")) ?? string.Empty;
            person.ReferenceId = reader.GetValue("reference_id")?.ToString();
            person.Code = reader.GetNullableString("code") ?? string.Empty;
			person.TitleId = reader.GetNullableInt32("title_id") ?? 0;
			person.FirstName = reader.GetNullableString("firstname") ?? string.Empty;
			person.LastName = reader.GetNullableString("lastname") ?? string.Empty;
			person.IsLegalPerson = reader.GetNullableBoolean("is_legal_person");
			person.BirthDate = reader.GetNullableDateTime("birthdate");
			person.IsSpecificYear = reader.GetNullableBoolean("is_specific_year");
			person.ContactNumber = reader.GetNullableString("contact_number") ?? string.Empty;
			person.EmailAddress = reader.GetNullableString("email_address") ?? string.Empty;
			person.UserName = reader.GetNullableString("username") ?? string.Empty;
			person.EncryptedPassword = reader.GetNullableString("password") ?? string.Empty;
			person.RecStatus = (RecStatus)(reader.GetNullableInt16("rec_status") ?? 99);
			person.RecCreatedWhen = reader.GetNullableDateTime("rec_created_when") ?? DateTime.MinValue;
		    person.RecCreatedBy = reader.GetNullableString("rec_created_by");
			person.RecModifiedBy = reader.GetNullableString("rec_modified_by");
			person.RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when");

			return person;
		}

		private MemberMapping GetMemberMappingEntity(EntityDataReader reader)
		{
			return new MemberMapping()
			{
				PersonId = reader.GetInt32("person_id"),
				MapWithId = reader.GetString("map_with_id"),
				Source = (Source)reader.GetByte("map_source"),
				PointTransfered = reader.GetInt32("point_transfered"),
				RecStatus = (RecStatus)(int)reader.GetInt16("rec_status"),
				RecCreatedBy = reader.GetString("rec_created_by"),
				RecCreatedWhen = reader.GetDateTime("rec_created_when"),
				RecModifiedBy = reader.GetNullableString("rec_modified_by"),
				RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when")
			};
		}

        public string UpdateUserPasswordApi(int personId, string oldPassword, string newPassword, int updateBy)
        {
            var returnCode = "-999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_update_password_v1]"
                                                    , (reader) =>
                                                    {
                                                        if(reader != null)
                                                        {
                                                            if (reader.Read())
                                                            {
                                                                returnCode = reader.GetString(0);
                                                            }
                                                        }
                                                    }
                                                    , new Parameter("@personId", personId)
                                                    , new Parameter("@oldPassword", oldPassword)
                                                    , new Parameter("@newPassword", newPassword)
                                                    , new Parameter("@by", updateBy));
            return returnCode;
        }

        public Tuple<string, int> MemberUpsertApi(API_MODEL.MemberDto dto, int updateBy)
        {
            var returnCode = "-999";
			var personId = 0;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[sp_api_member_upsert_member_v1]",
                (reader) =>
                {
                    if (reader != null)
                    {
                        if (reader.Read())
                        {
                            returnCode = reader.GetString("return_code");
							personId = reader.GetInt32("person_id");
                        }
                    }
                }
                , new Parameter("@person_id", dto.PersonId)
                , new Parameter("@title_id", dto.Person.PersonTitle)
                , new Parameter("@firstname", dto.Person.FirstName)
                , new Parameter("@lastname", dto.Person.LastName)
                , new Parameter("@is_legal_person", true)
                , new Parameter("@birthdate", dto.Person.DOB)
				, new Parameter("@is_specific_year", dto.Person.IsSpecificYear)
				, new Parameter("@contact_number", dto.Person.Phone)
                , new Parameter("@email_address", dto.Person.Email)
                , new Parameter("@corporateTitle", dto.Corporate.CorporateTitle)
                , new Parameter("@corporateName", dto.Corporate.CorporateName)
                , new Parameter("@corporatePhone", dto.Corporate.CorporatePhone)
                , new Parameter("@corporateEmail", dto.Corporate.CorporateEmail)
                , new Parameter("@username", dto.Person.Email)
                , new Parameter("@rec_status", dto.RecStatus)
                , new Parameter("@address", CreateSqlDataRecords(dto.Address))
                , new Parameter("@question", CreateSqlDataRecords(dto.Question))
                , new Parameter("@password", dto.Password)
                , new Parameter("@by", updateBy)
                , new Parameter("@customerRef", dto.CustomerRef)
            );

            return new Tuple<string, int>(returnCode, personId);
        }

        public DataTable CreateSqlDataRecords(IEnumerable<API_MODEL.PersonAddress> datas)
        {
            var dt = new DataTable();
            dt.Columns.Add("addressTypeId", typeof(int));
            dt.Columns.Add("address", typeof(string));
            dt.Columns.Add("cityId", typeof(int));
            dt.Columns.Add("postalCode", typeof(string));

            foreach (var data in datas)
            {
                var dr = dt.NewRow();
                dr["addressTypeId"] = data.AddressType;
                dr["address"] = data.Address;
                dr["cityId"] = data.CityId;
                dr["postalCode"] = data.PostalCode;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public DataTable CreateSqlDataRecords(IEnumerable<API_MODEL.PersonQuestion> datas)
        {
            var dt = new DataTable();
            dt.Columns.Add("questionId", typeof(int));
            dt.Columns.Add("answer", typeof(string));
            foreach (var data in datas)
            {
                var dr = dt.NewRow();
                dr[0] = data.QuestionId;
                dr[1] = data.Answer;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public API_MODEL.PersonInfo FetchMember(string objectId)
        {
            API_MODEL.PersonInfo personInfo = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_member_fetch_code_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            if (personInfo == null)
                                                                personInfo = new API_MODEL.PersonInfo();

                                                            personInfo.PersonId = reader.GetInt32(0);
                                                            personInfo.Code = reader.GetString(1);
                                                            personInfo.TitleId = reader.GetInt32(2);
                                                            personInfo.FirstName = reader.GetString(3);
                                                            personInfo.LastName = reader.GetString(4);
                                                            personInfo.BirthDate = reader.GetDateTime(5);
                                                            personInfo.ContactNumber = reader.GetString(6);
                                                            personInfo.Email = reader.GetString(7);
                                                            personInfo.RecStatus = reader.GetInt16(8);
                                                            personInfo.CreateDate = reader.GetDateTime(9);
															personInfo.IsSpecificYear = reader.GetBoolean(10);
                                                        }
                                                    }
                                                    , new Parameter("@cusId", objectId));
            return personInfo;
        }

		public API_MODEL.PersonInfo FetchMemberByMemberCode(string memberCode)
		{
			API_MODEL.PersonInfo personInfo = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "[dbo].[sp_api_member_fetch_by_member_code_v1]"
													, (reader) =>
													{
														while (reader.Read())
														{
															if (personInfo == null)
																personInfo = new API_MODEL.PersonInfo();

															personInfo.PersonId = reader.GetInt32(0);
															personInfo.Code = reader.GetString(1);
															personInfo.TitleId = reader.GetInt32(2);
															personInfo.FirstName = reader.GetString(3);
															personInfo.LastName = reader.GetString(4);
															personInfo.BirthDate = reader.GetDateTime(5);
															personInfo.ContactNumber = reader.GetString(6);
															personInfo.Email = reader.GetString(7);
															personInfo.RecStatus = reader.GetInt16(8);
															personInfo.CreateDate = reader.GetDateTime(9);
															personInfo.IsSpecificYear = reader.GetBoolean(10);
														}
													}
													, new Parameter("@memberCode", memberCode));
			return personInfo;
		}

		public int FetchMemberId(string memberCode)
		{
			int personId = 0;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.sp_api_member_fetch_id_v1"
													, (reader) => {
														if (reader.Read())
														{
															personId = reader.GetInt32("person_id");
														}
													}
													, new Parameter("@member_code", memberCode));

			return personId;
		}

        public string MemberUpdatePasswordApi(int personId, string password, string newPassword, int updateBy)
        {
            var returnCode = "-999";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString,
                "[dbo].[sp_api_member_update_password_v1]",
                (reader) =>
                {
                    if (reader != null)
                    {
                        if (reader.Read())
                        {
                            returnCode = reader.GetString(0);
                        }
                    }
                }
                , new Parameter("@person_id", personId)
                , new Parameter("@password", password)
                , new Parameter("@newPassword", newPassword)
                , new Parameter("@by", updateBy)
            );

            return returnCode;
        }
    }
}