﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Person;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories
{
	public class PersonQuestionRepository : IPersonQuestionRepository
	{
		private readonly IDBHelper _dbHelper;

		public PersonQuestionRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}

		public IEnumerable<PersonQuestion> GetPersonQuestions(int personId)
		{
			var questions = new List<PersonQuestion>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.member_question_fetch_by_person_id_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															questions.Add(GetPersonQuestionEntity(reader));
														}
													}
													, new Parameter("@person_id", personId));
			return questions;
		}

		public void UpdatePersonQuestion(PersonQuestion question)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												   , "dbo.member_question_update_v1"
												   , new Parameter("@person_question_id", question.PersonQuestionId)
												   , new Parameter("@person_id", question.PersonId)
												   , new Parameter("@question_id", question.Question.QuestionId)
												   , new Parameter("@answer", question.Answer)
												   , new Parameter("@by", question.RecModifiedBy)
												   , new Parameter("@rec_status", (int)question.RecStatus));
		}

		public void InsertPersonQuestion(PersonQuestion question)
		{
			_dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
												   , "dbo.member_question_insert_v1"
												   , new Parameter("@person_id", question.PersonId)
												   , new Parameter("@question_id", question.Question.QuestionId)
												   , new Parameter("@answer", question.Answer)
												   , new Parameter("@by", question.RecCreatedBy));
		}

		private PersonQuestion GetPersonQuestionEntity(EntityDataReader reader)
		{
			return new PersonQuestion()
			{
				PersonQuestionId = reader.GetInt32("person_question_id"),
				PersonId = reader.GetInt32("person_id"),
				Question = new Models.CommonDTO.Lookup.Question()
				{
					QuestionId = reader.GetInt32("question_id"),
					QuestionDetail = reader.GetNullableString("question_detail")
				},
				Answer = reader.GetString("answer"),
				RecStatus = (RecStatus)reader.GetInt16("rec_status"),
				RecCreatedBy = reader.GetString("rec_created_by").ToString(),

				RecCreatedWhen = reader.GetDateTime("rec_created_when"),
				RecModifiedBy = reader.GetNullableString("rec_modified_by") ?? string.Empty,
				RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when")
			};
		}
	}
}