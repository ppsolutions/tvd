﻿using System;
using System.Collections.Generic;
using System.Configuration;
using PPSolution.Core.Models;
using tvd.model.Helpers;
using tvd.model.Models;
using tvd.model.Models.CommonDTO;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.model.Repositories
{
	public class EmployeeRepository : IEmployeeRepository
	{
		private readonly IDBHelper _dbHelper;

		public EmployeeRepository(IDBHelper dbHelper)
		{
			_dbHelper = dbHelper;
		}

	    public Employee Authenticate(string username, string password)
		{
            //TODO: apply LDAP checking
            string _user = "";
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                        , "[dbo].[employee_authenticate_v1]"
                                        , (reader) =>
                                        {
                                            while (reader.Read())
                                            {
                                                _user = reader.GetString("EMPLOYEE_CODE");
                                            }
                                        },
                                        new Parameter("@username", username)
                                        , new Parameter("@password", MD5Hash(password)));
            return GetEmployee(_user);            
		}

        public string MD5Hash(string input)
        {
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            System.Security.Cryptography.MD5CryptoServiceProvider md5provider = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new System.Text.UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }


        public IList<AccessControl> GetAccessControlRole(int roleId)
        {
            var accessControlList = new List<AccessControl>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_access_control_fetch_role_access_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var accessControl = new AccessControl();
                                                            accessControl.PageTypeId = reader.GetInt32("page_type_id");
                                                            accessControl.RecStatus = reader.GetInt16("rec_status");
                                                            accessControlList.Add(accessControl);
                                                        }
                                                    },
                                                    new Parameter("@i_role_id", roleId));
            return accessControlList;
        }

        public IList<AccessControl> GetAccessControlUser(string employeeCode)
        {
            var accessControlList = new List<AccessControl>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_access_control_fetch_employee_access_v1]"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var accessControl = new AccessControl();
                                                            accessControl.PageTypeId = reader.GetInt32("page_type_id");
                                                            accessControl.RecStatus = reader.GetInt16("rec_status");
                                                            accessControlList.Add(accessControl);
                                                        }
                                                    },
                                                    new Parameter("@employee_code", employeeCode));
            return accessControlList;
        }

        public Employee GetEmployee(string employeeCode)
		{
			Employee employee = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.employee_fetch_by_employee_code_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															employee = GetEmployeeEntity(reader);
														}
													},
													new PPSolution.Core.Models.Parameter("@employee_code", employeeCode));
			return employee;
		}

		public ResultSet<Employee> GetEmployees(string criteria, int? roleId, int pageSize, int pageNumber)
		{
			var employees = new ResultSet<Employee>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.employee_fetch_by_criteria_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															employees.Add(GetEmployeeEntity(reader));

														    if (employees.TotalItems == 0)
														    {
														        employees.TotalItems = reader.GetInt32("max_rows");
														    }
														}
													},
													new Parameter("@criteria", criteria),
													new Parameter("@role_id", roleId),
                                                    new Parameter("@page_size", pageSize),
                                                    new Parameter("@page_number", pageNumber));
			return employees;
		}

		private Employee GetEmployeeEntity(EntityDataReader reader)
		{
            string[] names = reader.GetString("FULLNAMETHAI").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Employee emp = new Employee();
            emp.EmployeeCode = reader.GetString("EMPCODE");
            emp.FullName = reader.GetString("FULLNAMETHAI");
            emp.Age = reader.GetInt32("EMPAGE");
            emp.ContactNumber = string.Empty; //Need more info
            emp.EmailAddress = string.Empty; // Need more info
            emp.Gender = (Gender)reader.GetInt32("CODSEX");
            emp.JobTitle = reader.GetNullableString("POSITIONNAME");

            if (names.Length > 2)
            {
                emp.PrefixName = names[0];
                emp.FirstName = names[1];
                emp.LastName = names[2];
            }
            else
            {
                emp.PrefixName = string.Empty;
                emp.FirstName = names[0];
                emp.LastName = names[1];
            }


            //Mapping Role and Permission
            var roleId = reader.GetNullableInt32("role_id");

            if (roleId.HasValue)
            {
                emp.EmployeeInRole = new RolePermission();
                emp.EmployeeInRole.IsPermisInAuditReportSystem = reader.GetBoolean("is_permit_in_audit_report_system");
                emp.EmployeeInRole.IsPermitInApproveSystem = reader.GetBoolean("is_permit_in_approve_system");
                emp.EmployeeInRole.IsPermitInEmployeeManagementSystem = reader.GetBoolean("is_permit_in_employee_management_system");
                emp.EmployeeInRole.IsPermitInMemberRegistrationSystem = reader.GetBoolean("is_permit_in_member_registration_system");
                emp.EmployeeInRole.IsPermitInNewsSystem = reader.GetBoolean("is_permit_in_news_system");
                emp.EmployeeInRole.IsPermitInPointSettingSystem = reader.GetBoolean("is_permit_in_point_setting_system");
                emp.EmployeeInRole.IsPermitInReportSystem = reader.GetBoolean("is_permit_in_report_system");
                emp.EmployeeInRole.IsPermitInRoleSystem = reader.GetBoolean("is_permit_in_role_system");
                emp.EmployeeInRole.RecStatus = (RecStatus)reader.GetInt32("rp_rec_status");
                //RecCreatedBy = new Models.CommonDTO.Person.PersonLite()
                //{
                //    PersonId = reader.GetInt32("rp_rec_created_by"),
                //},
                //RecCreatedWhen = reader.GetDateTime("rp_rec_created_when"),
                //RecModifiedBy = reader.GetNullableInt32("rp_rec_modified_by") ?? 0,
                //RecModifiedWhen = reader.GetNullableDateTime("rp_rec_modified_when") ?? new DateTime(1900, 1, 1),
                emp.EmployeeInRole.Role = new Role
                {
                    RoleId = roleId.Value,
                    RoleName = reader.GetString("role_name"),
                    //RecCreatedBy = new Models.CommonDTO.Person.PersonLite()
                    //{
                    //    PersonId = reader.GetInt32("r_rec_created_by"),
                    //},
                    //RecCreatedWhen = reader.GetDateTime("r_rec_created_when"),
                    //RecModifiedBy = reader.GetNullableInt32("r_rec_modified_by") ?? 0,
                    //RecModifiedWhen = reader.GetNullableDateTime("r_rec_modified_when") ?? new DateTime(1900, 1, 1)
                };

            }
            return emp;
        }
	}
}