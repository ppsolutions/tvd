﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.CommonDTO.MemberPoint;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.CommonDTO.Products;
using tvd.model.Models.Enum;
using tvd.model.V;

namespace tvd.model.Repositories
{
    public class LookupRepository : ILookupRepository
    {
        private readonly IDBHelper _dbHelper;

        public LookupRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<City> GetCities()
        {
            var cities = new List<City>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.city_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var city = new City()
                                                            {
                                                                CityId = reader.GetInt32("city_id"),
                                                                CityName = reader.GetString("city_name"),
                                                                RecStatus = (RecStatus)reader.GetInt16("rec_status")
                                                            };
                                                            cities.Add(city);
                                                        }
                                                    });
            return cities;
        }

        public IEnumerable<Title> GetTitles()
        {
            var titles = new List<Title>();
            _dbHelper.GetConnection().ExecuteReader(System.Configuration.ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.title_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var title = new Title()
                                                            {
                                                                TitleId = reader.GetInt32("title_id"),
                                                                TitleName = reader.GetString("title_name"),
                                                                IsLegalPerson = reader.GetBoolean("is_legal_person"),
                                                                RecStatus = (RecStatus)reader.GetInt32("rec_status")
                                                            };
                                                            titles.Add(title);
                                                        }
                                                    });
            return titles;
        }

        public IEnumerable<Question> GetQuestions()
        {
            var questions = new List<Question>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.question_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var question = new Question()
                                                            {
                                                                QuestionId = reader.GetInt32("question_id"),
                                                                QuestionDetail = reader.GetString("question_detail"),
                                                                RecStatus = (RecStatus)reader.GetInt16("rec_status")
                                                            };
                                                            questions.Add(question);
                                                        }
                                                    });
            return questions;
        }

        public IEnumerable<AddressType> GetAddressTypes()
        {
            return Enum.GetValues(typeof(AddressType)).Cast<AddressType>();
        }

        public IEnumerable<ApplyType> GetApplyTypes()
        {
            return Enum.GetValues(typeof(ApplyType)).Cast<ApplyType>();
        }

        public IEnumerable<PersonType> GetPersonTypes()
        {
            var personTypes = new List<PersonType>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.person_type_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var addressType = new PersonType()
                                                            {
                                                                PersonTypeId = reader.GetInt32("person_type_id"),
                                                                PersonTypeName = reader.GetString("person_type_name"),
                                                                RecStatus = (RecStatus)reader.GetInt16("rec_status")
                                                            };
                                                            personTypes.Add(addressType);
                                                        }
                                                    });
            return personTypes;
        }

        public IEnumerable<PointLevel> GetPointLevels()
        {
            var pointLevels = new List<PointLevel>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.point_level_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            var pointLevel = new PointLevel()
                                                            {
                                                                PointLevelId = reader.GetInt32("point_level_id"),
                                                                LevelName = reader.GetString("point_level_name"),
                                                                PricePerPoint = reader.GetDouble("price_per_point"),
                                                                RequiredPoint = reader.GetInt32("required_point"),
                                                                RequiredPaid = reader.GetDouble("required_paid"),
                                                                RecStatus = (RecStatus)reader.GetInt16("rec_status")
                                                            };
                                                            pointLevels.Add(pointLevel);
                                                        }
                                                    });
            return pointLevels;
        }

		public IEnumerable<Bank> GetBanks()
		{
			var banks = new List<Bank>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.bank_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
                                                            var bank = new Bank()
                                                            {
                                                                Id = Convert.ToInt16(reader.GetDouble("bank_id")),
                                                                Name = reader.GetString("bank_name"),
                                                                RecStatus = (RecStatus)(reader.GetInt32("rec_status"))
                                                            };
															banks.Add(bank);
														}
													});
			return banks;
		}

		public IEnumerable<CardType> GetCardTypes()
		{
			var cardTypes = new List<CardType>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.card_type_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var bank = new CardType()
															{
																Id = Convert.ToInt16(reader.GetDouble("card_type_id")),
																Name = reader.GetString("card_type_name"),
																RecStatus = (RecStatus)reader.GetInt32("rec_status")
															};
															cardTypes.Add(bank);
														}
													});
			return cardTypes;
		}

		public IEnumerable<PaymentType> GetPaymentType()
		{
			var paymentTypes = new List<PaymentType>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.payment_type_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var bank = new PaymentType()
															{
																Id = Convert.ToInt16(reader.GetDouble("payment_type_id")),
																Name = reader.GetString("payment_type_name"),
																RecStatus = (RecStatus)reader.GetInt32("rec_status")
															};
															paymentTypes.Add(bank);
														}
													});
			return paymentTypes;
		}

		public IEnumerable<Product> GetProducts()
		{
			var products = new List<Product>();
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.product_info_fetch_v1"
													, (reader) =>
													{
														while (reader.Read())
														{
															var product = new Product()
															{
																ProductId = reader.GetString("product_id"),
																ProductCode = reader.GetString("product_code"),
																ProductName = reader.GetString("product_name"),
																RecStatus = (RecStatus)reader.GetInt32("rec_status")
															};
															products.Add(product);
														}
													});
			return products;
		}

		public Product GetProduct(string productCode)
		{
			Product product = null;
			_dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
													, "dbo.product_info_fetch_by_code_v1"
													, (reader) =>
													{
														if (reader.Read())
														{
															product = new Product()
															{
																ProductId = reader.GetString("product_id"),
																ProductCode = reader.GetString("product_code"),
																ProductName = reader.GetString("product_name"),
																RecStatus = (RecStatus)reader.GetInt32("rec_status")
															};
														}
													},
													new PPSolution.Core.Models.Parameter("@code", productCode));
			return product;

		}

        public ProductInfo GetProductInfoFromView(string code)
        {
            ProductInfo productInfo = null;

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.sp_view_fetch_product_info_by_code_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader != null)
                                                        {
                                                            if (reader.Read())
                                                            {
                                                                if (productInfo == null)
                                                                    productInfo = new ProductInfo();

                                                                productInfo.Id = reader.GetDouble("ID").ToString();
                                                                productInfo.Code = reader.GetString("CODE");
                                                                productInfo.Name = reader.GetString("NAME_TH");
                                                                productInfo.Active = reader.GetString("ACTIVE");
                                                            }
                                                        }
                                                    },
                                                    new Parameter("@code", code));
            return productInfo;
        }

        public IEnumerable<ProductInfo> GetProductInfoFromName(string name)
        {
            List<ProductInfo> productInfos = new List<ProductInfo>();

            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                , "dbo.sp_view_fetch_product_info_by_name_v1"
                , (reader) =>
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            var productInfo = new ProductInfo
                            {
                                Id = Convert.ToString(reader.GetDouble("ID")),
                                Code = reader.GetString("CODE"),
                                Name = reader.GetString("NAME_TH"),
                                Active = reader.GetString("ACTIVE")
                            };
                            productInfos.Add(productInfo);

                        }
                    }
                },
                new Parameter("@name", name));
            return productInfos;
        }

        public Product GetProductApi(string productCode)
        {
            Product product = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "[dbo].[sp_api_product_info_fetch_by_code_v1]"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            product = new Product()
                                                            {
                                                                ProductId = reader.GetString("product_id"),
                                                                ProductCode = reader.GetString("product_code"),
                                                                ProductName = reader.GetString("product_name"),
                                                                RecStatus = (RecStatus)reader.GetInt32("rec_status")
                                                            };
                                                        }
                                                    },
                                                    new Parameter("@code", productCode));
            return product;

        }

        public DateTime? GetPointLevelExpireDate()
        {
            DateTime? expireDate = null;
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.point_level_expire_fetch_v1"
                                                    , (reader) =>
                                                    {
                                                        if (reader.Read())
                                                        {
                                                            expireDate = reader.GetNullableDateTime("expire_date");
                                                        }
                                                    });
            return expireDate;
        }
    }
}