﻿using PPSolution.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Address;
using tvd.model.Models.CommonDTO.Lookup;
using tvd.model.Models.Enum;

namespace tvd.model.Repositories
{
    public class PersonAddressRepository : IPersonAddressRepository
    {
        private readonly IDBHelper _dbHelper;

        public PersonAddressRepository(IDBHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<PersonAddress> GetPersonAddress(int personId)
        {
            var addresses = new List<PersonAddress>();
            _dbHelper.GetConnection().ExecuteReader(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                    , "dbo.member_address_fetch_by_person_id_v1"
                                                    , (reader) =>
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            addresses.Add(GetPersonAddressEntity(reader));
                                                        }
                                                    }
                                                    , new Parameter("@person_id", personId));
            return addresses;
        }

        public void InsertPersonAddress(PersonAddress address)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                                   , "dbo.member_address_insert_v1"
                                                   , new Parameter("@person_id", address.PersonId)
                                                   , new Parameter("@address_type_id", address.AddressTypeId)
                                                   , new Parameter("@address_1", address.Address1)
                                                   , new Parameter("@address_2", address.Address2)
                                                   , new Parameter("@city_id", address.City.CityId)
                                                   , new Parameter("@postal_code", address.PostalCode)
                                                   , new Parameter("@by", address.RecCreatedBy));
        }

        public void UpdatePersonAddress(PersonAddress address)
        {
            _dbHelper.GetConnection().ExecuteNonQuery(ConfigurationManager.ConnectionStrings["tvd_db"].ConnectionString
                                       , "dbo.member_address_update_v1"
									   , new Parameter("@person_address_id", address.PersonAddressId)
                                       , new Parameter("@person_id", address.PersonId)
                                       , new Parameter("@address_type_id", address.AddressTypeId)
                                       , new Parameter("@address_1", address.Address1)
                                       , new Parameter("@address_2", address.Address2)
                                       , new Parameter("@city_id", address.City.CityId)
                                       , new Parameter("@postal_code", address.PostalCode)
                                       , new Parameter("@by", address.RecModifiedBy)
                                       , new Parameter("@rec_status", (int)address.RecStatus));
        }

        private PersonAddress GetPersonAddressEntity(EntityDataReader reader)
        {  
            return new PersonAddress()
            {
                PersonAddressId = reader.GetInt32("person_address_id"),
                PersonId = reader.GetInt32("person_id"),
                AddressTypeId = (AddressType)reader.GetInt32("address_type_id"),
                Address1 = reader.GetString("address_1"),
                Address2 = reader.GetNullableString("address_2") ?? string.Empty,
                City = new City
                {
                    CityId = reader.GetInt32("city_id"),
                    CityName = reader.GetNullableString("city_name") ?? string.Empty
                },
                PostalCode = reader.GetNullableString("postal_code"),
                RecStatus = (RecStatus)reader.GetInt16("rec_status"),
                RecCreatedBy = reader.GetString("rec_created_by"),
                RecCreatedWhen = reader.GetDateTime("rec_created_when"),
                RecModifiedBy = reader.GetNullableString("rec_modified_by") ?? string.Empty,
                RecModifiedWhen = reader.GetNullableDateTime("rec_modified_when")
            };
        }

    }
}