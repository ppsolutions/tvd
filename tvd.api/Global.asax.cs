﻿using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using tvd.api.Controllers;
using tvd.model.Helpers;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;

namespace tvd.api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            var config = GlobalConfiguration.Configuration;
            var formatters = config.Formatters;
            formatters.Remove(formatters.XmlFormatter);
            formatters.Add(formatters.JsonFormatter);

            AreaRegistration.RegisterAllAreas();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.Register(c =>
              HttpContext.Current != null ?
                  new HttpContextWrapper(HttpContext.Current) :
                    c.Resolve<System.Net.Http.HttpRequestMessage>().Properties["MS_HttpContext"])
                .As<HttpContextBase>()
                .InstancePerRequest();
            builder.RegisterType<NewsController>().InstancePerRequest();
            builder.Register(c => new Logger()).As<ILogger>().InstancePerRequest();
            builder.RegisterType<DBHelper>().As<IDBHelper>().SingleInstance();
            builder.RegisterType<NewsRepository>().As<INewsRepository>().SingleInstance();
            builder.RegisterType<MemberRepository>().As<IMemberRepository>().SingleInstance();
            builder.RegisterType<LookupRepository>().As<ILookupRepository>().SingleInstance();
            builder.RegisterType<RuleRepository>().As<IRuleRepository>().SingleInstance();
            builder.RegisterType<PointRepository>().As<IPointRepository>().SingleInstance();
            builder.RegisterType<ApproveRepository>().As<IApproveRepository>().SingleInstance();
            builder.RegisterType<GamificationPointRepository>().As<IGamificationPointRepository>().SingleInstance();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>().SingleInstance();
            
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }

    internal interface ILogger
    {

    }

    internal class Logger : ILogger
    {
        public Logger()
        {
        }
    }
}
