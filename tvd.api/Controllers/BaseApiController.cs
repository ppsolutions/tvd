﻿using System.Web;
using System.Web.Http;

namespace tvd.api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public readonly HttpContextBase ContextBase;

        public BaseApiController(HttpContextBase contextBase)
        {
            this.ContextBase = contextBase;
        }
    }
}