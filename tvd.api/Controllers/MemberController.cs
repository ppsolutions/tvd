﻿using System.Web;
using System.Web.Http;
using tvd.api.Attributes;
using tvd.api.Models;
using tvd.api.Models.Constants;
using API_MODEL = tvd.model.Api.Models.Member;
using tvd.model.Repositories.Interfaces;
using tvd.api.Models.Member;
using System.Text;
using System.Security.Cryptography;
using System;
using System.IO;
using tvd.model.Helpers;

namespace tvd.api.Controllers
{
	[CamelCaseControllerConfig]
	public class MemberController : BaseApiController
	{
		private readonly IMemberRepository _memberRepository;

		public MemberController(HttpContextBase contextBase, IMemberRepository memberRepository) : base(contextBase)
		{
			_memberRepository = memberRepository;
		}

		/// <summary>
		/// ค้นหาข้อมูลสมาชิก โดยรหัสลูกค้า ของ TVD
		/// </summary>
		/// <param name="customerId">รหัสลูกค้า ของ TVD</param>
		/// <response StatusCode="BAD_REQUEST">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <returns>ข้อมูลสมาชิก</returns>
		[HttpGet]
		[Route("api/v1/tvd/member/{customerId}")]
		public IHttpActionResult GetMember([FromUri]string customerId)
		{
			if (string.IsNullOrEmpty(customerId))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}
			try
			{
				var person = _memberRepository.FetchMember(customerId);
				return Ok(person);
			}
			catch (Exception ex)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = ex.Message//EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}
		}

		/// <summary>
		/// ค้นหาข้อมูลสมาชิก โดยรหัสลูกค้า 16 หลัก
		/// </summary>
		/// <param name="memberCode">รหัสลูกค้า 16 หลัก</param>
		/// <response StatusCode="BAD_REQUEST">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <returns>ข้อมูลสมาชิก</returns>
		[HttpGet]
		[Route("api/v1/tvd/memberbymembercode/{memberCode}")]
		public IHttpActionResult GetMemberByMemberCode([FromUri]string memberCode)
		{
			if (string.IsNullOrEmpty(memberCode))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}
			try
			{
				var person = _memberRepository.FetchMemberByMemberCode(memberCode);
				return Ok(person);
			}
			catch (Exception ex)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = ex.Message//EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

		}

		/// <summary>
		/// สร้าง หรือ ปรับปรุงข้อมูล สมาชิก
		/// </summary>
		/// <param name="dto">MemberDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INSERT_MEMBER_FAIL">Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลใหม่ได้</response>
		/// <response StatusCode="UPDATE_MEMBER_FAIL">Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลได้</response>
		[HttpPost]
		[Route("api/v1/tvd/member")]
		public IHttpActionResult PostMember(API_MODEL.MemberDto dto)
		{
			dto.Password = PasswordHasher.EncryptString(dto.Password);

			var result = this._memberRepository.MemberUpsertApi(dto, App.AppId);

			return Ok(GetResult(result.Item1));
		}

		/// <summary>
		/// ปรับปรุงข้อมูลสมาชิก
		/// </summary>
		/// <param name="dto">MemberDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INSERT_MEMBER_FAIL">Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลใหม่ได้</response>
		/// <response StatusCode="UPDATE_MEMBER_FAIL">Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลได้</response>
		[HttpPut]
		[Route("api/v1/tvd/member")]
		public IHttpActionResult PutMember(API_MODEL.MemberDto dto)
		{
			if (!dto.PersonId.HasValue)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			dto.Password = PasswordHasher.EncryptString(dto.Password);

			var result = this._memberRepository.MemberUpsertApi(dto, App.AppId);
			return Ok(GetResult(result.Item1));
		}

		/// <summary>
		/// ปรับปรุง password
		/// </summary>
		/// <param name="dto">MemberUpdatePasswordDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INVALID_CURRENT_PASSWORD">รหัสผ่านปัจจุบันไม่ถูกต้อง</response>
		/// <response StatusCode="MEMBER_NOT_FOUND">ไม่พบสมาชิกในระบบ</response>
		[HttpPut]
		[Route("api/v1/tvd/member/updatepwd")]
		public IHttpActionResult UpdatePasswordPut(MemberUpdatePasswordDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var passwordIsValid = IsMatchPassword(dto.PersonId, dto.OldPassword);
			var newPasswordIsValid = IsValidEncrypt(dto.NewPassword);
			if (!passwordIsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.INVALID_CURRENT_PASSWORD,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_CURRENT_PASSWORD)
				});
			}

			var oldPwd = PasswordHasher.EncryptString(dto.OldPassword);
			var newPwd = PasswordHasher.EncryptString(dto.NewPassword);

			var returnCode = this._memberRepository.MemberUpdatePasswordApi(dto.PersonId, oldPwd, newPwd, App.AppId);

			var operationResult = new OperationResult();
			switch (returnCode)
			{
				case "000":
					{
						operationResult.StatusCode = ApiStatusCode.SUCCESSFUL;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL);
						break;
					}
				case "101":
					{
						operationResult.StatusCode = ApiStatusCode.MEMBER_NOT_FOUND;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_NOT_FOUND);
						break;
					}
				case "102":
					{
						operationResult.StatusCode = ApiStatusCode.BAD_REQUEST;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST);
						break;
					}
				case "103":
					{
						operationResult.StatusCode = ApiStatusCode.INVALID_CURRENT_PASSWORD;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_CURRENT_PASSWORD);
						break;
					}
				default:
					{
						operationResult.StatusCode = ApiStatusCode.BAD_REQUEST;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST);
						break;
					}
			}

			return Ok(operationResult);
		}

		private bool IsMatchPassword(int personId, string password)
		{
			var encryptPassword = PasswordHasher.EncryptString(password);

			var member = _memberRepository.GetMember(personId, model.Models.Enum.Source.MEMBERSHIP_WEBSITE);

			return member.EncryptedPassword == encryptPassword;
		}

		private string Encrypt(string value)
		{
			string EncryptionKey = "abc123";
			byte[] clearBytes = Encoding.Unicode.GetBytes(value);
			using (Aes encryptor = Aes.Create())
			{
				Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
				encryptor.Key = pdb.GetBytes(32);
				encryptor.IV = pdb.GetBytes(16);
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
					{
						cs.Write(clearBytes, 0, clearBytes.Length);
						cs.Close();
					}
					value = Convert.ToBase64String(ms.ToArray());
				}
			}
			return value;
		}

		private bool IsValidEncrypt(string cipherText)
		{
			var valid = false;
			if (!string.IsNullOrEmpty(cipherText))
			{
				try
				{
					string EncryptionKey = "abc123";
					cipherText = cipherText.Replace(" ", "+");
					byte[] cipherBytes = Convert.FromBase64String(cipherText);
					using (Aes encryptor = Aes.Create())
					{
						Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
						encryptor.Key = pdb.GetBytes(32);
						encryptor.IV = pdb.GetBytes(16);
						using (MemoryStream ms = new MemoryStream())
						{
							using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
							{
								cs.Write(cipherBytes, 0, cipherBytes.Length);
								cs.Close();
							}
							cipherText = Encoding.Unicode.GetString(ms.ToArray());
						}
					}
					valid = true;
				}
				catch { }
			}
			return valid;
		}

		private OperationResult GetResult(string returnCode)
		{
			var operationResult = new OperationResult();
			switch (returnCode)
			{
				case "000":
					{
						operationResult.StatusCode = ApiStatusCode.SUCCESSFUL;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL);
						break;
					}
				case "101":
					{
						operationResult.StatusCode = ApiStatusCode.INSERT_MEMBER_FAIL;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INSERT_MEMBER_FAIL);
						break;
					}
				case "102":
					{
						operationResult.StatusCode = ApiStatusCode.UPDATE_MEMBER_FAIL;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.UPDATE_MEMBER_FAIL);
						break;
					}
				default:
					{
						operationResult.StatusCode = ApiStatusCode.BAD_REQUEST;
						operationResult.StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST);
						break;
					}
			}

			return operationResult;
		}
	}
}
