﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using tvd.api.Attributes;
using tvd.api.Models;
using tvd.api.Models.Constants;
using tvd.api.Models.Gamification;
using tvd.api.Models.Member;
using tvd.api.Models.Order;
using tvd.model.Api.Models.Member;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Models.Order;
using tvd.model.Models.Point;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;

namespace tvd.api.Controllers
{
	[CamelCaseControllerConfig]
	public class PointController : BaseApiController
	{
		private readonly IRuleRepository _ruleRepository;
		private readonly IPointRepository _pointRepository;
		private readonly IApproveRepository _approveRepository;
		private readonly IGamificationPointRepository _gamificationPointRepository;
		private readonly IOrderRepository _orderRepository;
		private readonly ILookupRepository _lookupRepository;
		private readonly IMemberRepository _memberRepository;

		public PointController(HttpContextBase contextBase,
			IRuleRepository ruleRepository,
			IPointRepository pointRepository,
			IApproveRepository approveRepository,
			ILookupRepository lookupRepository,
			IGamificationPointRepository gamificationPointRepository,
			IOrderRepository orderRepository,
			IMemberRepository memberRepository) : base(contextBase)
		{
			_ruleRepository = ruleRepository;
			_pointRepository = pointRepository;
			_approveRepository = approveRepository;
			_gamificationPointRepository = gamificationPointRepository;
			_orderRepository = orderRepository;
			_lookupRepository = lookupRepository;
			_memberRepository = memberRepository;
		}

		/// <summary>
		/// แสดง rule ส่ง campaign id, promotion id มา return rule การคิดคะแนน
		/// </summary>
		[HttpGet]
		[Route("api/v1/tvd/point/getrules/{code}/{codeType}")]
		public IHttpActionResult GetRules([FromUri]string code, int codeType)
		{
			var result = _ruleRepository.GetRulesByCodeApi(code, codeType);
			return Ok(result);
		}

		/// <summary>
		/// แสดงคะแนนสมาชิก customer ส่ง source,customer_id มา return membership_code, total_point, total_point_pending
		/// </summary>
		[HttpGet]
		[Route("api/v1/tvd/point/memberpoint/{personId}")]
		public IHttpActionResult GetMemberPoint([FromUri]int personId)
		{
			var result = _pointRepository.GetMemberPointsApi(personId, true).FirstOrDefault();
			if (result != null)
			{
				return Ok(new
				{
					personId = result.PersonId,
					availablePoint = result.Point,
					pendingPoint = result.PendingPoint
				});
			}

			return Ok();
		}

		///// <summary>
		///// ปรับระดับสมาชิก ส่ง membership_code, membership_level มาปรับปรุง (รออนุมัติ)
		///// 000 = Success
		///// 001 = Bad request!
		///// 101 = ไม่พบสมาชิกนี้ในระบบ
		///// 102 = สมาชิกอยู่ระดับนี้อยู่แล้ว
		///// 103 = Not found point configuration!
		///// 104 = ไม่พบระดับสมาชิกที่ต้องการอัพเดท
		///// 105 = Request approve is failed!
		///// 999 - Unknow error
		///// </summary>


		/// <summary>
		/// ปรับระดับสมาชิก ส่ง membership_code, membership_level มาปรับปรุง (รออนุมัติ)
		/// </summary>
		/// <param name="dto">MemberLevelDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="MEMBER_NOT_FOUND">ไม่พบสมาชิกในระบบ</response>
		/// <response StatusCode="MEMBER_LEVEL_ALREADY_THE_SAME">สมาชิกอยู่ระดับนี้อยู่แล้ว</response>
		/// <response StatusCode="POINT_CONFIG_NOT_FOUND">ไม่พบคะแนนที่ตั้งค่าไว้</response>
		/// <response StatusCode="MEMBER_UPDATE_LEVEL_NOT_FOUND">ไม่พบระดับสมาชิกที่ต้องการอัพเดท</response>
		/// <response StatusCode="REQUEST_TO_APPROVE_IS_FAILED">ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้</response>
		[HttpPost]
		[Route("api/v1/tvd/point/memberlevel")]
		public IHttpActionResult UpdateMemberLevel(MemberLevelDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var currentPointLevel = _pointRepository.GetMemberPointLevelApi(dto.PersonId);
			if (currentPointLevel == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_NOT_FOUND)
				});
			}

			if (currentPointLevel.PointLevelId == dto.PointLevel)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_LEVEL_ALREADY_THE_SAME,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_LEVEL_ALREADY_THE_SAME)
				});
			}

			var pointLevels = _pointRepository.GetPointLevel();
			if (pointLevels == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.POINT_CONFIG_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.POINT_CONFIG_NOT_FOUND)
				});
			}

			var requestPointLevel = pointLevels.Where(l => l.PointLevelId == dto.PointLevel).FirstOrDefault();
			if (requestPointLevel == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_UPDATE_LEVEL_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_UPDATE_LEVEL_NOT_FOUND)
				});
			}

			var jsonObject = JsonConvert.SerializeObject(new MemberPointLevel
			{
				PersonId = dto.PersonId,
				Point = Math.Abs(requestPointLevel.RequiredPoint),
				PointLevelId = dto.PointLevel,
				LevelName = requestPointLevel.LevelName,
				Source = new EmployeeInfo() { EmployeeCode = App.AppKey }
			});
			var pendingAppoveObject = new Approve();
			pendingAppoveObject.ApproveType = ApproveType.UPGRADE_MEMBER_LEVEL;
			pendingAppoveObject.ObjectId = dto.PersonId;
			pendingAppoveObject.ObjectValue = jsonObject;
			pendingAppoveObject.RecCreatedBy = App.AppKey;
			var requestApprove = _approveRepository.InsertApi(pendingAppoveObject);

			if (!requestApprove.Equals("000"))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED)
				});
			}

			return Ok(new OperationResult()
			{
				StatusCode = ApiStatusCode.SUCCESSFUL,
				StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL)
			});
		}

		/// <summary>
		/// ปรับคะแนนสมาชิก ส่ง membership_code, point  มาปรับปรุง (รออนุมัติ)
		/// </summary>
		/// <param name="dto">MemberPointDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INVALID_ACTION_TYPE">ประเภท Action ไม่ถูกต้อง โปรดระบุระหว่าง Add หรือ Delete</response>
		/// <response StatusCode="MEMBER_NOT_FOUND">ไม่พบสมาชิกในระบบ</response>
		/// <response StatusCode="INSUFFICIENT_POINT">คะแนนสะสมไม่เพียงพอ</response>
		/// <response StatusCode="POINT_EXPIRE_DATE_NOT_FOUND">ไม่พบวันหมดอายุคะแนน</response>
		/// <response StatusCode="MEMBER_UPDATE_LEVEL_NOT_FOUND">ไม่พบระดับสมาชิกที่ต้องการอัพเดท</response>
		/// <response StatusCode="REQUEST_TO_APPROVE_IS_FAILED">ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้</response>
		[HttpPost]
		[Route("api/v1/tvd/point/memberpoint")]
		public IHttpActionResult UpdateMemberPoint(MemberPointDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			if (dto.ActionType != ActionType.ADD && dto.ActionType != ActionType.DELETE)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.INVALID_ACTION_TYPE,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_ACTION_TYPE)
				});
			}

			var type = dto.ActionType;
			var memberPoint = _pointRepository.GetMemberPointsApi(dto.PersonId, true);
			if (memberPoint == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_NOT_FOUND)
				});
			}

			if (type == ActionType.DELETE)
			{
				if (!ValidateMemberPointIsEnough(dto.PersonId, dto.Point))
				{
					return Ok(new OperationResult()
					{
						StatusCode = ApiStatusCode.INSUFFICIENT_POINT,
						StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INSUFFICIENT_POINT)
					});
				}
			}

			var updateMemberPoint = new MemberPoint
			{
				PersonId = dto.PersonId,
				RecCreatedBy = App.AppKey,
				Type = UpdatePointType.ADJUST,
				RecStatus = RecStatus.PENDING
			};
			switch (type)
			{
				case ActionType.ADD:
					updateMemberPoint.Point = Math.Abs(dto.Point);
					break;
				case ActionType.DELETE:
					updateMemberPoint.Point = Math.Abs(dto.Point) * -1;
					break;
			}
			if (updateMemberPoint.Point <= 0)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.INSUFFICIENT_POINT,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INSUFFICIENT_POINT)
				});
			}

			var expireYear = _pointRepository.GetPointConfig().ExpireYear;
			if (expireYear == null || !expireYear.HasValue)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.POINT_EXPIRE_DATE_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.POINT_EXPIRE_DATE_NOT_FOUND)
				});
			}


			int calculateExpireYear = 0;
			if (expireYear == 0)
			{
				calculateExpireYear = 9999;
			}
			else
			{
				calculateExpireYear = DateTime.Now.AddYears((expireYear.Value - 1)).Year;
			}

			var pointExpiredDate = new DateTime(calculateExpireYear, 12, 31, 23, 59, 59);
			updateMemberPoint.RecExpiredWhen = pointExpiredDate;

			var result = _pointRepository.InsertMemberPointApi(updateMemberPoint, (int)UpdatePointType.ADJUST);
			if (!result.Equals("000"))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var preFix = updateMemberPoint.Point > 0 ? "เพิ่ม" : "ลด";
			var json = new
			{
				PersonId = dto.PersonId,
				Description = $"{preFix} {Math.Abs(dto.Point):##,###} คะแนน",
				Source = new EmployeeInfo() { EmployeeCode = App.AppKey }
			};

			var jsonObject = JsonConvert.SerializeObject(json);
			var pendingAppoveObject = new Approve();
			pendingAppoveObject.ApproveType = ApproveType.UPDATE_REWARD_POINT;
			pendingAppoveObject.ObjectId = dto.PersonId;
			pendingAppoveObject.ObjectValue = jsonObject;
			pendingAppoveObject.RecCreatedBy = App.AppKey;
			var requestApprove = _approveRepository.InsertApi(pendingAppoveObject);
			if (!requestApprove.Equals("000"))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED)
				});
			}

			return Ok(new OperationResult()
			{
				StatusCode = ApiStatusCode.SUCCESSFUL,
				StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL)
			});
		}

		///// <summary>
		///// ปรับคะแนนสมาชิก จาก gamification ส่ง membership_code, gamification_code
		///// 000 = Success
		///// 001 = Bad request!
		///// 101 = Not found gamification code!
		///// 102 = Gamification expired!
		///// 103 = Unavailable gamification!
		///// 104 = Not found member point in system!
		///// 105 = Gamification point not enough!
		///// 106 = Point expired date is faile!
		///// 107 = Request change member level is failed!
		///// 108 = Request approve is failed!
		///// 999 - Unknow error
		///// </summary>


		/// <summary>
		/// ปรับคะแนนสมาชิก จาก gamification ส่ง membership_code, gamification_code
		/// </summary>
		/// <param name="dto">GamificationPointDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="GAMIFICATION_CODE_NOT_FOUND">ไม่พบรหัส Gamification</response>
		/// <response StatusCode="GAMIFICATION_EXPIRED">Gamification นี้หมดอายุ</response>
		/// <response StatusCode="GAMIFICATION_NOT_AVAILABLE">Gamification ไม่สามารถใช้งานได้ในขณะนี้</response>
		/// <response StatusCode="MEMBER_POINT_NOT_FOUND">ไม่พบคะแนนสมาชิก</response>
		/// <response StatusCode="NOT_ENOUGH_GAMIFICATION">คะแนน Gamification ไม่เพียงพอ</response>
		/// <response StatusCode="POINT_EXPIRE_DATE_NOT_FOUND">ไม่พบวันหมดอายุคะแนน</response>
		/// <response StatusCode="CHANGE_MEMBER_LEVEL_FAILED">ไม่สามารถเปลี่ยนแปลงระดับสมาชิกเนื่องจากเกิดข้อผิดพลาด</response>
		/// <response StatusCode="REQUEST_TO_APPROVE_IS_FAILED">ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้</response>
		[HttpPost]
		[Route("api/v1/tvd/point/gamification")]
		public IHttpActionResult UpdateGamificationPoint(GamificationPointDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var gameInfo = _gamificationPointRepository.GetGamificationInfoApi(dto.GamificationCode);
			if (gameInfo == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.GAMIFICATION_CODE_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.GAMIFICATION_CODE_NOT_FOUND)
				});
			}

			if (!(DateTime.Now >= gameInfo.ValidFrom && DateTime.Now <= gameInfo.ValidTo))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.GAMIFICATION_EXPIRED,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.GAMIFICATION_EXPIRED)
				});
			}

			if (gameInfo.RecStatus != RecStatus.ACTIVE)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.GAMIFICATION_NOT_AVAILABLE,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.GAMIFICATION_NOT_AVAILABLE)
				});
			}

			var memberPoint = _pointRepository.GetMemberPointsApi(dto.PersonId, true);
			if (memberPoint == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_POINT_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_POINT_NOT_FOUND)
				});
			}

			var updateMemberPoint = new MemberPoint
			{
				PersonId = dto.PersonId,
				RecCreatedBy = App.AppKey,
				Point = Math.Abs(gameInfo.IncreasePoint),
				RecStatus = RecStatus.PENDING,
				Type = UpdatePointType.ADJUST,
			};
			if (updateMemberPoint.Point <= 0)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.NOT_ENOUGH_GAMIFICATION,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.NOT_ENOUGH_GAMIFICATION)
				});
			}

			var pointExpiredDate = _pointRepository.GetPointConfig()?.ExpireDate;
			if (pointExpiredDate == null || !pointExpiredDate.HasValue)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.POINT_EXPIRE_DATE_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.POINT_EXPIRE_DATE_NOT_FOUND)
				});
			}
			updateMemberPoint.RecExpiredWhen = pointExpiredDate.Value;

			var result = _pointRepository.InsertMemberPointApi(updateMemberPoint, (int)UpdatePointType.ADJUST);
			if (!result.Equals("000"))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.CHANGE_MEMBER_LEVEL_FAILED,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.CHANGE_MEMBER_LEVEL_FAILED)
				});
			}

			var preFix = "เพิ่ม";
			var json = new
			{
				PersonId = dto.PersonId,
				Description = $"{preFix} {Math.Abs(updateMemberPoint.Point):##,###} คะแนน จาก Gamification code: {gameInfo.GamificationCode}",
				Source = new EmployeeInfo() { EmployeeCode = App.AppKey }
			};

			var jsonObject = JsonConvert.SerializeObject(json);
			var pendingAppoveObject = new Approve();
			pendingAppoveObject.ApproveType = ApproveType.UPDATE_REWARD_POINT;
			pendingAppoveObject.ObjectId = dto.PersonId;
			pendingAppoveObject.ObjectValue = jsonObject;
			pendingAppoveObject.RecCreatedBy = App.AppKey;
			var requestApprove = _approveRepository.InsertApi(pendingAppoveObject);
			if (!requestApprove.Equals("000"))
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED)
				});
			}


			return Ok(new OperationResult()
			{
				StatusCode = ApiStatusCode.SUCCESSFUL,
				StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL)
			});
		}

		private bool ValidateMemberPointIsEnough(int personId, int usedPoints)
		{
			var currentPoints = _pointRepository.GetMemberPointsApi(personId, false).ToList();
			var currentActivePoints = GetActivePoints(currentPoints);
			return currentActivePoints >= usedPoints;
		}

		private int GetActivePoints(List<MemberPoint> points)
		{
			//Active points
			var activePoints = points.Where(o => o.RecStatus == RecStatus.ACTIVE);
			//Get minus point
			var minusPoint = points
				.Where(o => o.Point < 0)
				.Sum(o => o.Point);

			//Get positive point that not expired
			var positivePoints = activePoints
				.Where(o => o.Point >= 0)
				.Sum(o => o.Point);

			//subtract by minus point
			positivePoints += minusPoint;

			if (positivePoints < 0) positivePoints = 0;
			return positivePoints;
		}

		/// <summary>
		/// แสดงคำนวณคะแนน ส่ง order, member id มา return คะแนนสรุปจากการคำนวณ
		/// </summary>
		/// <param name="dto">OrderPointDto</param>
		/// <returns>OrderPointDto</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="MEMBER_NOT_FOUND">ไม่พบสมาชิกในระบบ</response>
		/// <response StatusCode="INVALID_ORDER_ID">รหัสสั่งซื้อสินค้าไม่ถูกต้อง</response>
		/// <response StatusCode="ORDER_INFO_NOT_FOUND">ไม่พบรายการสั่งซื้อที่ระบุ</response>
		/// <response StatusCode="MEMBER_POINT_NOT_FOUND">ไม่พบคะแนนสมาชิก</response>
		[HttpPost]
		[Route("api/v1/tvd/point/member/order")]
		public IHttpActionResult GetMemberPointByOrder(OrderPointDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var personId = _memberRepository.FetchMemberId(dto.MemberCode);
			if (personId <= 0)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_NOT_FOUND)
				});
			}

			var orderId = _orderRepository.CheckOrderId(dto.OrderId);
			if (orderId <= 0)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.INVALID_ORDER_ID,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_ORDER_ID)
				});
			}

			var orderInfos = _orderRepository.FetchOrderInfo(dto.OrderId);
			if (orderInfos == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.ORDER_INFO_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.ORDER_INFO_NOT_FOUND)
				});
			}

			var memberPointLevel = _pointRepository.GetMemberPointLevelApi(personId);
			if (memberPointLevel == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_POINT_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_POINT_NOT_FOUND)
				});
			}

			//Get point config --> Expire and Min Purchase
			var pointConfig = _pointRepository.GetPointConfig();
			var basicPointConfig = _lookupRepository.GetPointLevels()?.ToList();

			var promotionRules = new List<PromotionRuleInfo>();
			var orderInfoList = new List<OrderInfo>();
			foreach (var orderInfo in orderInfos)
			{
				if (basicPointConfig != null && basicPointConfig.Any())
				{
					orderInfo.RegularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.SilverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.GoldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.PlatinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
				}

				var promoRules = _pointRepository.FetchAllRuleInPromotion(new PromotionDto() { Id = orderInfo.CampaignCode });
				//promotionRules.AddRange(promoRules);

				if (promoRules != null && promoRules.Any())
				{
					foreach (var r in promoRules)
					{
						orderInfoList.Add(ApplyPoint(r, orderInfo, memberPointLevel));
					}
				}
			}


			OrderInfo selectedOrderInfo = new OrderInfo();
			var earnedPoint = 0;

			if (orderInfoList.Count > 0)
			{
				switch ((PointLevelType)memberPointLevel.PointLevelId)
				{
					case PointLevelType.Regular:
						{
							selectedOrderInfo = orderInfoList.OrderByDescending(o => o.RegularPoint).First();
							earnedPoint = selectedOrderInfo.RegularPoint;
							break;
						}
					case PointLevelType.Silver:
						{
							selectedOrderInfo = orderInfoList.OrderByDescending(o => o.SilverPoint).First();
							earnedPoint = selectedOrderInfo.SilverPoint;
							break;
						}
					case PointLevelType.Gold:
						{
							selectedOrderInfo = orderInfoList.OrderByDescending(o => o.GoldPoint).First();
							earnedPoint = selectedOrderInfo.GoldPoint;
							break;
						}
					case PointLevelType.Platinum:
						{
							selectedOrderInfo = orderInfoList.OrderByDescending(o => o.PlatinumPoint).First();
							earnedPoint = selectedOrderInfo.PlatinumPoint;
							break;
						}
					default: break;
				}

				if (dto.ConfirmPoint)
				{
					var memberPoint = new MemberPoint
					{
						PersonId = personId,
						RecCreatedBy = App.AppId.ToString(),
						Type = UpdatePointType.ADJUST,
						Point = Math.Abs(earnedPoint),
						Remark = "ปรับปรุงคะแนน จาก API"
					};

					if (memberPoint.Point > 0)
					{
						int expireYear = _pointRepository.GetPointConfig().ExpireYear ?? 0;
						int calculateExpireYear = 0;
						if (expireYear == 0)
						{
							calculateExpireYear = 9999;
						}
						else
						{
							calculateExpireYear = DateTime.Now.AddYears((expireYear - 1)).Year;
						}

						var pointExpiredDate = new DateTime(calculateExpireYear, 12, 31, 23, 59, 59);
						memberPoint.RecExpiredWhen = pointExpiredDate;
					}

					var preFix = memberPoint.Point > 0 ? "เพิ่ม" : "ลด";
					var icon = memberPoint.Point > 0 ? "+" : "";
					var person = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE);

					if (!CheckAutoApproveAndUpdateMemberpoint())
					{
						memberPoint.RecStatus = RecStatus.PENDING;

						var json = new
						{
							Person = person,
							Description = $"{preFix} {Math.Abs(memberPoint.Point):##,###} คะแนน",
							Source = new EmployeeInfo() { EmployeeCode = App.AppId.ToString() }
						};

						//convert pending object to json
						var jsonObject = JsonConvert.SerializeObject(json);
						var pendingAppoveObject = new Approve();
						pendingAppoveObject.ApproveType = ApproveType.UPDATE_REWARD_POINT;
						pendingAppoveObject.ObjectId = personId;
						pendingAppoveObject.ObjectValue = jsonObject;
						pendingAppoveObject.RecCreatedBy = App.AppId.ToString();
						_approveRepository.Insert(pendingAppoveObject);

					}
					else
					{
						memberPoint.RecStatus = RecStatus.ACTIVE;
						_pointRepository.InsertMemberPoint(memberPoint);
					}
				}
			}

			return Ok(new OperationResult()
			{
				StatusCode = ApiStatusCode.SUCCESSFUL,
				StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL),
				Result = earnedPoint
			});

		}


		/// <summary>
		/// แสดงคำนวณคะแนน ส่ง order มา return คะแนนสรุปจากการคำนวณกับ Regular
		/// </summary>
		/// <param name="dto">OrderPointDto Object</param>
		/// <returns>OrderPointDto</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INVALID_ORDER_ID">รหัสสั่งซื้อสินค้าไม่ถูกต้อง</response>
		/// <response StatusCode="ORDER_INFO_NOT_FOUND">ไม่พบรายการสั่งซื้อที่ระบุ</response>
		/// <response StatusCode="ORDER_BALANCE_NOT_ENOUGH">ยอดการสั่งซื้อไม่ถึงค่ากำหนดขั้นต่ำของทางระบบ</response>
		/// <response StatusCode="MEMBER_POINT_NOT_FOUND">ไม่พบคะแนนสมาชิก</response>
		[HttpPost]
		[Route("api/v1/tvd/point/order")]
		public IHttpActionResult GetPointByOrder(OrderDto dto)
		{
			if (!ModelState.IsValid)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}

			var orderId = _orderRepository.CheckOrderId(dto.OrderId);
			if (orderId <= 0)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.INVALID_ORDER_ID,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_ORDER_ID)
				});
			}

			var orderInfo = _orderRepository.FetchSpecificOrderInfo(dto.OrderId);
			if (orderInfo == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.ORDER_INFO_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.ORDER_INFO_NOT_FOUND)
				});
			}

			var pointConfig = _pointRepository.GetPointConfig();
			var minPurchase = Convert.ToInt32(pointConfig.MinPurchase);
			var orderPrice = Convert.ToInt32(orderInfo.GrandTotal);
			if (orderPrice < minPurchase)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.ORDER_BALANCE_NOT_ENOUGH,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.ORDER_BALANCE_NOT_ENOUGH)
				});
			}

			var personId = _memberRepository.FetchMemberId(orderInfo.Customer);
			Tuple<string, int> result = null;

			if (personId == 0)
			{
				var customerNames = orderInfo.CustomerName.Split(' ');

				// create new member
				var memberDto = new MemberDto();
				memberDto.CustomerRef = orderInfo.Customer;
				memberDto.Person = new Person
				{
					PersonTitle = 0,
					FirstName = customerNames[0],
					LastName = customerNames[1],
					DOB = "",
					IsSpecificYear = false,
					Email = "",
					Phone = ""
				};
				var personAddress = new List<PersonAddress>();
				if (!string.IsNullOrEmpty(orderInfo.ShipmentAddress))
				{
					personAddress.Add(new PersonAddress()
					{
						AddressType = model.Api.Models.Member.AddressType.SHIPMENT,
						Address = orderInfo.ShipmentAddress,
						CityId = 0,
						PostalCode = orderInfo.ShipmentPostalCode
					});
				}
				if (!string.IsNullOrEmpty(orderInfo.BillingAddress))
				{
					personAddress.Add(new PersonAddress()
					{
						AddressType = model.Api.Models.Member.AddressType.INVOICE,
						Address = orderInfo.BillingAddress,
						CityId = 0,
						PostalCode = orderInfo.BillingPostalCode
					});
				}
				memberDto.Address = personAddress;
				memberDto.RecStatus = 1;
				memberDto.Password = PasswordHasher.EncryptString(App.DefaultPassword);
				var questions = new List<PersonQuestion>();
				questions.Add(new PersonQuestion() { QuestionId = 1, Answer = "" });
				memberDto.Question = questions;
				memberDto.Corporate = new Corporate();

				result = this._memberRepository.MemberUpsertApi(memberDto, App.AppId);
			}
			else
			{
				result = new Tuple<string, int>("000", personId);
			}


			var memberPointLevel = _pointRepository.GetMemberPointLevelApi(result.Item2);
			if (memberPointLevel == null)
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.MEMBER_POINT_NOT_FOUND,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.MEMBER_POINT_NOT_FOUND)
				});
			}


			if (result != null)
			{
				var basicPointConfig = _lookupRepository.GetPointLevels()?.ToList();
				if (basicPointConfig != null && basicPointConfig.Any())
				{
					orderInfo.RegularPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Regular).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.SilverPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Silver).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.GoldPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Gold).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
					orderInfo.PlatinumPoint = (int)basicPointConfig.Where(p => (PointLevelType)p.PointLevelId == PointLevelType.Platinum).Select(ppp => ppp.PricePerPoint).FirstOrDefault();
				}

				var promotionRules = _pointRepository.FetchAllRuleInPromotion(new PromotionDto() { Id = orderInfo.PromotionId });
				if (promotionRules == null)
				{
					promotionRules = _pointRepository.FetchAllRuleInPromotion(new PromotionDto() { Id = orderInfo.CampaignId });
				}

				List<OrderInfo> orderInfos = new List<OrderInfo>();
				if (promotionRules != null && promotionRules.Any())
				{
					foreach (var r in promotionRules)
					{
						orderInfos.Add(ApplyPoint(r, orderInfo, memberPointLevel));
					}
				}

				OrderInfo selectedOrderInfo = new OrderInfo();
				var earnedPoint = 0;
				if (orderInfos.Count == 0)
				{
					selectedOrderInfo = orderInfo;
				}
				else if (orderInfos.Count == 1)
				{
					selectedOrderInfo = orderInfos[0];
				}
				else
				{
					if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Regular)
					{
						selectedOrderInfo = orderInfos.OrderByDescending(o => o.RegularPoint).First();
					}
					if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Silver)
					{
						selectedOrderInfo = orderInfos.OrderByDescending(o => o.SilverPoint).First();
					}
					if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Gold)
					{
						selectedOrderInfo = orderInfos.OrderByDescending(o => o.GoldPoint).First();
					}
					if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Platinum)
					{
						selectedOrderInfo = orderInfos.OrderByDescending(o => o.PlatinumPoint).First();
					}
				}

				if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Regular)
				{
					earnedPoint = orderInfo.RegularPoint;
				}
				if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Silver)
				{
					earnedPoint = orderInfo.SilverPoint;
				}
				if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Gold)
				{
					earnedPoint = orderInfo.GoldPoint;
				}
				if ((PointLevelType)memberPointLevel.PointLevelId == PointLevelType.Platinum)
				{
					earnedPoint = orderInfo.PlatinumPoint;
				}

				if (dto.ConfirmPoint)
				{
					var memberPoint = new MemberPoint
					{
						PersonId = personId,
						RecCreatedBy = App.AppId.ToString(),
						Type = UpdatePointType.ADJUST,
						Point = Math.Abs(earnedPoint),
						Remark = "ปรับปรุงคะแนน จาก API"
					};

					if (memberPoint.Point > 0)
					{
						int expireYear = _pointRepository.GetPointConfig().ExpireYear ?? 0;
						int calculateExpireYear = 0;
						if (expireYear == 0)
						{
							calculateExpireYear = 9999;
						}
						else
						{
							calculateExpireYear = DateTime.Now.AddYears((expireYear - 1)).Year;
						}

						var pointExpiredDate = new DateTime(calculateExpireYear, 12, 31, 23, 59, 59);
						memberPoint.RecExpiredWhen = pointExpiredDate;
					}

					var preFix = memberPoint.Point > 0 ? "เพิ่ม" : "ลด";
					var icon = memberPoint.Point > 0 ? "+" : "";
					var person = _memberRepository.GetMember(memberPoint.PersonId, Source.MEMBERSHIP_WEBSITE);

					if (!CheckAutoApproveAndUpdateMemberpoint())
					{
						memberPoint.RecStatus = RecStatus.PENDING;

						var json = new
						{
							Person = person,
							Description = $"{preFix} {Math.Abs(memberPoint.Point):##,###} คะแนน",
							Source = new EmployeeInfo() { EmployeeCode = App.AppId.ToString() }
						};

						//convert pending object to json
						var jsonObject = JsonConvert.SerializeObject(json);
						var pendingAppoveObject = new Approve();
						pendingAppoveObject.ApproveType = ApproveType.UPDATE_REWARD_POINT;
						pendingAppoveObject.ObjectId = personId;
						pendingAppoveObject.ObjectValue = jsonObject;
						pendingAppoveObject.RecCreatedBy = App.AppId.ToString();
						_approveRepository.Insert(pendingAppoveObject);

					}
					else
					{
						memberPoint.RecStatus = RecStatus.ACTIVE;
						_pointRepository.InsertMemberPoint(memberPoint);
					}
				}

				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.SUCCESSFUL,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL),
					Result = earnedPoint
				});
			}
			else
			{
				return Ok(new OperationResult()
				{
					StatusCode = ApiStatusCode.BAD_REQUEST,
					StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
				});
			}
		}

		private OrderInfo ApplyPoint(PromotionRuleInfo r, OrderInfo o, PointLevel m)
		{
			if (r.IsPerOrder) //Change to be per item
			{
				if (ValidateDate(r.RuleStartDate, r.RuleEndDate))
				{
					if (ValidateMemberLevel(r.PointLevelId, m.PointLevelId))
					{
						if (ValidatePayment(r.PaymentTypeId, o.PaymentTypeId))
						{
							if (ValidateCardType(r.CardTypeId, o.CardTypeId))
							{
								if (ValidateBankType(r.BankTypeId, o.BankTypeId))
								{
									if ((PointLevelType)r.PointLevelId == PointLevelType.Regular)
									{
										o.RegularPoint = SetPoint(r, o.RegularPoint, Convert.ToInt32(o.UnitPrice));
									}
									if ((PointLevelType)r.PointLevelId == PointLevelType.Silver)
									{
										o.SilverPoint = SetPoint(r, o.SilverPoint, Convert.ToInt32(o.UnitPrice));
									}
									if ((PointLevelType)r.PointLevelId == PointLevelType.Gold)
									{
										o.GoldPoint = SetPoint(r, o.GoldPoint, Convert.ToInt32(o.UnitPrice));
									}
									if ((PointLevelType)r.PointLevelId == PointLevelType.Platinum)
									{
										o.PlatinumPoint = SetPoint(r, o.PlatinumPoint, Convert.ToInt32(o.UnitPrice));
									}
									if ((PointLevelType)r.PointLevelId == PointLevelType.All)
									{
										o.RegularPoint = SetPoint(r, o.RegularPoint, Convert.ToInt32(o.UnitPrice));
										o.SilverPoint = SetPoint(r, o.SilverPoint, Convert.ToInt32(o.UnitPrice));
										o.GoldPoint = SetPoint(r, o.GoldPoint, Convert.ToInt32(o.UnitPrice));
										o.PlatinumPoint = SetPoint(r, o.PlatinumPoint, Convert.ToInt32(o.UnitPrice));
									}
								}
							}
						}
					}
				}
			}

			return o;
		}

		private bool ValidateBankType(int ruleBankTypeId, int memberBankTypeId)
		{
			if (ruleBankTypeId == 0)
			{
				return true;
			}
			else
			{
				return (ruleBankTypeId == memberBankTypeId);
			}
		}

		private bool ValidateCardType(int ruleCardTypeId, int memberCardTypeId)
		{
			if (ruleCardTypeId == 0)
			{
				return true;
			}
			else
			{
				return (ruleCardTypeId == memberCardTypeId);
			}
		}

		private bool ValidatePayment(int rulePaymentId, int memberPaymentId)
		{
			if (rulePaymentId == 0)
			{
				return true;
			}
			else
			{
				return (rulePaymentId == memberPaymentId);
			}
		}

		private bool ValidateMemberLevel(int rulePointLevelId, int memberPointLevelId)
		{
			if ((PointLevelType)rulePointLevelId == PointLevelType.All)
			{
				return true;
			}
			else
			{
				return (rulePointLevelId == memberPointLevelId);
			}
		}

		private bool ValidateDate(DateTime validFrom, DateTime validTo)
		{
			var valid = true;
			var currentDate = DateTime.Now;
			if (currentDate < validFrom || currentDate > validTo)
			{
				valid = false;
			}
			return valid;
		}

		private int SetPoint(PromotionRuleInfo r, int basePoint, int baseUnitPrice)
		{
			var point = 0;
			switch ((CalculationType)r.CalculationType)
			{
				case CalculationType.INCREASE_POINT:
					{
						point = IncreasePoint(basePoint, r.ReceivedPoint);
					}
					break;
				case CalculationType.MULTIPLE_POINT:
					{
						point = MultiplePoint(basePoint, r.ReceivedPoint);
					}
					break;
				case CalculationType.CHANGE_BASE_POINT:
					{
						point = ChangeBasePoint(Convert.ToInt32(baseUnitPrice), r.ReceivedPoint);
					}
					break;
			}
			return point;
		}

		private int IncreasePoint(int currentPoint, int incPoint)
		{
			return currentPoint + incPoint;
		}

		private int MultiplePoint(int currentPoint, int incPoint)
		{
			if (currentPoint == 0)
			{
				currentPoint = 1;
			}
			return currentPoint * incPoint;
		}

		private int ChangeBasePoint(int productPrice, int unitPrice)
		{
			return (productPrice / unitPrice);
		}

		private bool CheckAutoApproveAndUpdateMemberpoint()
		{
			var settings = _approveRepository.GetApproveSetting();
			return settings.IsAutoApproveLevelUpMembership;
		}
	}
}
