﻿using System.Web;
using System.Web.Http;
using tvd.api.Attributes;
using tvd.model.Repositories;

namespace tvd.api.Controllers
{
    [CamelCaseControllerConfig]
    public class LocationController : BaseApiController
    {
        private readonly ILookupRepository _lookupRepository;

        public LocationController(HttpContextBase contextBase, ILookupRepository lookupRepository) : base(contextBase)
        {
            _lookupRepository = lookupRepository;
        }

        /// <summary>
        /// ข้อมูลจังหวัดทั้งหมด
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/location/cities")]
        public IHttpActionResult GetCities()
        {
            var cities = _lookupRepository.GetCities();
            return Ok(cities);
        }
    }
}