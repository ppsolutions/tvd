﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using tvd.api.Attributes;
using tvd.api.Models;
using tvd.api.Models.Constants;
using tvd.api.Models.Reward;
using tvd.model.Helpers;
using tvd.model.Models.CommonDTO.Approval;
using tvd.model.Models.CommonDTO.Employee;
using tvd.model.Models.CommonDTO.Enum;
using tvd.model.Models.CommonDTO.Point;
using tvd.model.Models.Enum;
using tvd.model.Repositories;
using tvd.model.Repositories.Interfaces;

namespace tvd.api.Controllers
{
    [CamelCaseControllerConfig]
    public class RedemptionController : BaseApiController
    {
        private readonly IPointRepository _pointRepository;
        private readonly ILookupRepository _lookupRepository;
        private readonly IApproveRepository _approveRepository;

        public RedemptionController(HttpContextBase contextBase,
            IPointRepository pointRepository,
            ILookupRepository lookupRepository,
            IApproveRepository approveRepository) : base(contextBase)
        {
            _pointRepository = pointRepository;
            _lookupRepository = lookupRepository;
            _approveRepository = approveRepository;
        }

        /// <summary>
        /// เรียกมาแล้วตอบกลับเป็นรายการ แสดงของรางวัล
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/redemption/getreward/{productCode}")]
        public IHttpActionResult GetRewards(string productCode)
        {
            var result = _pointRepository.GetRedeemPointsApi(productCode);

            return Ok(result);
        }

		/// <summary>
		/// ขอแลกของรางวัล ส่ง membership_code, item_redemption_code มา
		/// </summary>
		/// <param name="dto">RewardDto</param>
		/// <returns>OperationResult</returns>
		/// <response StatusCode="SUCCESSFUL">Request ที่ส่งมาไม่สมบูรณ์</response>
		/// <response StatusCode="BAD_REQUEST">Request เสร็จสมบูรณ์</response>
		/// <response StatusCode="INVALID_REQUEST_TYPE">ประเภทคำขอไม่ถูกต้อง</response>
		/// <response StatusCode="REDEEM_PRODUCT_NOT_FOUND">ไม่พบสินค้าที่ต้องการแลก</response>
		/// <response StatusCode="CURRENT_POINT_NOT_FOUND">ไม่พบคะแนนที่สามารถใช้งานได้ในระบบ</response>
		/// <response StatusCode="INSUFFICIENT_POINT">คะแนนสะสมไม่เพียงพอ</response>
		/// <response StatusCode="REQUEST_TO_APPROVE_IS_FAILED">ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้</response>
		[HttpPost]
        [Route("api/v1/tvd/redemption/exchange")]
        public IHttpActionResult ExchangeRewards(RewardDto dto)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.BAD_REQUEST,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.BAD_REQUEST)
                });
            }

            var type = dto.RedeemPointsType;
            if (type != RedeemPointsType.POINT_AND_PAID && type !=  RedeemPointsType.POINT_ONLY)
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.INVALID_REQUEST_TYPE,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INVALID_REQUEST_TYPE)
                });
            }

            var redeemPoint = _pointRepository.GetRedeemPointInfoApi(dto.RedeemPointId);
            if (redeemPoint == null || 
                redeemPoint.RecStatus != RecStatus.ACTIVE)
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.REDEEM_PRODUCT_NOT_FOUND,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.REDEEM_PRODUCT_NOT_FOUND)
                });
            }

            var usedsPoints = type == RedeemPointsType.POINT_AND_PAID ? redeemPoint.RequiredPoint : redeemPoint.RequiredPointOnly;
            var currentActivePoints = _pointRepository.GetMemberPointsApi(dto.PersonId, false).ToList();
            if (currentActivePoints == null)
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.CURRENT_POINT_NOT_FOUND,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.CURRENT_POINT_NOT_FOUND)
                });
            }

            if (!ValidateMemberPointIsEnough(currentActivePoints, usedsPoints))
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.INSUFFICIENT_POINT,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.INSUFFICIENT_POINT)
                });
            }

            var transaction = new RedeemPointTransaction
            {
                PersonId = dto.PersonId,
                RecCreatedBy = new EmployeeInfo() { EmployeeCode = App.AppKey },
                RecStatus = RecStatus.PENDING,
                Type = dto.RedeemPointsType,
                RedeemPoint = redeemPoint
            };

            var jsonObject = JsonConvert.SerializeObject(transaction);
            var pendingAppoveObject = new Approve();
            pendingAppoveObject.ApproveType = ApproveType.PRODUCT_REDEMPTION;
            pendingAppoveObject.ObjectId = transaction.PersonId;
            pendingAppoveObject.ObjectValue = jsonObject;
            pendingAppoveObject.RecCreatedBy = App.AppKey;
            var requestApprove = _approveRepository.InsertApi(pendingAppoveObject);
            if (!requestApprove.Equals("000"))
            {
                return Ok(new OperationResult()
                {
                    StatusCode = ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED,
                    StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.REQUEST_TO_APPROVE_IS_FAILED)
                });
            }

            return Ok(new OperationResult()
            {
                StatusCode = ApiStatusCode.SUCCESSFUL,
                StatusDescription = EnumHelper.GetEnumDescription(ApiStatusCode.SUCCESSFUL)
            });
        }

        private bool ValidateMemberPointIsEnough(List<MemberPoint> points, int usedPoints)
        {
            var currentActivePoints = GetActivePoints(points);
            return currentActivePoints >= Math.Abs(usedPoints);
        }
        private int GetActivePoints(List<MemberPoint> points)
        {
            //Active points
            var activePoints = points.Where(o => o.RecStatus == RecStatus.ACTIVE);
            //Get minus point
            var minusPoint = points
                .Where(o => o.Point < 0)
                .Sum(o => o.Point);

            //Get positive point that not expired
            var positivePoints = activePoints
                .Where(o => o.Point >= 0)
                .Sum(o => o.Point);

            //subtract by minus point
            positivePoints += minusPoint;

            if (positivePoints < 0) positivePoints = 0;
            return positivePoints;
        }
    }
}
