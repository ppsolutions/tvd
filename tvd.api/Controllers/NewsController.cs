﻿using System.Web;
using System.Web.Http;
using tvd.api.Attributes;
using tvd.model.Models.Enum;
using tvd.model.Repositories.Interfaces;

namespace tvd.api.Controllers
{
    [CamelCaseControllerConfigAttribute]
    public class NewsController : BaseApiController
    {
        private readonly INewsRepository _newsRepository;

        public NewsController(HttpContextBase contextBase, INewsRepository newsRepository) : base(contextBase)
        {
            _newsRepository = newsRepository;
        }

        /// <summary>
        /// แสดงรายการข่าวสารทั้งหมดทุกประเภท
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/news")]
        public IHttpActionResult Get()
        {
            var result = _newsRepository.GetNews(NewsType.All);
            if(result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        /// <summary>
        /// แสดงรายการระเบียบสมาชิก
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/news/rules")]
        public IHttpActionResult GetRules()
        {
            var result = _newsRepository.GetNews(NewsType.MemberRule);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        /// <summary>
        /// แสดงรายการสิทธิประโยชน์
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/news/benefits")]
        public IHttpActionResult GetBenefits()
        {
            var result = _newsRepository.GetNews(NewsType.MemberBenefit);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        /// <summary>
        /// แสดงรายการข่าวสารสมาชิก
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/news/members")]
        public IHttpActionResult GetMembers()
        {
            var result = _newsRepository.GetNews(NewsType.MemberNews);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        /// <summary>
        /// แสดงรายการข่าวสารโปรโมรชั่น
        /// </summary>
        [HttpGet]
        [Route("api/v1/tvd/news/promotions")]
        public IHttpActionResult GetPromotions()
        {
            var result = _newsRepository.GetNews(NewsType.PromotionNews);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
    }
}