﻿namespace tvd.api.Http
{
    public class ResponseMessage
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public string Url { get; set; }
    }
}