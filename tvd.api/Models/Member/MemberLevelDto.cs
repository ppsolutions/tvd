﻿namespace tvd.api.Models.Member
{
    public class MemberLevelDto
    {
        public int PersonId { get; set; }
        public int PointLevel { get; set; }
    }
}