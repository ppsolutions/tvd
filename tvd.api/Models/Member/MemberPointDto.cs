﻿using tvd.model.Models.Enum;

namespace tvd.api.Models.Member
{
    public class MemberPointDto
    {
        public int PersonId { get; set; }
        public int Point { get; set; }
        public ActionType ActionType { get; set; }
    }
}