﻿namespace tvd.api.Models.Member
{
    public class MemberUpdatePasswordDto
    {
        public int PersonId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}