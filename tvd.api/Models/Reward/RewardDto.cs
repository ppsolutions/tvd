﻿using tvd.model.Models.Enum;

namespace tvd.api.Models.Reward
{
    public class RewardDto
    {
        public int PersonId { get; set; }
        public int RedeemPointId { get; set; }
        public RedeemPointsType RedeemPointsType { get; set; }
    }
}