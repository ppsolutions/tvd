﻿using tvd.api.Models.Constants;

namespace tvd.api.Models
{
	/// <summary>
	/// Response Result
	/// </summary>
    public class OperationResult
    {
		/// <summary>
		/// Status of request
		/// </summary>
        public ApiStatusCode StatusCode { get; set; }

		/// <summary>
		/// Description of request
		/// </summary>
		public string StatusDescription { get; set; }

		/// <summary>
		/// Result request
		/// </summary>
		public object Result { get; set; }
    }
}