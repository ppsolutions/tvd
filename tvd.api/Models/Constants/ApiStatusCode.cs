﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace tvd.api.Models.Constants
{
	public enum ApiStatusCode
	{
		/// <summary>
		/// Request เสร็จสมบูรณ์
		/// </summary>
		[Description("Request เสร็จสมบูรณ์")]
		SUCCESSFUL					= 0,

		/// <summary>
		/// Request ที่ส่งมาไม่สมบูรณ์
		/// </summary>
		[Description("Request ที่ส่งมาไม่สมบูรณ์")]
		BAD_REQUEST					= 1,

		/// <summary>
		/// Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลใหม่ได้
		/// </summary>
		[Description("Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลใหม่ได้")]
		INSERT_MEMBER_FAIL			= 101,

		/// <summary>
		/// Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลได้
		/// </summary>
		[Description("Request ที่ส่งมาไม่สมบูรณ์ จึงไม่สามารถ บันทึกข้อมูลได้")]
		UPDATE_MEMBER_FAIL			= 102,

		/// <summary>
		/// ไม่พบสมาชิกในระบบ
		/// </summary>
		[Description("ไม่พบสมาชิกในระบบ")]
		MEMBER_NOT_FOUND			= 103,

		/// <summary>
		/// รหัสผ่านปัจจุบันไม่ถูกต้อง
		/// </summary>
		[Description("รหัสผ่านปัจจุบันไม่ถูกต้อง")]
		INVALID_CURRENT_PASSWORD	= 104,

		/// <summary>
		/// สมาชิกอยู่ระดับนี้อยู่แล้ว
		/// </summary>
		[Description("สมาชิกอยู่ระดับนี้อยู่แล้ว")]
		MEMBER_LEVEL_ALREADY_THE_SAME = 105,

		/// <summary>
		/// ไม่พบคะแนนที่ตั้งค่าไว้
		/// </summary>
		[Description("ไม่พบคะแนนที่ตั้งค่าไว้")]
		POINT_CONFIG_NOT_FOUND		= 106,

		/// <summary>
		/// ไม่พบระดับสมาชิกที่ต้องการอัพเดท
		/// </summary>
		[Description("ไม่พบระดับสมาชิกที่ต้องการอัพเดท")]
		MEMBER_UPDATE_LEVEL_NOT_FOUND = 107,

		/// <summary>
		/// ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้
		/// </summary>
		[Description("ไม่สามารถส่งคำขอเพื่อรอการอนุมัติได้")]
		REQUEST_TO_APPROVE_IS_FAILED = 108,

		/// <summary>
		/// ประเภท Action ไม่ถูกต้อง โปรดระบุระหว่าง Add หรือ Delete
		/// </summary>
		[Description("ประเภท Action ไม่ถูกต้อง โปรดระบุระหว่าง Add หรือ Delete")]
		INVALID_ACTION_TYPE			= 109,

		/// <summary>
		/// คะแนนสะสมไม่เพียงพอ
		/// </summary>
		[Description("คะแนนสะสมไม่เพียงพอ")]
		INSUFFICIENT_POINT			= 110,

		/// <summary>
		/// ไม่พบวันหมดอายุคะแนน
		/// </summary>
		[Description("ไม่พบวันหมดอายุคะแนน")]
		POINT_EXPIRE_DATE_NOT_FOUND = 111,

		/// <summary>
		/// ไม่พบรหัส Gamification
		/// </summary>
		[Description("ไม่พบรหัส Gamification")]
		GAMIFICATION_CODE_NOT_FOUND = 112,

		/// <summary>
		/// Gamification นี้หมดอายุ
		/// </summary>
		[Description("Gamification นี้หมดอายุ")]
		GAMIFICATION_EXPIRED		= 113,

		/// <summary>
		/// Gamification ไม่สามารถใช้งานได้ในขณะนี้
		/// </summary>
		[Description("Gamification ไม่สามารถใช้งานได้ในขณะนี้")]
		GAMIFICATION_NOT_AVAILABLE	= 114,

		/// <summary>
		/// ไม่พบคะแนนสมาชิก
		/// </summary>
		[Description("ไม่พบคะแนนสมาชิก")]
		MEMBER_POINT_NOT_FOUND		= 115,

		/// <summary>
		/// คะแนน Gamification ไม่เพียงพอ
		/// </summary>
		[Description("คะแนน Gamification ไม่เพียงพอ")]
		NOT_ENOUGH_GAMIFICATION		= 116,

		/// <summary>
		/// ไม่สามารถเปลี่ยนแปลงระดับสมาชิกเนื่องจากเกิดข้อผิดพลาด
		/// </summary>
		[Description("ไม่สามารถเปลี่ยนแปลงระดับสมาชิกเนื่องจากเกิดข้อผิดพลาด")]
		CHANGE_MEMBER_LEVEL_FAILED = 117,

		/// <summary>
		/// รหัสสั่งซื้อสินค้าไม่ถูกต้อง
		/// </summary>
		[Description("รหัสสั่งซื้อสินค้าไม่ถูกต้อง")]
		INVALID_ORDER_ID			= 118,

		/// <summary>
		/// ไม่พบรายการสั่งซื้อที่ระบุ
		/// </summary>
		[Description("ไม่พบรายการสั่งซื้อที่ระบุ")]
		ORDER_INFO_NOT_FOUND		= 119,

		/// <summary>
		/// ยอดการสั่งซื้อไม่ถึงค่ากำหนดขั้นต่ำของทางระบบ
		/// </summary>
		[Description("ยอดการสั่งซื้อไม่ถึงค่ากำหนดขั้นต่ำของทางระบบ")]
		ORDER_BALANCE_NOT_ENOUGH	= 120,

		/// <summary>
		/// ยอดการสั่งซื้อไม่ถึงค่ากำหนดขั้นต่ำของทางระบบ
		/// </summary>
		[Description("ประเภทคำขอไม่ถูกต้อง")]
		INVALID_REQUEST_TYPE		= 121,

		/// <summary>
		/// ไม่พบสินค้าที่ต้องการแลก
		/// </summary>
		[Description("ไม่พบสินค้าที่ต้องการแลก")]
		REDEEM_PRODUCT_NOT_FOUND	= 122,

		/// <summary>
		/// ไม่พบคะแนนที่สามารถใช้งานได้ในระบบ
		/// </summary>
		[Description("ไม่พบคะแนนที่สามารถใช้งานได้ในระบบ")]
		CURRENT_POINT_NOT_FOUND		= 123,
	}
}