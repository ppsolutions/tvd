﻿using System.Configuration;

namespace tvd.api.Models.Constants
{
    public static class App
    {
        public static int AppId { get { return int.Parse(ConfigurationManager.AppSettings["AppId"]); } }
        public static string AppKey { get { return ConfigurationManager.AppSettings["AppKey"]; } }

		public static string DefaultPassword { get { return ConfigurationManager.AppSettings["DefaultPassword"]; } }
	}
}