﻿namespace tvd.api.Models.Order
{
    public class OrderDto
    {
		/// <summary>
		/// Order Id
		/// </summary>
        public int OrderId { get; set; }

		/// <summary>
		/// Confirm Claim Point
		/// </summary>
		public bool ConfirmPoint { get; set; }

	}
}