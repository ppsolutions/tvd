﻿namespace tvd.api.Models.Gamification
{
    public class GamificationPointDto
    {
        public int PersonId { get; set; }
        public string GamificationCode { get; set; }
    }
}